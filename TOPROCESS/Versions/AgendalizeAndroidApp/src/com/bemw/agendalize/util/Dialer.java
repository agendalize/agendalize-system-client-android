package com.bemw.agendalize.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class Dialer {
	
	public static void call(Context context, String phoneNumber) {
		Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
		phoneIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(phoneIntent);
	}

}
