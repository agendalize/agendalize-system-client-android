package com.bemw.agendalize;

public class AppConstants {
	
	// names
	public static final String TAG = "Agendalize";
	public static final String CACHE_FILE = "agendalize_cache";
	
	// domains and url
	public static final String domain = "http://192.168.1.143:8080/";
	//public static final String domain = "http://www.agendalize.com.br/";
	
	// web addresses
	public static final String OUR_FACEBOOK = "https://www.facebook.com/pages/Agendalize/496570267115536";
	public static final String OUR_TWITTER = "http://www.google.com";
	public static final String OUR_WORDPRESS = "http://blogagendalize.com.br/";
	
	// final values
	public static final String DEVICE_NAME = "android-aglz";
	public static final String DEVICE_KEY = "qa123";
	public static final String DEVICE_CREDENTIAL = "client_credentials";
	public static final int POSITION_TIMER = 5*60000;
	public static final int BOOTSPLASH_TIMER = 2000;
	public static final int CONN_TIME_OUT = 20000;
	public static final int SIZE_LIST = 7;
	public static final int SMS_TIMER = 300000;
	public static final int SCHEDULE_INTERVALS_MIN = 15;
	public static final int SCHEDULE_START_MIN = 8*60;
	public static final int SCHEDULE_DURATION_MIN = 14*60;
	
	// tags
	public static final String PRIVATE_PREFERENCES = "PRIVATE_PREFERENCES";
	public static final String PIN_CODE = "PIN_CODE";
	public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
	
	// controls
	public static final boolean IS_DEBUG = true;
	
}
