package com.bemw.agendalize.view;

import java.util.List;
import java.util.Locale;

import org.apache.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.util.DeviceUtils;

public class LoginActivity extends ActionBarActivity {
	
	//===================================================================================
	// Fields
	//===================================================================================

	// used to manage lazy load pages
	private static Object lock = new Object();
	private static boolean isLoading = false;
	// data for store fetch
    private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;
    // objects
 	private static LoginActivity context;
 	private static int countryIndex;
 	private static String completePhone;
 	private static String pinCode = null;
 	private static String accessToken = null;
 	private static boolean isUserNameSet = false;
 	private static String countryCode;
 	private static CountDownTimer countDownTimer;
 	public static final String PIN_RECEIVED = "com.bemw.agendalize.action.pinreceived";
 	// views
 	private static ImageButton activity_login_facebook;
 	private static ImageButton activity_login_twitter;
 	private static ImageButton activity_login_wordpress;
 	private static Spinner activity_login_countrycode;
 	private static EditText activity_login_phonenumbertxt;
 	private static ProgressBar activity_login_progress;
 	private static TextView activity_login_smswaittext;
 	private static TextView activity_login_smstimer;
 	private static Button activity_login_confirmbtn;
 	private static Button activity_login_makeatourbtn;
 	private static LinearLayout activity_login_countrycodelyt;
 	private static TextView activity_login_placeyourpintxt;
 	private static EditText activity_login_pintxt;
 	
 	// broadcast receiver
 	private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(PIN_RECEIVED)) {
            	showCommands();
            	String phoneNumber = intent.getExtras().getString("phoneNumber");
            	String message = intent.getExtras().getString("message");
            	// now, getting access token...
            	String[] words = message.split(" ");
            	pinCode = words[words.length-1];
            	Log.i(AppConstants.TAG, "SenderNumber: " + phoneNumber + "; Received PIN: " + pinCode);
            	savePinCode(pinCode);
            	createTokenFetcher();
            }
        }
    };
 	
	private static final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	if (waitDialog != null) {
            	waitDialog.dismiss();
            	waitDialog = null;
            }
        	switch (msg.what) {
		        case HttpStatus.SC_REQUEST_TIMEOUT:
		        	DeviceUtils.justShowDialogWarning(context, R.string.msg_timeout, false);
		        	if (pinCode != null && accessToken == null) {
		        		showInsertPin();
		        	}
		            break;
		        case -1: case HttpStatus.SC_NOT_FOUND:
		        	DeviceUtils.justShowDialogWarning(context, R.string.msg_noconnection, false);
	        		if (pinCode != null && accessToken == null) {
	        			showInsertPin();
		        	}
		        	break;
		        case HttpStatus.SC_INTERNAL_SERVER_ERROR:
		        	DeviceUtils.justShowDialogWarning(context, R.string.msg_servererror, false);
		        	if (pinCode != null && accessToken == null) {
		        		showInsertPin();
		        	}
		            break;
		        case HttpStatus.SC_OK:
		        	if (pinCode == null) {
		        		// now, we show a counter and wait for the sms...
		        		hideCommands();
		        	} else if (pinCode != null && accessToken != null && !isUserNameSet) {
		        		showInsertUsername();
		        	} else if (pinCode != null && accessToken != null && isUserNameSet) {
		        		// at this point, we have already pin and access token, so we can start application
		        		Intent newApp = new Intent(context, HomeActivity.class);
						newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						context.startActivity(newApp);
		        	}
		        	break;
	        	default:;
        	}
        	synchronized(lock) {
            	isLoading = false;
            }
        }
	};
	
	private static final Runnable pinRequest = new Runnable() {
        @Override
        public void run() {
        	if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                client.requestPinSms(completePhone);
                handler.sendEmptyMessage(client.getStatus());
            }
        }
	};
	
	private static final Runnable accessTokenRequest = new Runnable() {
        @Override
        public void run() {
        	if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
            	TelephonyManager telephonyManager = (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE);
                RESTClient client = new RESTClient();
                accessToken = client.requestAccessToken(
                		completePhone, pinCode, telephonyManager.getDeviceId()
        		);
                Log.i(AppConstants.TAG, "Received access token " + accessToken);
                saveAccessToken(accessToken);
                handler.sendEmptyMessage(client.getStatus());
            }
        }
	};
	
	private static final Runnable updateAccountRequest = new Runnable() {
        @Override
        public void run() {
        	if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                isUserNameSet = client.updateUsername(
                		activity_login_pintxt.getText().toString(), accessToken
        		);
                handler.sendEmptyMessage(client.getStatus());
            }
        }
	};
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        
        TelephonyManager tMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number() == null ? "" : tMgr.getLine1Number();
        
        // range, need to be in onResume to proper update on activityForResult return
        ArrayAdapter<CharSequence> spinnerArrayAdapter = new ArrayAdapter<CharSequence>(
        		context,
        		android.R.layout.simple_spinner_item,
        		DeviceUtils.getCountryCodeArray());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activity_login_countrycode = (Spinner) findViewById(R.id.activity_login_countrycode);
        activity_login_countrycode.setAdapter(spinnerArrayAdapter);
        activity_login_countrycode.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int selectedItem, long arg3) {
            	countryIndex = selectedItem;
            }
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
        activity_login_countrycode.setSelection(getCountryZipCodeIndex() != -1 ? getCountryZipCodeIndex() : 0);
        
        activity_login_phonenumbertxt = (EditText) findViewById(R.id.activity_login_phonenumbertxt);
        activity_login_phonenumbertxt.setText(mPhoneNumber);
        activity_login_phonenumbertxt.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        
        activity_login_confirmbtn = (Button) findViewById(R.id.activity_login_confirmbtn);
        activity_login_confirmbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				completePhone = activity_login_phonenumbertxt.getText().toString();
				completePhone = completePhone.replace(" ", "").replace("-", "").replace("+", "");
				if (activity_login_phonenumbertxt.getText().length() == 0 || !DeviceUtils.isPhoneValid(completePhone)) {
					DeviceUtils.justShowDialogWarning(context, R.string.msg_pleasefillvalidphonenumber, false);
				} else {
					Log.i(AppConstants.TAG, "Request PIN for " + completePhone);
					createNewLoader();
				}
			}
		});
        
        activity_login_facebook = (ImageButton) findViewById(R.id.activity_login_facebook);
        activity_login_facebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_FACEBOOK)));
			}
		});
        activity_login_twitter = (ImageButton) findViewById(R.id.activity_login_twitter);
        activity_login_twitter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_TWITTER)));
			}
		});
        activity_login_wordpress = (ImageButton) findViewById(R.id.activity_login_wordpress);
        activity_login_wordpress.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_WORDPRESS)));
			}
		});
        
        activity_login_makeatourbtn = (Button) findViewById(R.id.activity_login_makeatourbtn);
        activity_login_makeatourbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: make a tour...
			}
		});
        
        activity_login_progress = (ProgressBar) findViewById(R.id.activity_login_progress);
        activity_login_smswaittext = (TextView) findViewById(R.id.activity_login_smswaittext);
        activity_login_smstimer = (TextView) findViewById(R.id.activity_login_smstimer);
        
        activity_login_countrycodelyt = (LinearLayout) findViewById(R.id.activity_login_countrycodelyt);
        activity_login_placeyourpintxt = (TextView) findViewById(R.id.activity_login_placeyourpintxt);
        activity_login_pintxt = (EditText) findViewById(R.id.activity_login_pintxt);
     	
        // register broadcast receiver to receive sms with pin
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(context);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PIN_RECEIVED);
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
        
        // action bar menu
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setTitle(R.string.action_login);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);
    } 
    
    //===================================================================================
	// General methods
	//===================================================================================
	
    private static void showCommands() {
    	countDownTimer.cancel();
    	activity_login_countrycode.setEnabled(true);
    	activity_login_countrycode.setAlpha(1.0f);
    	activity_login_phonenumbertxt.setEnabled(true);
    	activity_login_phonenumbertxt.setAlpha(1.0f);
    	activity_login_confirmbtn.setEnabled(true);
    	activity_login_confirmbtn.setAlpha(1.0f);
    	activity_login_makeatourbtn.setEnabled(true);
    	activity_login_makeatourbtn.setAlpha(1.0f);
    	activity_login_progress.setVisibility(View.INVISIBLE);
		activity_login_smswaittext.setVisibility(View.INVISIBLE);
		activity_login_smstimer.setVisibility(View.INVISIBLE);
    }
    
    private static void hideCommands() {
    	countDownTimer = new CountDownTimer(AppConstants.SMS_TIMER, 1000) {
	        @Override
	        public void onTick(long millisUntilFinished) {
	        	int progress = (int)(AppConstants.SMS_TIMER - millisUntilFinished)/1000;
	        	activity_login_progress.setProgress(progress);
	        	String minutes = String.format(Locale.getDefault(), "%02d", (4-progress/60));
	        	String seconds = String.format(Locale.getDefault(), "%02d", (59-progress%60));
	        	activity_login_smstimer.setText(minutes + "m" + seconds+"s");
	        }
	        @Override
	        public void onFinish() {
	        	showCommands();
				DeviceUtils.justShowDialogWarning(context, R.string.msg_serverbusyforsms, false);
	        }
	    };
	    countDownTimer.start();
    	activity_login_countrycode.setEnabled(false);
    	activity_login_countrycode.setAlpha(0.5f);
    	activity_login_phonenumbertxt.setEnabled(false);
    	activity_login_phonenumbertxt.setAlpha(0.5f);
    	activity_login_confirmbtn.setEnabled(false);
    	activity_login_confirmbtn.setAlpha(0.5f);
    	activity_login_makeatourbtn.setEnabled(false);
    	activity_login_makeatourbtn.setAlpha(0.5f);
    	activity_login_progress.setVisibility(View.VISIBLE);
		activity_login_smswaittext.setVisibility(View.VISIBLE);
		activity_login_smstimer.setVisibility(View.VISIBLE);
    }
    
    private static void showInsertPin() {
    	activity_login_countrycodelyt.setVisibility(View.GONE);
        activity_login_placeyourpintxt.setVisibility(View.VISIBLE);
        activity_login_phonenumbertxt.setVisibility(View.GONE);
        activity_login_pintxt.setVisibility(View.VISIBLE);
        activity_login_confirmbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (activity_login_phonenumbertxt.getText().length() != 4) {
					DeviceUtils.justShowDialogWarning(context, R.string.msg_placeyourpintxt, false);
				} else {
					activity_login_pintxt.setText(pinCode);
					createTokenFetcher();
				}
			}
		});
    }
    
    private static void showInsertUsername() {
    	activity_login_countrycodelyt.setVisibility(View.GONE);
        activity_login_placeyourpintxt.setVisibility(View.VISIBLE);
        activity_login_placeyourpintxt.setText(context.getResources().getString(R.string.msg_howcanwecallyou));
        activity_login_phonenumbertxt.setVisibility(View.GONE);
        activity_login_pintxt.setVisibility(View.VISIBLE);
        activity_login_pintxt.setHint(context.getResources().getString(R.string.label_name));
        activity_login_confirmbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (activity_login_phonenumbertxt.getText().length() == 0) {
					DeviceUtils.justShowDialogWarning(context, R.string.msg_pleasefillaname, false);
				} else {
					updateUsername();
				}
			}
		});
    }
    
    public int getCountryZipCodeIndex() {
    	try {
	        String CountryID = "";
	        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	        CountryID = manager.getSimCountryIso().toUpperCase(Locale.getDefault());
	        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
	        for (int i=0; i<rl.length; i++) {
	            String[] g = rl[i].split(",");
	            if (g[1].trim().equals(CountryID.trim())) {
	            	List<CharSequence> cl = DeviceUtils.getCountryCodeArray();
	            	for (int j=0; j<cl.size(); j++) {
	            		String[] c = cl.get(j).toString().split(" ");
	            		if (g[0].trim().equals(c[1].replace("+", "").replace("(", "").replace(")", ""))) {
	            			return j;
	            		}
	            	}
	            	break;
	            }
	        }
    	} catch (Exception e) {
    		return 0;
    	}
		return 0;
    }
    
	synchronized private static void createNewLoader() {
		if (!isLoading) {
			synchronized(lock) {
				isLoading = true;
	        }
      		waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.msg_sendingdata),
                    context.getText(R.string.msg_pleasewait),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
	      	contentLoader = new Thread(pinRequest);
	      	contentLoader.start();
		}
	}
	
	synchronized private static void createTokenFetcher() {
		if (!isLoading) {
			synchronized(lock) {
				isLoading = true;
	        }
      		waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.msg_loadingdata),
                    context.getText(R.string.msg_waitingfortoken),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
	      	contentLoader = new Thread(accessTokenRequest);
	      	contentLoader.start();
		}
	}
	
	synchronized private static void updateUsername() {
		if (!isLoading) {
			synchronized(lock) {
				isLoading = true;
	        }
      		waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.msg_sendingdata),
                    context.getText(R.string.msg_updatingaccount),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
	      	contentLoader = new Thread(updateAccountRequest);
	      	contentLoader.start();
		}
	}
	
	private static void savePinCode(String pinCode) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		Editor prefsPrivateEditor = prefsPrivate.edit();
		prefsPrivateEditor.putString(AppConstants.PIN_CODE, pinCode);
		prefsPrivateEditor.commit();
	}
	
	private static void saveAccessToken(String accessToken) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		Editor prefsPrivateEditor = prefsPrivate.edit();
		prefsPrivateEditor.putString(AppConstants.ACCESS_TOKEN, accessToken);
		prefsPrivateEditor.commit();
	}

}
