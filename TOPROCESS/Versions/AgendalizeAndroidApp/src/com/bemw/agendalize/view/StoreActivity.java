package com.bemw.agendalize.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.net.MyLocation;
import com.bemw.agendalize.net.MyLocation.LocationResult;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.util.DeviceUtils;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.LongSparseArray;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class StoreActivity extends ActionBarActivity {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private Menu menu;
	private double myLocation[];
	private LongSparseArray<Drawable> photos;
	private List<StoreModel> stores;
	private final int NUM_PAGES = 3;
	private ViewPager activity_store_pager;
	private ScreenSlidePagerAdapter pagerAdapter;
	private ProgressDialog waitDialog;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        
        activity_store_pager = (ViewPager) findViewById(R.id.activity_store_pager);
        
        stores = new ArrayList<StoreModel>();
        photos = new LongSparseArray<Drawable>();
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        
        waitDialog = ProgressDialog.show(this,
                getText(R.string.msg_checklocation),
                getText(R.string.msg_pleasewait),
                false, false);
        waitDialog.setIcon(R.drawable.ic_launcher);
        LocationResult locationResult = new LocationResult() {
            @Override
            public void gotLocation(Location location) {
            	if (location == null) {
            		runOnUiThread(new Runnable() {
	            	    public void run() {
	            	    	waitDialog.dismiss();
	            	    	DeviceUtils.justShowDialogWarning(
	            	    			StoreActivity.this,
	            	    			R.string.msg_notpossibletogetlocation,
	            	    			true);
	            	    }
            		});
            	} else {
	            	myLocation = new double[2];
	            	myLocation[0] = location.getLatitude();
	            	myLocation[1] = location.getLongitude();
	            	runOnUiThread(new Runnable() {
	            	    public void run() {
	            	    	waitDialog.dismiss();
	            	    	activity_store_pager.setAdapter(pagerAdapter);
	            	        if (savedInstanceState == null)
	            	        	activity_store_pager.setCurrentItem(1);
	            	    }
	            	});
            	}
            }
        };
        MyLocation myLocation = new MyLocation();
        if (!myLocation.getLocation(this, locationResult)) {
        	DeviceUtils.justShowDialogWarning(
	    			StoreActivity.this,
	    			R.string.msg_notpossibletogetlocation,
	    			true);
        }
        
        // action bar menu
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_chair_w);
        getSupportActionBar().setTitle(R.string.title_stores);
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_store, menu);
        return true;
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
			case R.id.action_map:
				activity_store_pager.setCurrentItem(0);
                getSupportActionBar().setLogo(R.drawable.ic_overlay_w);
                getSupportActionBar().setTitle(R.string.action_map);
                if (menu != null) {
                    menu.getItem(0).setVisible(false);
                    menu.getItem(1).setVisible(false);
                }
                break;
			case R.id.action_filter:
				activity_store_pager.setCurrentItem(2);
                getSupportActionBar().setLogo(R.drawable.ic_filter_w);
                getSupportActionBar().setTitle(R.string.action_filter);
                if (menu != null) {
                    menu.getItem(0).setVisible(false);
                    menu.getItem(1).setVisible(false);
                }
				break;
            case R.id.action_back:
                if (activity_store_pager.getCurrentItem() != 1) {
                	activity_store_pager.setCurrentItem(1);
                    getSupportActionBar().setLogo(R.drawable.ic_chair_w);
                    getSupportActionBar().setTitle(R.string.title_stores);
                    if (menu != null) {
                        menu.getItem(0).setVisible(true);
                        menu.getItem(1).setVisible(true);
                    }
                } else finish();
			default:;
		}
        return super.onOptionsItemSelected(item);
    }

	//===================================================================================
	// General methods
	//===================================================================================
	
	public void requestFilteredSearch(ArrayList<ServiceModel> services, double[] latlon,
			int hourFrom, int hourTo, Calendar dayTm, int rangeIndex) {
		activity_store_pager.setCurrentItem(1);
        getSupportActionBar().setLogo(R.drawable.ic_chair_w);
        getSupportActionBar().setTitle(R.string.title_stores);
        if (menu != null) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(true);
        }
        StoreListFragment fragment = (StoreListFragment) getSupportFragmentManager().getFragments().get(1);
        fragment.requestFilteredSearch(services, latlon, hourFrom, hourTo, dayTm, rangeIndex);
	}
	
    public ViewPager getPagerView() {
        return activity_store_pager;
    }
    
    public void setStores(List<StoreModel> stores) {
    	this.stores = stores;
    }
    
    public List<StoreModel> getStores() {
    	return stores;
    }
    
    public double[] getLocation() {
    	return myLocation;
    }
    
    public LongSparseArray<Drawable> getPhotos() {
    	return photos;
    }
    
    public void setPhoto(long id, Drawable photo) {
    	photos.put(id, photo);
    }
    
    //===================================================================================
    // Inner classes
    //===================================================================================

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    	
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new StoreMapFragment();
                case 1:
                    return new StoreListFragment();
                case 2:
                    return new StoreFilterFragment();
                default:
                    return new StoreListFragment();
            }
        }
        
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
        
    }

}
