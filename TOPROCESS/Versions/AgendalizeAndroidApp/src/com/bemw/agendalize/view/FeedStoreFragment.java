package com.bemw.agendalize.view;

import org.apache.http.HttpStatus;

import com.bemw.agendalize.R;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.util.DeviceUtils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FeedStoreFragment extends Fragment {
	
	// used to manage lazy load pages
	private static Object lock = new Object();
	private static boolean isLoading = false;
	private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;
	    
    private static String name = null;
    private static String address = null;
    private static String contact = null;
	private static HomeActivity activity = null;
	
	private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	if (waitDialog != null) {
            	waitDialog.dismiss();
            	waitDialog = null;
            }
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                	DeviceUtils.justShowDialogWarning(activity, R.string.msg_timeout, false);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                	DeviceUtils.justShowDialogWarning(activity, R.string.msg_noconnection, false);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                	DeviceUtils.justShowDialogWarning(activity, R.string.msg_servererror, false);
                    break;
                case HttpStatus.SC_OK:
                	new AlertDialog.Builder(activity).setMessage(activity.getResources()
        				.getString(R.string.label_thankyouforfeedingstore)).setCancelable(false)
            			.setPositiveButton(activity.getResources().getString(R.string.action_ok),
            			new DialogInterface.OnClickListener() {
            				public void onClick(final DialogInterface dialog, final int id) {
            					activity.goToMainMenu();
            				}
            			}
            		).create().show();
                    break;
            }
            synchronized(lock) {
            	isLoading = false;
            }
        }
    };
    
    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(activity)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
        		RESTClient client = new RESTClient();
        		client.sendStoreSugestion(name, address, contact);
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };
    
    //===================================================================================
    // Override methods
  	//===================================================================================
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_feedstore, container, false);
        activity = (HomeActivity)getActivity();
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		((Button) activity.findViewById(R.id.frag_feedstore_feedbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				name = ((EditText) activity.findViewById(R.id.frag_feedstore_storename)).getText().toString();
				address = ((EditText) activity.findViewById(R.id.frag_feedstore_storeaddress)).getText().toString();
				contact = ((EditText) activity.findViewById(R.id.frag_feedstore_storecontact)).getText().toString();
				if (name.length() > 0 || address.length() > 0 || contact.length() > 0) {
					createNewLoader();
				} else {
					DeviceUtils.justShowDialogWarning(getActivity(), R.string.label_pleasefillfeedinfo, false);
				}
			}
		});
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	synchronized private static void createNewLoader() {
    	if (!isLoading) {
        	synchronized(lock) {
            	isLoading = true;
            }
        	waitDialog = ProgressDialog.show(
                    activity,
                    activity.getText(R.string.msg_sendingdata),
                    activity.getText(R.string.msg_pleasewait),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
            contentLoader = new Thread(loadContent);
            contentLoader.start();
    	}
    }

}
