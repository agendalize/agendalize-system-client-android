package com.bemw.agendalize.view;

import com.bemw.agendalize.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class BookingResumeFragment extends Fragment {

	//===================================================================================
	// Fields
	//===================================================================================
	
	private BookingActivity activity;
	private Button frag_bookingresume_previousbtn;
	private Button frag_bookingresume_nextbtn;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingresume, container, false);
        activity = (BookingActivity)getActivity();
        
        frag_bookingresume_previousbtn = (Button) rootView.findViewById(R.id.frag_bookingresume_previousbtn);
        frag_bookingresume_nextbtn = (Button) rootView.findViewById(R.id.frag_bookingresume_nextbtn);
        
        setupFields();
        
        return rootView;
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	public void setupFields() {
		frag_bookingresume_previousbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.getPagerView().setCurrentItem(2);
			}
		});
		frag_bookingresume_nextbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.getPagerView().setCurrentItem(4);
			}
		});
	}
		
}
