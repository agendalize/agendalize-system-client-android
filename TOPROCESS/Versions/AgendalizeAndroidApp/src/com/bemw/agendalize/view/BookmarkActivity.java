package com.bemw.agendalize.view;

import java.util.List;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.dummydata.DummyBookmark;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.util.ImageUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class BookmarkActivity extends ActionBarActivity {

	//===================================================================================
	// Fields
	//===================================================================================
	
	private ListView storeList					= null;
	private BookmarkListAdapter adapter			= null;
	private TextView emptyLabel					= null;
	private TextView todeletetxt				= null;
	private List<StoreModel> stores				= null;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmark);
		
		storeList = (ListView) findViewById(R.id.activity_bookmark_list);
		emptyLabel = (TextView) findViewById(R.id.activity_bookmark_empty);
		todeletetxt = (TextView) findViewById(R.id.activity_bookmark_todeletetxt);
		
		getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
		getSupportActionBar().setTitle(getResources().getString(R.string.title_bookmark));
		getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.ic_star_w));
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (AppConstants.IS_DEBUG) {
			stores = DummyBookmark.getDummyBookmark();
		} else {
			// TODO: implement bookmark load
			//stores = 
		}
		
		if (stores.size() == 0) {
			emptyLabel.setVisibility(View.VISIBLE);
			storeList.setVisibility(View.GONE);
			todeletetxt.setVisibility(View.GONE);
		} else {
			emptyLabel.setVisibility(View.GONE);
			storeList.setVisibility(View.VISIBLE);
			todeletetxt.setVisibility(View.VISIBLE);
			adapter = new BookmarkListAdapter();
			storeList.setAdapter(adapter);
			storeList.setOnItemClickListener(new OnItemClickListener() {
				@Override
			    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
					// TODO: implement this...
					/*Intent detailIntent = new Intent(BookmarkFragment.this, StoreDetailActivity.class);
					detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					detailIntent.putExtra(AppConstants.STOREDATA, bookmark.get(position).toJson().toString());
					startActivity(detailIntent);*/
				}
			});
			storeList.setLongClickable(true);
			storeList.setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
					final AlertDialog.Builder builder = new AlertDialog.Builder(BookmarkActivity.this);
					builder.setMessage(getResources().getString(R.string.msg_removebookmark)).setCancelable(true).
					setPositiveButton(getResources().getString(R.string.action_yes),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {
								// TODO: implement here...
								adapter.notifyDataSetChanged();
							}
						}
					).setNegativeButton(getResources().getString(R.string.action_no),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
					});
					final AlertDialog alert = builder.create();
					alert.show();
					return true;
				}
			});
			adapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.action_back:
				finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	public class BookmarkListAdapter extends BaseAdapter {
		
		@Override
		public int getCount() {
			return stores.size();
		}
		
		@Override
		public Object getItem(int position) {
			return stores.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_storeitem, parent, false);
			}
			ImageView photo			= ((ImageView)convertView.findViewById(R.id.row_storeitem_photo));
			ProgressBar photoHolder	= ((ProgressBar)convertView.findViewById(R.id.row_storeitem_photoholder));
			TextView name			= ((TextView)convertView.findViewById(R.id.row_storeitem_storename));
			RatingBar rating		= ((RatingBar)convertView.findViewById(R.id.row_storeitem_storerating));
			TextView nremployee		= ((TextView)convertView.findViewById(R.id.row_storeitem_numemployees));
			TextView nrservice		= ((TextView)convertView.findViewById(R.id.row_storeitem_numservices));
			TextView city			= ((TextView)convertView.findViewById(R.id.row_storeitem_citystate));
			TextView address		= ((TextView)convertView.findViewById(R.id.row_storeitem_streetname));
			new PhotoLoader(photo, photoHolder, stores.get(position).getUrlPhoto()).execute();
			name.setText(stores.get(position).getName());
			rating.setRating(stores.get(position).getEvaluation());
			nremployee.setText("" + stores.get(position).getNrEmployees());
			nrservice.setText("" + stores.get(position).getNrServices());
			city.setText(
					stores.get(position).getAddressInfo().getComplement() + " " +
					stores.get(position).getAddressInfo().getCity()
			);
			address.setText(
					stores.get(position).getAddressInfo().getAddress() + " " +
					stores.get(position).getAddressInfo().getAddressNumber() +
					((stores.get(position).getAddressInfo().getComplement() != "") ?
							", " + stores.get(position).getAddressInfo().getComplement() : "")
			);
			return convertView;
		}
		
	}
	
	private class PhotoLoader extends AsyncTask<Void, Void, Void> {
		
	    private Drawable drawable;
	    private ProgressBar progress;
	    private ImageView photo;
	    private String url;
	    
	    public PhotoLoader(ImageView photo, ProgressBar progress, String url) {
	        this.photo = photo;
	        this.progress = progress;
	        this.url = url;
	    }
	    
	    @Override
	    protected Void doInBackground(Void... params) {
	        try {
	        	drawable = ImageUtils.loadImageFromWebOperations(url);
	        	Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
	            drawable = new BitmapDrawable(getResources(), bitmap);
	        } catch (Exception e) {
	        	drawable = null;
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);
	        progress.setVisibility(View.GONE);
	        if (drawable != null) {
	        	photo.setImageDrawable(drawable);
	        } else {
	        	photo.setImageResource(R.drawable.noimage);
	    	}
	    }
	    
	}
	
}
