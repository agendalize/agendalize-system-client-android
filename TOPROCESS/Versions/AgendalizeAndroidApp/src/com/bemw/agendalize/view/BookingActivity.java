package com.bemw.agendalize.view;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.EmployeesPerServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.model.StoreSearchRequestModel;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.LongSparseArray;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * This class should know only which store it is working with. Services and employees are
 * loaded inside its fragments, and its fields are only loaded form store model.
 * 
 * @author mthama
 */
public class BookingActivity extends ActionBarActivity {

	//===================================================================================
	// Fields
	//===================================================================================
	
	private Menu menu;
	private BookingPreviewFragment bookingPreviewFrag;
	private BookingServiceToEmployeeFragment bookingServiceToEmployeeFrag;
	private BookingScheduleToEmployeeFragment bookingScheduleToEmployeeFrag;
	private BookingResumeFragment bookingScheduleResumeFrag;
	private BookingFinishFragment bookingFinishFrag;
	
	private Drawable photo;
	private StoreModel store;
	private StoreSearchRequestModel requestConfigs;
	private LongSparseArray<String> selectedServicesToEmployees;
	private List<EmployeesPerServiceModel> employeesPerService;
	private List<Drawable> albumDrawable;
	
	private final int NUM_PAGES = 5;
	private ViewPager activity_booking_pager;
	private ScreenSlidePagerAdapter pagerAdapter;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        
        try { // populating the store list
			store = new StoreModel(new JSONObject(getIntent().getStringExtra("storeJsonString")));
			requestConfigs = new StoreSearchRequestModel(new JSONObject(getIntent().getStringExtra("requestJsonString")));
			getSupportActionBar().setDisplayShowHomeEnabled(true);
	        getSupportActionBar().setDisplayUseLogoEnabled(true);
	        // TODO: place the store photo
	        //getSupportActionBar().setLogo(R.drawable.ic_chair_w);
	        getSupportActionBar().setTitle(store.getName());
		} catch (Exception e) {
			// this view is only displayed if we have a valid store model
			Toast.makeText(this, R.string.msg_notpossibletoload, Toast.LENGTH_LONG).show();
			finish();
		}
        
        bookingPreviewFrag = new BookingPreviewFragment();
        bookingServiceToEmployeeFrag = new BookingServiceToEmployeeFragment();
        bookingScheduleToEmployeeFrag = new BookingScheduleToEmployeeFragment();
        bookingScheduleResumeFrag = new BookingResumeFragment();
        bookingFinishFrag = new BookingFinishFragment();
        
        albumDrawable = new ArrayList<Drawable>();
        activity_booking_pager = (ViewPager) findViewById(R.id.activity_booking_pager);
        employeesPerService = new ArrayList<EmployeesPerServiceModel>();
        selectedServicesToEmployees = new LongSparseArray<String>();
        
        // Instantiate a ViewPager and a PagerAdapter.
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        activity_booking_pager.setAdapter(pagerAdapter);
        activity_booking_pager.setCurrentItem(0);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.action_back:
				finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    public void onBackPressed() {
        if (activity_booking_pager.getCurrentItem() == 0) {
            finish();
        } else {
        	activity_booking_pager.setCurrentItem(activity_booking_pager.getCurrentItem() - 1);
        }
    }
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	public ViewPager getPagerView() {
        return activity_booking_pager;
    }
	
	public void notifyServiceToEmployeeDataListChange() {
		bookingServiceToEmployeeFrag.notifyDataListChange();
	}
    
    public StoreModel getStore() {
    	return store;
    }
    
    public List<EmployeesPerServiceModel> getEmployeesPerService() {
    	return employeesPerService;
    }
    
    public LongSparseArray<String> getSelectedServicesToEmployees() {
    	return selectedServicesToEmployees;
    }
    
    public List<Drawable> getAlbumDrawables() {
    	return albumDrawable;
    }
    
    public void setAlbumDrawables(List<Drawable> albumDrawable) {
    	this.albumDrawable = albumDrawable;
    }
    
    public StoreSearchRequestModel getStoreSearchRequest() {
    	return requestConfigs;
    }
    
	//===================================================================================
    // Inner classes
    //===================================================================================

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    	
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return bookingPreviewFrag;
                case 1:
                    return bookingServiceToEmployeeFrag;
                case 2:
                    return bookingScheduleToEmployeeFrag;
                case 3:
                	return bookingScheduleResumeFrag;
                case 4:
            		return bookingFinishFrag;
                default:
                    return bookingPreviewFrag;
            }
        }
        
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
        
    }
	
}
