package com.bemw.agendalize.view;

import java.util.Arrays;
import java.util.List;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.SideBarRowModel;
import com.bemw.agendalize.view.MainMenuFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class HomeActivity extends ActionBarActivity {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private int mTitle;
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private final int[] mPlanetTitles = {
			R.string.action_mainmenu,
			R.string.action_feedstore,
			R.string.action_feedback,
			R.string.action_share,
			R.string.action_about,
			R.string.action_logout
	};
	private final List<SideBarRowModel> mDrawerRows = Arrays.asList(
		new SideBarRowModel(mPlanetTitles[0], R.drawable.ic_menu_b),
		new SideBarRowModel(mPlanetTitles[1], R.drawable.ic_chair_b),
		new SideBarRowModel(mPlanetTitles[2], R.drawable.ic_feedback_b),
		new SideBarRowModel(mPlanetTitles[3], R.drawable.ic_share_b),
		new SideBarRowModel(mPlanetTitles[4], R.drawable.ic_help_b),
		new SideBarRowModel(mPlanetTitles[5], R.drawable.ic_logout_b)
	);
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		// creates the left drawer-list
		mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_home_drawerlayout);
		mDrawerList = (ListView) findViewById(R.id.activity_home_leftdrawer);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		mDrawerToggle = new ActionBarDrawerToggle(
			this, mDrawerLayout,
			R.string.app_contentDescription, R.string.app_contentDescription) {
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getSupportActionBar().setTitle(getResources().getString(mTitle));
			}
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getSupportActionBar().setTitle(getResources().getString(mTitle));
			}
		};
		mDrawerList.setAdapter(new MainMenuAdapter(mDrawerRows));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener(mDrawerList, mDrawerLayout));
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// show action bar icon
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		// select the first page (main menu)
		if (savedInstanceState == null) selectItem(0);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Fragment f = getSupportFragmentManager().findFragmentById(R.id.activity_home_contentframe);
	    	if (!(f instanceof MainMenuFragment)) {
	    		selectItem(0);
	    		return true;
	    	}
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	public void goToMainMenu() {
		selectItem(0);
    }
	
	private void selectItem(int position) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		switch (position) {
			case 0:
				fragmentManager.beginTransaction().replace(R.id.activity_home_contentframe, new MainMenuFragment()).commit();
				break;
			case 1:
				fragmentManager.beginTransaction().replace(R.id.activity_home_contentframe, new FeedStoreFragment()).commit();
				break;
			case 2:
				fragmentManager.beginTransaction().replace(R.id.activity_home_contentframe, new FeedbackFragment()).commit();
				break;
			/*case 3:
				fragment = new PromoteFragment();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
				break;*/
			case 4:
				fragmentManager.beginTransaction().replace(R.id.activity_home_contentframe, new AboutFragment()).commit();
				break;
			case 5:
				final AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
				builder.setMessage(getResources().getString(R.string.msg_logout)).setCancelable(false).
				setPositiveButton(getResources().getString(R.string.action_yes),
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog, final int id) {
							SharedPreferences prefsPrivate = getSharedPreferences(AppConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
							Editor prefsPrivateEditor = prefsPrivate.edit();
							prefsPrivateEditor.remove(AppConstants.PIN_CODE);
							prefsPrivateEditor.remove(AppConstants.ACCESS_TOKEN);
							prefsPrivateEditor.commit();
							Intent newApp = new Intent(HomeActivity.this, LoginActivity.class);
							newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(newApp);
							finish();
						}
					}
				).setNegativeButton(getResources().getString(R.string.action_no),
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog, final int id) {}
					}
				).create().show();
				break;
			default:
				break;
		}
		mTitle = mPlanetTitles[position];
		getSupportActionBar().setTitle(mTitle);
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	public class MainMenuAdapter extends BaseAdapter {
		
		private List<SideBarRowModel> rows;
		
		public MainMenuAdapter(List<SideBarRowModel> rows) {
			this.rows = rows;
		}
		
		@Override
		public int getCount() {
			return (rows != null ? rows.size() : 0);
		}
		
		@Override
		public Object getItem(int position) {
			return rows.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_sidemenu, parent, false);
			}
			ImageView sidebarIcon = (ImageView)convertView.findViewById(R.id.row_sidebar_icon);
			sidebarIcon.setBackgroundResource(rows.get(position).getIconId());
			TextView sidebarText = (TextView)convertView.findViewById(R.id.row_sidebar_label);
			sidebarText.setText(getResources().getString(rows.get(position).getLabelId()));
			return convertView;
		}
		
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		
		private ListView mDrawerList;
		private DrawerLayout mDrawerLayout;
		
		public DrawerItemClickListener(ListView mDrawerList, DrawerLayout mDrawerLayout) {
			this.mDrawerList = mDrawerList;
			this.mDrawerLayout = mDrawerLayout;
		}
		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
			mDrawerList.setItemChecked(position, true);
			mDrawerLayout.closeDrawer(mDrawerList);
		}
		
	}

}
