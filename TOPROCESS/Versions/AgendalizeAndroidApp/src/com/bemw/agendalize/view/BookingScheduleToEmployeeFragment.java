package com.bemw.agendalize.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.SchedulesForEmployeeModel;
import com.bemw.agendalize.util.DeviceUtils;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class BookingScheduleToEmployeeFragment extends Fragment {

	//===================================================================================
	// Fields
	//===================================================================================
	
	private BookingActivity activity;
	private Button frag_bookingscheduletoemployee_previousbtn;
	private Button frag_bookingscheduletoemployee_nextbtn;
	
	private ListView openHoursList;
	private ScheduleListAdapter adapter;
	private List<SchedulesForEmployeeModel> items;
	
	private TextView frag_bookingscheduletoemployee_monthinview;
	private TextView frag_bookingscheduletoemployee_dayinview;
	private Calendar calendar;
	
	// we name the left, middle and right page
	private static final int PAGE_LEFT = 0;
	private static final int PAGE_MIDDLE = 1;
	private static final int PAGE_RIGHT = 2;

	private LayoutInflater mInflater;
	private int mSelectedPageIndex = 1;
	// we save each page in a model
	private PageModel[] mPageModel = new PageModel[3];
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingscheduletoemployee, container, false);
		activity = (BookingActivity)getActivity();
		
		frag_bookingscheduletoemployee_previousbtn = (Button) rootView.findViewById(R.id.frag_bookingscheduletoemployee_previousbtn);
        frag_bookingscheduletoemployee_nextbtn = (Button) rootView.findViewById(R.id.frag_bookingscheduletoemployee_nextbtn);
        
        setupFields();
        
        frag_bookingscheduletoemployee_monthinview = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_monthinview);
		frag_bookingscheduletoemployee_dayinview = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_dayinview);
		
		// initializing the model
		initPageModel();

		mInflater = getActivity().getLayoutInflater();
		SchedulePageAdapter adapter = new SchedulePageAdapter();

		final ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.frag_bookingscheduletoemployee_viewpager);
		viewPager.setAdapter(adapter);
		// we don't want any smooth scroll. This enables us to switch the page without the user notifiying this
		viewPager.setCurrentItem(PAGE_MIDDLE, false);
		
		// get current date and center it on the scheduler
		calendar = Calendar.getInstance();
		calendar.setTime(new Date(activity.getStoreSearchRequest().getBaseDate()));
		frag_bookingscheduletoemployee_monthinview.setText(getMonthNameByIndex(calendar.get(Calendar.MONTH)));
		frag_bookingscheduletoemployee_dayinview.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				mSelectedPageIndex = position;
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == ViewPager.SCROLL_STATE_IDLE) {
					final PageModel leftPage = mPageModel[PAGE_LEFT];
					final PageModel middlePage = mPageModel[PAGE_MIDDLE];
					final PageModel rightPage = mPageModel[PAGE_RIGHT];
					final int oldLeftIndex = leftPage.getIndex();
					final int oldMiddleIndex = middlePage.getIndex();
					final int oldRightIndex = rightPage.getIndex();
					// user swiped to right direction --> left page
					if (mSelectedPageIndex == PAGE_LEFT) {
						leftPage.setIndex(oldLeftIndex - 1);
						middlePage.setIndex(oldLeftIndex);
						rightPage.setIndex(oldMiddleIndex);
						setContent(PAGE_RIGHT);
						setContent(PAGE_MIDDLE);
						setContent(PAGE_LEFT);
						calendar.add(Calendar.DAY_OF_YEAR, - 1);
						frag_bookingscheduletoemployee_monthinview.setText(getMonthNameByIndex(calendar.get(Calendar.MONTH)));
						frag_bookingscheduletoemployee_dayinview.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));
					// user swiped to left direction --> right page
					} else if (mSelectedPageIndex == PAGE_RIGHT) {
						leftPage.setIndex(oldMiddleIndex);
						middlePage.setIndex(oldRightIndex);
						rightPage.setIndex(oldRightIndex + 1);
						setContent(PAGE_LEFT);
						setContent(PAGE_MIDDLE);
						setContent(PAGE_RIGHT);
						calendar.add(Calendar.DAY_OF_YEAR, + 1);
						frag_bookingscheduletoemployee_monthinview.setText(getMonthNameByIndex(calendar.get(Calendar.MONTH)));
						frag_bookingscheduletoemployee_dayinview.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));
					}
					viewPager.setCurrentItem(PAGE_MIDDLE, false);
				}
			}
		});
		
		return rootView;
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	private String getMonthNameByIndex(int index) {
		switch (index) {
			case 0: return "Jan";
			case 1: return "Fev";
			case 2: return "Mar";
			case 3: return "Abr";
			case 4: return "Mai";
			case 5: return "Jun";
			case 6: return "Jul";
			case 7: return "Ago";
			case 8: return "Set";
			case 9: return "Out";
			case 10: return "Nov";
			case 11: return "Dez";
			default: return "Jan";
		}
	}
	
	public void setupFields() {
		frag_bookingscheduletoemployee_previousbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.getPagerView().setCurrentItem(1);
			}
		});
		frag_bookingscheduletoemployee_nextbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isAllFieldsOk()) {
					activity.getPagerView().setCurrentItem(3);
				} else {
					DeviceUtils.justShowDialogWarning(activity, R.string.msg_selectaschedules, false);
				}
			}
		});
	}
	
	private boolean isAllFieldsOk() {
		// TODO: implement here...
		return true;
	}

	private void initPageModel() {
		for (int i = 0; i < mPageModel.length; i++) {
			// initing the pagemodel with indexes of -1, 0 and 1
			mPageModel[i] = new PageModel(i - 1);
		}
	}
	
	private void setContent(int index) {
		final PageModel model = mPageModel[index];
		// TODO: this should be removed from here
		items = new ArrayList<SchedulesForEmployeeModel>();
		for (int i=0; i<AppConstants.SCHEDULE_DURATION_MIN*(60/AppConstants.SCHEDULE_INTERVALS_MIN)/60; i++) {
			items.add(new SchedulesForEmployeeModel());
		}
		openHoursList = (ListView)model.listview.findViewById(R.id.helper_listschedule_openHours);
		adapter = new ScheduleListAdapter(getActivity(), items);
		openHoursList.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class ScheduleListAdapter extends BaseAdapter {

		private Context context;
		private List<SchedulesForEmployeeModel> items;
		
		public ScheduleListAdapter(Context context, List<SchedulesForEmployeeModel> items) {
			this.context = context;
			this.items = items;
		}
		
		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_schedule, parent, false);
			}
			// this fill the first columns: the hours
			TextView hour = ((TextView)convertView.findViewById(R.id.row_schedule_hour));
			hour.setText(
					(int)((position/4)+(AppConstants.SCHEDULE_START_MIN/60))+":"+
					(position%4==0?"00":(position+1)%4==0?"45":(position+2)%4==0?"30":"15")
			);
			// here, for each employee we fill his schedules
			LinearLayout row_schedule_statusathour = ((LinearLayout) convertView.findViewById(R.id.row_schedule_statusathour));
			for (int i=1; i<((ViewGroup)row_schedule_statusathour).getChildCount(); ++i) {
			    View statusView = ((ViewGroup)row_schedule_statusathour).getChildAt(i);
			    try {
			    	Random randomGenerator = new Random();
			    	if (randomGenerator.nextInt(100)%3 == 0)
			    		((ImageButton)statusView).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
					else if (randomGenerator.nextInt(100)%2 == 0)
						((ImageButton)statusView).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
					else
						((ImageButton)statusView).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			    } catch (Exception e) {}
			}
			return convertView;
		}

	}
	
	private class SchedulePageAdapter extends PagerAdapter {
		
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ListView listview = (ListView)mInflater.inflate(R.layout.helper_listschedule, container, false);
			PageModel currentPage = mPageModel[position];
			currentPage.listview = listview;
			// TODO:
			ArrayList<SchedulesForEmployeeModel> items = new ArrayList<SchedulesForEmployeeModel>();
			for (int i=0; i<AppConstants.SCHEDULE_DURATION_MIN*(60/AppConstants.SCHEDULE_INTERVALS_MIN)/60; i++) {
				items.add(new SchedulesForEmployeeModel());
			}
			ScheduleListAdapter adapter = new ScheduleListAdapter(getActivity(), items);
			listview.setAdapter(adapter);
			adapter.notifyDataSetChanged();
			container.addView(listview);
			return listview;
		}

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			return view == obj;
		}
		
	}
	
	private class PageModel {

		private int index;
		public ListView listview;

		public PageModel(int index) {
			this.index = index;
			setIndex(index);
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}
		
	}
	
}
