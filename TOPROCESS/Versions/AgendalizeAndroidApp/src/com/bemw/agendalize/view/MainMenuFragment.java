package com.bemw.agendalize.view;

import com.bemw.agendalize.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class MainMenuFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_mainmenu, container, false);
		
		((Button) rootView.findViewById(R.id.frag_mainmenu_searchandschedulebtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), StoreActivity.class));
			}
		});
		/*((Button) rootView.findViewById(R.id.frag_mainmenu_nearbybtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//createNewLoader();
			}
		});
		((Button) rootView.findViewById(R.id.frag_mainmenu_myschedulebtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//startActivity(new Intent(getActivity(), BookmarkActivity.class));
			}
		});
		((Button) rootView.findViewById(R.id.frag_mainmenu_feedstorebtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//startActivity(new Intent(getActivity(), FeedStoreActivity.class));
			}
		});*/
		((Button) rootView.findViewById(R.id.frag_mainmenu_bookmarkbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), BookmarkActivity.class));
			}
		});
        /*((Button) rootView.findViewById(R.id.frag_mainmenu_historybtn)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity(), FeedbackActivity.class));
            }
        });*/

		return rootView;
	}
	
}
