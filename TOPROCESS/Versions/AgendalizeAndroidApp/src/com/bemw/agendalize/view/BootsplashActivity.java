package com.bemw.agendalize.view;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

public class BootsplashActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bootsplash);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		new Thread() {
			@Override
			public void run() {
				try {
					sleep(AppConstants.BOOTSPLASH_TIMER);
				} catch (InterruptedException e) {
					Log.w(AppConstants.TAG, e.getLocalizedMessage());
				} finally {
					SharedPreferences prefsPrivate = BootsplashActivity.this.getSharedPreferences(
							AppConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
					String pin = prefsPrivate.getString(AppConstants.PIN_CODE, null);
					String token = prefsPrivate.getString(AppConstants.ACCESS_TOKEN, null);
					
					if (AppConstants.IS_DEBUG) {
						Intent newApp = new Intent(BootsplashActivity.this, HomeActivity.class);
						newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(newApp);
					} else {
						if (pin != null && token != null) {
							// creates a new application instance
							Log.w(AppConstants.TAG, "PIN: "+pin + " / TOKEN: "+token);
							Intent newApp = new Intent(BootsplashActivity.this, HomeActivity.class);
							newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(newApp);
						} else {
							// TODO: uncomment and implement this
							/*Intent newApp = new Intent(BootsplashActivity.this, LoginActivity.class);
							newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(newApp);*/
						}
					}
					
					finish();
				}
			}
		}.start();
	}
	
	public static int getNavigationBarHeight(Context context) {
	    Resources resources = context.getResources();
	    int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
	    if (resourceId > 0) {
	        return resources.getDimensionPixelSize(resourceId);
	    }
	    return 0;
	}

}
