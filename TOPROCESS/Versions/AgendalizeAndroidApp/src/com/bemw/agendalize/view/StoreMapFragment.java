package com.bemw.agendalize.view;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class StoreMapFragment extends Fragment {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private static StoreActivity activity;
	private static Fragment fragMan;
	private static FragmentManager fragmentManager;
	private static GoogleMap frag_storemap_mapview;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storemap, container, false);
        activity = (StoreActivity)getActivity();
        fragmentManager = getChildFragmentManager();
        frag_storemap_mapview = null;
        fragMan = null;
        
        setUpMapIfNeeded();
        
        return rootView;
    }
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
	    if (frag_storemap_mapview != null)
	    	setUpMap();
	    if (frag_storemap_mapview == null) {
	    	frag_storemap_mapview =
	    			((SupportMapFragment) fragmentManager.findFragmentById(R.id.frag_storemap_mapview)).getMap();
	        if (frag_storemap_mapview != null)
	        	setUpMap();
	    }
	}
	
	@Override
	public void onDestroyView() {
		if (frag_storemap_mapview != null) {
	    	try {
		    	FragmentTransaction transaction = fragmentManager.beginTransaction();
		    	// this is throwing an IllegalStateException because activity is being destroyed...
		    	transaction.remove(fragmentManager.findFragmentById(R.id.frag_storemap_mapview)).commit();
		        frag_storemap_mapview = null;
            } catch (Exception e) {
            	Log.w(AppConstants.TAG, "StoreMapFragment throwing exception: " + e);
            }
	    }
	    super.onDestroyView();
	}
	
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && frag_storemap_mapview != null) {
        	frag_storemap_mapview.clear();
        	// place each store...
    		for (int i=0; i<activity.getStores().size(); i++) {
    			LatLng l = new LatLng(
    					activity.getStores().get(i).getAddressInfo().getLatitude(),
    					activity.getStores().get(i).getAddressInfo().getLongitude());
    			frag_storemap_mapview.addMarker(
    					new MarkerOptions().position(l)
    					.title(activity.getStores().get(i).getName())
    					.snippet(
    							activity.getStores().get(i).getAddressInfo().getAddress() + " " +
    							activity.getStores().get(i).getAddressInfo().getAddressNumber())
						.icon(BitmapDescriptorFactory.fromBitmap(
							BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_chair_b)
						))
    			);
    			Log.i(AppConstants.TAG, "Added store " + activity.getStores().get(i).getName() +
    					" at location " + activity.getStores().get(i).getAddressInfo().getLatitude() + 
    					" / " + activity.getStores().get(i).getAddressInfo().getLongitude());
    			// TODO: properly set onClick for each
    		}
        }
    }
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	public static void setUpMap() {
		// put my location
		frag_storemap_mapview.setMyLocationEnabled(true);
        LatLng location = new LatLng(
        		activity.getLocation()[0],
        		activity.getLocation()[1]
        );
        frag_storemap_mapview.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 11.0f));
        frag_storemap_mapview.getUiSettings().setZoomControlsEnabled(true);
	}
	
	public static void setUpMapIfNeeded() {
	    if (frag_storemap_mapview == null) {
	    	fragMan = fragmentManager.findFragmentById(R.id.frag_storemap_mapview);
	    	SupportMapFragment support = ((SupportMapFragment)fragMan);
	    	frag_storemap_mapview = support.getMap();
	        if (frag_storemap_mapview != null) {
	        	setUpMap();
	        }
	    }
	}
	
	//===================================================================================
    // Inner classes
    //===================================================================================

}
