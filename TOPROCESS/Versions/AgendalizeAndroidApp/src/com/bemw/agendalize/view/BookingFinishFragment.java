package com.bemw.agendalize.view;

import com.bemw.agendalize.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BookingFinishFragment extends Fragment {

	//===================================================================================
	// Fields
	//===================================================================================
	
	private BookingActivity activity;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingfinish, container, false);
        activity = (BookingActivity)getActivity();
        
        return rootView;
	}
	
}
