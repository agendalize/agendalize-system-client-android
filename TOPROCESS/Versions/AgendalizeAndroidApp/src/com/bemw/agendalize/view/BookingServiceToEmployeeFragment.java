package com.bemw.agendalize.view;

import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.EmployeeModel;
import com.bemw.agendalize.model.EmployeesPerServiceModel;
import com.bemw.agendalize.util.DeviceUtils;
import com.bemw.agendalize.util.ImageUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BookingServiceToEmployeeFragment extends Fragment {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private BookingActivity activity;
	private BookingServiceToEmployeeAdapter adapter;
	private ExpandableListView frag_bookingservicetoemployee_listview;
	
	private Button frag_bookingservicetoemployee_checkstorebtn;
	private Button frag_bookingservicetoemployee_nextbtn;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingservicetoemployee, container, false);
        activity = (BookingActivity)getActivity();
        
        frag_bookingservicetoemployee_listview = (ExpandableListView) rootView.findViewById(R.id.frag_bookingservicetoemployee_listview);
        frag_bookingservicetoemployee_checkstorebtn = (Button) rootView.findViewById(R.id.frag_bookingservicetoemployee_checkstorebtn);
        frag_bookingservicetoemployee_nextbtn = (Button) rootView.findViewById(R.id.frag_bookingservicetoemployee_nextbtn);
        
        setupFields();
        
        return rootView;
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	public void notifyDataListChange() {
		adapter.notifyDataSetChanged();
		// at this point, we already know which services/employees were selected, so we can initiate this array
        for (int i=0; i<activity.getEmployeesPerService().size(); i++) {
        	activity.getSelectedServicesToEmployees().put(activity.getEmployeesPerService().get(i).getCod(), null);
		}
	}
	
	private void setupFields() {
		adapter = new BookingServiceToEmployeeAdapter(activity.getEmployeesPerService());
		frag_bookingservicetoemployee_listview.setAdapter(adapter);
		frag_bookingservicetoemployee_listview.setGroupIndicator(null);
		frag_bookingservicetoemployee_listview.setOnChildClickListener(new OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
                final EmployeeModel selected = (EmployeeModel) adapter.getChild(groupPosition, childPosition);
                activity.getSelectedServicesToEmployees().put(
                		activity.getEmployeesPerService().get(groupPosition).getCod(),
                		selected.getLogin());
            	frag_bookingservicetoemployee_listview.collapseGroup(groupPosition);
                return true;
            }
        });
	    frag_bookingservicetoemployee_checkstorebtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.getPagerView().setCurrentItem(0);
			}
		});
	    frag_bookingservicetoemployee_nextbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isAllFieldsOk()) {
					activity.getPagerView().setCurrentItem(2);
				} else {
					DeviceUtils.justShowDialogWarning(activity, R.string.msg_selectaemployee, false);
				}
			}
		});
	}
	
	private boolean isAllFieldsOk() {
		for (int i=0; i<activity.getSelectedServicesToEmployees().size(); i++) {
		   long key = activity.getSelectedServicesToEmployees().keyAt(i);
		   if (activity.getSelectedServicesToEmployees().get(key) == null)
			   return false;
		}
		return true;
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class BookingServiceToEmployeeAdapter extends BaseExpandableListAdapter {

		private List<EmployeesPerServiceModel> service2employees;
	    
	    public BookingServiceToEmployeeAdapter(List<EmployeesPerServiceModel> service2employees) {
	        this.service2employees = service2employees;
	    }
	    
	    @Override
	    public Object getChild(int groupPosition, int childPosition) {
	        return service2employees.get(groupPosition).getEmployees().get(childPosition);
	    }
	 
	    @Override
	    public long getChildId(int groupPosition, int childPosition) {
	        return childPosition;
	    }

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
	        LayoutInflater inflater = activity.getLayoutInflater();
	        if (convertView == null) {
	            convertView = inflater.inflate(R.layout.row_employeeitem, parent, false);
	        }
	        ImageButton row_employeeitem_photo = (ImageButton)convertView.findViewById(R.id.row_employeeitem_photo);
		    TextView row_employeeitem_name = (TextView)convertView.findViewById(R.id.row_employeeitem_name);
		    TextView row_employeeitem_phone = (TextView)convertView.findViewById(R.id.row_employeeitem_phone);
		    // TODO: load the correct employee photo
		    //new PhotoLoader(row_employeeitem_photo, "http://bestinspired.com/wp-content/uploads/2015/05/beautiful-girl-beautiful-girl.jpg").execute();
		    row_employeeitem_name.setText(
		    		service2employees.get(groupPosition).getEmployees().get(childPosition).getName() + " " +
    				service2employees.get(groupPosition).getEmployees().get(childPosition).getLastName()
    		);
		    row_employeeitem_phone.setText(
	    		service2employees.get(groupPosition).getEmployees().get(childPosition).getWorkPhoneNumber().length() > 0 ?
    				service2employees.get(groupPosition).getEmployees().get(childPosition).getWorkPhoneNumber() : (
					service2employees.get(groupPosition).getEmployees().get(childPosition).getCellPhoneNumber().length() > 0 ?
						service2employees.get(groupPosition).getEmployees().get(childPosition).getCellPhoneNumber() : (
						service2employees.get(groupPosition).getEmployees().get(childPosition).getPhoneNumber().length() > 0 ?
							service2employees.get(groupPosition).getEmployees().get(childPosition).getPhoneNumber() : ""
						)
					)
    		);
	        return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return service2employees.get(groupPosition).getEmployees().size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return service2employees.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return service2employees.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			if (convertView == null) {
	            LayoutInflater infalInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = infalInflater.inflate(R.layout.row_servicelabel, parent, false);
	        }
	        TextView row_servicelabel_name = ((TextView) convertView.findViewById(R.id.row_servicelabel_name));
	        ProgressBar row_servicelabel_progress = ((ProgressBar) convertView.findViewById(R.id.row_servicelabel_progress));
	        row_servicelabel_name.setText(
	        		service2employees.get(groupPosition).getShortName() + " (" +
    				(activity.getSelectedServicesToEmployees().get(service2employees.get(groupPosition).getCod()) == null ?
						activity.getResources().getString(R.string.label_employeepending) :
							activity.getSelectedServicesToEmployees().get(service2employees.get(groupPosition).getCod())) + ")"
    		);
	        row_servicelabel_name.setCompoundDrawablesWithIntrinsicBounds(
	        		0, 0, isExpanded ? R.drawable.ic_collapse_g : R.drawable.ic_expand_g, 0);
	        row_servicelabel_progress.setVisibility(View.GONE);
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
	}
	
	private class PhotoLoader extends AsyncTask<Void, Void, Void> {
		
	    private Drawable drawable;
	    private ImageButton photo;
	    private String url;
	    
	    public PhotoLoader(ImageButton photo, String url) {
	        this.photo = photo;
	        this.url = url;
	    }
	    
	    @Override
	    protected Void doInBackground(Void... params) {
	        try {
	        	drawable = ImageUtils.loadCachedImage(getActivity(), url);
	        	if (drawable == null) {
					Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.noimage), true);
					drawable = new BitmapDrawable(getResources(), bmp);
				} else {
					drawable = ImageUtils.resize(drawable, 48, 48);
					Bitmap bmp = ((BitmapDrawable)drawable).getBitmap();
					bmp = ImageUtils.getCroppedBitmap(bmp, true);
					drawable = new BitmapDrawable(getResources(), bmp);
				}
	        } catch (Exception e) {
	        	Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.noimage), true);
	        	drawable = new BitmapDrawable(activity.getResources(), bmp);
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPostExecute(Void result) {
	        photo.setBackground(drawable);
	    }
	    
	}

}
