package com.bemw.agendalize.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.AlbumModel;
import com.bemw.agendalize.model.EmployeeModel;
import com.bemw.agendalize.model.EmployeesPerServiceModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.util.Dialer;
import com.bemw.agendalize.util.ImageUtils;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * This class is responsible to load the list of employees and services.
 * 
 * @author mthama
 */
public class BookingPreviewFragment extends Fragment {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private BookingActivity activity;
	private AlbumModel album;
	
	private ProgressBar row_bookingpreview_albumprogress;
	private LinearLayout frag_bookingpreview_innerlayer;
	private TextView frag_bookingpreview_address;
	private Button frag_bookingpreview_callbtn;
	private Button frag_bookingpreview_favoritebtn;
	private Button frag_bookingpreview_locationbtn;
	private TextView frag_bookingpreview_description;
	private LinearLayout frag_bookingpreview_employeelist;
	private LinearLayout frag_bookingpreview_servicelist;
	private Button frag_bookingpreview_bookbtn;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingpreview, container, false);
        activity = (BookingActivity)getActivity();
        album = new AlbumModel();

        // fill information
        row_bookingpreview_albumprogress = (ProgressBar) rootView.findViewById(R.id.row_bookingpreview_albumprogress);
        frag_bookingpreview_innerlayer = (LinearLayout) rootView.findViewById(R.id.frag_bookingpreview_innerlayer);
        frag_bookingpreview_address = (TextView) rootView.findViewById(R.id.frag_bookingpreview_address);
        frag_bookingpreview_callbtn = (Button) rootView.findViewById(R.id.frag_bookingpreview_callbtn);
        frag_bookingpreview_favoritebtn = (Button) rootView.findViewById(R.id.frag_bookingpreview_favoritebtn);
        frag_bookingpreview_locationbtn = (Button) rootView.findViewById(R.id.frag_bookingpreview_locationbtn);
        frag_bookingpreview_description = (TextView) rootView.findViewById(R.id.frag_bookingpreview_description);
        frag_bookingpreview_employeelist = (LinearLayout) rootView.findViewById(R.id.frag_bookingpreview_employeelist);
        frag_bookingpreview_servicelist = (LinearLayout) rootView.findViewById(R.id.frag_bookingpreview_servicelist);
        frag_bookingpreview_bookbtn = (Button) rootView.findViewById(R.id.frag_bookingpreview_bookbtn);
        
    	setupFields();
        
        return rootView;
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	private void setupFields() {
		frag_bookingpreview_address.setText(
				activity.getStore().getAddressInfo().getAddress() + " " +
				activity.getStore().getAddressInfo().getAddressNumber() + "\n" +
				activity.getStore().getAddressInfo().getCity() + " " +
				activity.getStore().getAddressInfo().getState()
		);
		frag_bookingpreview_description.setText(
				activity.getStore().getDescription()
		);
		frag_bookingpreview_callbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Dialer.call(activity, activity.getStore().getContactInfo().getPhoneNumberOne());
			}
		});
		frag_bookingpreview_favoritebtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: add to bookmarks
			}
		});
		frag_bookingpreview_locationbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: locate in the map
			}
		});
		frag_bookingpreview_bookbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.notifyServiceToEmployeeDataListChange();
				activity.getPagerView().setCurrentItem(1);
			}
		});
		frag_bookingpreview_bookbtn.setEnabled(false);
		frag_bookingpreview_bookbtn.setAlpha(0.5f);
		
		new ServicesLoaderTask().execute();
	}
	
	//===================================================================================
    // Inner classes
    //===================================================================================
	
	private class ServicesLoaderTask extends AsyncTask<Void, Void, Void> {

		private List<EmployeesPerServiceModel> employeesPerService;
		private HashMap<String, EmployeeModel> employees = new HashMap<String, EmployeeModel>();
		
		public ServicesLoaderTask() {
			employees.clear();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO: is it the right way to get store services?
			long[] servID = new long[activity.getStore().getListServices().size()];
			for (int i=0; i<activity.getStore().getListServices().size(); i++) {
				servID[i] = activity.getStore().getListServices().get(i).getCod();
			}
			RESTClient client = new RESTClient();
			employeesPerService = client.getEmployeesPerServiceList(activity.getStore().getId(), servID);
			album = client.getStoreAlbum(activity.getStore().getId());
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// fill service list view
			activity.getEmployeesPerService().clear();
			activity.getEmployeesPerService().addAll(employeesPerService);
			for (int i=0; i<employeesPerService.size(); i++) {
				for (int j=0; j<employeesPerService.get(i).getEmployees().size(); j++) {
					String login = employeesPerService.get(i).getEmployees().get(j).getLogin();
					if (!employees.containsKey(login)) {
						employees.put(login,
								employeesPerService.get(i).getEmployees().get(j)
						);
					}
				}
				RelativeLayout row = (RelativeLayout) activity.getLayoutInflater().inflate(R.layout.row_serviceitem, frag_bookingpreview_servicelist, false);
			    ImageButton row_serviceitem_photo = (ImageButton)row.findViewById(R.id.row_serviceitem_photo);
			    TextView row_serviceitem_name = (TextView)row.findViewById(R.id.row_serviceitem_name);
			    TextView row_serviceitem_description = (TextView)row.findViewById(R.id.row_serviceitem_description);
			    CheckBox row_serviceitem_isselected = (CheckBox)row.findViewById(R.id.row_serviceitem_isselected);
			    new ServiceImageLoader(row_serviceitem_photo, employeesPerService.get(i).getCod()).execute();
			    row_serviceitem_name.setText(
			    		employeesPerService.get(i).getShortName()
	    		);
			    row_serviceitem_description.setVisibility(View.GONE);
			    row_serviceitem_isselected.setVisibility(View.GONE);
			    frag_bookingpreview_servicelist.addView(row);
			}
			((ProgressBar) frag_bookingpreview_servicelist.findViewById(R.id.row_servicelabel_progress)).setVisibility(View.GONE);
			// fill employee list view
			for (Map.Entry<String, EmployeeModel> entry : employees.entrySet()) {
				LinearLayout row = (LinearLayout) activity.getLayoutInflater().inflate(R.layout.row_employeeitem, frag_bookingpreview_employeelist, false);
			    ImageButton row_employeeitem_photo = (ImageButton)row.findViewById(R.id.row_employeeitem_photo);
			    TextView row_employeeitem_name = (TextView)row.findViewById(R.id.row_employeeitem_name);
			    TextView row_employeeitem_phone = (TextView)row.findViewById(R.id.row_employeeitem_phone);
			    // TODO: load the correct employee photo
			    //new PhotoLoader(row_employeeitem_photo, "http://bestinspired.com/wp-content/uploads/2015/05/beautiful-girl-beautiful-girl.jpg").execute();
			    row_employeeitem_name.setText(
			    		entry.getValue().getName() + " " + entry.getValue().getLastName()
	    		);
			    row_employeeitem_phone.setText(
			    		entry.getValue().getWorkPhoneNumber().length() > 0 ? entry.getValue().getWorkPhoneNumber() : (
			    				entry.getValue().getCellPhoneNumber().length() > 0 ? entry.getValue().getCellPhoneNumber() : (
			    						entry.getValue().getPhoneNumber().length() > 0 ? entry.getValue().getPhoneNumber() : ""
								)
						)
	    		);
			    frag_bookingpreview_employeelist.addView(row);
			}
			((ProgressBar) frag_bookingpreview_employeelist.findViewById(R.id.row_employeelabel_progress)).setVisibility(View.GONE);
			if (album != null && album.getPhotoUrls().size() > 0) {
				row_bookingpreview_albumprogress.setVisibility(View.VISIBLE);
				// TODO: load album
				//new AlbumLoaderTask(album).execute();
			}
			// now that all is loaded, we can go to next view
			frag_bookingpreview_bookbtn.setEnabled(true);
			frag_bookingpreview_bookbtn.setAlpha(1.0f);
			super.onPostExecute(result);
		}
		
	}
	
	private class ServiceImageLoader extends AsyncTask<Void, Void, Void> {
		
	    private Drawable drawable;
	    private ImageButton svcImg;
	    private long type;
	    
	    public ServiceImageLoader(ImageButton svcImg, long type) {
	        this.svcImg = svcImg;
	        this.type = type;
	    }
	    
	    @Override
	    protected Void doInBackground(Void... params) {
	        try {
	        	drawable = new RESTClient().getServicePhotoByType(activity, type);
	        	if (drawable == null) {
					Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.noimage), false);
					drawable = new BitmapDrawable(getResources(), bmp);
				} else {
					drawable = ImageUtils.resize(drawable, 48, 48);
					Bitmap bmp = ((BitmapDrawable)drawable).getBitmap();
					bmp = ImageUtils.getCroppedBitmap(bmp, false);
					drawable = new BitmapDrawable(getResources(), bmp);
				}
	        } catch (Exception e) {
	        	Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.noimage), false);
	        	drawable = new BitmapDrawable(getResources(), bmp);
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);
	        svcImg.setBackground(drawable);
	    }
	    
	}
	
	private class PhotoLoader extends AsyncTask<Void, Void, Void> {
		
	    private Drawable drawable;
	    private ImageButton photo;
	    private String url;
	    
	    public PhotoLoader(ImageButton photo, String url) {
	        this.photo = photo;
	        this.url = url;
	    }
	    
	    @Override
	    protected Void doInBackground(Void... params) {
	        try {
	        	drawable = ImageUtils.loadCachedImage(getActivity(), url);
	        	if (drawable == null) {
					Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.noimage), true);
					drawable = new BitmapDrawable(getResources(), bmp);
				} else {
					drawable = ImageUtils.resize(drawable, 48, 48);
					Bitmap bmp = ((BitmapDrawable)drawable).getBitmap();
					bmp = ImageUtils.getCroppedBitmap(bmp, true);
					drawable = new BitmapDrawable(getResources(), bmp);
				}
	        } catch (Exception e) {
	        	Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.noimage), true);
	        	drawable = new BitmapDrawable(activity.getResources(), bmp);
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPostExecute(Void result) {
	        photo.setBackground(drawable);
	    }
	    
	}
	
	private class AlbumLoaderTask extends AsyncTask<Void, Void, Void> {
		
		private AlbumModel album;
		
		public AlbumLoaderTask(AlbumModel album) {
			this.album = album;
		}
		
		@Override
	    protected Void doInBackground(Void... params) {
			for (int i=0; i<album.getPhotoUrls().size(); i++) {
				Drawable d = ImageUtils.loadCachedImage(getActivity(), album.getPhotoUrls().get(i));
				if (d == null) {
					Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.noimage), false);
					d = new BitmapDrawable(getResources(), bmp);
				} else {
					d = ImageUtils.resize(d, 48, 48);
					Bitmap bmp = ((BitmapDrawable)d).getBitmap();
					bmp = ImageUtils.getCroppedBitmap(bmp, false);
					d = new BitmapDrawable(getResources(), bmp);
				}
				activity.getAlbumDrawables().add(d);
			}
			return null;
		}
		
		@Override
	    protected void onPostExecute(Void result) {
		    for (int i=0; i<activity.getAlbumDrawables().size(); i++) {
			    ImageView photo = new ImageView(activity);
			    photo.setImageDrawable(activity.getAlbumDrawables().get(i));
			    int pixels = (int) (getPxForResolution() * activity.getResources().getDisplayMetrics().density + 0.5f);
			    int margin = (int) (8 * activity.getResources().getDisplayMetrics().density + 0.5f);
			    LinearLayout.LayoutParams lyt = new LinearLayout.LayoutParams(pixels, pixels);
			    lyt.setMargins(margin, margin, margin, margin);
			    photo.setLayoutParams(lyt);
			    frag_bookingpreview_innerlayer.addView(photo);
		    }
		    row_bookingpreview_albumprogress.setVisibility(View.GONE);
		}
		
		private int getPxForResolution() {
			Configuration config = activity.getResources().getConfiguration();
			if (config.smallestScreenWidthDp >= 720) {
				return (int) (72 * activity.getResources().getDisplayMetrics().density + 0.5f);
			} else if (config.smallestScreenWidthDp >= 600) {
				return (int) (64 * activity.getResources().getDisplayMetrics().density + 0.5f);
			} else if (config.smallestScreenWidthDp >= 480) {
				return (int) (48 * activity.getResources().getDisplayMetrics().density + 0.5f);
			} else {
				return (int) (32 * activity.getResources().getDisplayMetrics().density + 0.5f);
			}
		}
		 
	}

}
