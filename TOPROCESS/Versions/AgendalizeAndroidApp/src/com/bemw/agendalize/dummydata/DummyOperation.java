package com.bemw.agendalize.dummydata;

import java.util.ArrayList;
import java.util.List;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.model.OperationModel;

public class DummyOperation {
	
	public static List<OperationModel> getDummyOperation() {
		ArrayList<OperationModel> employeesperservices = new ArrayList<OperationModel>();
		for (int i=0; i<AppConstants.SIZE_LIST; i++) {
			employeesperservices.add(new OperationModel());
		}
		return employeesperservices;
	}

}
