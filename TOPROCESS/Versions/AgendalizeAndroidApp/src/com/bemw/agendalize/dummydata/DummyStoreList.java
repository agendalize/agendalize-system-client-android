package com.bemw.agendalize.dummydata;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.model.AddressModel;
import com.bemw.agendalize.model.ContactsModel;
import com.bemw.agendalize.model.OperationModel;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.google.android.gms.maps.model.LatLng;

public class DummyStoreList {
	
	public static LatLng random(Random r) {
        return new LatLng((r.nextDouble() * -180.0) + 90.0, (r.nextDouble() * -360.0) + 180.0);
	}
	
	public static List<StoreModel> getDummyStoreList() {
		ArrayList<StoreModel> stores = new ArrayList<StoreModel>();
		for (int i=0; i<AppConstants.SIZE_LIST; i++) {
			LatLng geo = random(new Random());
			stores.add(new StoreModel(
					i, "Dummy Store " + i, "Dummy Company " + i, "loren ipson " + i, "000000000",
					new AddressModel(
							"Address " + i, "" + i, "Complement " + i, "00000000", "City " + i,
							"State " + i, "Country " + i, geo.latitude, geo.longitude),
					new ContactsModel(i + "8000000", "0800000" + i, "email" + i + "@agendalize.com",
							"http://site" + i + ".com", null),
					i, i, "http://www.elrst.com/wp-content/uploads/2012/06/A-forest-in-Assam-India.jpg",
					i, new ArrayList<OperationModel>(), new ArrayList<ServiceModel>()));
		}
		return stores;
	}

}
