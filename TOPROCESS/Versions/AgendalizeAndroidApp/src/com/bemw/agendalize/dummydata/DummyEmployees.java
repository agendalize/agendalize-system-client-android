package com.bemw.agendalize.dummydata;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.model.EmployeeModel;
import com.bemw.agendalize.model.ScheduleStatus;
import com.bemw.agendalize.model.SchedulesForEmployeeModel;

public class DummyEmployees {

	public static List<EmployeeModel> getDummyEmployees(int num) {
		ArrayList<EmployeeModel> employees = new ArrayList<EmployeeModel>();
		for (int i=0; i<num; i++) {
			EmployeeModel employee = new EmployeeModel();
			employee.setLogin("Employee " + i);
			employee.setName("Name " + i);
			employee.setLastName("LastName " + i);
			employee.setCellPhoneNumber("08001010" + i);
			employees.add(employee);
		}
		return employees;
	}
	
	public static List<SchedulesForEmployeeModel> getSchedulesForEmployees(int numOfEmployees) {
		List<SchedulesForEmployeeModel> schedules = new ArrayList<SchedulesForEmployeeModel>();
		for (int i=0; i<numOfEmployees; i++) {
			SchedulesForEmployeeModel schedule = new SchedulesForEmployeeModel();
			// this simulates a employee schedule
			for (int j=0; j<AppConstants.SCHEDULE_DURATION_MIN*(60/AppConstants.SCHEDULE_INTERVALS_MIN)/60; j++) {
				Random randomGenerator = new Random();
				if (randomGenerator.nextInt(100)%3 == 0)
					schedule.getStatuses().set(j, ScheduleStatus.CHOSED);
				else if (randomGenerator.nextInt(100)%2 == 0)
					schedule.getStatuses().set(j, ScheduleStatus.LOCKED);
				else
					schedule.getStatuses().set(j, ScheduleStatus.OPEN);
			}
			schedules.add(schedule);
		}
		return schedules;
	}
	
}
