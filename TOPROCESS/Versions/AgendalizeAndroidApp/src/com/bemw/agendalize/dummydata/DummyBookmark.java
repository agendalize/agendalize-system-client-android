package com.bemw.agendalize.dummydata;

import java.util.ArrayList;
import java.util.List;

import com.bemw.agendalize.model.AddressModel;
import com.bemw.agendalize.model.ContactsModel;
import com.bemw.agendalize.model.OperationModel;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;

public class DummyBookmark {
	
	public static List<StoreModel> getDummyBookmark() {
		ArrayList<StoreModel> stores = new ArrayList<StoreModel>();
		stores.add(new StoreModel(
				1, "Dummy Store 1", "Dummy Company 1", "loren ipson 1", "000000000",
				new AddressModel(
						"Address A", "001", "Complement A", "00000000", "City A",
						"State A", "Country A", -21.513412, -46.123761),
				new ContactsModel("08000000", "18000000", "emaila@agendalize.com",
						"http://siteA.com", null),
				1, 1, "http://www.elrst.com/wp-content/uploads/2012/06/A-forest-in-Assam-India.jpg",
				1, new ArrayList<OperationModel>(), new ArrayList<ServiceModel>()));
		stores.add(new StoreModel(
				2, "Dummy Store 2", "Dummy Company 2", "loren ipson 2", "000000000",
				new AddressModel(
						"Address B", "002", "Complement B", "00000000", "City B",
						"State B", "Country B", -11.513412, -36.123761),
				new ContactsModel("18000000", "28000000", "emailb@agendalize.com",
						"http://siteB.com", null),
				2, 2, "http://www.elrst.com/wp-content/uploads/2012/06/A-forest-in-Assam-India.jpg",
				2, new ArrayList<OperationModel>(), new ArrayList<ServiceModel>()));
		stores.add(new StoreModel(
				3, "Dummy Store 3", "Dummy Company 3", "loren ipson 3", "000000000",
				new AddressModel(
						"Address C", "003", "Complement C", "00000000", "City C",
						"State C", "Country C", -29.513412, -6.123761),
				new ContactsModel("28000000", "38000000", "emailc@agendalize.com",
						"http://siteC.com", null),
				3, 3, "http://www.elrst.com/wp-content/uploads/2012/06/A-forest-in-Assam-India.jpg",
				3, new ArrayList<OperationModel>(), new ArrayList<ServiceModel>()));
		stores.add(new StoreModel(
				4, "Dummy Store 4", "Dummy Company 4", "loren ipson 4", "000000000",
				new AddressModel(
						"Address D", "004", "Complement D", "00000000", "City D",
						"State D", "Country D", -49.513412, -66.123761),
				new ContactsModel("38000000", "48000000", "emaild@agendalize.com",
						"http://siteD.com", null),
				4, 4, "http://www.elrst.com/wp-content/uploads/2012/06/A-forest-in-Assam-India.jpg",
				4, new ArrayList<OperationModel>(), new ArrayList<ServiceModel>()));
		return stores;
	}

}
