package com.bemw.agendalize.dummydata;

import java.util.ArrayList;
import java.util.List;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.model.EmployeesPerServiceModel;

public class DummyEmployeesPerService {
	
	public static List<EmployeesPerServiceModel> getEmployeesPerServiceList() {
		ArrayList<EmployeesPerServiceModel> employeesperservices = new ArrayList<EmployeesPerServiceModel>();
		//for (int i=0; i<AppConstants.SIZE_LIST; i++) {
		for (int i=0; i<3; i++) {
			employeesperservices.add(new EmployeesPerServiceModel(
					i, "Service " + i, DummyEmployees.getDummyEmployees(AppConstants.SIZE_LIST+1-i)));
		}
		return employeesperservices;
	}

}
