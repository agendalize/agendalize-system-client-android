package com.bemw.agendalize.dummydata;

import java.util.Arrays;

import com.bemw.agendalize.model.AlbumModel;

public class DummyAlbum {
	
	public static AlbumModel getDummyAlbum() {
		return new AlbumModel(
			Arrays.asList(
					"https://38.media.tumblr.com/avatar_1e640e94ba06_128.png",
					"https://38.media.tumblr.com/avatar_1e640e94ba06_128.png",
					"https://38.media.tumblr.com/avatar_1e640e94ba06_128.png",
					"https://38.media.tumblr.com/avatar_1e640e94ba06_128.png",
					"https://38.media.tumblr.com/avatar_1e640e94ba06_128.png",
					"https://38.media.tumblr.com/avatar_1e640e94ba06_128.png"
			)
		);
	}

}
