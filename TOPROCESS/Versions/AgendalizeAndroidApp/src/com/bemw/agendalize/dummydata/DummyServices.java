package com.bemw.agendalize.dummydata;

import java.util.ArrayList;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.ServiceModel;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class DummyServices {
	
	public static List<ServiceModel> getServiceList() {
		ArrayList<ServiceModel> services = new ArrayList<ServiceModel>();
		services.add(new ServiceModel(1, "Corte de Cabelo", "Descrição 1", 1, "R$ 20,00", 30));
		services.add(new ServiceModel(2, "Depilação", "Descrição 2", 2, "R$ 40,00", 40));
		services.add(new ServiceModel(3, "Unhas", "Descrição 3", 3, "R$ 60,00", 50));
		services.add(new ServiceModel(4, "Peeling", "Descrição 4", 4, "R$ 80,00", 60));
		services.add(new ServiceModel(5, "Massagem", "Descrição 5", 5, "R$ 100,00", 70));
		services.add(new ServiceModel(6, "Spa", "Descrição 6", 6, "R$ 120,00", 80));
		return services;
	}
	
	public static Drawable getServicePhotoByType(Context context, long type) {
		switch ((int)type) {
			/*case 1: return context.getResources().getDrawable(R.drawable.depilacao);
			case 2: return context.getResources().getDrawable(R.drawable.cabelo);
			case 3: return context.getResources().getDrawable(R.drawable.pele);
			case 4: return context.getResources().getDrawable(R.drawable.unha);
			case 5: return context.getResources().getDrawable(R.drawable.maquiagem);
			case 6: return context.getResources().getDrawable(R.drawable.spa);
			case 7: return context.getResources().getDrawable(R.drawable.massagem);*/
			default: return context.getResources().getDrawable(R.drawable.noimage);
		}
	};
	
	

}
