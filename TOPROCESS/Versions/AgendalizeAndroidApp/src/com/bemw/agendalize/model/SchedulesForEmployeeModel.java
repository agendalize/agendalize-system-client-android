package com.bemw.agendalize.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bemw.agendalize.AppConstants;

public class SchedulesForEmployeeModel {
	
	// a timestamp defining the given moment
	private Calendar tm;
	// the employee's login
	private String employeeLogin;
	// a list of status of the employee in the given moment
	private List<ScheduleStatus> statuses;
	
	public SchedulesForEmployeeModel() {
		tm = Calendar.getInstance();
		employeeLogin = null;
		statuses = new ArrayList<ScheduleStatus>();
		for (int i=0; i<AppConstants.SCHEDULE_DURATION_MIN*(60/AppConstants.SCHEDULE_INTERVALS_MIN)/60; i++) {
			statuses.add(ScheduleStatus.OPEN);
		}
	}
	public SchedulesForEmployeeModel(Calendar tm, String employeeLogin, List<ScheduleStatus> statuses) {
		this();
		this.tm = tm;
		this.employeeLogin = employeeLogin;
		this.statuses = statuses;
	}
	public SchedulesForEmployeeModel(JSONObject obj) {
		this();
		try { tm.setTime(new Date(obj.getLong("tm"))); } catch (Exception e) {}
		try { employeeLogin = obj.getString("employeeLogin"); } catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("statuses");
			for (int i=0; i<arr.length(); i++) {
				statuses.add(ScheduleStatus.values()[arr.getInt(i)]);
			}
		} catch (Exception e) {}
	}
	
	public Calendar getTm() {
		return tm;
	}
	public void setTm(Calendar tm) {
		this.tm = tm;
	}
	public String getEmployeeLogin() {
		return employeeLogin;
	}
	public void setEmployeeLogin(String employeeLogin) {
		this.employeeLogin = employeeLogin;
	}
	public List<ScheduleStatus> getStatuses() {
		return statuses;
	}
	public void setStatuses(List<ScheduleStatus> statuses) {
		this.statuses = statuses;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("tm", tm.getTimeInMillis()); } catch (Exception e) {}
		try { obj.put("employeeLogin", employeeLogin); } catch(Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<statuses.size(); i++) {
				arr.put(statuses.get(i).ordinal());
			}
			obj.put("statuses", arr);
		} catch (Exception e) {}
		return obj;
	}

}
