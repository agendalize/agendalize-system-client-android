package com.bemw.agendalize.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class AlbumModel {
	
	private List<String> photoUrls;
	
	public AlbumModel() {
		photoUrls = new ArrayList<String>();
	}
	public AlbumModel(List<String> photoUrls) {
		this();
		this.photoUrls = photoUrls;
	}
	public AlbumModel(JSONObject obj) {
		this();
		try {
			JSONArray arr = obj.getJSONArray("photoUrls");
			for (int i=0; i<arr.length(); i++) {
				photoUrls.add(arr.getString(i));
			}
		} catch (Exception e) {}
	}
	
	public List<String> getPhotoUrls() {
		return photoUrls;
	}
	public void setPhotoUrls(List<String> photoUrls) {
		this.photoUrls = photoUrls;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<photoUrls.size(); i++) {
				arr.put(photoUrls.get(i));
			}
			obj.put("photoUrls", arr);
		} catch (Exception e) {}
		return obj;
	}

}
