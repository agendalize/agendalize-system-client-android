package com.bemw.agendalize.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class EmployeesPerServiceModel {
	
	private long cod;
	private String shortName;
	private List<EmployeeModel> employees;
	
	public EmployeesPerServiceModel() {
		cod = -1;
		shortName = "shortName";
		employees = new ArrayList<EmployeeModel>();
	}
	public EmployeesPerServiceModel(long cod, String shortName, List<EmployeeModel> employees) {
		this();
		this.cod = cod;
		this.shortName = shortName;
		this.employees = employees;
	}
	public EmployeesPerServiceModel(JSONObject obj) {
		this();
		try { cod = obj.getLong("cod"); } catch (Exception e) {}
		try { shortName = obj.getString("shortName"); } catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("employees");
			for (int i=0; i<arr.length(); i++) {
				employees.add(new EmployeeModel(arr.getJSONObject(i)));
			}
		} catch (Exception e) {}
	}
	
	public long getCod() {
		return cod;
	}
	public void setCod(long cod) {
		this.cod = cod;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public List<EmployeeModel> getEmployees() {
		return employees;
	}
	public void setEmployees(List<EmployeeModel> employees) {
		this.employees = employees;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<employees.size(); i++) {
				arr.put(employees.get(i).toJson());
			}
			obj.put("employees", arr);
		} catch (Exception e) {}
		return obj;
	}
	
}
