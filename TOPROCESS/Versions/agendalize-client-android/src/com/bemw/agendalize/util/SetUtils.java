package com.bemw.agendalize.util;

import java.util.ArrayList;
import java.util.Iterator;

public class SetUtils {
	
	public static ArrayList<String> copyIterator(Iterator<String> iter) {
		ArrayList<String> copy = new ArrayList<String>();
	    while (iter.hasNext())
	        copy.add(iter.next());
	    return copy;
	}

}
