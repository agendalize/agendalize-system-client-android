package com.bemw.agendalize.model;

import org.json.JSONObject;

public class ServiceModel {
	
	private long cod;
	private String description;
	private long codTypeService;
	private String shortName;
	
	public ServiceModel() {
		cod = -1;
		description = "description";
		codTypeService = -1;
		shortName = "shortName";
	}
	public ServiceModel(long cod, String description, long codTypeService, String shortName) {
		this();
		this.cod = cod;
		this.description = description;
		this.codTypeService = codTypeService;
		this.shortName = shortName;
	}
	public ServiceModel(JSONObject obj) {
		this();
		try { cod = obj.getLong("cod"); } catch (Exception e) {}
		try { description = obj.getString("description"); } catch (Exception e) {}
		try { codTypeService = obj.getLong("codTypeService"); } catch (Exception e) {}
		try { shortName = obj.getString("shortName"); } catch (Exception e) {}
	}
	
	public long getCod() {
		return cod;
	}
	public void setCod(long cod) {
		this.cod = cod;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getCodTypeService() {
		return codTypeService;
	}
	public void setCodTypeService(long codTypeService) {
		this.codTypeService = codTypeService;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("cod", cod); } catch (Exception e) {}
		try { obj.put("description", description); } catch (Exception e) {}
		try { obj.put("codTypeService", codTypeService); } catch (Exception e) {}
		try { obj.put("shortName", shortName); } catch (Exception e) {}
		return obj;
	}	

}
