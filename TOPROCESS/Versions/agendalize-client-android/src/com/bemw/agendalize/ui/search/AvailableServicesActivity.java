package com.bemw.agendalize.ui.search;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.util.ImageUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @author Marcelo Tomio Hama
 * Lists all available services
 * This class is 100% done!
 */
public class AvailableServicesActivity extends ActionBarActivity {
	
	public static List<ServiceModel> services = null;
	
	private AutoCompleteTextView searcInput = null;
	private ImageButton clearEdit = null;
	private ServicesAdapter servicesAdapter = null;
	private ListView serviceList = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_availableservices);
		
		clearEdit = (ImageButton) findViewById(R.id.activity_availableservices_cleartxtbtn);
		searcInput = (AutoCompleteTextView) findViewById(R.id.activity_availableservices_autocompleteedit);
		serviceList = (ListView) findViewById(R.id.activity_availableservices_listview);
		((Button) findViewById(R.id.activity_availableservices_finishbtn)).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				int[] selecteds = servicesAdapter.getSelected();
				JSONArray arr = new JSONArray();
				for (int i=3; i>=0; i--) {
					if (selecteds[i] != -1) arr.put(services.get(selecteds[i]).toJson());
				}
				Intent returnIntent = new Intent();
				returnIntent.putExtra("selecteds", arr.toString());
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
		
		getSupportActionBar().setTitle(getResources().getString(R.string.label_services));
		getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_paste_hd));
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		servicesAdapter = new ServicesAdapter(AvailableServicesActivity.this, services);
		serviceList.setAdapter(servicesAdapter);
		serviceList.setOnItemClickListener(new OnItemClickListener() {
			@Override
		    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
				servicesAdapter.setSelected(position);
			}
		});
		serviceList.setFastScrollEnabled(true);
		
		setSearchEditFeatures();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void setSearchEditFeatures() {
		searcInput.requestFocus();
		if (!searcInput.getText().toString().equals("")) clearEdit.setVisibility(View.VISIBLE);
		else clearEdit.setVisibility(View.INVISIBLE);
		clearEdit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searcInput.setText("");
				clearEdit.setVisibility(View.INVISIBLE);
				searcInput.requestFocus();
			}
		});
		searcInput.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {
				if (searcInput.getText().length() >= 1) {
					clearEdit.setVisibility(View.VISIBLE);
					// set filter service list
					List<ServiceModel> filtered = new ArrayList<ServiceModel>();
					for (int i=0; i<services.size(); i++) {
						if (services.get(i).getShortName().contains(searcInput.getText())) filtered.add(services.get(i));
					}
					servicesAdapter = new ServicesAdapter(AvailableServicesActivity.this, filtered);
					serviceList.setAdapter(servicesAdapter);
					serviceList.setOnItemClickListener(new OnItemClickListener() {
						@Override
					    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
							servicesAdapter.setSelected(position);
						}
					});
					serviceList.setFastScrollEnabled(true);
				} else if (searcInput.getText().length() == 0) {
					clearEdit.setVisibility(View.INVISIBLE);
					// set default service list
					servicesAdapter = new ServicesAdapter(AvailableServicesActivity.this, services);
					serviceList.setAdapter(servicesAdapter);
					serviceList.setOnItemClickListener(new OnItemClickListener() {
						@Override
					    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
							servicesAdapter.setSelected(position);
						}
					});
					serviceList.setFastScrollEnabled(true);
				}
			}
		});
		searcInput.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {}
		});
		searcInput.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					return true;
			    } else if (keyCode == 66) {}
				return false;
			}
		});
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class ServicesAdapter extends BaseAdapter {
		private Context context;
		private List<ServiceModel> services;
		private int selected1;
		private int selected2;
		private int selected3;
		private int selected4;
		public ServicesAdapter(Context context, List<ServiceModel> services) {
			this.context = context;
			this.services = services;
			this.selected1 = -1;
			this.selected2 = -1;
			this.selected3 = -1;
			this.selected4 = -1;
		}
		public int[] getSelected() {
			int[] s = {selected1, selected2, selected3, selected4};
			return s;
		}
		public void setSelected(int selected) {
			if (selected4 == selected) {
				selected4 = -1;
			} else if (selected3 == selected) {
				selected3 = selected4;
				selected4 = -1;
			} else if (selected2 == selected) {
				selected2 = selected3;
				selected3 = selected4;
				selected4 = -1;
			} else if (selected1 == selected) {
				selected1 = selected2;
				selected2 = selected3;
				selected3 = selected4;
				selected4 = -1;
			} else {
				selected4 = selected3;
				selected3 = selected2;
				selected2 = selected1;
				selected1 = selected;
			}
			notifyDataSetChanged();
		}
		@Override
		public int getCount() {
			return services.size();
		}
		@Override
		public Object getItem(int position) {
			return services.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@SuppressWarnings("deprecation") @SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// predefines the view as an inflated layout
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_serviceitem, null);
			}
			ImageView photo = ((ImageView) convertView.findViewById(R.id.row_serviceitem_photo));
			TextView title = ((TextView) convertView.findViewById(R.id.row_serviceitem_name));
			TextView description = ((TextView) convertView.findViewById(R.id.row_serviceitem_description));
			CheckBox isselected = ((CheckBox) convertView.findViewById(R.id.row_serviceitem_isselected));
			
			title.setText(services.get(position).getShortName());
			description.setText(services.get(position).getDescription());
			if (selected1 == position || selected2 == position || selected3 == position || selected4 == position)
				isselected.setChecked(true);
			else
				isselected.setChecked(false);
			photo.setBackgroundDrawable(new BitmapDrawable(getResources(), ImageUtils.getCroppedBitmap(
				((BitmapDrawable)AppConstants.getServicePhotoByType(context, services.get(position).getCodTypeService())).getBitmap(), false
			)));
			return convertView;
		}
	}
	
}
