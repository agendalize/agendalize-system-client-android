package com.bemw.agendalize.ui.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.EmployeeModel;
import com.bemw.agendalize.model.EmployeesPerServiceModel;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.util.ImageUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class EmployeeAndServiceFragment extends Fragment {

	private StoreModel store = null;
	private List<EmployeeModel> employees = null;
	private List<ServiceModel> services = null;
	private ListView storeListContext = null;
	private EmployeeAndServiceAdapter adapter = null;
	private List<Object> EmpServList = null;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_employeeandservice, container, false);
        store = ((StoreDetailActivity)getActivity()).getStore();
        employees = new ArrayList<EmployeeModel>();
        services = new ArrayList<ServiceModel>();
        EmpServList = new ArrayList<Object>();
        
        storeListContext = (ListView) rootView.findViewById(R.id.frag_employeeandservice_listview);
        
        if (adapter == null) {
			adapter = new EmployeeAndServiceAdapter();
			storeListContext.setAdapter(adapter);
		}
		
        return rootView;
    }
	
	@Override
	public void onResume() {
		super.onResume();

	}
	
	public void updateServiceEmployeer(List<EmployeesPerServiceModel> employeePerServices) {
		services.clear();
		employees.clear();
		HashMap<String, Object> dict = new HashMap<String, Object>();
		for (int i=0; i<employeePerServices.size(); i++) {
			ServiceModel s = new ServiceModel();
			s.setCod(employeePerServices.get(i).getCod());
			s.setShortName(employeePerServices.get(i).getShortName());
			services.add(s);
			EmpServList.add(s);
			EmpServList.add("NO_PREF");
			for (int j=0; j<employeePerServices.get(i).getEmployees().size(); j++) {
				EmployeeModel e = new EmployeeModel();
				e.setName(employeePerServices.get(i).getEmployees().get(j).getName());
				e.setLastName(employeePerServices.get(i).getEmployees().get(j).getLastName());
				e.setImageUrl(employeePerServices.get(i).getEmployees().get(j).getImageUrl());
				EmpServList.add(e);
				// TODO: use bellow
				//if (!dict.containsKey(employeePerServices.get(i).getEmployees().get(j).getLogin())) {
				if (!dict.containsKey(employeePerServices.get(i).getEmployees().get(j).getName()+employeePerServices.get(i).getEmployees().get(j).getLastName())) {
					dict.put(employeePerServices.get(i).getEmployees().get(j).getName()+employeePerServices.get(i).getEmployees().get(j).getLastName(), null);
					employees.add(e);
				}
			}
		}
		adapter.notifyDataSetInvalidated();
		adapter.notifyDataSetChanged();
	}
	
	private class EmployeeAndServiceAdapter extends BaseAdapter {
		public EmployeeAndServiceAdapter() {}
		@Override
		public int getCount() {
			return EmpServList.size();
		}
		@Override
		public Object getItem(int position) {
			return EmpServList.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@SuppressWarnings("deprecation") @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			/*if (EmpServList.get(position) instanceof ServiceModel) {
				convertView = inflater.inflate(R.layout.row_servicelabel, null);
				ServiceModel model = (ServiceModel)EmpServList.get(position);
				((TextView) convertView.findViewById(R.id.row_servicelabel_name)).setText(
						model.getShortName() + " (" + model.getTimeMinutes() + " min)"
				);
				((ProgressBar) convertView.findViewById(R.id.row_servicelabel_progress)).setVisibility(View.GONE);
			} else if (EmpServList.get(position) instanceof EmployeeModel) {
				convertView = inflater.inflate(R.layout.row_employeedata, null);
				EmployeeModel model = (EmployeeModel)EmpServList.get(position);
				Drawable photo = ImageUtils.loadCachedImage(getActivity(), model.getImageUrl());
				if (photo == null) {
					Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.noimage), true);
					((ImageView) convertView.findViewById(R.id.row_employeedata_photo)).setBackgroundDrawable(new BitmapDrawable(getResources(), bmp));
				} else {
					photo = ImageUtils.resize(photo, 48, 48);
					Bitmap bmp = ((BitmapDrawable)photo).getBitmap();
					bmp = ImageUtils.getCroppedBitmap(bmp, true);
					((ImageView) convertView.findViewById(R.id.row_employeedata_photo)).setBackgroundDrawable(new BitmapDrawable(getResources(), bmp));
				}
				((TextView) convertView.findViewById(R.id.row_employeedata_name)).setText(
						model.getName() + " " + model.getLastName()
				);
				((TextView) convertView.findViewById(R.id.row_employeedata_phone)).setText(
						model.getWorkPhoneNumber().length() > 0 ? model.getPhoneNumber() : (
								model.getCellPhoneNumber().length() > 0 ? model.getPhoneNumber() : (
										model.getPhoneNumber().length() > 0 ? model.getPhoneNumber() : ""
								)
						)
				);
			} else if (EmpServList.get(position).equals("NO_PREF")) {
				convertView = inflater.inflate(R.layout.row_employeedata, null);
				((TextView) convertView.findViewById(R.id.row_employeedata_name)).setText(
						getResources().getString(R.string.label_nopreference)
				);
			}*/
			
			return convertView;
		}
	}
	
}
