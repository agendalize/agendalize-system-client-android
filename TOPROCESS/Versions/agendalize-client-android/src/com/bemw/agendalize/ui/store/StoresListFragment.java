package com.bemw.agendalize.ui.store;

import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.util.ImageUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class StoresListFragment extends Fragment {
	
	private ListView storeList;
	private StoreListAdapter storeListAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storelist, container, false);
		
		storeList = (ListView) rootView.findViewById(R.id.frag_storelist_listview);
		storeListAdapter = new StoreListAdapter(getActivity(), ((SetOfStoresActivity)getActivity()).getStores());
		storeList.setAdapter(storeListAdapter);
		storeList.setOnItemClickListener(new OnItemClickListener() {
			@Override
		    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
				Intent storDetail = new Intent(getActivity(), StoreDetailActivity.class);
				storDetail.putExtra("storeData", ((SetOfStoresActivity)getActivity()).getStores().get(position).toJson().toString());
				// TODO: bellow line is just for test... remove it later
				storDetail.putExtra("serviceIDs", "[13]");
				startActivity(storDetail);
			}
		});
		storeList.setFastScrollEnabled(true);
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	private class StoreListAdapter extends BaseAdapter {
		private List<StoreModel> stores;
		private Context context;
		public StoreListAdapter(Context context, List<StoreModel> stores) {
			this.stores = stores;
			this.context = context;
		}
		@Override
		public int getCount() {
			return stores.size();
		}
		@Override
		public Object getItem(int position) {
			return stores.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@SuppressLint("InflateParams") @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// predefines the view as an inflated layout
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_storeitem, null);
			}
			ImageView photo = ((ImageView) convertView.findViewById(R.id.row_storeitem_photo));
			ProgressBar progress = ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photo_progress));
			TextView name = ((TextView) convertView.findViewById(R.id.row_storeitem_name));
			RatingBar rating = ((RatingBar)convertView.findViewById(R.id.row_storeitem_rating));
			TextView info1 = ((TextView) convertView.findViewById(R.id.row_storeitem_info1));
			TextView info2 = ((TextView) convertView.findViewById(R.id.row_storeitem_info2));
			TextView address1 = ((TextView) convertView.findViewById(R.id.row_storeitem_address1));
			TextView address2 = ((TextView) convertView.findViewById(R.id.row_storeitem_address2));
			name.setText(stores.get(position).getName());
			rating.setRating(stores.get(position).getEvaluation()/2);
			info1.setText(stores.get(position).getNrEmployees() + " " + context.getResources().getString(R.string.label_employees));
			info2.setText(stores.get(position).getNrServices() + " " + context.getResources().getString(R.string.label_services));
			address1.setText(stores.get(position).getAddressInfo().getCity() + ", " + stores.get(position).getAddressInfo().getState());
			address2.setText(stores.get(position).getAddressInfo().getAddress() + " " + stores.get(position).getAddressInfo().getAddressNumber());
			new PhotoLoader(context, photo, progress, stores.get(position)).execute();
			return convertView;
		}
	}
	
	@SuppressWarnings("deprecation")
	private class PhotoLoader extends AsyncTask<Void, Void, Void> {
		private Context context;
		private StoreModel store;
		private ImageView photo;
		private ProgressBar progress;
		private Drawable image;
		public PhotoLoader(Context context, ImageView photo, ProgressBar progress, StoreModel store) {
			this.context = context;
			this.photo = photo;
			this.progress = progress;
			this.store = store;
		}
		@Override
		protected Void doInBackground(Void... params) {
			/*image = ImageUtils.loadCachedImage(context, "http://wallpaperbook.net/wp-content/uploads/2014/10/Beautiful-Girl1-1440x1280.jpg");*/
			image = ImageUtils.loadCachedImage(context, store.getUrlPhoto());
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progress.setVisibility(View.GONE);
			photo.setVisibility(View.VISIBLE);
			if (image != null) photo.setBackgroundDrawable(image);
			else photo.setBackgroundResource(R.drawable.noimage);
		}
	}

}
