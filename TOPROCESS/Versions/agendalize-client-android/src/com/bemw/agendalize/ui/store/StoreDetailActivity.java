package com.bemw.agendalize.ui.store;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.EmployeesPerServiceModel;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.net.IRESTClient;
import com.bemw.agendalize.net.RESTClient;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class StoreDetailActivity extends ActionBarActivity implements IRESTClient {
	
    private static final int NUM_PAGES = 3;
    private StoreModel store;
    private Button leftBtn = null;
	private Button rightBtn = null;
	private ViewPager mPager = null;
    private PagerAdapter mPagerAdapter = null;
    
	private long[] servID = null;
	private List<EmployeesPerServiceModel> employeePerServices = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storedetail);

        try { // populating the store list, this activity must receive something!
			store = new StoreModel(new JSONObject(getIntent().getStringExtra("storeData")));
			getSupportActionBar().setTitle(store.getName());
			getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.noimage)); // TODO: place the store photo
			try {
				JSONArray serviceIDs = new JSONArray(getIntent().getStringExtra("serviceIDs"));
				if (serviceIDs.length() == 0) {
					//new EmployeeLoader().execute();
					new ServiceLoader().execute();
				} else  {
					servID = new long[serviceIDs.length()];
					for (int i=0; i<serviceIDs.length(); i++) {
						servID[i] = serviceIDs.getLong(i);
					}
					new ServiceEmployeerLoader().execute();
				}
			} catch (Exception e) {
				//new EmployeeLoader().execute();
				new ServiceLoader().execute();
			}
		} catch (Exception e) {
			Log.w(AppConstants.TAG, e.getLocalizedMessage());
			finish();
		}
        
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.activity_storedetail_pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        
        // setup buttons
        leftBtn = (Button) findViewById(R.id.activity_storedetail_leftbtn);
    	rightBtn = (Button) findViewById(R.id.activity_storedetail_rightbtn);
		leftBtn.setText(getResources().getString(R.string.label_storelist));
		rightBtn.setText(getResources().getString(R.string.label_employee));
		leftBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mPager.setCurrentItem(mPager.getCurrentItem() - 1);
				if (mPager.getCurrentItem() == 0) {
					leftBtn.setText(getResources().getString(R.string.label_storelist));
					rightBtn.setText(getResources().getString(R.string.label_employee));
				} else if (mPager.getCurrentItem() == 1) {
					leftBtn.setVisibility(View.VISIBLE);
					leftBtn.setText(getResources().getString(R.string.label_storeinfo));
					rightBtn.setText(getResources().getString(R.string.label_schedule));
				}
				rightBtn.setVisibility(View.VISIBLE);
			}
		});
    	rightBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mPager.setCurrentItem(mPager.getCurrentItem() + 1);
				if (mPager.getCurrentItem() == 2) {
					rightBtn.setText(getResources().getString(R.string.label_finish));
					leftBtn.setText(getResources().getString(R.string.label_employee));
				} else if (mPager.getCurrentItem() == 1) {
					rightBtn.setVisibility(View.VISIBLE);
					leftBtn.setText(getResources().getString(R.string.label_storeinfo));
					rightBtn.setText(getResources().getString(R.string.label_schedule));
				}
				leftBtn.setVisibility(View.VISIBLE);
			}
		});
    }
    
    @Override
    public void onBackPressed() {
    	// If the user is currently looking at the first step, allow the system
    	// to handle the back button. This calls finish() on this activity and
    	// pops the back stack, otherwise, select the previous step.
        if (mPager.getCurrentItem() == 0) super.onBackPressed();
        else mPager.setCurrentItem(mPager.getCurrentItem() - 1);
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
    
	//===================================================================================
	// Inner classes
	//===================================================================================
	
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            //do nothing here! no call to super.restoreState(arg0, arg1);
        }
        @Override
        public Fragment getItem(int position) {
        	switch (position) {
	        	case 0:
	        		return new StoreInfoFragment();
	        	case 1:
	        		return new EmployeeAndServiceFragment();
	        	/*case 2:
	        		return new EmployeeAndHourFragment();*/
	        	default: return new StoreInfoFragment();
        	}
        }
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
    
    public StoreModel getStore() {
		return store;
	}
	
	public List<EmployeesPerServiceModel> getEmployeesPerServices() {
		return employeePerServices;
	}
	
	private class ServiceLoader extends AsyncTask<Void, Void, Void> {
		private List<ServiceModel> services = null;
		@Override
		protected Void doInBackground(Void... params) {
			/*services = new RESTClient().GetServicesByStore(store.getId());*/
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			servID = new long[services.size()];
			for (int i=0; i<services.size(); i++) {
				servID[i] = services.get(i).getCod();
			}
			new ServiceEmployeerLoader().execute();
			super.onPostExecute(result);
		}
	}
	
	private class ServiceEmployeerLoader extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			/*employeePerServices = new RESTClient().GetEmployeesPerServiceList(store.getId(), servID);
			
			HashMap<String, Object> dict = new HashMap<String, Object>();
			for (int i=0; i<employeePerServices.size(); i++) {
				for (int j=0; j<employeePerServices.get(i).getEmployees().size(); j++) {
					// TODO: use bellow
					//if (!dict.containsKey(employeePerServices.get(i).getEmployees().get(j).getLogin())) {
					if (!dict.containsKey(employeePerServices.get(i).getEmployees().get(j).getName()+employeePerServices.get(i).getEmployees().get(j).getLastName())) {
						dict.put(employeePerServices.get(i).getEmployees().get(j).getName()+employeePerServices.get(i).getEmployees().get(j).getLastName(), null);
						ImageUtils.loadCachedImage(StoreDetailActivity.this, employeePerServices.get(i).getEmployees().get(j).getImageUrl());
					}
				}
			}*/
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			/*StoreInfoFragment frag1 = (StoreInfoFragment)mPager.getAdapter().instantiateItem(mPager, 0);
			frag1.updateServiceEmployeer(employeePerServices);
			EmployeeAndServiceFragment frag2 = (EmployeeAndServiceFragment)mPager.getAdapter().instantiateItem(mPager, 1);
			frag2.updateServiceEmployeer(employeePerServices);*/
			//EmployeeAndHourFragment frag3 = (EmployeeAndHourFragment)mPager.getAdapter().instantiateItem(mPager, mPager.getCurrentItem());
			//frag3.updateServiceEmployeer(employeePerServices);
			super.onPostExecute(result);
		}
	}

	@Override
	public void onConnectionSuccess(String method, Object response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEmptyServerResponse(String method) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionTimeout(String method) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFail(String method, Exception e) {
		// TODO Auto-generated method stub
		
	}

}
