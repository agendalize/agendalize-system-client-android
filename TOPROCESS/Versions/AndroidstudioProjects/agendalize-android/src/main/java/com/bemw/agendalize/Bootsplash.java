package com.bemw.agendalize;

import com.bemw.agendalize.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * @author Marcelo Tomio Hama
 * Shows the application bootsplash
 */
public class Bootsplash extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bootsplash);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		new Thread() {
			@Override
			public void run() {
				try {
					sleep(AppConstants.BOOTSPLASH_TIMER);
				} catch (InterruptedException e) {
					Log.w(AppConstants.TAG, e.getLocalizedMessage());
				} finally {
					// creates a new application instance
					Intent new_app = new Intent(Bootsplash.this, HomeActivity.class);
					new_app.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(new_app);
					finish();
				}
			}
		}.start();
	}

}
