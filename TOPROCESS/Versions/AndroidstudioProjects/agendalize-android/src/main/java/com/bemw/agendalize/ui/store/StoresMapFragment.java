package com.bemw.agendalize.ui.store;

import java.util.Locale;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class StoresMapFragment extends Fragment {
	
	private String languageInUse = null;
	private SupportMapFragment fragment = null;
	private GoogleMap map = null;
	private StoreMapListAdapter adapter = null;
	private ListView storeList = null;
	private RelativeLayout lyt = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storemap, container, false);
		
		lyt = (RelativeLayout) rootView.findViewById(R.id.frag_storemap_maplyt);
		storeList = (ListView) rootView.findViewById(R.id.frag_storemap_listview);
		storeList.setOnItemClickListener(new OnItemClickListener() {
			@Override
		    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
				/*Intent storDetail = new Intent(getActivity(), StoreDetailActivity.class);
				storDetail.putExtra("storeData", ((SetOfStoresActivity)getActivity()).getStores().get(position).toJson().toString());
				startActivity(storDetail);*/
			}
		});
		
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    FragmentManager fm = getChildFragmentManager();
	    fragment = (SupportMapFragment) fm.findFragmentById(R.id.frag_storemap_maplyt);
	    if (fragment == null) {
	        fragment = SupportMapFragment.newInstance();
	        fm.beginTransaction().replace(R.id.frag_storemap_maplyt, fragment).commit();
	    }
	}

	@Override
	public void onResume() {
	    super.onResume();
	    
	    if (map == null) {
	        map = fragment.getMap();
	        
	        try {
				MapsInitializer.initialize(getActivity());
		        map.setMyLocationEnabled(true);
		        LocationManager service = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		        Criteria criteria = new Criteria();
		        String provider = service.getBestProvider(criteria, false);
		        LatLng location = new LatLng(service.getLastKnownLocation(provider).getLatitude(), service.getLastKnownLocation(provider).getLongitude());
		        map.animateCamera(CameraUpdateFactory.newLatLngZoom(location , 11.0f));
		        
		        /*for (int i=0; i<((SetOfStoresActivity)getActivity()).getStores().size(); i++) {
		        	new StoreLocationLoader(((SetOfStoresActivity)getActivity()).getStores().get(i)).execute();
		        }*/
		        
		        map.setOnMarkerClickListener(new OnMarkerClickListener() {
					@Override
					public boolean onMarkerClick(Marker arg0) {
						try {
							if (arg0 == null) {
								// set margin
								int dpValue = 4;
								RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)lyt.getLayoutParams();
								params.setMargins(4, 4, 4, (int)(dpValue * getActivity().getResources().getDisplayMetrics().density));
								lyt.setLayoutParams(params);
								storeList.setVisibility(View.GONE);
								return false;
							}
							// set margin
							int dpValue = 148;
							RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)lyt.getLayoutParams();
							params.setMargins(4, 4, 4, (int)(dpValue * getActivity().getResources().getDisplayMetrics().density));
							lyt.setLayoutParams(params);
							// set item values
							final int id = Integer.valueOf(arg0.getId().replaceAll("\\D+",""));
							adapter = new StoreMapListAdapter(((SetOfStoresActivity)getActivity()).getStores().get(id));
							storeList.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							/*name.setText(((StoresActivity)getActivity()).getStores().get(id).getName());
							rating.setRating(4.5f);
							//info1.setText(stores.get(position).);
							//info2.setText(stores.get(position).);
							photo.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									// TODO: show photos of this provider
								}
							});*/
							storeList.setVisibility(View.VISIBLE);
							/*goToStoreBtn.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									Intent storDetail = new Intent(getActivity(), StoreDetailActivity.class);
									storDetail.putExtra("storeData", ((StoresActivity)getActivity()).getStores().get(id).toJson().toString());
									startActivity(storDetail);
								}
							});*/
						} catch (Exception e) {
							Log.w(AppConstants.TAG, e.getLocalizedMessage());
						}
						return false;
					}
				});
	        } catch (Exception e) {}
	    }
	}
	
	private class StoreLocationLoader extends AsyncTask<Void, Void, Void> {
		private double[] location;
		private StoreModel store;
		public StoreLocationLoader(StoreModel store) {
			this.store = store;
		}
		@Override
		protected Void doInBackground(Void... params) {
			/*location = GoogleGeoLocation.getLocationInfo(
					store.getAddress().getAddress() + " " +
					store.getAddress().getAddressNumber() + " " +
					store.getAddress().getCity() + " " +
					store.getAddress().getState() + " " +
					store.getAddress().getCountry());*/
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			map.addMarker(new MarkerOptions()
				.position(new LatLng(location[0], location[1]))
				.icon(BitmapDescriptorFactory.fromBitmap(
						BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher)))
				.title(store.getName())
				.snippet(
					store.getContactInfo().getPhoneNumberOne().length() > 0 ? store.getContactInfo().getPhoneNumberOne() : (
					store.getContactInfo().getPhoneNumberTwo().length() > 0 ? store.getContactInfo().getPhoneNumberTwo() : (
					store.getContactInfo().getEmail().length() > 0 ? store.getContactInfo().getEmail() : store.getAddressInfo().getAddress()
				)))
			);
		}
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class StoreMapListAdapter extends BaseAdapter {
		private StoreModel storeInView = null;
		private StoreMapListAdapter(StoreModel storeInView) {
			this.storeInView = storeInView;
		}
		@Override
		public int getCount() {
			return 1;
		}
		@Override
		public Object getItem(int position) {
			return storeInView;
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// predefines the view as an inflated layout
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_storeitem, null);
			}
			//ImageView photo = ((ImageView)convertView.findViewById(R.id.row_storeitem_photo));
			TextView name = ((TextView)convertView.findViewById(R.id.row_storeitem_name));
			RatingBar rating = ((RatingBar)convertView.findViewById(R.id.row_storeitem_rating));
			//TextView info1 = ((TextView)convertView.findViewById(R.id.row_storeitem_info1));
			//TextView info2 = ((TextView)convertView.findViewById(R.id.row_storeitem_info2));
			TextView address1 = ((TextView)convertView.findViewById(R.id.row_storeitem_address1));
			TextView address2 = ((TextView)convertView.findViewById(R.id.row_storeitem_address2));
			name.setText(storeInView.getName());
			rating.setRating(4.5f);
			//info1.setText(storeInView.);
			//info2.setText(storeInView.);
			address1.setText(storeInView.getAddressInfo().getCity() + ", " + storeInView.getAddressInfo().getState());
			address2.setText(storeInView.getAddressInfo().getAddress() + " " + storeInView.getAddressInfo().getAddressNumber());
			/*photo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO: show photos of this provider
				}
			});*/
			return convertView;
		}
	}

}
