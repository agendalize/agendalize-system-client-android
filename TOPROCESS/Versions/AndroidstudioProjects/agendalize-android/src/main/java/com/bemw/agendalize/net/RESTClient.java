package com.bemw.agendalize.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.model.EmployeesPerServiceModel;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;

public class RESTClient {

	private int status							= -1;
	private InputStream instream				= null;
	private DefaultHttpClient httpClient		= null;
	private HttpGet httpGet						= null;
	private HttpPost httpPost					= null;
	private HttpResponse response				= null;
	private HttpParams httpParameters 			= new BasicHttpParams();
	
	public RESTClient() {
		HttpConnectionParams.setConnectionTimeout(httpParameters, AppConstants.CONN_TIME_OUT);
		HttpConnectionParams.setSoTimeout(httpParameters, AppConstants.CONN_TIME_OUT);
		httpClient = new DefaultHttpClient(httpParameters);
	}
	
	public int getStatus() {
		return status;
	}
	
	public double[] getLocationInfo(final String address) {
		double LatLon[] = new double[2];
		try {
			int b;
	        httpPost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address.replaceAll(" ","%20") + "&sensor=false");
            response = httpClient.execute(httpPost);
            if (response != null) {
				status = response.getStatusLine().getStatusCode();
	            HttpEntity entity = response.getEntity();
	            StringBuilder stringBuilder = new StringBuilder();
	            InputStream stream = entity.getContent();
	            while ((b = stream.read()) != -1) {
	                stringBuilder.append((char) b);
	            }
	            JSONObject jsonObject = new JSONObject(stringBuilder.toString());
	        	LatLon[0] = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
	    			.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
	        	LatLon[1] = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
	                .getJSONObject("geometry").getJSONObject("location").getDouble("lng");
            }
		} catch (ConnectTimeoutException e) {
			status = HttpStatus.SC_REQUEST_TIMEOUT;
	    } catch (SocketTimeoutException e) {
	    	status = HttpStatus.SC_REQUEST_TIMEOUT;
		} catch (final UnsupportedEncodingException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final ClientProtocolException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final JSONException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
	    } catch (final IOException e) {
	    	status = HttpStatus.SC_NOT_FOUND;
		}
		disconnect();
		return LatLon;
	}

	public List<StoreModel> getStoresByContext(final JSONObject request) {
		List<StoreModel> stores = new ArrayList<StoreModel>();
		try {
			httpPost = new HttpPost(AppConstants.domain + "stores/filter");
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setEntity(new ByteArrayEntity(
					request.toString().getBytes("UTF8")
			));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String wsResponse = "";
					String inputLine = null;
					BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
					while ((inputLine = reader.readLine()) != null) {
						wsResponse += inputLine;
					}
					JSONArray arr = new JSONArray(wsResponse);
					for (int i=0; i<arr.length(); i++) {
						JSONObject obj = arr.getJSONObject(i);
						stores.add(new StoreModel(obj));
					}
				}
			}
		} catch (ConnectTimeoutException e) {
			status = HttpStatus.SC_REQUEST_TIMEOUT;
	    } catch (SocketTimeoutException e) {
	    	status = HttpStatus.SC_REQUEST_TIMEOUT;
		} catch (final UnsupportedEncodingException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final ClientProtocolException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final JSONException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
	    } catch (final IOException e) {
	    	status = HttpStatus.SC_NOT_FOUND;
		}
		disconnect();
		return stores;
	}
	
	public void sendStoreSugestion(final String name, final String Address, final String contact) {
		try {
			httpPost = new HttpPost(AppConstants.domain + "feedbacks/store-sugestions/create");
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setEntity(new ByteArrayEntity(
					new JSONObject()
					.put("name", name)
					.put("addressInfo", Address)
					.put("contactInfo", contact)
					.toString().getBytes("UTF8")
			));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
			}
		} catch (ConnectTimeoutException e) {
			status = HttpStatus.SC_REQUEST_TIMEOUT;
	    } catch (SocketTimeoutException e) {
	    	status = HttpStatus.SC_REQUEST_TIMEOUT;
		} catch (final UnsupportedEncodingException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final ClientProtocolException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final JSONException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
	    } catch (final IOException e) {
	    	status = HttpStatus.SC_NOT_FOUND;
		}
		disconnect();
	}
	
	public void sendFeedback(final String message) {
		try {
			httpPost = new HttpPost(AppConstants.domain + "feedbacks/agendalize-opinions/create");
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setEntity(new ByteArrayEntity(
					new JSONObject()
					.put("opinion", message)
					.toString().getBytes("UTF8")
			));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
			}
		} catch (ConnectTimeoutException e) {
			status = HttpStatus.SC_REQUEST_TIMEOUT;
	    } catch (SocketTimeoutException e) {
	    	status = HttpStatus.SC_REQUEST_TIMEOUT;
		} catch (final UnsupportedEncodingException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final ClientProtocolException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final JSONException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
	    } catch (final IOException e) {
	    	status = HttpStatus.SC_NOT_FOUND;
		}
		disconnect();
	}
	
	public List<EmployeesPerServiceModel> GetEmployeesPerServiceList(long storeID, List<Long> services) {
		List<EmployeesPerServiceModel> employeesPerService = new ArrayList<EmployeesPerServiceModel>();
		try {
			httpGet = new HttpGet(AppConstants.getEmployeesPerService(storeID, services.toArray(new Long[services.size()])));
			response = httpClient.execute(httpGet);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String wsResponse = "";
					String inputLine = null;
					BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
					while ((inputLine = reader.readLine()) != null) {
						wsResponse += inputLine;
					}
					JSONArray arr = new JSONArray(wsResponse);
					for (int i=0; i<arr.length(); i++) {
						employeesPerService.add(new EmployeesPerServiceModel(arr.getJSONObject(i)));
					}
				}
			}
		} catch (ConnectTimeoutException e) {
			status = HttpStatus.SC_REQUEST_TIMEOUT;
	    } catch (SocketTimeoutException e) {
	    	status = HttpStatus.SC_REQUEST_TIMEOUT;
		} catch (final UnsupportedEncodingException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final ClientProtocolException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final JSONException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
	    } catch (final IOException e) {
	    	status = HttpStatus.SC_NOT_FOUND;
		}
		disconnect();
		return employeesPerService;
	}
	
	public List<ServiceModel> getServicesList() {
		List<ServiceModel> services = new ArrayList<ServiceModel>();
		try {
			httpGet = new HttpGet(AppConstants.domain + "services/list");
			response = httpClient.execute(httpGet);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String wsResponse = "";
					String inputLine = null;
					BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
					while ((inputLine = reader.readLine()) != null) {
						wsResponse += inputLine;
					}
					JSONArray arr = new JSONArray(wsResponse);
					for (int i=0; i<arr.length(); i++) {
						services.add(new ServiceModel(arr.getJSONObject(i)));
					}
				}
			}
		} catch (ConnectTimeoutException e) {
			status = HttpStatus.SC_REQUEST_TIMEOUT;
	    } catch (SocketTimeoutException e) {
	    	status = HttpStatus.SC_REQUEST_TIMEOUT;
		} catch (final UnsupportedEncodingException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final ClientProtocolException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final JSONException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
	    } catch (final IOException e) {
	    	status = HttpStatus.SC_NOT_FOUND;
		}
		disconnect();
		return services;
	}
	
	// closes the connection
	public void disconnect() {
		try {
			httpPost.abort();
			instream.close();
			httpClient.getConnectionManager().shutdown();
		} catch (IOException e) {
		} catch (NullPointerException n) {}
	}

}
