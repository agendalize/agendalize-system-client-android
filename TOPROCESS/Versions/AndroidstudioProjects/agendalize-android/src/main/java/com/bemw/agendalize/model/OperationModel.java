package com.bemw.agendalize.model;

import org.json.JSONObject;

public class OperationModel {
	
	private long id;
	private long codWeek;
	private String descriptionWeek;
	private long codHourOpen;
	private String descriptionHourOpen;
	private long codHourClose;
	private String descriptionHourClose;
	private boolean flagOpen;
	
	public OperationModel() {
		id = -1;
		codWeek = -1;
		descriptionWeek = "descriptionWeek";
		codHourOpen = -1;
		descriptionHourOpen = "descriptionHourOpen";
		codHourClose = -1;
		descriptionHourClose = "descriptionHourClose";
		flagOpen = false;
	}
	public OperationModel(long id, long codWeek, String descriptionWeek,
			long codHourOpen, String descriptionHourOpen, long codHourClose,
			String descriptionHourClose, boolean flagOpen) {
		super();
		this.id = id;
		this.codWeek = codWeek;
		this.descriptionWeek = descriptionWeek;
		this.codHourOpen = codHourOpen;
		this.descriptionHourOpen = descriptionHourOpen;
		this.codHourClose = codHourClose;
		this.descriptionHourClose = descriptionHourClose;
		this.flagOpen = flagOpen;
	}
	public OperationModel(JSONObject obj) {
		this();
		try { id = obj.getLong("id"); } catch (Exception e) {}
		try { codWeek = obj.getLong("codWeek"); } catch (Exception e) {}
		try { descriptionWeek = obj.getString("descriptionWeek"); } catch (Exception e) {}
		try { codHourOpen = obj.getLong("codHourOpen"); } catch (Exception e) {}
		try { descriptionHourOpen = obj.getString("descriptionHourOpen"); } catch (Exception e) {}
		try { codHourClose = obj.getLong("codHourClose"); } catch (Exception e) {}
		try { descriptionHourClose = obj.getString("descriptionHourClose"); } catch (Exception e) {}
		try { flagOpen = obj.getBoolean("flagOpen"); } catch (Exception e) {}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getCodWeek() {
		return codWeek;
	}
	public void setCodWeek(long codWeek) {
		this.codWeek = codWeek;
	}
	public String getDescriptionWeek() {
		return descriptionWeek;
	}
	public void setDescriptionWeek(String descriptionWeek) {
		this.descriptionWeek = descriptionWeek;
	}
	public long getCodHourOpen() {
		return codHourOpen;
	}
	public void setCodHourOpen(long codHourOpen) {
		this.codHourOpen = codHourOpen;
	}
	public String getDescriptionHourOpen() {
		return descriptionHourOpen;
	}
	public void setDescriptionHourOpen(String descriptionHourOpen) {
		this.descriptionHourOpen = descriptionHourOpen;
	}
	public long getCodHourClose() {
		return codHourClose;
	}
	public void setCodHourClose(long codHourClose) {
		this.codHourClose = codHourClose;
	}
	public String getDescriptionHourClose() {
		return descriptionHourClose;
	}
	public void setDescriptionHourClose(String descriptionHourClose) {
		this.descriptionHourClose = descriptionHourClose;
	}
	public boolean isFlagOpen() {
		return flagOpen;
	}
	public void setFlagOpen(boolean flagOpen) {
		this.flagOpen = flagOpen;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("id", id); } catch (Exception e) {}
		try { obj.put("codWeek", codWeek); } catch (Exception e) {}
		try { obj.put("descriptionWeek", descriptionWeek); } catch (Exception e) {}
		try { obj.put("codHourOpen", codHourOpen); } catch (Exception e) {}
		try { obj.put("descriptionHourOpen", descriptionHourOpen); } catch (Exception e) {}
		try { obj.put("codHourClose", codHourClose); } catch (Exception e) {}
		try { obj.put("descriptionHourClose", descriptionHourClose); } catch (Exception e) {}
		try { obj.put("flagOpen", flagOpen); } catch (Exception e) {}
		return obj;
	}

}
