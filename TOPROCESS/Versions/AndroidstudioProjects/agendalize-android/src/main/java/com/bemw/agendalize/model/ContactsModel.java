package com.bemw.agendalize.model;

import org.json.JSONObject;

public class ContactsModel {
	
	private String phoneNumberOne;
	private String phoneNumberTwo;
	private String email;
	private String site;
	private String facebook;
	
	public ContactsModel() {
		phoneNumberOne = "";
		phoneNumberTwo = "";
		email = "";
		site = "";
		facebook = "";
	}
	public ContactsModel(String phoneNumberOne, String phoneNumberTwo,
			String email, String site, String facebook) {
		this();
		this.phoneNumberOne = phoneNumberOne;
		this.phoneNumberTwo = phoneNumberTwo;
		this.email = email;
		this.site = site;
		this.facebook = facebook;
	}
	public ContactsModel(JSONObject obj) {
		this();
		try { phoneNumberOne = obj.getString("phoneNumberOne"); } catch (Exception e) {}
		try { phoneNumberTwo = obj.getString("phoneNumberTwo"); } catch (Exception e) {}
		try { email = obj.getString("email"); } catch (Exception e) {}
		try { site = obj.getString("site"); } catch (Exception e) {}
		try { facebook = obj.getString("facebook"); } catch (Exception e) {}
	}

	public String getPhoneNumberOne() {
		return phoneNumberOne;
	}
	public void setPhoneNumberOne(String phoneNumberOne) {
		this.phoneNumberOne = phoneNumberOne;
	}
	public String getPhoneNumberTwo() {
		return phoneNumberTwo;
	}
	public void setPhoneNumberTwo(String phoneNumberTwo) {
		this.phoneNumberTwo = phoneNumberTwo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("phoneNumberOne", phoneNumberOne); } catch (Exception e) {}
		try { obj.put("phoneNumberTwo", phoneNumberTwo); } catch (Exception e) {}
		try { obj.put("email", email); } catch (Exception e) {}
		try { obj.put("site", site); } catch (Exception e) {}
		try { obj.put("facebook", facebook); } catch (Exception e) {}
		return obj;
	}

}
