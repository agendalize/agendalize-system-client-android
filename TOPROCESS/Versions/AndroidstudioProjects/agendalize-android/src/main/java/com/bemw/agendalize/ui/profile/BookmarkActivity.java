package com.bemw.agendalize.ui.profile;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.BookmarkModel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class BookmarkActivity extends ActionBarActivity {

	private ListView providerList				= null;
	private BookmarkListAdapter adapter			= null;
	private BookmarkModel bookmark				= null;
	private TextView emptyLabel					= null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmark);
		
		providerList = (ListView) findViewById(R.id.activity_bookmark_list);
		emptyLabel = (TextView) findViewById(R.id.activity_bookmark_empty);
		
		getSupportActionBar().setTitle(getResources().getString(R.string.main_bookmark));
		getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_favorite));
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		bookmark = BookmarkModel.loadBookmark(BookmarkActivity.this);
		if (bookmark.size() == 0) {
			emptyLabel.setVisibility(View.VISIBLE);
			providerList.setVisibility(View.GONE);
		} else {
			emptyLabel.setVisibility(View.GONE);
			providerList.setVisibility(View.VISIBLE);
			adapter = new BookmarkListAdapter();
			providerList.setAdapter(adapter);
			providerList.setOnItemClickListener(new OnItemClickListener() {
				@Override
			    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
					/*Intent detailIntent = new Intent(BookmarkFragment.this, StoreDetailActivity.class);
					detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					detailIntent.putExtra(AppConstants.STOREDATA, bookmark.get(position).toJson().toString());
					startActivity(detailIntent);*/
				}
			});
			adapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	public class BookmarkListAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return bookmark.size();
		}
		@Override
		public Object getItem(int position) {
			return bookmark.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@SuppressLint("InflateParams") @Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_storeitem, null);
			}
			ImageButton photo		= ((ImageButton)convertView.findViewById(R.id.row_storeitem_photo));
			TextView name			= ((TextView)convertView.findViewById(R.id.row_storeitem_name));
			TextView address		= ((TextView)convertView.findViewById(R.id.row_storeitem_address1));
			/*ImageButton call		= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt1));
			ImageButton portfolio	= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt2));
			ImageButton schedule	= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt3));
			ImageButton location	= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt4));
			ImageButton delete		= ((ImageButton)convertView.findViewById(R.id.row_bmitem_delete));*/
			
			//photo.setBackgroundDrawable(ImageUtils.loadCachedImage(context, bookmark.get(position).getPhotoIds().get(0)));
			name.setText(bookmark.get(position).getName());
			address.setText(
					bookmark.get(position).getAddressInfo().getAddress() + " " +
					bookmark.get(position).getAddressInfo().getAddressNumber() + "\n" +
					bookmark.get(position).getAddressInfo().getComplement() + " - " +
					bookmark.get(position).getAddressInfo().getCity()
			);
			
			photo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO: show photos of this provider
				}
			});
			/*schedule.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO: show photos of this provider
				}
			});
			portfolio.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//iprovider.showTheServices(bookmark.get(position));
				}
			});
			call.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Dialer.call(context.getActivity(), bookmark.get(position).getContact().getPhoneNumberOne());
				}
			});
			location.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//iprovider.showTheProvider(bookmark.get(position));
				}
			});
			delete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					final AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
					builder.setMessage(context.getResources().getString(R.string.label_removebookmark)).setCancelable(true).
					setPositiveButton(context.getResources().getString(R.string.btn_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {
								if (bookmark.hasTheRegistry(bookmark.get(position))) {
									bookmark.removeRegistry(context.getActivity(), bookmark.get(position));
									notifyDataSetChanged();
								}
							}
						}
					).setNegativeButton(context.getResources().getString(R.string.btn_no),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
					});
					final AlertDialog alert = builder.create();
					alert.show();
				}
			});*/
			
			return convertView;
		}
		
	}
	
}
