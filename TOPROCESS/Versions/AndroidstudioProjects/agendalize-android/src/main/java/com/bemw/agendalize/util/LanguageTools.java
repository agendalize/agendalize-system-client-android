package com.bemw.agendalize.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.res.Configuration;

public class LanguageTools {
	
	public static enum LANGUAGES {ENGLISH, PORTUGUESE, SPANISH}
	
	public static final String
		ENGLISH_NATIVE_LANG			= "English",
		PORTUGUESE_NATIVE_LANG		= "Português",
		SPANISH_NATIVE_LANG			= "Español";
	
	/**
	 * Define the language on the determined context
	 * 
	 * @param context
	 * @param languageInUse
	 */
	public static void setLocalizationStringValues(Context context, LanguageTools.LANGUAGES languageInUse) {
		String localizationLang = getLocalizationLanguage(languageInUse);
        Locale locale = new Locale(localizationLang); 
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	}

	/**
	 * Gets the string name related to the underlying given language
	 * 
	 * @param languageToLoad
	 * @return
	 */
	private static String getLocalizationLanguage(LanguageTools.LANGUAGES languageToLoad) {
		switch(languageToLoad) {
			case ENGLISH: return "en";
			case PORTUGUESE: return "pt";
			case SPANISH: return "es";
		}
		return null;
	}
	
	public static String getCheckedNativeLanguage(String language) {
		Map<String, String> languagesMap = new HashMap<String, String>();
		languagesMap.put(LANGUAGES.ENGLISH.toString(), ENGLISH_NATIVE_LANG);
		languagesMap.put(LANGUAGES.PORTUGUESE.toString(), PORTUGUESE_NATIVE_LANG);
		languagesMap.put(LANGUAGES.SPANISH.toString(), SPANISH_NATIVE_LANG);
		return languagesMap.get(language);
	}

}
