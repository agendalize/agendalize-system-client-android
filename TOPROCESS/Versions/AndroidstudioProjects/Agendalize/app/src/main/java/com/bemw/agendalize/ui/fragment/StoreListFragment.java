package com.bemw.agendalize.ui.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.model.StoreSearchRequestModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.ui.StoreListActivity;
import com.bemw.agendalize.util.DevicePlatform;
import com.bemw.agendalize.util.ImageUtils;

import org.apache.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by mthama on 31/01/15.
 */
public class StoreListFragment extends Fragment {

    private static StoreListActivity context;
    private static ListView storeList;
    // data for searches
    private static String baseAddress = null;
    private static LocationManager lm;
    private static Location location;
    // data for store fetch
    private static double LatLon[] = null;
    //private static ProgressDialog waitDialog = null;
    private static Thread locationLoader = null;
    private static Thread contentLoader = null;
    private static boolean isLoading = false;
    private static List<StoreModel> stores = null;
    private static StoreListAdapter storeListAdapter;
    private static int lastLoadCount = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storelist, container, false);
        context = (StoreListActivity)getActivity();

        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 10, locationListener);

        // widgets and components
        storeList = (ListView) rootView.findViewById(R.id.frag_storelist_listview);
        storeList.setVisibility(View.VISIBLE);
        storeListAdapter = new StoreListAdapter();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        createNewLoader();
    }

    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //if (waitDialog != null) waitDialog.dismiss();
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                    AppConstants.justShowDialogWarning(context, R.string.label_timeout);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                    AppConstants.justShowDialogWarning(context, R.string.label_noconnection);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                    AppConstants.justShowDialogWarning(context, R.string.label_servererror);
                    break;
                case HttpStatus.SC_OK:
                    if (LatLon != null && stores == null) { // got location
                        isLoading = false;
                        createNewLoader();
                    } else if (LatLon != null && stores != null) { // got to next view
                        if (stores.size() == 0) {
                            AppConstants.justShowDialogWarning(context, R.string.label_noresults);
                        } else {
                            lastLoadCount = stores.size();
                            storeList.setAdapter(storeListAdapter);
                            storeList.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(AbsListView view, int scrollState) {}
                                @Override
                                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    if (
                                        storeList.getLastVisiblePosition()+1 == totalItemCount && // user has reached the end of the list
                                        storeList.getAdapter().getCount() >= AppConstants.SIZE_LIST && // only enable pagination if we already done at least 1 load
                                        lastLoadCount == AppConstants.SIZE_LIST && // last load had fetch a complete page
                                        lastLoadCount != -1
                                    ) createNewLoader();
                                }
                            });
                            // add the informative item, if there is more contacts to load
                            if (
                                stores.size() > 0 && // it makes no sense to show "loading more" with an empty list
                                lastLoadCount == AppConstants.SIZE_LIST
                            ) {
                                StoreModel m = new StoreModel();
                                m.setName("LOAD_MORE");
                                stores.add(m);
                                storeListAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                    break;
            }
            isLoading = false;
        }
    };

    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DevicePlatform.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                stores = client.getStoresByContext(getSearchParams(LatLon).toJson());
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };

    synchronized private static void createNewLoader() {
        if (!DevicePlatform.isConnectedToInternet(context)) {
            AppConstants.justShowDialogWarning(context, R.string.label_noconnection);
        } else if (!isLoading) {
            isLoading = true;
            /*waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.label_sendingdata),
                    context.getText(R.string.label_pleasewait),
                    false,
                    false);
            waitDialog.setIcon(context.getResources().getDrawable(R.drawable.ic_launcher));*/
            contentLoader = new Thread(loadContent);
            contentLoader.start();
        }
    }

    private static StoreSearchRequestModel getSearchParams(double[] LatLon) {
        // construct the json request
        Calendar dayTm = Calendar.getInstance();
        StoreSearchRequestModel request = new StoreSearchRequestModel();
        request.setLatitude(LatLon[0]);
        request.setLongitude(LatLon[1]);
        request.setBaseDate(dayTm.getTime().getTime());
        request.setRange(50000);
        return request;
    }

    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            LatLon = new double[2];
            LatLon[0] = location.getLongitude();
            LatLon[1] = location.getLatitude();
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onProviderDisabled(String provider) {}
    };

    //===================================================================================
    // Inner classes
    //===================================================================================

    private class StoreListAdapter extends BaseAdapter {
        public StoreListAdapter() {}
        @Override
        public int getCount() {
            return stores.size();
        }
        @Override
        public Object getItem(int position) {
            return stores.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @SuppressLint("InflateParams") @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (stores.get(position).getName().equals("LOAD_MORE")) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_helperloading, null);
                TextView textinfo = ((TextView) convertView.findViewById(R.id.row_helperloading_infotxt));
                textinfo.setText(context.getString(R.string.label_loadingdata));
                return convertView;
            }

            // predefines the view as an inflated layout
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_storeitem, null);
            ImageView photo = ((ImageView) convertView.findViewById(R.id.row_storeitem_photo));
            TextView name = ((TextView) convertView.findViewById(R.id.row_storeitem_name));
            RatingBar rating = ((RatingBar)convertView.findViewById(R.id.row_storeitem_rating));
            TextView info1 = ((TextView) convertView.findViewById(R.id.row_storeitem_info1));
            TextView info2 = ((TextView) convertView.findViewById(R.id.row_storeitem_info2));
            TextView address1 = ((TextView) convertView.findViewById(R.id.row_storeitem_address1));
            TextView address2 = ((TextView) convertView.findViewById(R.id.row_storeitem_address2));
            name.setText(stores.get(position).getName());
            rating.setRating(stores.get(position).getEvaluation()*5/100);
            info1.setText(getResources().getString(R.string.label_nremployees) + " " + stores.get(position).getNrEmployees());
            info2.setText(getResources().getString(R.string.label_nrservices) + " " + stores.get(position).getNrServices());
            address1.setText(stores.get(position).getAddressInfo().getCity() + ", " + stores.get(position).getAddressInfo().getState());
            address2.setText(stores.get(position).getAddressInfo().getAddress() + " " + stores.get(position).getAddressInfo().getAddressNumber());
            if (stores.get(position).getUrlPhoto() != null) {
                if (stores.get(position).getUrlPhoto().length() > 0) {
                    //new PhotoLoader(photo, stores.get(position).getUrlPhoto()).execute();
                }
            }
            return convertView;
        }
    }

    private class PhotoLoader extends AsyncTask<Void, Void, Void> {
        private Drawable drawable;
        private ImageView photo;
        private String url;
        public PhotoLoader(ImageView photo, String url) {
            this.photo = photo;
            this.url = url;
        }
        @Override
        protected Void doInBackground(Void... params) {
            drawable = ImageUtils.loadCachedImage(getActivity(), url);
            Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
            drawable = new BitmapDrawable(getResources(), bitmap);
            return null;
        }
        @SuppressWarnings("deprecation") @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            photo.setBackgroundDrawable(drawable);
        }
    }

}
