package com.bemw.agendalize.ui;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StoreDetailAdapterServices extends BaseAdapter {

	private Context context;
	private StoreModel store;
	private int selected;
	
	public StoreDetailAdapterServices (Context context, StoreModel store) {
		this.context = context;
		this.store = store;
		this.selected = -1;
	}
	
	public int getSelected() {
		return selected;
	}
	
	public void setSelected(int selected) {
		this.selected = selected;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return store.getListServices().size();
	}

	@Override
	public Object getItem(int position) {
		return store.getListServices().get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// predefines the view as an inflated layout
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.row_servicedata, null);
		
		RelativeLayout lyt		= ((RelativeLayout)convertView.findViewById(R.id.row_servicedata_lyt));
		TextView name			= ((TextView)convertView.findViewById(R.id.row_servicedata_name));
		TextView description	= ((TextView)convertView.findViewById(R.id.row_servicedata_description));
		TextView price			= ((TextView)convertView.findViewById(R.id.row_servicedata_price));
		TextView delay			= ((TextView)convertView.findViewById(R.id.row_servicedata_delay));
		
		if (selected == position)
			lyt.setBackgroundDrawable(context.getResources().getDrawable(R.color.agendalize_colorB));
		else
			lyt.setBackgroundDrawable(null);
		name.setText(store.getListServices().get(position).getShortName());
		description.setText(store.getListServices().get(position).getDescription());
		price.setText(context.getResources().getString(R.string.label_currency) + " " + store.getListServices().get(position).getPrice());
		delay.setText(store.getListServices().get(position).getTimeMinutes() + " " + context.getResources().getString(R.string.label_minutes));
		
		return convertView;
	}

}
