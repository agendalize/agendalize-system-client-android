package com.bemw.agendalize.ui;

import java.util.List;
import java.util.Random;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.ScheduleModel;

public class ScheduleListAdapter extends BaseAdapter {

	private Context context;
	private List<ScheduleModel> items;
	private boolean isMine;
	
	public ScheduleListAdapter(Context context, List<ScheduleModel> items, boolean isMine) {
		this.context = context;
		this.items = items;
		this.isMine = isMine;
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (isMine) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_schedule, null);
			}
			TextView hour = ((TextView)convertView.findViewById(R.id.row_schedule_hour));
			hour.setText((int)(position/2)+":"+(position%2==0?"0":"3")+"0");
			
			Random randomGenerator = new Random();
			
			ImageButton sunday = ((ImageButton)convertView.findViewById(R.id.row_schedule_sunday));
			sunday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedclosed));
			
			ImageButton monday = ((ImageButton)convertView.findViewById(R.id.row_schedule_monday));
			if (randomGenerator.nextInt(100)%3 == 0)
				monday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				monday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				monday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			
			ImageButton tuesday = ((ImageButton)convertView.findViewById(R.id.row_schedule_tuesday));
			if (randomGenerator.nextInt(100)%3 == 0)
				tuesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				tuesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				tuesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			
			ImageButton wednesday = ((ImageButton)convertView.findViewById(R.id.row_schedule_wednesday));
			if (randomGenerator.nextInt(100)%3 == 0)
				wednesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				wednesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				wednesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			
			ImageButton thursday = ((ImageButton)convertView.findViewById(R.id.row_schedule_thursday));
			if (randomGenerator.nextInt(100)%3 == 0)
				thursday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				thursday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				thursday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			
			ImageButton friday = ((ImageButton)convertView.findViewById(R.id.row_schedule_friday));
			if (randomGenerator.nextInt(100)%3 == 0)
				friday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				friday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				friday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			
			ImageButton saturday = ((ImageButton)convertView.findViewById(R.id.row_schedule_saturday));
			saturday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedclosed));
		} else {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_storeschedule, null);
			}
			TextView hour = ((TextView)convertView.findViewById(R.id.row_storeschedule_hour));
			hour.setText((int)(position/2)+":"+(position%2==0?"0":"3")+"0");
			
			Random randomGenerator = new Random();
			
			ImageButton sunday = ((ImageButton)convertView.findViewById(R.id.row_storeschedule_sunday));
			if (randomGenerator.nextInt(100)%3 == 0)
				sunday.setBackgroundResource(R.drawable.shape_roundedchosed);
				//sunday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				sunday.setBackgroundResource(R.drawable.shape_roundedlocked);
				//sunday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				sunday.setBackgroundResource(R.drawable.shape_roundedopen);
				//sunday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			
			ImageButton monday = ((ImageButton)convertView.findViewById(R.id.row_storeschedule_monday));
			if (randomGenerator.nextInt(100)%3 == 0)
				monday.setBackgroundResource(R.drawable.shape_roundedchosed);
				//monday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				monday.setBackgroundResource(R.drawable.shape_roundedlocked);
				//monday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				monday.setBackgroundResource(R.drawable.shape_roundedopen);
				//monday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
			
			ImageButton tuesday = ((ImageButton)convertView.findViewById(R.id.row_storeschedule_tuesday));
			if (randomGenerator.nextInt(100)%3 == 0)
				tuesday.setBackgroundResource(R.drawable.shape_roundedchosed);
				//tuesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedchosed));
			else if (randomGenerator.nextInt(100)%2 == 0)
				tuesday.setBackgroundResource(R.drawable.shape_roundedlocked);
				//tuesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedlocked));
			else
				tuesday.setBackgroundResource(R.drawable.shape_roundedopen);
				//tuesday.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedopen));
		}
		return convertView;
	}

}
