package com.bemw.agendalize.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.PageModel;
import com.bemw.agendalize.model.ScheduleModel;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ScheduleFragment extends Fragment {

	private ListView openHoursList;
	private ScheduleListAdapter adapter;
	private List<ScheduleModel> items;
	
	private TextView firstDay;
	private TextView secondDay;
	private TextView thirdDay;
	private TextView fourthDay;
	private TextView fifthDay;
	private TextView sixthDay;
	private TextView seventhDay;
	private Button prevweekbtn;
	private Button currweekbtn;
	private Button nextweekbtn;
	private Calendar calendar;
	
	// we name the left, middle and right page
	private static final int PAGE_LEFT = 0;
	private static final int PAGE_MIDDLE = 1;
	private static final int PAGE_RIGHT = 2;

	private LayoutInflater mInflater;
	private int mSelectedPageIndex = 1;
	// we save each page in a model
	private PageModel[] mPageModel = new PageModel[3];
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.view_schedule, container, false);
		
		firstDay = (TextView)rootView.findViewById(R.id.view_schedule_firstday);
		secondDay = (TextView)rootView.findViewById(R.id.view_schedule_secondday);
		thirdDay = (TextView)rootView.findViewById(R.id.view_schedule_thirdday);
		fourthDay = (TextView)rootView.findViewById(R.id.view_schedule_fourthday);
		fifthDay = (TextView)rootView.findViewById(R.id.view_schedule_fifthday);
		sixthDay = (TextView)rootView.findViewById(R.id.view_schedule_sixthday);
		seventhDay = (TextView)rootView.findViewById(R.id.view_schedule_seventhday);
		
		prevweekbtn = (Button)rootView.findViewById(R.id.view_schedule_prevweekbtn);
		currweekbtn = (Button)rootView.findViewById(R.id.view_schedule_currweekbtn);
		nextweekbtn = (Button)rootView.findViewById(R.id.view_schedule_nextweekbtn);
		
		// initializing the model
		initPageModel();

		mInflater = getActivity().getLayoutInflater();
		SchedulePageAdapter adapter = new SchedulePageAdapter();

		final ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
		viewPager.setAdapter(adapter);
		// we dont want any smoothscroll. This enables us to switch the page
		// without the user notifiying this
		viewPager.setCurrentItem(PAGE_MIDDLE, false);
		
		// get current date and center it on the scheduler
		calendar = Calendar.getInstance();
		setWeekOffset(calendar);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		currweekbtn.setText(
			getResources().getString(R.string.label_currweek) + "\n" +
			String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
			+"/"+String.format("%02d", calendar.get(Calendar.MONTH))
			+" ~ "
			+String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)+6)
			+"/"+String.format("%02d", calendar.get(Calendar.MONTH))
		);
		setListeners();
				
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				mSelectedPageIndex = position;
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == ViewPager.SCROLL_STATE_IDLE) {

					final PageModel leftPage = mPageModel[PAGE_LEFT];
					final PageModel middlePage = mPageModel[PAGE_MIDDLE];
					final PageModel rightPage = mPageModel[PAGE_RIGHT];

					final int oldLeftIndex = leftPage.getIndex();
					final int oldMiddleIndex = middlePage.getIndex();
					final int oldRightIndex = rightPage.getIndex();

					// user swiped to right direction --> left page
					if (mSelectedPageIndex == PAGE_LEFT) {

						leftPage.setIndex(oldLeftIndex - 1);
						middlePage.setIndex(oldLeftIndex);
						rightPage.setIndex(oldMiddleIndex);

						setContent(PAGE_RIGHT);
						setContent(PAGE_MIDDLE);
						setContent(PAGE_LEFT);
						
						calendar.add(Calendar.DAY_OF_YEAR, - 7);
						setWeekOffset(calendar);

					// user swiped to left direction --> right page
					} else if (mSelectedPageIndex == PAGE_RIGHT) {

						leftPage.setIndex(oldMiddleIndex);
						middlePage.setIndex(oldRightIndex);
						rightPage.setIndex(oldRightIndex + 1);

						setContent(PAGE_LEFT);
						setContent(PAGE_MIDDLE);
						setContent(PAGE_RIGHT);
						
						calendar.add(Calendar.DAY_OF_YEAR, + 7);
						setWeekOffset(calendar);
					}
					
					viewPager.setCurrentItem(PAGE_MIDDLE, false);
				}
			}
		});
		
		return rootView;
	}
	
	private void setContent(int index) {
		final PageModel model = mPageModel[index];
		// TODO: this should be removed from here
		items = new ArrayList<ScheduleModel>();
		for (int i=0; i<48; i++) {
			items.add(new ScheduleModel());
		}
		openHoursList = (ListView)model.listview.findViewById(R.id.list_schedule_openHours);
		adapter = new ScheduleListAdapter(getActivity(), items, true);
		openHoursList.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	private void setWeekOffset(Calendar calendar) {
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		firstDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		secondDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		thirdDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
		fourthDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
		fifthDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		sixthDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		seventhDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);
	}
	
	private void setListeners() {
		prevweekbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				PageModel leftPage = mPageModel[PAGE_LEFT];
				PageModel middlePage = mPageModel[PAGE_MIDDLE];
				PageModel rightPage = mPageModel[PAGE_RIGHT];

				int oldLeftIndex = leftPage.getIndex();
				int oldMiddleIndex = middlePage.getIndex();
				
				leftPage.setIndex(oldLeftIndex - 1);
				middlePage.setIndex(oldLeftIndex);
				rightPage.setIndex(oldMiddleIndex);

				setContent(PAGE_RIGHT);
				setContent(PAGE_MIDDLE);
				setContent(PAGE_LEFT);
				
				calendar.add(Calendar.DAY_OF_YEAR, - 7);
				setWeekOffset(calendar);
			}
		});
		currweekbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				calendar = Calendar.getInstance();
				setWeekOffset(calendar);
			}
		});
		nextweekbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				PageModel leftPage = mPageModel[PAGE_LEFT];
				PageModel middlePage = mPageModel[PAGE_MIDDLE];
				PageModel rightPage = mPageModel[PAGE_RIGHT];

				int oldMiddleIndex = middlePage.getIndex();
				int oldRightIndex = rightPage.getIndex();
				
				leftPage.setIndex(oldMiddleIndex);
				middlePage.setIndex(oldRightIndex);
				rightPage.setIndex(oldRightIndex + 1);

				setContent(PAGE_LEFT);
				setContent(PAGE_MIDDLE);
				setContent(PAGE_RIGHT);
				
				calendar.add(Calendar.DAY_OF_YEAR, + 7);
				setWeekOffset(calendar);
			}
		});
	}

	private void initPageModel() {
		for (int i = 0; i < mPageModel.length; i++) {
			// initing the pagemodel with indexes of -1, 0 and 1
			mPageModel[i] = new PageModel(i - 1);
		}
	}
	
	private class SchedulePageAdapter extends PagerAdapter {
		
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ListView listview = (ListView)mInflater.inflate(R.layout.list_schedule, null);
			PageModel currentPage = mPageModel[position];
			currentPage.listview = listview;
			// TODO:
			ArrayList<ScheduleModel> items = new ArrayList<ScheduleModel>();
			for (int i=0; i<48; i++) {
				items.add(new ScheduleModel());
			}
			ScheduleListAdapter adapter = new ScheduleListAdapter(getActivity(), items, true);
			listview.setAdapter(adapter);
			adapter.notifyDataSetChanged();
			container.addView(listview);
			return listview;
		}

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			return view == obj;
		}
		
	}

}
