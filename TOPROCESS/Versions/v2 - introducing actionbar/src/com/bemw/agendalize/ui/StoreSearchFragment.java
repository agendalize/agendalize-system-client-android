package com.bemw.agendalize.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.dao.LanguageDAO;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.util.DevicePlatform;
import com.bemw.agendalize.util.LanguageTools;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

public class StoreSearchFragment extends Fragment {

	private String languageInUse;
	
	// listview objects
	private ListView provider_list;
	private StoreSearchAdapter adapter;
	private List<StoreModel> providers			= null;
	private int offset							= 0;
	private int total_found						= -1;
		
	// search objects
	private AutoCompleteTextView search_input	= null;
	private ImageButton clearedit				= null;
	
	// loader objects
	private static Thread content_loader		= null;
	private static boolean is_loading			= false;
	private static ProgressDialog wait_dialog	= null;
	private RESTClient ws_connection			= null;
	private JSONObject param_obj				= null;
	/*private JSONArray providers_arr				= null;*/
	private String server_message				= null,
				   searched						= null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.view_storesearch, container, false);
		
		provider_list = (ListView) rootView.findViewById(R.id.menu_storesearch_listview);
		clearedit = (ImageButton) rootView.findViewById(R.id.menu_storesearch_cleartxtbtn);
		search_input = (AutoCompleteTextView) rootView.findViewById(R.id.menu_storesearch_autocompleteedit);
				
		setSearchEditFeatures();
		providers = new ArrayList<StoreModel>();
    	providers.clear();
    	
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		languageInUse = LanguageDAO.loadLangPreferences(getActivity());
		LanguageTools.setLocalizationStringValues(getActivity(), LanguageTools.LANGUAGES.valueOf(languageInUse.toUpperCase(Locale.getDefault())));
	}
	
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {			
			switch (msg.what) {
				case -1: // server returned null
					handler.removeCallbacks(timeoutRunnable);
					break;
				case 0: // displays connection fail
					//logo_splash.setVisibility(View.GONE);
					handler.removeCallbacks(timeoutRunnable);
					//showInternetConnectionFailure();
					break;
				case 1: // displays connection timeout
					handler.removeCallbacks(timeoutRunnable);
					//showInternetConnectionTimeout();
					break;
				case 2: // web service result (first time)
					/*boolean auth = providers.get(0).isAuthenticated();
					if (total_found == 1) {
						History.saveAddressAsHistory(providers.get(0).getLabel(), providers.get(0), true, auth, DomainSearchActivity.this, history);
					} else {
						History.saveAddressAsHistory(searched, null, false, auth, DomainSearchActivity.this, history);
					}*/
					//setupHeaderCallFeatures(searched);
					adapter = new StoreSearchAdapter(StoreSearchFragment.this.getActivity(), providers);
					adapter.updateProviderList(providers, total_found);
					provider_list.setAdapter(adapter);
					provider_list.setOnScrollListener(new OnScrollListener() {
						@Override
						public void onScrollStateChanged(AbsListView view, int scrollState) {}
						@Override
						public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
							if (provider_list.getLastVisiblePosition()+1 == totalItemCount && provider_list.getAdapter().getCount() >= InternalConstants.PAGE_SIZE) {
								createNewLoader();
							}
						}
					});
					provider_list.setOnItemClickListener(new OnItemClickListener() {
						@Override
					    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
							Intent detailIntent = new Intent(getActivity(), StoreDetailActivity.class);
							detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
							detailIntent.putExtra(InternalConstants.STOREDATA, providers.get(position).toJson().toString());
							getActivity().startActivity(detailIntent);
						}
					});
					searched = search_input.getText().toString();
					search_input.setText("");
					//logo_splash.setVisibility(View.GONE);
					offset += InternalConstants.PAGE_SIZE+1;
					// open the next view the soon as possible if there is only one item
					/*if (providers.size() == 1) {
						showTheProvider(providers.get(0));
					}*/
					break;
				case 3: // content lazy load result
					adapter.updateProviderList(providers, total_found);
					provider_list.setVisibility(View.VISIBLE);
					adapter.notifyDataSetChanged();
					search_input.setText("");
					//logo_splash.setVisibility(View.GONE);
					offset += InternalConstants.PAGE_SIZE+1;
					break;
				case 4: // toast server message
					final AlertDialog.Builder builder = new AlertDialog.Builder(StoreSearchFragment.this.getActivity());
					builder.setMessage(server_message).
					setCancelable(false).setPositiveButton(StoreSearchFragment.this.getActivity().getResources().getString(R.string.btn_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
					break;
			}
			
			hideSoftInput(StoreSearchFragment.this.getActivity(), R.id.menu_storesearch_autocompleteedit);
			ws_connection.disconnect();
			if (wait_dialog != null) {
				wait_dialog.dismiss();
				wait_dialog = null;
			}
			is_loading = false;
		}
	};
	
	private final Runnable loadContent = new Runnable() {
		@Override
		public void run() {
			try {
				// fetch for providers that matches the searched term
				//param_obj = DataInterchangeUtils.getProviderSearchParamsAsJson(StoreSearchFragment.this.getActivity(), offset, searched);
				ws_connection = new RESTClient();
				if (!DevicePlatform.isConnectedToInternet(StoreSearchFragment.this.getActivity())) {
					// probably the connection has been already closed
					handler.sendEmptyMessage(0);
				} else {
					providers = ws_connection.GetStoresList();
					// stops the timer, since we already got the answer
					handler.removeCallbacks(timeoutRunnable);
					// toast server message
					/*server_message = obj.getString("message");
					if (!server_message.equals("null")) {
						handler.sendEmptyMessage(4);
					}*/
					// update the quantity of incoming domains
					if (total_found == -1) {
						/*total_found = obj.getInt("providerListSize");*/
						total_found = providers.size();
						// setup the list
						/*providers_arr = obj.getJSONArray("providers");*/
						// removes the loading placeholder
						if (providers.size() > 0 && providers.get(providers.size()-1).getName() == "LOAD_MORE") {
							providers.remove(providers.size()-1);
						}
						/*for (int i=0; i<providers_arr.length(); ++i) {
							providers.add(new StoreModel(providers_arr.getJSONObject(i)));
						}*/
						if (providers.size() > 0 && providers.size() < total_found) {
							StoreModel m = new StoreModel();
							m.setName("LOAD_MORE");
							providers.add(m);
						}
						handler.sendEmptyMessage(2);
					} else {
						/*total_found = obj.getInt("providerListSize");*/
						total_found = providers.size();
						// setup the list
						/*providers_arr = obj.getJSONArray("providers");*/
						// removes the loading placeholder
						if (providers.size() > 0 && providers.get(providers.size()-1).getName() == "LOAD_MORE") {
							providers.remove(providers.size()-1);
						}
						/*for (int i=0; i<providers_arr.length(); ++i) {
							providers.add(new StoreModel(providers_arr.getJSONObject(i)));
						}*/
						if (providers.size() > 0 && providers.size() < total_found) {
							StoreModel m = new StoreModel();
							m.setName("LOAD_MORE");
							providers.add(m);
						}
						handler.sendEmptyMessage(3);
					}
				}
			} catch (Exception e) {
				server_message = e.getLocalizedMessage();
				handler.sendEmptyMessage(4);
			}
		}
	};
	
	private final Runnable timeoutRunnable = new Runnable() {
		@Override
		public void run() {
			// closing connections
			if (ws_connection != null) {
				ws_connection.disconnect();
			}
			handler.sendEmptyMessage(1);
		}
	};
	
	synchronized private void createNewLoader() {
		if (!is_loading) {
			is_loading = true;
			handler.postDelayed(timeoutRunnable, InternalConstants.CONN_TIME_OUT);
			content_loader = new Thread(loadContent);
			content_loader.start();
		}
	}
	
	public void callWebService(final String term) {
		if (isAddressEmpty(term)) return;
		else if (!DevicePlatform.isConnectedToInternet(getActivity())) {
			//showInternetConnectionFailure();
		}
		
		if (wait_dialog == null) {
			wait_dialog = ProgressDialog.show(getActivity(), getText(R.string.label_search), getText(R.string.label_wait), false, true);
			wait_dialog.setIcon(getResources().getDrawable(R.drawable.ic_action_search));
			wait_dialog.setOnCancelListener(new OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					// canceling the search if the user presses the back/home button
					try {
						ws_connection.disconnect();
						handler.removeCallbacks(timeoutRunnable);
				    	is_loading = false;
					} catch (Exception e) {}
				}
			});
		}

		// prepares the view (hide fail messages, show the list, show loading views and pops dialog box
		//error_layout.setVisibility(View.GONE);
		provider_list.setVisibility(View.VISIBLE);
		//logo_splash.setVisibility(View.VISIBLE);
		// get/reset the web service params
		offset = 0;
		total_found = -1;
		providers.clear();
		searched = term;
		// creates the load task
		createNewLoader();
	}
	
	protected void showSoftInput(Context context, int input_id) {
		try {
			((InputMethodManager) getActivity().getSystemService(
					Context.INPUT_METHOD_SERVICE)).toggleSoftInput(
					InputMethodManager.SHOW_IMPLICIT,
					InputMethodManager.HIDE_NOT_ALWAYS);
		} catch (Exception e) {}
	}
	
	protected void hideSoftInput(Context context, int input_id) {
		try {
			((InputMethodManager) getActivity().getSystemService(
					Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
					getActivity().findViewById(input_id).getWindowToken(), 0);
		} catch (Exception e) {}
	}
	
	private void setSearchEditFeatures() {
		search_input.requestFocus();
		//showSoftInput(getActivity(), R.id.menu_storesearch_autocompleteedit);
		if (!search_input.getText().toString().equals("")) {
			clearedit.setVisibility(View.VISIBLE);
		} else {
			clearedit.setVisibility(View.INVISIBLE);
		}
		clearedit.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					//clear_domain.setBackgroundDrawable(getResources().getDrawable(R.drawable.clear_btn_selected));
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					search_input.setText("");
					clearedit.setVisibility(View.INVISIBLE);
					//setupHeaderCallFeatures(null);
					//keyboard.enableKeyboard();
					search_input.requestFocus();
				}
				return false;
			}
		});
		search_input.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {
				if (search_input.getText().length() >= 1) {
					clearedit.setVisibility(View.VISIBLE);
				} else if (search_input.getText().length() == 0) {
					clearedit.setVisibility(View.INVISIBLE);
				}
			}
		});
		search_input.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				callWebService(arg0.getAdapter().getItem(arg2).toString());
			}
		});
		search_input.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					searched = search_input.getText().toString();
					callWebService(searched);
					return true;
			    } else if (keyCode == 66) {
					//keyboard.disableKeyboard();
				}
				return false;
			}
		});
	}
	
	private boolean isAddressEmpty(String address) {
		if (address == null || address.trim().equals("")) {
			new AlertDialog.Builder(getActivity())
			.setTitle(R.string.label_error)
			.setMessage(R.string.label_enterterm)
			.setIcon(getResources().getDrawable(R.drawable.ic_action_warning))
			.setNeutralButton(R.string.btn_ok,
					new android.content.DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							//showSoftInput(getActivity(), R.id.menu_storesearch_autocompleteedit);
						}
					}).show();
			return true;
		}
		return false;
	}
	
}
