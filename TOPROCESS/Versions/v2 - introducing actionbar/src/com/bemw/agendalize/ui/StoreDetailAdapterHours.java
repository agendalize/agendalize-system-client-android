package com.bemw.agendalize.ui;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StoreDetailAdapterHours extends BaseAdapter {

	private Context context;
	private StoreModel store;
	private int selected;
	
	public StoreDetailAdapterHours (Context context, StoreModel store) {
		this.context = context;
		this.store = store;
		this.selected = -1;
	}
	
	public int getSelected() {
		return selected;
	}
	
	public void setSelected(int selected) {
		this.selected = selected;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return store.getListOperation().size();
	}

	@Override
	public Object getItem(int position) {
		return store.getListOperation().get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// predefines the view as an inflated layout
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.row_hourdata, null);
		
		RelativeLayout lyt		= ((RelativeLayout)convertView.findViewById(R.id.row_hourdata_lyt));
		TextView week			= ((TextView)convertView.findViewById(R.id.row_hourdata_week));
		TextView start			= ((TextView)convertView.findViewById(R.id.row_hourdata_start));
		TextView stop			= ((TextView)convertView.findViewById(R.id.row_hourdata_stop));
		
		if (selected == position && store.getListOperation().get(position).isFlagOpen())
			lyt.setBackgroundResource(R.color.agendalize_colorB);
		else
			lyt.setBackgroundResource(0);
		week.setText(store.getListOperation().get(position).getDescriptionWeek().subSequence(0, 1));
		start.setText(context.getResources().getString(R.string.label_from) + " " + store.getListOperation().get(position).getDescriptionHourOpen());
		stop.setText(context.getResources().getString(R.string.label_to) + " " + store.getListOperation().get(position).getDescriptionHourClose());
		if (!store.getListOperation().get(position).isFlagOpen()) {
			convertView.setEnabled(false);
			convertView.setAlpha(0.20f);
		}
		return convertView;
	}

}
