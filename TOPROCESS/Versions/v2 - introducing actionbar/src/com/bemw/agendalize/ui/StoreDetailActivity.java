package com.bemw.agendalize.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.dao.BookmarkDAO;
import com.bemw.agendalize.dao.LanguageDAO;
import com.bemw.agendalize.model.BookmarkModel;
import com.bemw.agendalize.model.PageModel;
import com.bemw.agendalize.model.ScheduleModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.util.Dialer;
import com.bemw.agendalize.util.LanguageTools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StoreDetailActivity extends ActionBarActivity {
	
	private ListView openHoursList;
	private ScheduleListAdapter adapter;
	private List<ScheduleModel> items;
	private static final int PAGE_LEFT = 0;
	private static final int PAGE_MIDDLE = 1;
	private static final int PAGE_RIGHT = 2;
	private LayoutInflater mInflater;
	private int mSelectedPageIndex = 1;
	private PageModel[] mPageModel = new PageModel[3];
	private TextView firstDay;
	private Calendar calendar;
	
	private ListView service_list				= null;
	private ListView hour_list					= null;
	private TextView description				= null;
	private StoreDetailAdapterServices services	= null;
	private StoreDetailAdapterHours hours		= null;
	private BookmarkModel bookmark				= null;
	private String languageInUse;
	private SharedPreferences privatePrefs;
	private Editor prefsPrivateEditor;
	private StoreModel store;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// title and icon as default
		getSupportActionBar().setTitle(R.string.app_name);
		getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_person));
		// retrieve the store
		privatePrefs = getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		prefsPrivateEditor = privatePrefs.edit();
		try {
			store = new StoreModel(new JSONObject(getIntent().getStringExtra(InternalConstants.STOREDATA)));
		} catch (Exception e) {
			// TODO: correctly treat and emit feedback to the user about store load fail
			finish();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		languageInUse = LanguageDAO.loadLangPreferences(this);
		LanguageTools.setLocalizationStringValues(this, LanguageTools.LANGUAGES.valueOf(languageInUse.toUpperCase(Locale.getDefault())));
		setContentView(R.layout.view_storedetail);		
		
		// schedule pager
		//firstDay = (TextView)findViewById(R.id.view_schedule_firstday);
		initPageModel();
		mInflater = getLayoutInflater();
		SchedulePageAdapter adapter = new SchedulePageAdapter();
		final ViewPager viewPager = (ViewPager) findViewById(R.id.view_storedetail_viewpager);
		viewPager.setAdapter(adapter);
		// we dont want any smoothscroll. This enables us to switch the page
		// without the user notifiying this
		viewPager.setCurrentItem(PAGE_MIDDLE, false);
		// get current date and center it on the scheduler
		calendar = Calendar.getInstance();
		setWeekOffset(calendar);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				mSelectedPageIndex = position;
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == ViewPager.SCROLL_STATE_IDLE) {
					final PageModel leftPage = mPageModel[PAGE_LEFT];
					final PageModel middlePage = mPageModel[PAGE_MIDDLE];
					final PageModel rightPage = mPageModel[PAGE_RIGHT];
					final int oldLeftIndex = leftPage.getIndex();
					final int oldMiddleIndex = middlePage.getIndex();
					final int oldRightIndex = rightPage.getIndex();
					// user swiped to right direction --> left page
					if (mSelectedPageIndex == PAGE_LEFT) {
						leftPage.setIndex(oldLeftIndex - 1);
						middlePage.setIndex(oldLeftIndex);
						rightPage.setIndex(oldMiddleIndex);
						setContent(PAGE_RIGHT);
						setContent(PAGE_MIDDLE);
						setContent(PAGE_LEFT);
						calendar.add(Calendar.DAY_OF_YEAR, - 7);
						setWeekOffset(calendar);
					// user swiped to left direction --> right page
					} else if (mSelectedPageIndex == PAGE_RIGHT) {
						leftPage.setIndex(oldMiddleIndex);
						middlePage.setIndex(oldRightIndex);
						rightPage.setIndex(oldRightIndex + 1);
						setContent(PAGE_LEFT);
						setContent(PAGE_MIDDLE);
						setContent(PAGE_RIGHT);
						calendar.add(Calendar.DAY_OF_YEAR, + 7);
						setWeekOffset(calendar);
					}
					viewPager.setCurrentItem(PAGE_MIDDLE, false);
				}
			}
		});
		
		// views instantiations
		service_list = (ListView)findViewById(R.id.view_storedetail_services);
		hour_list = (ListView)findViewById(R.id.view_storedetail_hours);
		description = (TextView)findViewById(R.id.view_storedetail_description);
			
		bookmark = BookmarkDAO.loadBookmark(this);
		getSupportActionBar().setTitle(store.getName());
		description.setText(store.getDescription());
		if (store.getListServices().size() == 0 || store.getListOperation().size() == 0) {
			//empty_label.setVisibility(View.VISIBLE);
			//provider_list.setVisibility(View.GONE);
		} else {
			//empty_label.setVisibility(View.GONE);
			//provider_list.setVisibility(View.VISIBLE);
			services = new StoreDetailAdapterServices(this, store);
			hours = new StoreDetailAdapterHours(this, store);
			service_list.setAdapter(services);
			hour_list.setAdapter(hours);
			service_list.setOnItemClickListener(new OnItemClickListener() {
				@Override
			    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
					services.setSelected(position);
				}
			});
			hour_list.setOnItemClickListener(new OnItemClickListener() {
				@Override
			    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
					if (store.getListOperation().get(position).isFlagOpen())
						hours.setSelected(position);
				}
			});
			services.notifyDataSetChanged();
			hours.notifyDataSetChanged();
		}
		/*NinePatch ninePatch;
		NinePatchDrawable drawable = new NinePatchDrawable(ninePatch);
		drawable.setColorFilter(Color.RED, Mode.MULTIPLY);*/
		//Toast.makeText(this, "store: "+store.toJson(), Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menustore, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}*/
		switch(item.getItemId()) {
			case R.id.menu_call:
				if (store.getContact().getPhoneNumberOne() == "" && store.getContact().getPhoneNumberTwo() == "") {
					final AlertDialog.Builder builder = new AlertDialog.Builder(StoreDetailActivity.this);
					builder.setMessage(getResources().getString(R.string.label_nocontact)).
					setCancelable(false).setIcon(R.drawable.ic_action_warning_hd).
					setPositiveButton(getResources().getString(R.string.btn_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
				} else {
					Dialer.call(this,
							store.getContact().getPhoneNumberOne() == "" ?
							store.getContact().getPhoneNumberTwo() :
							store.getContact().getPhoneNumberOne()
					);
				}
				break;
			case R.id.menu_bookmark:
				if (!bookmark.hasTheRegistry(store)) {
					final AlertDialog.Builder builder = new AlertDialog.Builder(StoreDetailActivity.this);
					builder.setMessage(store.getName() + ". " + getResources().getString(R.string.label_addasbookmark)).
					setCancelable(true).setPositiveButton(getResources().getString(R.string.btn_yes),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {
								Toast.makeText(getApplicationContext(), R.string.label_addedbookmark, Toast.LENGTH_LONG).show();
								bookmark.addRegistry(StoreDetailActivity.this, store);
							}
						}
					).setNegativeButton(getResources().getString(R.string.btn_no),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(StoreDetailActivity.this);
					builder.setMessage(getResources().getString(R.string.label_alreadyaddedbookmark)).
					setCancelable(false).setIcon(R.drawable.ic_action_warning_hd).
					setPositiveButton(getResources().getString(R.string.btn_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
				}
				break;
			case R.id.menu_map:
				startActivity(new Intent(this, AgendalizeMapActivity.class));
				break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void setContent(int index) {
		final PageModel model = mPageModel[index];
		// TODO: this should be removed from here
		items = new ArrayList<ScheduleModel>();
		for (int i=0; i<48; i++) {
			items.add(new ScheduleModel());
		}
		openHoursList = (ListView)model.listview.findViewById(R.id.list_schedule_openHours);
		adapter = new ScheduleListAdapter(StoreDetailActivity.this, items, false);
		openHoursList.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	private void setWeekOffset(Calendar calendar) {
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		/*firstDay.setText(
				String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
				+"/"+String.format("%02d", calendar.get(Calendar.MONTH)+1)
		);*/
	}
	
	private void initPageModel() {
		for (int i = 0; i < mPageModel.length; i++) {
			// initing the pagemodel with indexes of -1, 0 and 1
			mPageModel[i] = new PageModel(i - 1);
		}
	}
	
	private class SchedulePageAdapter extends PagerAdapter {
		
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ListView listview = (ListView)mInflater.inflate(R.layout.list_schedule, null);
			PageModel currentPage = mPageModel[position];
			currentPage.listview = listview;
			// TODO:
			ArrayList<ScheduleModel> items = new ArrayList<ScheduleModel>();
			for (int i=0; i<48; i++) {
				items.add(new ScheduleModel());
			}
			ScheduleListAdapter adapter = new ScheduleListAdapter(StoreDetailActivity.this, items, false);
			listview.setAdapter(adapter);
			adapter.notifyDataSetChanged();
			container.addView(listview);
			return listview;
		}

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			return view == obj;
		}
		
	}

}
