package com.bemw.agendalize.ui;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.MainActivity;
import com.bemw.agendalize.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class BootsplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_bootsplash);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		
		new Thread() {
			@Override
			public void run() {
				try {
					sleep(InternalConstants.BOOTSPLASH_TIMER);
				} catch (InterruptedException e) {
				} finally {
					// creates a new application instance
					Intent new_app = new Intent(BootsplashActivity.this, MainActivity.class);
					new_app.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(new_app);
					finish();
				}
			}
		}.start();
	}

}

