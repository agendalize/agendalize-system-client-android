package com.bemw.agendalize.ui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;

public class StoreSearchAdapter extends BaseAdapter {

	private Context context;
	private List<StoreModel> providers;
	private int total = 0;
	
	public StoreSearchAdapter(Context context, List<StoreModel> providers) {
		this.context = context;
		this.providers = providers;
	}
	
	public void updateProviderList(List<StoreModel> providers, int total) {
		this.providers = providers;
		this.total = total;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return providers.size();
	}

	@Override
	public Object getItem(int position) {
		return providers.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		// in case of paginated load
		if (providers.get(position).getName() == "LOAD_MORE") {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_loadingitem, null);
			TextView textinfo = ((TextView)convertView.findViewById(R.id.row_loadingitem_infotxt));
			textinfo.setText(
					context.getString(R.string.label_loading)+" ("+
					context.getString(R.string.menu_stores)+": "+
					(providers.size()-1)+" / "+total+")");
			return convertView;
		}
		
		// predefines the view as an inflated layout
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.row_storedata, null);
		
		ImageView photo		= ((ImageView)convertView.findViewById(R.id.row_storedata_photo));
		//ImageButton detail	= ((ImageButton)convertView.findViewById(R.id.row_storedata_detail));
		TextView title		= ((TextView)convertView.findViewById(R.id.row_storedata_name));
		//TextView resume		= ((TextView)convertView.findViewById(R.id.row_storedata_resume));
		TextView address	= ((TextView)convertView.findViewById(R.id.row_storedata_address));
		
		title.setText(providers.get(position).getName());
		//resume.setText(providers.get(position).getDescription());
		address.setText(
				providers.get(position).getAddress().getAddress() + " " +
				providers.get(position).getAddress().getAddressNumber() + "\n" +
				providers.get(position).getAddress().getCity() + " - " +
				providers.get(position).getAddress().getCountry()
		);
		
		photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: show photos of this provider
				Toast.makeText(context, "photo", Toast.LENGTH_SHORT).show();
			}
		});
		/*detail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: go to store
			}
		});*/
		/*convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent detailIntent = new Intent(context, StoreDetailActivity.class);
				detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				detailIntent.putExtra(InternalConstants.STOREDATA, providers.get(position).toJson().toString());
				context.startActivity(detailIntent);
			}
		});*/
		
		return convertView;
	}
	
}