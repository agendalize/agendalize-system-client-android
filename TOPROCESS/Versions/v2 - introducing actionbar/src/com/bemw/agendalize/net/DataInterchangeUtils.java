package com.bemw.agendalize.net;

import org.json.JSONException;
import org.json.JSONObject;

import com.bemw.agendalize.InternalConstants;

import android.content.Context;
import android.content.SharedPreferences;

public class DataInterchangeUtils {
	
	public static JSONObject getProviderSearchParamsAsJson(Context context, int offset, String searched) {
		SharedPreferences privatePrefs = context.getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		
		JSONObject obj = new JSONObject();
		try {
			obj.put("partnerName", InternalConstants.DEVICEUSER);
			obj.put("password", InternalConstants.DEVICEPASS);
			obj.put("webAddress", searched);
			obj.put("offSet", offset);
			obj.put("maxProfessionals", InternalConstants.MAX_PROFESSIONALS);
			obj.put("pageSize", InternalConstants.PAGE_SIZE);
			String lat = privatePrefs.getString(InternalConstants.LATITUDE, null);
			String lon = privatePrefs.getString(InternalConstants.LONGITUDE, null);
			obj.put("latitude", lat);
			obj.put("longitude", lon);
			return obj;
		} catch (JSONException e) {
			return null;
		}
	}
	
}
