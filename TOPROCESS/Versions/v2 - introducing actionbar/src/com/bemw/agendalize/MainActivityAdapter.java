package com.bemw.agendalize;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bemw.agendalize.model.SidebarRowModel;

public class MainActivityAdapter extends BaseAdapter {

	private Context context;
	private List<SidebarRowModel> rows;
	
	public MainActivityAdapter(Context context, List<SidebarRowModel> rows) {
		this.context = context;
		this.rows = rows;
	}
	
	@Override
	public int getCount() {
		return (rows != null ? rows.size() : 0);
	}

	@Override
	public Object getItem(int position) {
		return rows.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("deprecation") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_sidemenu, null);
		}
		ImageView icon = ((ImageView)convertView.findViewById(R.id.row_sidebar_icon));
		icon.setBackgroundDrawable(rows.get(position).getIcon());
		TextView label = ((TextView)convertView.findViewById(R.id.row_sidebar_label));
		label.setText(rows.get(position).getLabel());
		return convertView;
	}
}
