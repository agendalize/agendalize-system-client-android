package com.bemw.agendalize.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class StoreModel {
	
	private long id;
	private String name;
	private String companyName;
	private String description;
	private String cnpj;
	private AddressModel address;
	private ContactsModel contact;
	private List<ListOperation> listOperation;
	private List<ListServices> listServices;
	
	public StoreModel() {
		id = -1;
		name = "name";
		companyName = "companyName";
		description = "description";
		cnpj = "cnpj";
		address = new AddressModel();
		contact = new ContactsModel();
		listOperation = new ArrayList<ListOperation>();
		listServices = new ArrayList<ListServices>();
	}
	public StoreModel(long id, String name, String companyName, String description,
			String cnpj, AddressModel address, ContactsModel contact,
			List<ListOperation> listOperation, List<ListServices> listServices) {
		this();
		this.id = id;
		this.name = name;
		this.companyName = companyName;
		this.description = description;
		this.cnpj = cnpj;
		this.address = address;
		this.contact = contact;
		this.listOperation = listOperation;
		this.listServices = listServices;
	}
	public StoreModel(JSONObject obj) {
		this();
		try { id = obj.getLong("id"); } catch (Exception e) {}
		try { name = obj.getString("name"); } catch (Exception e) {}
		try { companyName = obj.getString("companyName"); } catch (Exception e) {}
		try { description = obj.getString("description"); } catch (Exception e) {}
		try { cnpj = obj.getString("cnpj"); } catch (Exception e) {}
		try { address = new AddressModel(obj.getJSONObject("address")); } catch (Exception e) {}
		try { contact = new ContactsModel(obj.getJSONObject("contact")); } catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("listOperation");
			for (int i=0; i<arr.length(); i++) {
				listOperation.add(new ListOperation(arr.getJSONObject(i)));
			}
		} catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("listServices");
			for (int i=0; i<arr.length(); i++) {
				listServices.add(new ListServices(arr.getJSONObject(i)));
			}
		} catch (Exception e) {}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public AddressModel getAddress() {
		return address;
	}
	public void setAddress(AddressModel address) {
		this.address = address;
	}
	public ContactsModel getContact() {
		return contact;
	}
	public void setContact(ContactsModel contact) {
		this.contact = contact;
	}
	public List<ListOperation> getListOperation() {
		return listOperation;
	}
	public void setListOperation(List<ListOperation> listOperation) {
		this.listOperation = listOperation;
	}
	public List<ListServices> getListServices() {
		return listServices;
	}
	public void setListServices(List<ListServices> listServices) {
		this.listServices = listServices;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("id", id); } catch(Exception e) {}
		try { obj.put("name", name); } catch(Exception e) {}
		try { obj.put("companyName", companyName); } catch(Exception e) {}
		try { obj.put("description", description); } catch(Exception e) {}
		try { obj.put("cnpj", cnpj); } catch(Exception e) {}
		try { obj.put("address", address.toJson()); } catch (Exception e) {}
		try { obj.put("contact", contact.toJson()); } catch (Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<listOperation.size(); i++) {
				arr.put(listOperation.get(i).toJson());
			}
			obj.put("listOperation", arr);
		} catch (Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<listServices.size(); i++) {
				arr.put(listServices.get(i).toJson());
			}
			obj.put("listServices", arr);
		} catch (Exception e) {}
		return obj;
	}

}
