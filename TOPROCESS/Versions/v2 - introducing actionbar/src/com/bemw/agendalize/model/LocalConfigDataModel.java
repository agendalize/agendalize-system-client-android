package com.bemw.agendalize.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bemw.agendalize.util.LanguageTools;

public class LocalConfigDataModel {
	
	// user personal information
	private String fullName;
	private String birthday;
	private String document;
	private ContactsModel contacts;
	// app features
	private boolean useAlarm ;
	private int preAlarm;
	private String lang;
	private List<StoreModel> favorites;
	private List<ScheduleModel> bookings;
	
	public LocalConfigDataModel() {
		this.fullName = "";
		this.birthday = "";
		this.document = "";
		this.contacts = new ContactsModel();
		this.useAlarm = false;
		this.preAlarm = 0;
		this.lang = LanguageTools.LANGUAGES.ENGLISH.toString().toLowerCase(Locale.getDefault());
		this.favorites = new ArrayList<StoreModel>();
		this.bookings = new ArrayList<ScheduleModel>();
	}
	public LocalConfigDataModel(String fullName, String birthday, String document, ContactsModel contacts, boolean useAlarm, int preAlarm, String lang, List<StoreModel> favorites, ArrayList<ScheduleModel> bookings) {
		this();
		this.fullName = fullName;
		this.birthday = birthday;
		this.document = document;
		this.contacts = contacts;
		this.useAlarm = useAlarm;
		this.preAlarm = preAlarm;
		this.lang = lang;
		this.favorites = favorites;
		this.bookings = bookings;
	}
	public LocalConfigDataModel(JSONObject obj) {
		this();
		try { fullName = obj.getString("fullName"); } catch (Exception e) {}
		try { birthday = obj.getString("birthday"); } catch (Exception e) {}
		try { document = obj.getString("document"); } catch (Exception e) {}
		try { contacts = new ContactsModel(obj.getJSONObject("contacts")); } catch (Exception e) {}
		try { useAlarm = obj.getBoolean("useAlarm"); } catch (Exception e) {}
		try { preAlarm = obj.getInt("preAlarm"); } catch (Exception e) {}
		try { lang = obj.getString("lang"); } catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("favorites");
			for (int i=0; i<arr.length(); i++) {
				favorites.add(new StoreModel(arr.getJSONObject(i)));
			}
		} catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("bookings");
			for (int i=0; i<arr.length(); i++) {
				bookings.add(new ScheduleModel(arr.getJSONObject(i)));
			}
		} catch (Exception e) {}
	}

	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public ContactsModel getContacts() {
		return contacts;
	}
	public void setContacts(ContactsModel contacts) {
		this.contacts = contacts;
	}
	public boolean isUseAlarm() {
		return useAlarm;
	}
	public void setUseAlarm(boolean useAlarm) {
		this.useAlarm = useAlarm;
	}
	public int getPreAlarm() {
		return preAlarm;
	}
	public void setPreAlarm(int preAlarm) {
		this.preAlarm = preAlarm;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public List<StoreModel> getFavorites() {
		return favorites;
	}
	public void setFavorites(List<StoreModel> favorites) {
		this.favorites = favorites;
	}
	public List<ScheduleModel> getBookings() {
		return bookings;
	}
	public void setBookings(List<ScheduleModel> bookings) {
		this.bookings = bookings;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("fullName", fullName); } catch (Exception e) {}
		try { obj.put("birthday", birthday); } catch (Exception e) {}
		try { obj.put("document", document); } catch (Exception e) {}
		try { obj.put("contacts", contacts.toJson()); } catch (Exception e) {}
		try { obj.put("useAlarm", useAlarm); } catch (Exception e) {}
		try { obj.put("preAlarm", preAlarm); } catch (Exception e) {}
		try { obj.put("lang", lang); } catch (Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<favorites.size(); i++) {
				arr.put(favorites.get(i).toJson());
			}
			obj.put("favorites", arr);
		} catch (Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<bookings.size(); i++) {
				arr.put(bookings.get(i).toJson());
			}
			obj.put("bookings", arr);
		} catch (Exception e) {}
		return obj;
	}

}
