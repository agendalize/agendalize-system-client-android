package com.bemw.agendalize.model;

import org.json.JSONObject;

public class AddressModel {
	
	private String address;
	private String addressNumber;
	private String complement;
	private String zipCode;
	private String city;
	private String state;
	private String country;
	
	public AddressModel() {
		address = "address";
		addressNumber = "addressNumber";
		complement = "complement";
		zipCode = "zipCode";
		city = "city";
		state = "state";
		country = "country";
	}
	public AddressModel(String address, String addressNumber,
			String complement, String zipCode, String city, String state,
			String country) {
		this();
		this.address = address;
		this.addressNumber = addressNumber;
		this.complement = complement;
		this.zipCode = zipCode;
		this.city = city;
		this.state = state;
		this.country = country;
	}
	public AddressModel(JSONObject obj) {
		this();
		try { address = obj.getString("address"); } catch (Exception e) {}
		try { addressNumber = obj.getString("addressNumber"); } catch (Exception e) {}
		try { complement = obj.getString("complement"); } catch (Exception e) {}
		try { zipCode = obj.getString("zipCode"); } catch (Exception e) {}
		try { city = obj.getString("city"); } catch (Exception e) {}
		try { state = obj.getString("state"); } catch (Exception e) {}
		try { country = obj.getString("country"); } catch (Exception e) {}
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressNumber() {
		return addressNumber;
	}
	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}
	public String getComplement() {
		return complement;
	}
	public void setComplement(String complement) {
		this.complement = complement;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("address", address); } catch (Exception e) {}
		try { obj.put("addressNumber", addressNumber); } catch (Exception e) {}
		try { obj.put("complement", complement); } catch (Exception e) {}
		try { obj.put("zipCode", zipCode); } catch (Exception e) {}
		try { obj.put("city", city); } catch (Exception e) {}
		try { obj.put("state", state); } catch (Exception e) {}
		try { obj.put("country", country); } catch (Exception e) {}
		return obj;
	}
	
}
