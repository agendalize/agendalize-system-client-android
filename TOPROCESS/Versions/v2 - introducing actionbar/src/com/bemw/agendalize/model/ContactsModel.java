package com.bemw.agendalize.model;

import org.json.JSONObject;

public class ContactsModel {
	
	private String phoneNumberOne;
	private String phoneNumberTwo;
	private String email;
	
	public ContactsModel() {
		phoneNumberOne = "";
		phoneNumberTwo = "";
		email = "";
	}
	public ContactsModel(String phoneNumberOne, String phoneNumberTwo,
			String email) {
		this();
		this.phoneNumberOne = phoneNumberOne;
		this.phoneNumberTwo = phoneNumberTwo;
		this.email = email;
	}
	public ContactsModel(JSONObject obj) {
		this();
		try { phoneNumberOne = obj.getString("phoneNumberOne"); } catch (Exception e) {}
		try { phoneNumberTwo = obj.getString("phoneNumberTwo"); } catch (Exception e) {}
		try { email = obj.getString("email"); } catch (Exception e) {}
	}

	public String getPhoneNumberOne() {
		return phoneNumberOne;
	}
	public void setPhoneNumberOne(String phoneNumberOne) {
		this.phoneNumberOne = phoneNumberOne;
	}
	public String getPhoneNumberTwo() {
		return phoneNumberTwo;
	}
	public void setPhoneNumberTwo(String phoneNumberTwo) {
		this.phoneNumberTwo = phoneNumberTwo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("phoneNumberOne", phoneNumberOne); } catch (Exception e) {}
		try { obj.put("phoneNumberTwo", phoneNumberTwo); } catch (Exception e) {}
		try { obj.put("email", email); } catch (Exception e) {}
		return obj;
	}

}
