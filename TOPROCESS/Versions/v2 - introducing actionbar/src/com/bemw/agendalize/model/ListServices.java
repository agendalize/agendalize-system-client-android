package com.bemw.agendalize.model;

import org.json.JSONObject;

public class ListServices {
	
	private long cod;
	private String shortName;
	private String description;
	private long codTypeService;
	private String price;
	private long timeMinutes;
	
	public ListServices() {
		cod = -1;
		shortName = "shortName";
		description = "description";
		codTypeService = -1;
		price = "price";
		timeMinutes = -1;
	}
	public ListServices(long cod, String shortName, String description,
			long codTypeService, String price, long timeMinutes) {
		super();
		this.cod = cod;
		this.shortName = shortName;
		this.description = description;
		this.codTypeService = codTypeService;
		this.price = price;
		this.timeMinutes = timeMinutes;
	}
	public ListServices(JSONObject obj) {
		this();
		try { cod = obj.getLong("cod"); } catch (Exception e) {}
		try { shortName = obj.getString("shortName"); } catch (Exception e) {}
		try { description = obj.getString("description"); } catch (Exception e) {}
		try { codTypeService = obj.getLong("codTypeService"); } catch (Exception e) {}
		try { price = obj.getString("price"); } catch (Exception e) {}
		try { timeMinutes = obj.getLong("timeMinutes"); } catch (Exception e) {}
	}
	
	public long getCod() {
		return cod;
	}
	public void setCod(long cod) {
		this.cod = cod;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getCodTypeService() {
		return codTypeService;
	}
	public void setCodTypeService(long codTypeService) {
		this.codTypeService = codTypeService;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public long getTimeMinutes() {
		return timeMinutes;
	}
	public void setTimeMinutes(long timeMinutes) {
		this.timeMinutes = timeMinutes;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("cod", cod); } catch (Exception e) {}
		try { obj.put("shortName", shortName); } catch (Exception e) {}
		try { obj.put("description", description); } catch (Exception e) {}
		try { obj.put("codTypeService", codTypeService); } catch (Exception e) {}
		try { obj.put("price", price); } catch (Exception e) {}
		try { obj.put("timeMinutes", timeMinutes); } catch (Exception e) {}
		return obj;
	}	

}
