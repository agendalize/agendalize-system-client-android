package com.bemw.agendalize;

import java.util.ArrayList;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.SidebarRowModel;
import com.bemw.agendalize.ui.AboutActivity;
import com.bemw.agendalize.ui.BookmarkFragment;
import com.bemw.agendalize.ui.LocalConfigDataActivity;
import com.bemw.agendalize.ui.PromoteFragment;
import com.bemw.agendalize.ui.ScheduleFragment;
import com.bemw.agendalize.ui.StoreSearchFragment;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {

	private String[] mPlanetTitles;
	private Drawable[] mPlanetIcons;
	private ListView mDrawerList;
	
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private List<SidebarRowModel> mDrawerRows;
	private CharSequence mDrawerTitle;
	private Drawable mDrawerIcon;
	private CharSequence mTitle;
	private Drawable mIcon;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_main);

		mPlanetTitles = new String[] {
				getResources().getString(R.string.menu_myschedule),
				getResources().getString(R.string.menu_searchstore),
				getResources().getString(R.string.menu_mybookmark),
				getResources().getString(R.string.menu_promote)
		};
		mPlanetIcons = new Drawable[] {
				getResources().getDrawable(R.drawable.ic_action_go_to_today_hd),
				getResources().getDrawable(R.drawable.ic_action_search_hd),
				getResources().getDrawable(R.drawable.ic_action_important_hd),
				getResources().getDrawable(R.drawable.ic_action_labels_hd)
		};
				
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerRows = new ArrayList<SidebarRowModel>();
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[0], getResources().getDrawable(R.drawable.ic_action_go_to_today)));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[1], getResources().getDrawable(R.drawable.ic_action_search)));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[2], getResources().getDrawable(R.drawable.ic_action_important)));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[3], getResources().getDrawable(R.drawable.ic_action_labels)));
		mDrawerList.setAdapter(new MainActivityAdapter(this, mDrawerRows));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
			// Called when a drawer has settled in a completely closed state
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getSupportActionBar().setTitle(mTitle);
				getSupportActionBar().setIcon(mIcon);
			}
			// Called when a drawer has settled in a completely open state
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getSupportActionBar().setTitle(mDrawerTitle);
				getSupportActionBar().setIcon(mDrawerIcon);
			}
		};
		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

   		if (savedInstanceState == null)
			selectItem(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch(item.getItemId()) {
			case R.id.menu_settings:
				startActivity(new Intent(this, LocalConfigDataActivity.class));
				break;
			case R.id.menu_about:
				startActivity(new Intent(this, AboutActivity.class));
				break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
			// Highlight the selected item, update the title, and close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerLayout.closeDrawer(mDrawerList);
		}
	}
	
	/** Swaps fragments in the main content view */
	private void selectItem(int position) {
		Fragment fragment;
		FragmentManager fragmentManager = getSupportFragmentManager();
		switch (position) {
		case 0:
			fragment = new ScheduleFragment();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			break;
		case 1:
			fragment = new StoreSearchFragment();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			break;
		case 2:
			fragment = new BookmarkFragment();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			break;
		case 3:
			fragment = new PromoteFragment();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			break;
		default:
			break;
		}
		setTitle(mPlanetTitles[position]);
		setIcon(mPlanetIcons[position]);
	}
	
	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}
	
	public void setIcon(Drawable icon) {
		mIcon = icon;
		getSupportActionBar().setIcon(mIcon);
	}

}
