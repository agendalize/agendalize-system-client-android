package com.bemw.agendalize.dao;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.model.LocalConfigDataModel;

public class LocalConfigDAO {
	
	// local config persistence
	public static LocalConfigDataModel getLocalConfig(Context context) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		String lu = prefsPrivate.getString(InternalConstants.LOCAL_USER, null);
		try {
			return new LocalConfigDataModel(new JSONObject(lu));
		} catch (Exception e) {
			return new LocalConfigDataModel();
		}
	}
	public static void setLocalConfig(Context context, LocalConfigDataModel config) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		Editor prefsPrivateEditor = prefsPrivate.edit();
		prefsPrivateEditor.putString(InternalConstants.LOCAL_USER, config.toJson().toString());
		prefsPrivateEditor.commit();
	}
	
}
