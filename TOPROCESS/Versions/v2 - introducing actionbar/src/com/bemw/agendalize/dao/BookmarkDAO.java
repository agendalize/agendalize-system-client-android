package com.bemw.agendalize.dao;

import org.json.JSONObject;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.model.BookmarkModel;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class BookmarkDAO {

	public static BookmarkModel loadBookmark(Context context) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		String bookmark_data = prefsPrivate.getString(InternalConstants.BOOKMARK, null);
		BookmarkModel bookmark = new BookmarkModel();
		try {
			return bookmark.fromJson(new JSONObject(bookmark_data));
		} catch (Exception e) {
			return bookmark;
		}
	}
	
	public static void saveBookmark(Context context, BookmarkModel bookmark) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		Editor prefsPrivateEditor = prefsPrivate.edit();
		prefsPrivateEditor.putString(InternalConstants.BOOKMARK, bookmark.toJson().toString());
		prefsPrivateEditor.commit();
	}
	
}
