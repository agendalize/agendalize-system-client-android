package com.bemw.agendalize.ui.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.SideBarRowModel;

public class MainMenuAdapter extends BaseAdapter {
	
	private Context context;
	private List<SideBarRowModel> rows;
	
	public MainMenuAdapter(Context context, List<SideBarRowModel> rows) {
		this.context = context;
		this.rows = rows;
	}
	
	@Override
	public int getCount() {
		return (rows != null ? rows.size() : 0);
	}
	
	@Override
	public Object getItem(int position) {
		return rows.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_sidemenu, parent, false);
		}
		((ImageView)convertView.findViewById(R.id.row_sidebar_icon)).setBackgroundResource(rows.get(position).getIconId());
		((TextView)convertView.findViewById(R.id.row_sidebar_label)).setText(context.getResources().getString(rows.get(position).getLabelId()));
		return convertView;
	}
	
}
