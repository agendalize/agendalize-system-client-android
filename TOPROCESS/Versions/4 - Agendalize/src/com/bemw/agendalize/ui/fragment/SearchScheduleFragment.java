package com.bemw.agendalize.ui.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONArray;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.model.StoreSearchRequestModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.ui.AvailableServicesActivity;
import com.bemw.agendalize.ui.ChooseDateHourActivity;
import com.bemw.agendalize.ui.StoreListActivity;
import com.bemw.agendalize.util.DeviceUtils;

/**
 * @author Marcelo Tomio Hama
 * An activity intended to prompt users for a filtered search overs all stores
 */
public class SearchScheduleFragment extends Fragment {

	private static final int DATEHOUR_RETURN_VALUE = 0;
	private static final int SERVICE_RETURN_VALUE = 1;
	// data for searches
	private static String baseAddress = null;
	private static int rangeIndex;
	private static int hourFrom;
	private static int hourTo;
	private static Calendar dayTm;
	private static List<ServiceModel> services = null;
    private static StoreListActivity context;
	// data for store fetch
	private static double LatLon[] = null;
	private static ProgressDialog waitDialog = null;
	private static Thread locationLoader = null;
	private static Thread contentLoader = null;
	private static boolean isLoading = false;
	private static List<StoreModel> stores = null;
	// widgets, we maintain them as global fields because we need reference to them we call search web service
	private EditText locationEdit = null;
	private Spinner rangeCb = null;
	private ImageButton reloadBtn = null;
	private ProgressBar progress = null;
	private Button dayhourBtn = null;
	private Button serviceBtn1 = null;
	private Button serviceBtn2 = null;
	private Button serviceBtn3 = null;
	private Button serviceBtn4 = null;
    private ScrollView scroll = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_searchschedule, container, false);
        context = (StoreListActivity)getActivity();

        locationEdit = (EditText) rootView.findViewById(R.id.frag_searchschedule_locationEdit);
        rangeCb = (Spinner) rootView.findViewById(R.id.frag_searchschedule_rangecb);
        reloadBtn = (ImageButton) rootView.findViewById(R.id.frag_searchschedule_service_reloadbtn);
        progress = (ProgressBar) rootView.findViewById(R.id.frag_searchschedule_service_progress);
        dayhourBtn = (Button) rootView.findViewById(R.id.frag_searchschedule_datehourbtn);
        serviceBtn1 = (Button) rootView.findViewById(R.id.frag_searchschedule_service1btn);
        serviceBtn2 = (Button) rootView.findViewById(R.id.frag_searchschedule_service2btn);
        serviceBtn3 = (Button) rootView.findViewById(R.id.frag_searchschedule_service3btn);
        serviceBtn4 = (Button) rootView.findViewById(R.id.frag_searchschedule_service4btn);
        scroll = (ScrollView) rootView.findViewById(R.id.frag_searchschedule_scroll);

        // actions for navigation
        ((Button) rootView.findViewById(R.id.frag_searchschedule_clearbtn)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getDefaultValuesForFields();
                onResume();
            }
        });
        ((Button) rootView.findViewById(R.id.frag_searchschedule_searchbtn)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldsOk()) {
                    createNewFetcher();
                }
            }
        });
        getDefaultValuesForFields();

        // load list of available services
        if (AvailableServicesActivity.services == null) {
            new ServiceLoader().execute();
        } else if (AvailableServicesActivity.services.size() == 0) {
            new ServiceLoader().execute();
        } else {
            serviceBtn1.setEnabled(true);
            serviceBtn2.setEnabled(true);
            serviceBtn3.setEnabled(true);
            serviceBtn4.setEnabled(true);
            ((ProgressBar) rootView.findViewById(R.id.frag_searchschedule_service_progress)).setVisibility(View.GONE);
        }
        drawWidgets();

        return rootView;
    }

    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (waitDialog != null) waitDialog.dismiss();
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                	DeviceUtils.justShowDialogWarning(context, R.string.label_timeout);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                	DeviceUtils.justShowDialogWarning(context, R.string.label_noconnection);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                	DeviceUtils.justShowDialogWarning(context, R.string.label_servererror);
                    break;
                case HttpStatus.SC_OK:
                    if (LatLon != null && stores == null) { // got location
                        isLoading = false;
                        createNewLoader();
                    } else if (LatLon != null && stores != null) { // got to next view
                        if (stores.size() == 0) {
                        	DeviceUtils.justShowDialogWarning(context, R.string.label_noresults);
                        } else {
                            /*Intent storeList = new Intent(context, SetOfStoresActivity.class);
                            JSONArray arr = new JSONArray();
                            for (int i=0; stores!=null && i<stores.size(); i++) {
                                arr.put(stores.get(i).toJson());
                            }
                            storeList.putExtra("stores", arr.toString());
                            storeList.putExtra("searchParams", getSearchParams(LatLon).toJson().toString());
                            context.startActivity(storeList);*/
                        }
                    }
                    break;
            }
            isLoading = false;
        }
    };

    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                stores = client.getStoresByContext(getSearchParams(LatLon).toJson());
                // TODO: remove bellow snippet and let stores don't be updated
                stores = new ArrayList<StoreModel>();
                for (int i=0; i<6; i++) {
                    stores.add(new StoreModel());
                }
                // TODO: remove above snippet and let stores don't be updated
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };

    private static final Runnable fetchLocation = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                LatLon = client.getLocationInfo(baseAddress);
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };

    synchronized private static void createNewLoader() {
        if (!DeviceUtils.isConnectedToInternet(context)) {
        	DeviceUtils.justShowDialogWarning(context, R.string.label_noconnection);
        } else if (!isLoading) {
            isLoading = true;
            waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.label_sendingdata),
                    context.getText(R.string.label_pleasewait),
                    false,
                    false);
            waitDialog.setIcon(context.getResources().getDrawable(R.drawable.ic_launcher));
            contentLoader = new Thread(loadContent);
            contentLoader.start();
        }
    }

    synchronized private static void createNewFetcher() {
        if (!DeviceUtils.isConnectedToInternet(context)) {
        	DeviceUtils.justShowDialogWarning(context, R.string.label_noconnection);
        } else if (!isLoading) {
            isLoading = true;
            waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.label_sendingdata),
                    context.getText(R.string.label_pleasewait),
                    false,
                    false);
            waitDialog.setIcon(context.getResources().getDrawable(R.drawable.ic_launcher));
            locationLoader = new Thread(fetchLocation);
            locationLoader.start();
        }
    }

    private static StoreSearchRequestModel getSearchParams(double[] LatLon) {
        // construct the json request
        StoreSearchRequestModel request = new StoreSearchRequestModel();
        for (int i=0; i<services.size(); i++) request.getServices().add(services.get(i).getCod());
        request.setLatitude(LatLon[0]);
        request.setLongitude(LatLon[1]);
        request.setHourFrom(String.format("%02d", (hourFrom/3600))+String.format("%02d",(hourFrom%60)));
        request.setHourTo(String.format("%02d", (hourTo/3600))+String.format("%02d",(hourTo%60)));
        request.setBaseDate(dayTm.getTime().getTime());
        request.setRange(((rangeIndex == 0 ? 10 : (rangeIndex == 1 ? 25 : 50)))*1000);

        // TODO: remove bellow snippet and return request
        request = new StoreSearchRequestModel(
                1, new ArrayList<Long>(Arrays.asList(51l)), 5, -23.6267108d, -46.7081527d, "0800", "1800", 1407801600000l, 5000l
        );
        // TODO: remove above snippet and return request

        return request;
    }

    private boolean isFieldsOk() {
        if (services != null) if (services.size() > 0) if (baseAddress != null) if (baseAddress.length() > 0) return true;
        DeviceUtils.justShowDialogWarning(context, R.string.label_selectatleast1service);
        return false;
    }

    private void drawWidgets() {
        // location address data, need to be in onResume to proper update on activityForResult return
        locationEdit.setText(baseAddress);
        locationEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {
                baseAddress = locationEdit.getText().toString();
            }
        });
        // range, need to be in onResume to proper update on activityForResult return
        ArrayAdapter<CharSequence> spinnerArrayAdapter = new ArrayAdapter<CharSequence>(
                context, android.R.layout.simple_spinner_item, new CharSequence[] {
                getText(R.string.label_distanceby10km),
                getText(R.string.label_distanceby25km),
                getText(R.string.label_distanceby50km) });
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rangeCb.setAdapter(spinnerArrayAdapter);
        rangeCb.setSelection(rangeIndex);
        rangeCb.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int selectedItem, long arg3) {
                rangeIndex = selectedItem;
            }
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
        // date and hour, need to be in onResume to proper update on activityForResult return
        dayhourBtn.setText(
                String.format("%02d", dayTm.get(Calendar.DAY_OF_MONTH))
                        +"/"+String.format("%02d", dayTm.get(Calendar.MONTH))
                        +"/"+String.format("%04d", dayTm.get(Calendar.YEAR))
                        +" - "+getResources().getString(R.string.label_fromhour)
                        +" "+String.format("%02d", (hourFrom/3600))+":"+String.format("%02d",(hourFrom%60))
                        +" "+getResources().getString(R.string.label_tohour)
                        +" "+String.format("%02d", (hourTo/3600))+":"+String.format("%02d",(hourTo%60))
        );
        dayhourBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ChooseDateHourActivity.class);
                i.putExtra("dayTm", dayTm.getTime().getTime());
                i.putExtra("fromHour", hourFrom);
                i.putExtra("toHour", hourTo);
                startActivityForResult(i, DATEHOUR_RETURN_VALUE);
            }
        });
        // selected services, need to be in onResume to proper update on activityForResult return
        if (services.size() > 0) serviceBtn1.setText(services.get(0).getShortName());
        else serviceBtn1.setText(getResources().getString(R.string.label_noservice));
        serviceBtn1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 1) serviceBtn2.setText(services.get(1).getShortName());
        else serviceBtn2.setText(getResources().getString(R.string.label_noservice));
        serviceBtn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 2) serviceBtn3.setText(services.get(2).getShortName());
        else serviceBtn3.setText(getResources().getString(R.string.label_noservice));
        serviceBtn3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 3) serviceBtn4.setText(services.get(3).getShortName());
        else serviceBtn4.setText(getResources().getString(R.string.label_noservice));
        serviceBtn4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        // scrolls to top
        scroll.scrollTo(0, 0);
        scroll.scrollTo(0, 0);
    }

    private void getDefaultValuesForFields() {
        LatLon = null;
        stores = null;
        baseAddress = "";
        rangeIndex = 0;
        dayTm = Calendar.getInstance();
        hourFrom = 0;
        hourTo = 86399;
        services = new ArrayList<ServiceModel>();
    }

    //===================================================================================
    // Inner classes
    //===================================================================================

    private class ServiceLoader extends AsyncTask<Void, Void, Void> {
        private RESTClient client = new RESTClient();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            serviceBtn1.setEnabled(false);
            serviceBtn2.setEnabled(false);
            serviceBtn3.setEnabled(false);
            serviceBtn4.setEnabled(false);
            reloadBtn.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            client = new RESTClient();
            AvailableServicesActivity.services = client.getServicesList();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (client.getStatus() == HttpStatus.SC_OK) {
                serviceBtn1.setEnabled(true);
                serviceBtn2.setEnabled(true);
                serviceBtn3.setEnabled(true);
                serviceBtn4.setEnabled(true);
            } else {
                reloadBtn.setVisibility(View.VISIBLE);
                reloadBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reloadBtn.setVisibility(View.GONE);
                        new ServiceLoader().execute();
                    }
                });
            }
            progress.setVisibility(View.GONE);
        }
    }
	
}
