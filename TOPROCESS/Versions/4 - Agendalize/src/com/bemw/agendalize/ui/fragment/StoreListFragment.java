package com.bemw.agendalize.ui.fragment;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.model.StoreSearchRequestModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.ui.StoreListActivity;
import com.bemw.agendalize.ui.adapter.StoreListAdapter;
import com.bemw.agendalize.util.DeviceUtils;

import org.apache.http.HttpStatus;

import java.util.Calendar;
import java.util.List;

/**
 * Created by mthama on 31/01/15.
 */
public class StoreListFragment extends Fragment {

    private static StoreListActivity context;
    private static ListView storeList;
    // data for searches
    private static String baseAddress = null;
    private static LocationManager lm;
    private static Location location;
    // data for store fetch
    private static double LatLon[] = null;
    //private static ProgressDialog waitDialog = null;
    private static Thread locationLoader = null;
    private static Thread contentLoader = null;
    private static boolean isLoading = false;
    private static List<StoreModel> stores = null;
    private static StoreListAdapter storeListAdapter;
    private static int lastLoadCount = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storelist, container, false);
        context = (StoreListActivity)getActivity();

        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 10, locationListener);

        // widgets and components
        storeList = (ListView) rootView.findViewById(R.id.frag_storelist_listview);
        storeList.setVisibility(View.VISIBLE);
        storeListAdapter = new StoreListAdapter(context, stores);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        createNewLoader();
    }

    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //if (waitDialog != null) waitDialog.dismiss();
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                	DeviceUtils.justShowDialogWarning(context, R.string.label_timeout);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                	DeviceUtils.justShowDialogWarning(context, R.string.label_noconnection);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                	DeviceUtils.justShowDialogWarning(context, R.string.label_servererror);
                    break;
                case HttpStatus.SC_OK:
                    if (LatLon != null && stores == null) { // got location
                        isLoading = false;
                        createNewLoader();
                    } else if (LatLon != null && stores != null) { // got to next view
                        if (stores.size() == 0) {
                        	DeviceUtils.justShowDialogWarning(context, R.string.label_noresults);
                        } else {
                            lastLoadCount = stores.size();
                            storeList.setAdapter(storeListAdapter);
                            storeList.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(AbsListView view, int scrollState) {}
                                @Override
                                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    if (
                                        storeList.getLastVisiblePosition()+1 == totalItemCount && // user has reached the end of the list
                                        storeList.getAdapter().getCount() >= AppConstants.SIZE_LIST && // only enable pagination if we already done at least 1 load
                                        lastLoadCount == AppConstants.SIZE_LIST && // last load had fetch a complete page
                                        lastLoadCount != -1
                                    ) createNewLoader();
                                }
                            });
                            // add the informative item, if there is more contacts to load
                            if (
                                stores.size() > 0 && // it makes no sense to show "loading more" with an empty list
                                lastLoadCount == AppConstants.SIZE_LIST
                            ) {
                                StoreModel m = new StoreModel();
                                m.setName("LOAD_MORE");
                                stores.add(m);
                                storeListAdapter = new StoreListAdapter(context, stores);
                                storeListAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                    break;
            }
            isLoading = false;
        }
    };

    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                stores = client.getStoresByContext(getSearchParams(LatLon).toJson());
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };

    synchronized private static void createNewLoader() {
        if (!DeviceUtils.isConnectedToInternet(context)) {
        	DeviceUtils.justShowDialogWarning(context, R.string.label_noconnection);
        } else if (!isLoading) {
            isLoading = true;
            /*waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.label_sendingdata),
                    context.getText(R.string.label_pleasewait),
                    false,
                    false);
            waitDialog.setIcon(context.getResources().getDrawable(R.drawable.ic_launcher));*/
            contentLoader = new Thread(loadContent);
            contentLoader.start();
        }
    }

    private static StoreSearchRequestModel getSearchParams(double[] LatLon) {
        // construct the json request
        Calendar dayTm = Calendar.getInstance();
        StoreSearchRequestModel request = new StoreSearchRequestModel();
        request.setLatitude(LatLon[0]);
        request.setLongitude(LatLon[1]);
        request.setBaseDate(dayTm.getTime().getTime());
        request.setRange(50000);
        return request;
    }

    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            LatLon = new double[2];
            LatLon[0] = location.getLongitude();
            LatLon[1] = location.getLatitude();
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onProviderDisabled(String provider) {}
    };

}
