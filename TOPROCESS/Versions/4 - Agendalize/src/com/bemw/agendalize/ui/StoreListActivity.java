package com.bemw.agendalize.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.bemw.agendalize.R;
import com.bemw.agendalize.ui.fragment.SearchScheduleFragment;
import com.bemw.agendalize.ui.fragment.StoreListFragment;
import com.bemw.agendalize.ui.fragment.StoreMapFragment;

/**
 * Created by mthama on 31/01/15.
 */
public class StoreListActivity extends ActionBarActivity {

    private Menu menu;
    private static final int NUM_PAGES = 3;
    private ViewPager mPager = null;
    private PagerAdapter mPagerAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storelist);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.activity_storelist_pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {}
            @Override
            public void onPageSelected(int i) {}
            @Override
            public void onPageScrollStateChanged(int i) {}
        });
        mPager.setCurrentItem(1);

        // actionbar menu
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_datesearch_w);
        getSupportActionBar().setTitle(R.string.option_stores);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_storelist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
			case R.id.action_map:
                mPager.setCurrentItem(0);
                getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.ic_overlay_w));
                getSupportActionBar().setTitle(R.string.option_map);
                if (menu != null) {
                    menu.getItem(0).setVisible(false);
                    menu.getItem(1).setVisible(false);
                }
				break;
			case R.id.action_filter:
                mPager.setCurrentItem(2);
                getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.ic_filter_w));
                getSupportActionBar().setTitle(R.string.option_filter);
                if (menu != null) {
                    menu.getItem(0).setVisible(false);
                    menu.getItem(1).setVisible(false);
                }
				break;
            case R.id.action_back:
                if (mPager.getCurrentItem() != 1) {
                    mPager.setCurrentItem(1);
                    getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.ic_datesearch_w));
                    getSupportActionBar().setTitle(R.string.option_stores);
                    if (menu != null) {
                        menu.getItem(0).setVisible(true);
                        menu.getItem(1).setVisible(true);
                    }
                } else finish();
			default:;
		}
        return super.onOptionsItemSelected(item);
    }

    public ViewPager getPagerView() {
        return mPager;
    }

    //===================================================================================
    // Inner classes
    //===================================================================================

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new StoreMapFragment();
                case 1:
                    return new StoreListFragment();
                case 2:
                    return new SearchScheduleFragment();
                default:
                    return new StoreListFragment();
            }
        }
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}
