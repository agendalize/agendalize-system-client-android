package com.bemw.agendalize.ui;

import java.util.Arrays;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.SideBarRowModel;
import com.bemw.agendalize.ui.adapter.MainMenuAdapter;
import com.bemw.agendalize.ui.fragment.MainMenuFragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class HomeActivity extends ActionBarActivity {

	private int mTitle;
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	
	private final int[] mPlanetTitles = {
			R.string.option_main,
			R.string.option_profile,
			R.string.option_history,
			R.string.option_share,
			R.string.option_settings,
			R.string.option_about
	};
	private final List<SideBarRowModel> mDrawerRows = Arrays.asList(
		new SideBarRowModel(mPlanetTitles[0], R.drawable.ic_menu_w),
		new SideBarRowModel(mPlanetTitles[1], R.drawable.ic_person_w),
		new SideBarRowModel(mPlanetTitles[2], R.drawable.ic_clock_w),
		new SideBarRowModel(mPlanetTitles[3], R.drawable.ic_share_w),
		new SideBarRowModel(mPlanetTitles[4], R.drawable.ic_config_w),
		new SideBarRowModel(mPlanetTitles[5], R.drawable.ic_help_w)
	);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_home_drawerlayout);
		mDrawerList = (ListView) findViewById(R.id.activity_home_leftdrawer);
		
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		mDrawerToggle = new ActionBarDrawerToggle(
			this, mDrawerLayout, R.drawable.ic_drawer,
			R.string.app_contentDescription, R.string.app_contentDescription) {
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getSupportActionBar().setTitle(getResources().getString(mTitle));
			}
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getSupportActionBar().setTitle(getResources().getString(mTitle));
			}
		};
		mDrawerList.setAdapter(new MainMenuAdapter(this, mDrawerRows));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener(mDrawerList, mDrawerLayout));
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		getSupportActionBar().setHomeButtonEnabled(true);
	    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	    getSupportActionBar().setDisplayShowHomeEnabled(true);
	    getSupportActionBar().setIcon(R.color.transparent);
	    getSupportActionBar().setDisplayShowTitleEnabled(true);
		
		if (savedInstanceState == null) selectItem(0);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		/*switch(item.getItemId()) {
			case R.id.menu_home:
				startActivity(new Intent(this, LocalConfigDataActivity.class));
				break;
			case R.id.menu_about:
				startActivity(new Intent(this, AboutActivity.class));
				break;
			default:;
		}*/
		return super.onOptionsItemSelected(item);
	}
	
	private void selectItem(int position) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		switch (position) {
			case 0:
				fragmentManager.beginTransaction().replace(R.id.activity_home_contentframe, new MainMenuFragment()).commit();
				break;
			case 1:
				//fragmentManager.beginTransaction().replace(R.id.view_home_contentframe, ProfileFragment()).commit();
				break;
		    /*case 2:
				fragment = new BookmarkFragment();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
				break;
			case 3:
				fragment = new PromoteFragment();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
				break;*/
			/*case 5:
				fragmentManager.beginTransaction().replace(R.id.activity_menu_contentframe, new AboutFragment()).commit();
				break;*/
		default:
			break;
		}
		mTitle = mPlanetTitles[position];
		getSupportActionBar().setTitle(mTitle);
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		private ListView mDrawerList;
		private DrawerLayout mDrawerLayout;
		public DrawerItemClickListener(ListView mDrawerList, DrawerLayout mDrawerLayout) {
			this.mDrawerList = mDrawerList;
			this.mDrawerLayout = mDrawerLayout;
		}
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
			mDrawerList.setItemChecked(position, true);
			mDrawerLayout.closeDrawer(mDrawerList);
		}
	}
	
}
