package com.bemw.agendalize.ui.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.util.ImageUtils;

public class StoreListAdapter extends BaseAdapter {
	
	private Context context;
	private List<StoreModel> stores;
	
    public StoreListAdapter(Context context, List<StoreModel> stores) {
    	this.context = context;
    	this.stores = stores;
    }
    
    @Override
    public int getCount() {
        return stores.size();
    }
    
    @Override
    public Object getItem(int position) {
        return stores.get(position);
    }
    
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @SuppressLint("InflateParams") @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (stores.get(position).getName().equals("LOAD_MORE")) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_helperloading, null);
            TextView textinfo = ((TextView) convertView.findViewById(R.id.row_helperloading_infotxt));
            textinfo.setText(context.getString(R.string.label_loadingdata));
            return convertView;
        }

        // predefines the view as an inflated layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_storeitem, null);
        ImageView photo = ((ImageView) convertView.findViewById(R.id.row_storeitem_photo));
        TextView name = ((TextView) convertView.findViewById(R.id.row_storeitem_name));
        RatingBar rating = ((RatingBar)convertView.findViewById(R.id.row_storeitem_rating));
        TextView info1 = ((TextView) convertView.findViewById(R.id.row_storeitem_info1));
        TextView info2 = ((TextView) convertView.findViewById(R.id.row_storeitem_info2));
        TextView address1 = ((TextView) convertView.findViewById(R.id.row_storeitem_address1));
        TextView address2 = ((TextView) convertView.findViewById(R.id.row_storeitem_address2));
        name.setText(stores.get(position).getName());
        rating.setRating(stores.get(position).getEvaluation()*5/100);
        info1.setText(context.getResources().getString(R.string.label_nremployees) + " " + stores.get(position).getNrEmployees());
        info2.setText(context.getResources().getString(R.string.label_nrservices) + " " + stores.get(position).getNrServices());
        address1.setText(stores.get(position).getAddressInfo().getCity() + ", " + stores.get(position).getAddressInfo().getState());
        address2.setText(stores.get(position).getAddressInfo().getAddress() + " " + stores.get(position).getAddressInfo().getAddressNumber());
        if (stores.get(position).getUrlPhoto() != null) {
            if (stores.get(position).getUrlPhoto().length() > 0) {
                new PhotoLoader(context, photo, stores.get(position).getUrlPhoto()).execute();
            }
        }
        return convertView;
    }
}

class PhotoLoader extends AsyncTask<Void, Void, Void> {
	
	private Context context;
    private Drawable drawable;
    private ImageView photo;
    private String url;
    
    public PhotoLoader(Context context, ImageView photo, String url) {
    	this.context = context;
        this.photo = photo;
        this.url = url;
    }
    
    @Override
    protected Void doInBackground(Void... params) {
        drawable = ImageUtils.loadCachedImage(context, url);
        Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
        drawable = new BitmapDrawable(context.getResources(), bitmap);
        return null;
    }
    
    @SuppressWarnings("deprecation") @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        photo.setBackgroundDrawable(drawable);
    }
    
}