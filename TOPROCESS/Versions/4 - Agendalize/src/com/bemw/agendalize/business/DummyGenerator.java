package com.bemw.agendalize.business;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.bemw.agendalize.R;

public class DummyGenerator {
	
	public static Drawable getServicePhotoByType(Context context, long type) {
		switch ((int)type) {
			case 1: return context.getResources().getDrawable(R.drawable.depilacao);
			case 2: return context.getResources().getDrawable(R.drawable.cabelo);
			case 3: return context.getResources().getDrawable(R.drawable.pele);
			case 4: return context.getResources().getDrawable(R.drawable.unha);
			case 5: return context.getResources().getDrawable(R.drawable.maquiagem);
			case 6: return context.getResources().getDrawable(R.drawable.spa);
			case 7: return context.getResources().getDrawable(R.drawable.massagem);
			default: return context.getResources().getDrawable(R.drawable.noimage);
		}
	};

}
