package com.bemw.agendalize.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.model.AddressModel;
import com.bemw.agendalize.model.ContactsModel;
import com.bemw.agendalize.model.StoreModel;

public class RESTClient {
	
	private InputStream instream				= null;
	private DefaultHttpClient httpClient		= null;
	private HttpPost httpPost					= null;
	private HttpResponse response				= null;
	private HttpParams httpParameters 			= new BasicHttpParams();
	
	public RESTClient() {
		HttpConnectionParams.setConnectionTimeout(httpParameters, InternalConstants.CONN_TIME_OUT);
		HttpConnectionParams.setSoTimeout(httpParameters, InternalConstants.CONN_TIME_OUT);
		httpClient = new DefaultHttpClient(httpParameters);
	}
	
	/*public List<StoreModel> GetStoresList() {
		List<StoreModel> stores = new ArrayList<StoreModel>();
		try {
			httpPost = new HttpPost(InternalConstants.getStoresList);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				instream = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
				String inputLine = null;
				String wsResponse = "";
				while ((inputLine = reader.readLine()) != null)
					wsResponse += inputLine;
				httpClient.getConnectionManager().shutdown();
				// this bellow snippet just coerces the object to the model
				JSONArray arr = new JSONArray(inputLine);
				for (int i=0; i<arr.length(); i++) {
					JSONObject obj = arr.getJSONObject(i);
					ContactsModel contact = new ContactsModel(
							(String)obj.remove("phoneNumberOne"),
							(String)obj.remove("phoneNumberTwo"),
							(String)obj.remove("email")
					);
					AddressModel address = new AddressModel(
							(String)obj.remove("address"),
							(String)obj.remove("addressNumber"),
							(String)obj.remove("complement"),
							(String)obj.remove("zipCode"),
							(String)obj.remove("city"),
							(String)obj.remove("state"),
							(String)obj.remove("country")
					);
					obj.put("address", address.toJson());
					obj.put("contact", contact.toJson());
					stores.add(new StoreModel(obj));
				}
				disconnect();
				return stores;
			}
		} catch (UnsupportedEncodingException e) {
			Log.w(InternalConstants.TAG, "UnsupportedEncodingException: WebAddressConsult@RESTClient " + e.getMessage());
		} catch (ClientProtocolException e) {
			Log.w(InternalConstants.TAG, "ClientProtocolException: WebAddressConsult@RESTClient " +  e.getMessage());
		} catch (IOException e) {
			Log.w(InternalConstants.TAG, "IOException: WebAddressConsult@RESTClient " +  e.getMessage());
		} catch (JSONException e) {
			Log.w(InternalConstants.TAG, "JSONException: WebAddressConsult@RESTClient " +  e.getMessage());
		}
		disconnect();
		return stores;
	}*/
	
	public List<StoreModel> GetStoresList() {
		List<StoreModel> stores = new ArrayList<StoreModel>();
		try {
			InputStream is = new URL(InternalConstants.getStoresList).openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			StringBuilder sb = new StringBuilder();
			int cp;
			while ((cp = reader.read()) != -1) {
				sb.append((char) cp);
			}
			JSONArray arr = new JSONArray(sb.toString());
			for (int i=0; i<arr.length(); i++) {
				JSONObject obj = arr.getJSONObject(i);
				ContactsModel contact = new ContactsModel(
						obj.remove("phoneNumberOne").toString(),
						obj.remove("phoneNumberTwo").toString(),
						obj.remove("email").toString()
				);
				AddressModel address = new AddressModel(
						obj.remove("address").toString(),
						obj.remove("addressNumber").toString(),
						obj.remove("complement").toString(),
						obj.remove("zipCode").toString(),
						obj.remove("city").toString(),
						obj.remove("state").toString(),
						obj.remove("country").toString()
				);
				obj.put("address", address.toJson());
				obj.put("contact", contact.toJson());
				stores.add(new StoreModel(obj));
			}
			disconnect();
			return stores;
		} catch (UnsupportedEncodingException e) {
			Log.w(InternalConstants.TAG, "UnsupportedEncodingException: WebAddressConsult@RESTClient " + e.getMessage());
		} catch (ClientProtocolException e) {
			Log.w(InternalConstants.TAG, "ClientProtocolException: WebAddressConsult@RESTClient " +  e.getMessage());
		} catch (IOException e) {
			Log.w(InternalConstants.TAG, "IOException: WebAddressConsult@RESTClient " +  e.getMessage());
		} catch (JSONException e) {
			Log.w(InternalConstants.TAG, "JSONException: WebAddressConsult@RESTClient " +  e.getMessage());
		}
		disconnect();
		return stores;
	}
	
	// closes the connection
	public void disconnect() {
		try {
			httpPost.abort();
			closeInputStream();
			httpClient.getConnectionManager().shutdown();
		} catch (Exception e) {}
	}
	
	private void closeInputStream() {
		// Closing the input stream will trigger connection release
		try {
			instream.close();
		} catch (IOException e) {
			
		} catch (NullPointerException n) {

		}
	}

}
