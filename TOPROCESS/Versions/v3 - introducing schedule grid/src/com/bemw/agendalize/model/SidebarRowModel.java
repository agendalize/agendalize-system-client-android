package com.bemw.agendalize.model;

import android.graphics.drawable.Drawable;

public class SidebarRowModel {
	
	private String label = "label";
	private Drawable icon = null;
	
	public SidebarRowModel() {}
	public SidebarRowModel(String label, Drawable icon) {
		this();
		this.label = label;
		this.icon = icon;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Drawable getIcon() {
		return icon;
	}
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

}
