package com.bemw.agendalize.model;

import java.util.Calendar;
import java.util.Date;

import org.json.JSONObject;

/**
 * this class defines a date coupled with a given a city location
 * @author MTHAMA
 *
 */
public class ScheduleModel {
	
	private long id;
	// data of the store
	private long storeId;
	private String storeName;
	private AddressModel address;
	private ContactsModel contact;
	// data of the service
	private long serviceId;
	private ListServices listServices;
	// booked day/hour
	private Calendar calendar;
	
	public ScheduleModel() {
		id = -1;
		storeId = -1;
		storeName = "storeName";
		address = new AddressModel();
		contact = new ContactsModel();
		serviceId = -1;
		listServices = new ListServices();
		calendar = Calendar.getInstance();
	}	
	public ScheduleModel(long id, long storeId, String storeName,
			AddressModel address, ContactsModel contact, long serviceId,
			ListServices listServices, Calendar calendar) {
		this();
		this.id = id;
		this.storeId = storeId;
		this.storeName = storeName;
		this.address = address;
		this.contact = contact;
		this.serviceId = serviceId;
		this.listServices = listServices;
		this.calendar = calendar;
	}
	public ScheduleModel(JSONObject obj) {
		this();
		try { id = obj.getLong("id"); } catch (Exception e) {}
		try { storeId = obj.getLong("storeId"); } catch (Exception e) {}
		try { storeName = obj.getString("storeName"); } catch (Exception e) {}
		try { address = new AddressModel(obj.getJSONObject("address")); } catch (Exception e) {}
		try { contact = new ContactsModel(obj.getJSONObject("contact")); } catch (Exception e) {}
		try { serviceId = obj.getLong("serviceId"); } catch (Exception e) {}
		try { listServices = new ListServices(obj.getJSONObject("listServices")); } catch (Exception e) {}
		try { calendar.setTime(new Date(obj.getLong("calendar"))); } catch (Exception e) {}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getStoreId() {
		return storeId;
	}
	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public AddressModel getAddress() {
		return address;
	}
	public void setAddress(AddressModel address) {
		this.address = address;
	}
	public ContactsModel getContact() {
		return contact;
	}
	public void setContact(ContactsModel contact) {
		this.contact = contact;
	}
	public long getServiceId() {
		return serviceId;
	}
	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}
	public ListServices getListServices() {
		return listServices;
	}
	public void setListServices(ListServices listServices) {
		this.listServices = listServices;
	}
	public Calendar getCalendar() {
		return calendar;
	}
	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("id", id); } catch (Exception e) {}
		try { obj.put("storeId", storeId); } catch (Exception e) {}
		try { obj.put("storeName", storeName); } catch (Exception e) {}
		try { obj.put("address", address.toJson()); } catch (Exception e) {}
		try { obj.put("contact", contact.toJson()); } catch (Exception e) {}
		try { obj.put("serviceId", serviceId); } catch (Exception e) {}
		try { obj.put("listServices", listServices.toJson()); } catch (Exception e) {}
		try { obj.put("calendar", calendar.getTimeInMillis()); } catch (Exception e) {}
		return obj;
	}

}
