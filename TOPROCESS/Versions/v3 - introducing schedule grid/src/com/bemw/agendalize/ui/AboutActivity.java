package com.bemw.agendalize.ui;

import java.util.Locale;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.dao.LanguageDAO;
import com.bemw.agendalize.util.LanguageTools;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class AboutActivity extends Activity {

	private String languageInUse;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Window w = getWindow();
		w.requestFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.view_about);
		w.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_info);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		languageInUse = LanguageDAO.loadLangPreferences(this);
		LanguageTools.setLocalizationStringValues(this, LanguageTools.LANGUAGES.valueOf(languageInUse.toUpperCase(Locale.getDefault())));
		
		setTitle(R.string.menu_about);
		((Button) findViewById(R.id.view_about_btnok)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});

		/* Starts the web browser activity redirecting to social page's contact url */
		((Button) findViewById(R.id.view_about_facebook)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(InternalConstants.OUR_FACEBOOK));
				startActivity(browserIntent);
			}
		});
		((Button) findViewById(R.id.view_about_twitter)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(InternalConstants.OUR_TWITTER));
				startActivity(browserIntent);
			}
		});

	}

	/* any touch event will close down the activity */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		finish();
		return super.onKeyDown(keyCode, event);
	}
	
}
