package com.bemw.agendalize.ui;

import java.util.Locale;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.dao.LanguageDAO;
import com.bemw.agendalize.dao.LocalConfigDAO;
import com.bemw.agendalize.model.ContactsModel;
import com.bemw.agendalize.model.LocalConfigDataModel;
import com.bemw.agendalize.util.DevicePlatform;
import com.bemw.agendalize.util.LanguageTools;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class LocalConfigDataActivity extends ActionBarActivity {
	
	private String languageInUse;
	private SharedPreferences privatePrefs;
	private Editor prefsPrivateEditor;
	
	private boolean use_alarm;
	private Spinner notification_timer;
	private LocalConfigDataModel lcm;
	private ContactsModel contacts	= null;
	private TextView full_name		= null;
	private TextView birtday		= null;
	private TextView document		= null;
	private TextView email			= null;
	private TextView phone			= null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		privatePrefs = getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		prefsPrivateEditor = privatePrefs.edit();
		
		getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_settings_hd));
		getSupportActionBar().setTitle(getResources().getString(R.string.action_settings));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		languageInUse = LanguageDAO.loadLangPreferences(this);
		LanguageTools.setLocalizationStringValues(this, LanguageTools.LANGUAGES.valueOf(languageInUse.toUpperCase(Locale.getDefault())));
		setContentView(R.layout.view_localconfigdata);
		/*setHeader();*/
		setButtonListeners();

		full_name = (TextView)findViewById(R.id.view_localconfigdata_fullname);
		birtday	= (TextView)findViewById(R.id.view_localconfigdata_birtday);
		document = (TextView)findViewById(R.id.view_localconfigdata_document);
		email = (TextView)findViewById(R.id.view_localconfigdata_email);
		phone = (TextView)findViewById(R.id.view_localconfigdata_phone);
		contacts = new ContactsModel();
		
		// loading the saved preferences
		loadPreferences();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		lcm.setFullName(full_name.getText().toString());
		lcm.setBirthday(birtday.getText().toString());
		lcm.setDocument(document.getText().toString());
		contacts.setEmail(email.getText().toString());
		contacts.setPhoneNumberOne(phone.getText().toString());
		lcm.setContacts(contacts);
		LocalConfigDAO.setLocalConfig(LocalConfigDataActivity.this, lcm);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menuconfig, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_settings_back) {
			finish();
	    }
		return super.onOptionsItemSelected(item);
	}
	
	private void setButtonListeners() {
		// Language
		((Button) findViewById(R.id.view_localconfigdata_lang)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(LocalConfigDataActivity.this, LanguageActivity.class);
				startActivityForResult(intent, LanguageActivity.LANGUAGE_RETURN_VALUE);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case (LanguageActivity.LANGUAGE_RETURN_VALUE): {
				/* This snippet will only be executed whenever we start the Language
				 * activity so that we can refresh the view for the chosen language */
				languageInUse = LanguageDAO.loadLangPreferences(this);
				LanguageTools.setLocalizationStringValues(this, LanguageTools.LANGUAGES.valueOf(languageInUse.toUpperCase(Locale.getDefault())));
				// refreshing the view
				setContentView(R.layout.view_localconfigdata);
				setButtonListeners();
				break;
			}
		}
	}
	
	private void loadPreferences() {
		lcm = LocalConfigDAO.getLocalConfig(this);
		// personal data
		full_name.setText(lcm.getFullName());
		birtday.setText(lcm.getBirthday());
		document.setText(lcm.getDocument());
		// contacts
		email.setText(lcm.getContacts().getEmail());
		phone.setText(lcm.getContacts().getPhoneNumberOne());
		// use timer
		CheckBox alarmToggle = ((CheckBox)findViewById(R.id.view_localconfigdata_usealarm));
		alarmToggle.setChecked(lcm.isUseAlarm());
		alarmToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					((Spinner)findViewById(R.id.view_localconfigdata_alarmdelay)).setEnabled(true);
					((Spinner)findViewById(R.id.view_localconfigdata_alarmdelay)).getBackground().setAlpha(255);
				} else {
					((Spinner)findViewById(R.id.view_localconfigdata_alarmdelay)).setEnabled(false);
					((Spinner)findViewById(R.id.view_localconfigdata_alarmdelay)).getBackground().setAlpha(128);
				}
				lcm.setUseAlarm(isChecked);
				LocalConfigDAO.setLocalConfig(LocalConfigDataActivity.this, lcm);
			}
		});
		// timer
		notification_timer = (Spinner) findViewById(R.id.view_localconfigdata_alarmdelay);
		ArrayAdapter<CharSequence> spinnerArrayAdapter = new ArrayAdapter<CharSequence>(
				this, android.R.layout.simple_spinner_item, new CharSequence[] {
						getText(R.string.label_3day),
						getText(R.string.label_1day),
						getText(R.string.label_12hours),
						getText(R.string.label_6hours),
						getText(R.string.label_1hour) });
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		notification_timer.setAdapter(spinnerArrayAdapter);
		notification_timer.setSelection(lcm.getPreAlarm());
		notification_timer.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1, int selectedItem, long arg3) {
				lcm.setPreAlarm(selectedItem);
				LocalConfigDAO.setLocalConfig(LocalConfigDataActivity.this, lcm);
			}
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
		// language
		int size = 48;
		String s = DevicePlatform.getResolutionAsSizeString(this);
		if (s == "small") size = 32;
		else if (s == "medium") size = 48;
		else if (s == "large") size = 64;
		languageInUse = LanguageDAO.loadLangPreferences(this);
		Button languageBtn = (Button) findViewById(R.id.view_localconfigdata_lang);
		if (languageInUse.equals(LanguageTools.LANGUAGES.ENGLISH.name())) {
			Drawable img = getResources().getDrawable( R.drawable.ic_usa_flag );
			img.setBounds( 0, 0, size, size );
			languageBtn.setCompoundDrawables( img, null, null, null );
			languageBtn.setText(LanguageTools.ENGLISH_NATIVE_LANG);
		} else if (languageInUse.equals(LanguageTools.LANGUAGES.PORTUGUESE.name())) {
			Drawable img = getResources().getDrawable( R.drawable.ic_brazil_flag );
			img.setBounds( 0, 0, size, size );
			languageBtn.setCompoundDrawables( img, null, null, null );
			languageBtn.setText(LanguageTools.PORTUGUESE_NATIVE_LANG);
		} else if (languageInUse.equals(LanguageTools.LANGUAGES.SPANISH.name())) {
			Drawable img = getResources().getDrawable( R.drawable.ic_spain_flag );
			img.setBounds( 0, 0, size, size );
			languageBtn.setCompoundDrawables( img, null, null, null );
			languageBtn.setText(LanguageTools.SPANISH_NATIVE_LANG);
		}
		// history
	}

}
