package com.bemw.agendalize.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.BookmarkModel;

public class BookmarkListAdapter extends BaseAdapter {

	private BookmarkFragment context;
	private BookmarkModel bookmark;
	
	public BookmarkListAdapter(BookmarkFragment context, BookmarkModel bookmark) {
		this.context = context;
		this.bookmark = bookmark;
	}
	
	@Override
	public int getCount() {
		return bookmark.size();
	}

	@Override
	public Object getItem(int position) {
		return bookmark.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// predefines the view as an inflated layout
		LayoutInflater inflater = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.row_storedata, null);
		
		ImageButton photo		= ((ImageButton)convertView.findViewById(R.id.row_storedata_photo));
		TextView name			= ((TextView)convertView.findViewById(R.id.row_storedata_name));
		TextView address		= ((TextView)convertView.findViewById(R.id.row_storedata_address));
		/*ImageButton call		= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt1));
		ImageButton portfolio	= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt2));
		ImageButton schedule	= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt3));
		ImageButton location	= ((ImageButton)convertView.findViewById(R.id.row_bmitem_opt4));
		ImageButton delete		= ((ImageButton)convertView.findViewById(R.id.row_bmitem_delete));*/
		
		//photo.setBackgroundDrawable(ImageUtils.loadCachedImage(context, bookmark.get(position).getPhotoIds().get(0)));
		name.setText(bookmark.get(position).getName());
		address.setText(
				bookmark.get(position).getAddress().getAddress() + " " +
				bookmark.get(position).getAddress().getAddressNumber() + "\n" +
				bookmark.get(position).getAddress().getComplement() + " - " +
				bookmark.get(position).getAddress().getCity()
		);
		
		photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: show photos of this provider
			}
		});
		/*schedule.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: show photos of this provider
			}
		});
		portfolio.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//iprovider.showTheServices(bookmark.get(position));
			}
		});
		call.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Dialer.call(context.getActivity(), bookmark.get(position).getContact().getPhoneNumberOne());
			}
		});
		location.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//iprovider.showTheProvider(bookmark.get(position));
			}
		});
		delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
				builder.setMessage(context.getResources().getString(R.string.label_removebookmark)).setCancelable(true).
				setPositiveButton(context.getResources().getString(R.string.btn_ok),
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog, final int id) {
							if (bookmark.hasTheRegistry(bookmark.get(position))) {
								bookmark.removeRegistry(context.getActivity(), bookmark.get(position));
								notifyDataSetChanged();
							}
						}
					}
				).setNegativeButton(context.getResources().getString(R.string.btn_no),
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog, final int id) {}
				});
				final AlertDialog alert = builder.create();
				alert.show();
			}
		});*/
		
		return convertView;
	}
	
}
