package com.bemw.agendalize.ui;

import java.util.Locale;

import com.bemw.agendalize.R;
import com.bemw.agendalize.dao.LanguageDAO;
import com.bemw.agendalize.util.LanguageTools;
import com.bemw.agendalize.util.LanguageTools.LANGUAGES;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;

public class LanguageActivity extends Activity {

	private String languageInUse;
	private RadioButton englishRdBtn, portugueseRdBtn, spanishRdBtn;
	public static final int LANGUAGE_RETURN_VALUE = 0;

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		Window w = getWindow();
		w.requestFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.view_language);
		w.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_action_web_site_hd);
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		languageInUse = LanguageDAO.loadLangPreferences(this);
		LanguageTools.setLocalizationStringValues(this, LanguageTools.LANGUAGES.valueOf(languageInUse.toUpperCase(Locale.getDefault())));
		setTitle(R.string.menu_lang);
		
		englishRdBtn = (RadioButton) findViewById(R.id.view_language_btenglish);
		portugueseRdBtn = (RadioButton) findViewById(R.id.view_language_btportuguese);
		spanishRdBtn = (RadioButton) findViewById(R.id.view_language_btspanish);
		
		// loading the saved preferences
		String lang = LanguageDAO.loadLangPreferences(this);
		if (lang == null)
			englishRdBtn.setChecked(true); // default is English
		else {
			LANGUAGES selectedLanguage = LANGUAGES.valueOf(lang.toUpperCase(Locale.getDefault()));
			checkLanguage(selectedLanguage);
		}
		
		// add click listener
		Button confirmBtn = (Button) findViewById(R.id.view_language_btnok);
		confirmBtn.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View arg0) {
				LanguageDAO.saveLangPreferences(LanguageActivity.this, getCheckedLanguageInPortuguese().toString());
				finish();
			}
		});
	}

	private void checkLanguage(LanguageTools.LANGUAGES selectedLanguage) {
		switch (selectedLanguage) {
			case ENGLISH: englishRdBtn.setChecked(true); break;
			case PORTUGUESE: portugueseRdBtn.setChecked(true); break;
			case SPANISH: spanishRdBtn.setChecked(true); break;
		}
	}
	
	private LanguageTools.LANGUAGES getCheckedLanguageInPortuguese() {
		if (englishRdBtn.isChecked()) {
			return LanguageTools.LANGUAGES.ENGLISH;
		} else if (portugueseRdBtn.isChecked()) {
			return LanguageTools.LANGUAGES.PORTUGUESE;
		} else if (spanishRdBtn.isChecked()) {
			return LanguageTools.LANGUAGES.SPANISH;
		} else {
			return LanguageTools.LANGUAGES.ENGLISH;
		}
	}	

}