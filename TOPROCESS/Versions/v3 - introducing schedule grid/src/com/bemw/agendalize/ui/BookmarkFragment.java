package com.bemw.agendalize.ui;

import com.bemw.agendalize.InternalConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.dao.BookmarkDAO;
import com.bemw.agendalize.model.BookmarkModel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class BookmarkFragment extends Fragment {

	private ListView provider_list				= null;
	private BookmarkListAdapter adapter			= null;
	private BookmarkModel bookmark				= null;
	private TextView empty_label				= null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.view_bookmark, container, false);
		
		provider_list = (ListView)rootView.findViewById(R.id.view_bookmark_list);
		empty_label = (TextView)rootView.findViewById(R.id.view_bookmark_empty);
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		bookmark = BookmarkDAO.loadBookmark(getActivity());
		if (bookmark.size() == 0) {
			empty_label.setVisibility(View.VISIBLE);
			provider_list.setVisibility(View.GONE);
		} else {
			empty_label.setVisibility(View.GONE);
			provider_list.setVisibility(View.VISIBLE);
			adapter = new BookmarkListAdapter(this, BookmarkDAO.loadBookmark(getActivity()));
			provider_list.setAdapter(adapter);
			provider_list.setOnItemClickListener(new OnItemClickListener() {
				@Override
			    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
					Intent detailIntent = new Intent(getActivity(), StoreDetailActivity.class);
					detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					detailIntent.putExtra(InternalConstants.STOREDATA, bookmark.get(position).toJson().toString());
					getActivity().startActivity(detailIntent);
				}
			});
			adapter.notifyDataSetChanged();
		}
	}
	
}
