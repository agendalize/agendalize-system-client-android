package com.bemw.agendalize.dao;

import android.content.Context;

import com.bemw.agendalize.model.LocalConfigDataModel;

public class LanguageDAO {
	
	// language persistence
	public static String loadLangPreferences(Context context) {
		LocalConfigDataModel lc = LocalConfigDAO.getLocalConfig(context);
		return lc.getLang();
	}
	public static void saveLangPreferences(Context context, String lang) {
		LocalConfigDataModel lc = LocalConfigDAO.getLocalConfig(context);
		lc.setLang(lang);
		LocalConfigDAO.setLocalConfig(context, lc);
	}

}
