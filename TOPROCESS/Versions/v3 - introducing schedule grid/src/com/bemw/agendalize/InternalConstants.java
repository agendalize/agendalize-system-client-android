package com.bemw.agendalize;

public class InternalConstants {
	
	// system domain
	public static final String domain = "http://www.agendalize.com.br/qa/";
	// lists all closest stores
	public static final String getStoresList = domain + "stores/list";
	// lists all services of a given store ID
	public static String getStoreServices(long StoreID) {
		return domain + "stores/" + StoreID + "/services/list";
	}
	// lists all albums
	public static final String getAlbumsList = domain + "albums/list";
	// lists all albums of a given user ID
	public static String getUserAlbums(long UserID) {
		return domain + "users/" + UserID + "/albums/list";
	}
	// returns types of service
	public static final String getServiceTypes = domain + "type-services/list";
	// lists all services of a given service type ID
	public static String getTypeServices(long TypeID) {
		return domain + "type-services/" + TypeID + "/services/list";
	}
	// get available days of an employee within a period, given the store ID, employee ID and dates
	public static String getServiceTypes(long StoreID, long EmployeeID,
			int day, int month, int year, int daysBefore, int daysAfter) {
		return domain + "stores/" + StoreID + "/employees/" + EmployeeID +
				"/daysAvailable/list?baseDate=" + year + "" + month + "" + day +
				"&daysBefore=" + daysBefore + "&daysAfter=" + daysAfter;
	}
	// create a booking with the following json content:
	// {"userId": 12, "employeeId": 123, "codService": 1234, "dayHourBegin": 12345}
	public static final String postBooking = domain + "booking/create";
		
	public static final String OUR_FACEBOOK = "https://www.facebook.com/pages/Agendalize/496570267115536";
	public static final String OUR_TWITTER = "http://www.google.com";
	
	public static final int BOOTSPLASH_TIMER = 3000;
	public static final int CONN_TIME_OUT = 30000;
	public static final int PAGE_SIZE = 10;
	public static final int MAX_PROFESSIONALS = 10;
	public static final int MAX_BOOKMARK_SIZE = 50;
	public static final int SCHEDULE_FRAME_SIZE = 1;
	
	public static final String TAG = "Agendalize";
	
	public static final String CACHE_FILE = "agendalize_cache";

	public static final String STOREDATA = "STOREDATA";
	public static final String BOOKMARK = "BOOKMARK";
	public static final String LOCAL_USER = "LOCAL_USER";
	public static final String LATITUDE = "LATITUDE";
	public static final String LONGITUDE = "LONGITUDE";
	public static final String DEVICEUSER = "DEVICEUSER";
	public static final String DEVICEPASS = "DEVICEPASS";
	public static final String PRIVATE_PREFERENCES = "PRIVATE_PREFERENCES";
	
}
