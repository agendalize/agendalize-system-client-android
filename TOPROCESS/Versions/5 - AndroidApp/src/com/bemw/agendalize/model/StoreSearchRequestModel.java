package com.bemw.agendalize.model;

import com.bemw.agendalize.AppConstants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class StoreSearchRequestModel {
	
	private int listOffSet;
	private ArrayList<Long> services;
	private int sizeList;
	private double latitude;
	private double longitude;
	private String hourFrom;
	private String hourTo;
	private long baseDate;
	private long range;
	
	public StoreSearchRequestModel() {
		listOffSet = 0;
		services = new ArrayList<Long>();
		sizeList = AppConstants.SIZE_LIST;
		latitude = 0;
		longitude = 0;
		hourFrom = "0000";
		hourTo = "2359";
		baseDate = Calendar.getInstance().getTime().getTime();
		range = 0;
	}
	public StoreSearchRequestModel(int listOffSet, ArrayList<Long> services,
			int sizeList, double latitude, double longitude, String hourFrom,
			String hourTo, long baseDate, long range) {
		this();
		this.listOffSet = listOffSet;
		this.services = services;
		this.sizeList = sizeList;
		this.latitude = latitude;
		this.longitude = longitude;
		this.hourFrom = hourFrom;
		this.hourTo = hourTo;
		this.baseDate = baseDate;
		this.range = range;
	}

	public StoreSearchRequestModel(JSONObject obj) {
		this();
		try { listOffSet = Integer.valueOf(obj.getString("listOffSet")); } catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("services");
			for (int i=0; i<arr.length(); i++) {
				services.add(Long.valueOf(arr.getString(i)));
			}
		} catch (Exception e) {}
		try { sizeList = Integer.valueOf(obj.getString("sizeList")); } catch (Exception e) {}
		try { latitude = Double.valueOf(obj.getString("latitude")); } catch (Exception e) {}
		try { longitude = Double.valueOf(obj.getString("longitude")); } catch (Exception e) {}
		try { hourFrom = obj.getString("hourFrom"); } catch (Exception e) {}
		try { hourTo = obj.getString("hourTo"); } catch (Exception e) {}
		try { baseDate = Long.valueOf(obj.getString("baseDate")); } catch (Exception e) {}
		try { range = Long.valueOf(obj.getString("range")); } catch (Exception e) {}
	}
	
	public int getListOffSet() {
		return listOffSet;
	}
	public void setListOffSet(int listOffSet) {
		this.listOffSet = listOffSet;
	}
	public List<Long> getServices() {
		return services;
	}
	public void setServices(ArrayList<Long> services) {
		this.services = services;
	}
	public int getSizeList() {
		return sizeList;
	}
	public void setSizeList(int sizeList) {
		this.sizeList = sizeList;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getHourFrom() {
		return hourFrom;
	}
	public void setHourFrom(String hourFrom) {
		this.hourFrom = hourFrom;
	}
	public String getHourTo() {
		return hourTo;
	}
	public void setHourTo(String hourTo) {
		this.hourTo = hourTo;
	}
	public long getBaseDate() {
		return baseDate;
	}
	public void setBaseDate(long baseDate) {
		this.baseDate = baseDate;
	}
	public long getRange() {
		return range;
	}
	public void setRange(long range) {
		this.range = range;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("listOffSet", ""+listOffSet); } catch(Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<services.size(); i++) {
				arr.put(""+services.get(i));
			}
			obj.put("services", arr);
		} catch (Exception e) {}
		try { obj.put("sizeList", ""+sizeList); } catch(Exception e) {}
		try { obj.put("latitude", ""+latitude); } catch(Exception e) {}
		try { obj.put("longitude", ""+longitude); } catch(Exception e) {}
		try { obj.put("hourFrom", hourFrom); } catch (Exception e) {}
		try { obj.put("hourTo", hourTo); } catch (Exception e) {}
		try { obj.put("baseDate", ""+baseDate); } catch (Exception e) {}
		try { obj.put("range", ""+range); } catch (Exception e) {}
		return obj;
	}

}
