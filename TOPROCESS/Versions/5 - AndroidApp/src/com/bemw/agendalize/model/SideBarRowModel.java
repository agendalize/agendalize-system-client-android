package com.bemw.agendalize.model;

public class SideBarRowModel {
	
	private int labelId = -1;
	private int iconId = -1;
	
	public SideBarRowModel(int labelId, int iconId) {
		this.labelId = labelId;
		this.iconId = iconId;
	}
	
	public int getLabelId() {
		return labelId;
	}
	
	public int getIconId() {
		return iconId;
	}
	
}
