package com.bemw.agendalize.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class StoreModel {
	
	private long id;
	private String name;
	private String companyName;
	private String description;
	private String cnpj;
	private AddressModel addressInfo;
	private ContactsModel contactInfo;
	private int nrServices;
	private int nrEmployees;
    private String urlPhoto;
	private int evaluation;
	private List<OperationModel> listOperations;
	private List<ServiceModel> listServices;
	
	public StoreModel() {
		id = -1;
		name = "name";
		companyName = "companyName";
		description = "description";
		cnpj = "cnpj";
		addressInfo = new AddressModel();
		contactInfo = new ContactsModel();
		nrServices = 0;
	    nrEmployees = 0;
	    urlPhoto = null;
	    evaluation = 0;
		listOperations = new ArrayList<OperationModel>();
		listServices = new ArrayList<ServiceModel>();
	}
	public StoreModel(long id, String name, String companyName,
			String description, String cnpj, AddressModel addressInfo,
			ContactsModel contactInfo, int nrServices, int nrEmployees,
			String urlPhoto, int evaluation,
			List<OperationModel> listOperations, List<ServiceModel> listServices) {
		this();
		this.id = id;
		this.name = name;
		this.companyName = companyName;
		this.description = description;
		this.cnpj = cnpj;
		this.addressInfo = addressInfo;
		this.contactInfo = contactInfo;
		this.nrServices = nrServices;
		this.nrEmployees = nrEmployees;
		this.urlPhoto = urlPhoto;
		this.evaluation = evaluation;
		this.listOperations = listOperations;
		this.listServices = listServices;
	}
	public StoreModel(JSONObject obj) {
		this();
		try { id = obj.getLong("id"); } catch (Exception e) {}
		try { name = obj.getString("name"); } catch (Exception e) {}
		try { companyName = obj.getString("companyName"); } catch (Exception e) {}
		try { description = obj.getString("description"); } catch (Exception e) {}
		try { cnpj = obj.getString("cnpj"); } catch (Exception e) {}
		try { addressInfo = new AddressModel(obj.getJSONObject("addressInfo")); } catch (Exception e) {}
		try { contactInfo = new ContactsModel(obj.getJSONObject("contactInfo")); } catch (Exception e) {}
		try { nrServices = obj.getInt("nrServices"); } catch (Exception e) {}
		try { nrEmployees = obj.getInt("nrEmployees"); } catch (Exception e) {}
		try { urlPhoto = obj.getString("urlPhoto"); } catch (Exception e) {}
		try { evaluation = obj.getInt("evaluation"); } catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("listOperations");
			for (int i=0; i<arr.length(); i++) {
				listOperations.add(new OperationModel(arr.getJSONObject(i)));
			}
		} catch (Exception e) {}
		try {
			JSONArray arr = obj.getJSONArray("listServices");
			for (int i=0; i<arr.length(); i++) {
				listServices.add(new ServiceModel(arr.getJSONObject(i)));
			}
		} catch (Exception e) {}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public AddressModel getAddressInfo() {
		return addressInfo;
	}
	public void setAddressInfo(AddressModel addressInfo) {
		this.addressInfo = addressInfo;
	}
	public ContactsModel getContactInfo() {
		return contactInfo;
	}
	public void setContactInfo(ContactsModel contactInfo) {
		this.contactInfo = contactInfo;
	}
	public int getNrServices() {
		return nrServices;
	}
	public void setNrServices(int nrServices) {
		this.nrServices = nrServices;
	}
	public int getNrEmployees() {
		return nrEmployees;
	}
	public void setNrEmployees(int nrEmployees) {
		this.nrEmployees = nrEmployees;
	}
	public String getUrlPhoto() {
		return urlPhoto;
	}
	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}
	public int getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(int evaluation) {
		this.evaluation = evaluation;
	}
	public List<OperationModel> getListOperations() {
		return listOperations;
	}
	public void setListOperations(List<OperationModel> listOperations) {
		this.listOperations = listOperations;
	}
	public List<ServiceModel> getListServices() {
		return listServices;
	}
	public void setListServices(List<ServiceModel> listServices) {
		this.listServices = listServices;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("id", id); } catch(Exception e) {}
		try { obj.put("name", name); } catch(Exception e) {}
		try { obj.put("companyName", companyName); } catch(Exception e) {}
		try { obj.put("description", description); } catch(Exception e) {}
		try { obj.put("cnpj", cnpj); } catch(Exception e) {}
		try { obj.put("addressInfo", addressInfo.toJson()); } catch (Exception e) {}
		try { obj.put("contactInfo", contactInfo.toJson()); } catch (Exception e) {}
		try { obj.put("nrServices", nrServices); } catch(Exception e) {}
		try { obj.put("nrEmployees", nrEmployees); } catch(Exception e) {}
		try { obj.put("urlPhoto", urlPhoto); } catch(Exception e) {}
		try { obj.put("evaluation", evaluation); } catch(Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<listOperations.size(); i++) {
				arr.put(listOperations.get(i).toJson());
			}
			obj.put("listOperations", arr);
		} catch (Exception e) {}
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<listServices.size(); i++) {
				arr.put(listServices.get(i).toJson());
			}
			obj.put("listServices", arr);
		} catch (Exception e) {}
		return obj;
	}

}
