package com.bemw.agendalize;

public class AppConstants {
	
	// names
	public static final String TAG = "Agendalize";
	public static final String CACHE_FILE = "agendalize_cache";
	
	// domains and url
	public static final String domain = "http://www.agendalize.com.br/qa/";
	
	// web addresses
	public static final String OUR_FACEBOOK = "https://www.facebook.com/pages/Agendalize/496570267115536";
	public static final String OUR_TWITTER = "http://www.google.com";
	
	// final values
	public static final String DEVICE_NAME = "android-agdz";
	public static final String DEVICE_KEY = "qa123";
	public static final int POSITION_TIMER = 5*60000;
	public static final int BOOTSPLASH_TIMER = 2000;
	public static final int CONN_TIME_OUT = 20000;
	public static final int SIZE_LIST = 7;
	
	// tags
	public static final String PRIVATE_PREFERENCES = "PRIVATE_PREFERENCES";
	
}
