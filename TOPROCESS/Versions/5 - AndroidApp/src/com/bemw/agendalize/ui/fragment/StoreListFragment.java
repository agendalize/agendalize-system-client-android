package com.bemw.agendalize.ui.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpStatus;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.model.StoreSearchRequestModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.ui.StoreListActivity;
import com.bemw.agendalize.util.DeviceUtils;
import com.bemw.agendalize.util.ImageUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.util.LongSparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

public class StoreListFragment extends Fragment {

	//===================================================================================
	// Fields
	//===================================================================================
	
	// used to manage lazy load pages
	private static Object lock = new Object();
	private static boolean isLoading = false;
	private static int lastLoadCount = -1;
	// objects
	private static StoreListActivity context;
	private static LongSparseArray<Drawable> photos;
	private static List<StoreModel> stores;
	private static StoreListAdapter storeListAdapter;
	// views
	private static ListView frag_storelist_listview;
    // data for store fetch
    private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;
    
	private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	if (waitDialog != null) {
            	waitDialog.dismiss();
            	waitDialog = null;
            }
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                	DeviceUtils.justShowDialogWarning(context, R.string.msg_timeout, false);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                	DeviceUtils.justShowDialogWarning(context, R.string.msg_noconnection, false);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                	DeviceUtils.justShowDialogWarning(context, R.string.msg_servererror, false);
                    break;
                case HttpStatus.SC_OK:
                    if (stores != null) {
                        if (stores.size() == 0) {
                        	DeviceUtils.justShowDialogWarning(context, R.string.msg_noresults, false);
                        } else {
                            frag_storelist_listview.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(AbsListView view, int scrollState) {}
                                @Override
                                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    if (
                                		frag_storelist_listview.getLastVisiblePosition()+1 == totalItemCount && // user has reached the end of the list
                        				frag_storelist_listview.getAdapter().getCount() >= AppConstants.SIZE_LIST && // only enable pagination if we already done at least 1 load
                                        lastLoadCount == AppConstants.SIZE_LIST && // last load had fetch a complete page
                                        lastLoadCount != -1
                                    ) createNewLoader();
                                }
                            });
                            context.setStores(stores);
                            storeListAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
            }
            synchronized(lock) {
            	isLoading = false;
            }
        }
    };
    
    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                List<StoreModel> newStores = client.getStoresByContext(getSearchParams(context.getLocation()).toJson());
                // before add, remove possible "loading more" tags
                if (stores.size() > 0) {
                	if (stores.get(stores.size()-1).getName().equals("LOAD_MORE")) {
                		stores.remove(stores.size()-1);
                	}
                }
                // add new stores
                for (int i=0; i<newStores.size(); i++) {
                	stores.add(newStores.get(i));
                }
                lastLoadCount = newStores.size();
                // add the informative item, if there is more contacts to load (last fetch returned SIZE_LIST)
                if (stores.size() > 0 && lastLoadCount == AppConstants.SIZE_LIST) {
                    StoreModel m = new StoreModel();
                    m.setName("LOAD_MORE");
                    stores.add(m);
                }
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };
    
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storelist, container, false);
        context = (StoreListActivity)getActivity();
        stores = new ArrayList<StoreModel>();
        photos = new LongSparseArray<Drawable>();
        
        // widgets and components
        frag_storelist_listview = (ListView) rootView.findViewById(R.id.frag_storelist_listview);
        
        if (savedInstanceState == null) {
    		storeListAdapter = new StoreListAdapter();
    		frag_storelist_listview.setAdapter(storeListAdapter);
    		createNewLoader();
        }
        
        return rootView;
    }
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	synchronized private static void createNewLoader() {
    	if (!isLoading) {
        	synchronized(lock) {
            	isLoading = true;
            }
        	if (stores.size() == 0) { // we show this only if its the first loading
	        	waitDialog = ProgressDialog.show(
	                    context,
	                    context.getText(R.string.msg_loadingdata),
	                    context.getText(R.string.msg_pleasewait),
	                    false, false);
	            waitDialog.setIcon(R.drawable.ic_launcher);
        	}
            contentLoader = new Thread(loadContent);
            contentLoader.start();
    	}
    }
    
    private static StoreSearchRequestModel getSearchParams(double[] LatLon) {
        // construct the json request
        Calendar dayTm = Calendar.getInstance();
        StoreSearchRequestModel request = new StoreSearchRequestModel();
        request.setListOffSet(stores.size());
        request.setLatitude(LatLon[0]);
        request.setLongitude(LatLon[1]);
        request.setBaseDate(dayTm.getTime().getTime());
        request.setRange(50000);
        return request;
    }
    
    public List<StoreModel> getStores() {
    	return stores;
    }
	
	//===================================================================================
    // Inner classes
    //===================================================================================
	
	private static class StoreListAdapter extends BaseAdapter {
	    
	    @Override
	    public int getCount() {
	        return stores.size();
	    }
	    
	    @Override
	    public Object getItem(int position) {
	        return stores.get(position);
	    }
	    
	    @Override
	    public long getItemId(int position) {
	        return position;
	    }
	    
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        if (stores.get(position).getName().equals("LOAD_MORE")) {
	            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = inflater.inflate(R.layout.row_helperloading, parent, false);
	            return convertView;
	        }
	        // predefines the view as an inflated layout
	        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        convertView = inflater.inflate(R.layout.row_storeitem, parent, false);

	        ((TextView) convertView.findViewById(R.id.row_storeitem_storename)).setText(
	        		stores.get(position).getName()+""
	        );
	        ((RatingBar) convertView.findViewById(R.id.row_storeitem_storerating)).setRating(
	        		stores.get(position).getEvaluation()*5/100
	        );
	        ((TextView) convertView.findViewById(R.id.row_storeitem_numemployees)).setText(
	        		stores.get(position).getNrEmployees()+""
	        );
	        ((TextView) convertView.findViewById(R.id.row_storeitem_numservices)).setText(
	        		stores.get(position).getNrServices()+""
	        );
	        ((TextView) convertView.findViewById(R.id.row_storeitem_citystate)).setText(
	        		stores.get(position).getAddressInfo().getCity() + ", " +
    				stores.get(position).getAddressInfo().getState()
    		);
	        ((TextView) convertView.findViewById(R.id.row_storeitem_streetname)).setText(
	        		stores.get(position).getAddressInfo().getAddress() + " " +
    				stores.get(position).getAddressInfo().getAddressNumber() +
    				(stores.get(position).getAddressInfo().getComplement().equals("null") ? "" :
    				", " + stores.get(position).getAddressInfo().getComplement())
			);
	        if (photos.get(stores.get(position).getId()) != null) {
	        	((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageDrawable(photos.get(stores.get(position).getId()));
            	((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
	        } else {
	        	if (stores.get(position).getUrlPhoto() != null) {
		        	if (stores.get(position).getUrlPhoto().length() > 0) {
		                new PhotoLoader(
	                		((ImageView) convertView.findViewById(R.id.row_storeitem_photo)),
	                		((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)),
	                		stores.get(position).getId(),
	                		stores.get(position).getUrlPhoto()).execute();
		            } else {
		            	((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageResource(R.drawable.noimage);
		            	((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
		            }
		        } else {
		        	((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageResource(R.drawable.noimage);
		        	((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
		        	photos.put(stores.get(position).getId(), context.getResources().getDrawable(R.drawable.noimage));
		        }
	        }
	        return convertView;
	    }
	    
	}
	
	private static class PhotoLoader extends AsyncTask<Void, Void, Void> {
		
	    private Drawable drawable;
	    private ProgressBar progress;
	    private ImageView photo;
	    private long id;
	    private String url;
	    
	    public PhotoLoader(ImageView photo, ProgressBar progress, long id, String url) {
	        this.photo = photo;
	        this.progress = progress;
	        this.id = id;
	        this.url = url;
	    }
	    
	    @Override
	    protected Void doInBackground(Void... params) {
	        try {
	        	drawable = ImageUtils.loadImageFromWebOperations(url);
	        	Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), true);
	            drawable = new BitmapDrawable(context.getResources(), bitmap);
	        } catch (Exception e) {
	        	drawable = null;
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);
	        progress.setVisibility(View.GONE);
	        if (drawable != null) {
	        	photo.setImageDrawable(drawable);
	        	photos.put(id, drawable);
	        } else {
	        	photo.setImageResource(R.drawable.noimage);
	        	photos.put(id, context.getResources().getDrawable(R.drawable.noimage));
	    	}
	    }
	    
	}
	
}
