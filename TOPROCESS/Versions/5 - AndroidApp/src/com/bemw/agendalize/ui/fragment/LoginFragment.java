package com.bemw.agendalize.ui.fragment;

import org.apache.http.HttpStatus;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.ui.StoreListActivity;
import com.bemw.agendalize.util.DeviceUtils;

public class LoginFragment extends Fragment {
	
	//===================================================================================
	// Fields
	//===================================================================================

	// data for store fetch
    private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;
    // objects
 	private static HomeActivity context;
 	
	private static final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	switch (msg.what) {
		        case HttpStatus.SC_REQUEST_TIMEOUT:
		        	DeviceUtils.justShowDialogWarning(context, R.string.msg_timeout, false);
		            break;
		        case -1: case HttpStatus.SC_NOT_FOUND:
		        	DeviceUtils.justShowDialogWarning(context, R.string.msg_noconnection, false);
		            break;
		        case HttpStatus.SC_INTERNAL_SERVER_ERROR:
		        	DeviceUtils.justShowDialogWarning(context, R.string.msg_servererror, false);
		            break;
		        case HttpStatus.SC_OK:
		        	
		        	break;
	        	default:;
        	}
        }
	};
	
	private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
        	if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient();
                String pin = client.getPin(phoneNumber);
                handler.sendEmptyMessage(client.getStatus());
            }
        }
	};
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_login, container, false);
        context = (StoreListActivity)getActivity();
        
        TelephonyManager tMgr = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        
        return rootView;
    }

}
