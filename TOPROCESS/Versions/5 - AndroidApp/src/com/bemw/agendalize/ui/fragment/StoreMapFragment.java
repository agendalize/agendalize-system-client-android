package com.bemw.agendalize.ui.fragment;

import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.ui.StoreListActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class StoreMapFragment extends Fragment {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private static StoreListActivity activity;
	private List<StoreModel> stores;
	private static FragmentManager fragmentManager;
	private static GoogleMap frag_storemap_mapview;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storemap, container, false);
        activity = (StoreListActivity)getActivity();
        fragmentManager = getChildFragmentManager();
        
        setUpMapIfNeeded();
        
        return rootView;
    }
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
	    if (frag_storemap_mapview != null)
	    	setUpMap();
	    if (frag_storemap_mapview == null) {
	    	frag_storemap_mapview =
	    			((SupportMapFragment) fragmentManager.findFragmentById(R.id.frag_storemap_mapview)).getMap();
	        if (frag_storemap_mapview != null)
	        	setUpMap();
	    }
	}
	
	@Override
	public void onDestroyView() {
	    super.onDestroyView();
	    if (frag_storemap_mapview != null) {
	    	FragmentTransaction transaction = fragmentManager.beginTransaction();
	    	// TODO: this is throwing an IllegalStateException
	    	transaction.remove(fragmentManager.findFragmentById(R.id.frag_storemap_mapview)).commit();
	        frag_storemap_mapview = null;
	    }
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	public static void setUpMap() {
		// TODO: place each store...
		frag_storemap_mapview.addMarker(new MarkerOptions().position(new LatLng(0.0f, 0.0f)).title("My Home").snippet("Home Address"));
		// put my location
		frag_storemap_mapview.setMyLocationEnabled(true);
        LatLng location = new LatLng(
        		activity.getLocation()[0],
        		activity.getLocation()[1]
        );
        frag_storemap_mapview.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 11.0f));
	}
	
	public static void setUpMapIfNeeded() {
	    if (frag_storemap_mapview == null) {
	    	Fragment fragMan = fragmentManager.findFragmentById(R.id.frag_storemap_mapview);
	    	SupportMapFragment support = ((SupportMapFragment)fragMan);
	    	frag_storemap_mapview = support.getMap();
	        if (frag_storemap_mapview != null) {
	        	setUpMap();
	        }
	    }
	}
	
}
