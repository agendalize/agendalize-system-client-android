package com.bemw.agendalize.ui.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.ui.AvailableServicesActivity;
import com.bemw.agendalize.ui.StoreListActivity;
import com.bemw.agendalize.util.DeviceUtils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;

public class FilterFragment extends Fragment {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private static final int DATEHOUR_RETURN_VALUE = 0;
	private static final int SERVICE_RETURN_VALUE = 1;
	private static double latlon[];
	private static List<StoreModel> stores;
	private static String baseAddress;
	private static int rangeIndex;
	private static int hourFrom;
	private static int hourTo;
	private static Calendar dayTm;
	private static List<ServiceModel> services;
	private static StoreListActivity activity;
	private ScrollView frag_filter_scroll;
	private EditText frag_filter_locationEdit;
	private Spinner frag_filter_rangecb;
	private ImageButton frag_filter_service_reloadbtn;
	private ProgressBar frag_filter_service_progress;
	private Button frag_filter_datehourbtn;
	private Button frag_filter_service1btn;
	private Button frag_filter_service2btn;
	private Button frag_filter_service3btn;
	private Button frag_filter_service4btn;
	private Button frag_filter_clearbtn;
	private Button frag_filter_searchbtn;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_filter, container, false);
        activity = (StoreListActivity)getActivity();
        
        frag_filter_scroll = (ScrollView) rootView.findViewById(R.id.frag_filter_scroll);
        frag_filter_locationEdit = (EditText) rootView.findViewById(R.id.frag_filter_locationEdit);
        frag_filter_rangecb = (Spinner) rootView.findViewById(R.id.frag_filter_rangecb);
        frag_filter_datehourbtn = (Button) rootView.findViewById(R.id.frag_filter_datehourbtn);
        frag_filter_service_reloadbtn = (ImageButton) rootView.findViewById(R.id.frag_filter_service_reloadbtn);
        frag_filter_service_progress = (ProgressBar) rootView.findViewById(R.id.frag_filter_service_progress);
        frag_filter_service1btn = (Button) rootView.findViewById(R.id.frag_filter_service1btn);
        frag_filter_service2btn = (Button) rootView.findViewById(R.id.frag_filter_service2btn);
        frag_filter_service3btn = (Button) rootView.findViewById(R.id.frag_filter_service3btn);
        frag_filter_service4btn = (Button) rootView.findViewById(R.id.frag_filter_service4btn);
        frag_filter_clearbtn = (Button) rootView.findViewById(R.id.frag_filter_clearbtn);
        frag_filter_searchbtn = (Button) rootView.findViewById(R.id.frag_filter_searchbtn);
        
        getDefaultValuesForFields();
        drawWidgets();
        
        // actions for navigation
        frag_filter_clearbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getDefaultValuesForFields();
                drawWidgets();
            }
        });
        frag_filter_searchbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldsOk()) {
                    // TODO: createNewFetcher();
                }
            }
        });
        
        return rootView;
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	private boolean isFieldsOk() {
        if (services != null) if (services.size() > 0) if (baseAddress != null) if (baseAddress.length() > 0) return true;
        DeviceUtils.justShowDialogWarning(activity, R.string.msg_selectatleast1service, false);
        return false;
    }
	
	private void drawWidgets() {
        // location address data, need to be in onResume to proper update on activityForResult return
		frag_filter_locationEdit.setText(baseAddress);
		frag_filter_locationEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {
                baseAddress = frag_filter_locationEdit.getText().toString();
            }
        });
        // range, need to be in onResume to proper update on activityForResult return
        ArrayAdapter<CharSequence> spinnerArrayAdapter = new ArrayAdapter<CharSequence>(
                activity, android.R.layout.simple_spinner_item, new CharSequence[] {
                getText(R.string.label_distanceby10km),
                getText(R.string.label_distanceby25km),
                getText(R.string.label_distanceby50km) });
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        frag_filter_rangecb.setAdapter(spinnerArrayAdapter);
        frag_filter_rangecb.setSelection(rangeIndex);
        frag_filter_rangecb.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int selectedItem, long arg3) {
                rangeIndex = selectedItem;
            }
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
        // date and hour, need to be in onResume to proper update on activityForResult return
        frag_filter_datehourbtn.setText(
                String.format("%02d", dayTm.get(Calendar.DAY_OF_MONTH))
                        +"/"+String.format("%02d", dayTm.get(Calendar.MONTH))
                        +"/"+String.format("%04d", dayTm.get(Calendar.YEAR))
                        +" - "+getResources().getString(R.string.label_fromhour)
                        +" "+String.format("%02d", (hourFrom/3600))+":"+String.format("%02d",(hourFrom%60))
                        +" "+getResources().getString(R.string.label_tohour)
                        +" "+String.format("%02d", (hourTo/3600))+":"+String.format("%02d",(hourTo%60))
        );
        frag_filter_datehourbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent i = new Intent(activity, ChooseDateHourActivity.class);
                i.putExtra("dayTm", dayTm.getTime().getTime());
                i.putExtra("fromHour", hourFrom);
                i.putExtra("toHour", hourTo);
                startActivityForResult(i, DATEHOUR_RETURN_VALUE);*/
            }
        });
        // selected services, need to be in onResume to proper update on activityForResult return
        if (services.size() > 0) frag_filter_service1btn.setText(services.get(0).getShortName());
        else frag_filter_service1btn.setText(getResources().getString(R.string.label_noservice));
        frag_filter_service1btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 1) frag_filter_service2btn.setText(services.get(1).getShortName());
        else frag_filter_service2btn.setText(getResources().getString(R.string.label_noservice));
        frag_filter_service2btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 2) frag_filter_service3btn.setText(services.get(2).getShortName());
        else frag_filter_service3btn.setText(getResources().getString(R.string.label_noservice));
        frag_filter_service3btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 3) frag_filter_service4btn.setText(services.get(3).getShortName());
        else frag_filter_service4btn.setText(getResources().getString(R.string.label_noservice));
        frag_filter_service4btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        // scrolls to top
        frag_filter_scroll.scrollTo(0, 0);
        frag_filter_scroll.scrollTo(0, 0);
    }
	
	private void getDefaultValuesForFields() {
		latlon = null;
        stores = null;
        baseAddress = "";
        rangeIndex = 0;
        dayTm = Calendar.getInstance();
        hourFrom = 0;
        hourTo = 86399;
        services = new ArrayList<ServiceModel>();
    }

}
