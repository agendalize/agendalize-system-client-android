package com.bemw.agendalize.ui;

import java.util.ArrayList;
import java.util.List;

import com.bemw.agendalize.R;
import com.bemw.agendalize.geo.MyLocation;
import com.bemw.agendalize.geo.MyLocation.LocationResult;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.ui.fragment.FilterFragment;
import com.bemw.agendalize.ui.fragment.StoreListFragment;
import com.bemw.agendalize.ui.fragment.StoreMapFragment;
import com.bemw.agendalize.util.DeviceUtils;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class StoreListActivity extends ActionBarActivity {
	
	//===================================================================================
	// Fields
	//===================================================================================
	
	private Menu menu;
	private double myLocation[];
	private List<StoreModel> stores;
	private final int NUM_PAGES = 3;
	private ViewPager activity_storelist_pager;
	private ScreenSlidePagerAdapter pagerAdapter;
	private ProgressDialog waitDialog;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storelist);
        stores = new ArrayList<StoreModel>();
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        waitDialog = ProgressDialog.show(this,
                getText(R.string.msg_checklocation),
                getText(R.string.msg_pleasewait),
                false, false);
        waitDialog.setIcon(R.drawable.ic_launcher);
        LocationResult locationResult = new LocationResult() {
            @Override
            public void gotLocation(Location location) {
            	if (location == null) {
            		runOnUiThread(new Runnable() {
	            	    public void run() {
	            	    	DeviceUtils.justShowDialogWarning(
	            	    			StoreListActivity.this,
	            	    			R.string.msg_notpossibletogetlocation,
	            	    			true);
	            	    }
            		});
            	} else {
	            	myLocation = new double[2];
	            	myLocation[0] = location.getLongitude();
	            	myLocation[1] = location.getLatitude();
	            	runOnUiThread(new Runnable() {
	            	    public void run() {
	            	    	waitDialog.dismiss();
	            	    	createPager(savedInstanceState);
	            	    }
	            	});
            	}
            }
        };
        MyLocation myLocation = new MyLocation();
        if (!myLocation.getLocation(this, locationResult)) {
        	DeviceUtils.justShowDialogWarning(
	    			StoreListActivity.this,
	    			R.string.msg_notpossibletogetlocation,
	    			true);
        }
        
        // action bar menu
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_chair_w);
        getSupportActionBar().setTitle(R.string.title_stores);
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_storelist, menu);
        return true;
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
			case R.id.action_map:
				activity_storelist_pager.setCurrentItem(0);
                getSupportActionBar().setLogo(R.drawable.ic_overlay_w);
                getSupportActionBar().setTitle(R.string.action_map);
                if (menu != null) {
                    menu.getItem(0).setVisible(false);
                    menu.getItem(1).setVisible(false);
                }
                break;
			case R.id.action_filter:
				activity_storelist_pager.setCurrentItem(2);
                getSupportActionBar().setLogo(R.drawable.ic_filter_w);
                getSupportActionBar().setTitle(R.string.action_filter);
                if (menu != null) {
                    menu.getItem(0).setVisible(false);
                    menu.getItem(1).setVisible(false);
                }
				break;
            case R.id.action_back:
                if (activity_storelist_pager.getCurrentItem() != 1) {
                	activity_storelist_pager.setCurrentItem(1);
                    getSupportActionBar().setLogo(R.drawable.ic_chair_w);
                    getSupportActionBar().setTitle(R.string.title_stores);
                    if (menu != null) {
                        menu.getItem(0).setVisible(true);
                        menu.getItem(1).setVisible(true);
                    }
                } else finish();
			default:;
		}
        return super.onOptionsItemSelected(item);
    }

	//===================================================================================
	// General methods
	//===================================================================================
	
    public ViewPager getPagerView() {
        return activity_storelist_pager;
    }
    
    public void setStores(List<StoreModel> stores) {
    	this.stores = stores;
    }
    
    public List<StoreModel> getStores() {
    	return stores;
    }
    
    public double[] getLocation() {
    	return myLocation;
    }
    
    public void createPager(Bundle savedInstanceState) {
    	// Instantiate a ViewPager and a PagerAdapter
        activity_storelist_pager = (ViewPager) findViewById(R.id.activity_storelist_pager);
        activity_storelist_pager.setAdapter(pagerAdapter);
        activity_storelist_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {}
            @Override
            public void onPageSelected(int i) {}
            @Override
            public void onPageScrollStateChanged(int i) {}
        });
        if (savedInstanceState == null) activity_storelist_pager.setCurrentItem(1);
    }
    
    //===================================================================================
    // Inner classes
    //===================================================================================

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    	
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new StoreMapFragment();
                case 1:
                    return new StoreListFragment();
                case 2:
                    return new FilterFragment();
                default:
                    return new StoreListFragment();
            }
        }
        
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
        
    }

}
