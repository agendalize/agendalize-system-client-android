package com.bemw.agendalize.util;

import com.bemw.agendalize.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;

public class DeviceUtils {

	public static String getResolutionAsSizeString(Activity a) {
		DisplayMetrics dm = new DisplayMetrics();
		a.getWindowManager().getDefaultDisplay().getMetrics(dm);
		switch(dm.densityDpi) {
			case DisplayMetrics.DENSITY_LOW: return "small";
			case DisplayMetrics.DENSITY_MEDIUM: return "medium";
			case DisplayMetrics.DENSITY_HIGH: return "large";
			case DisplayMetrics.DENSITY_XHIGH: return "xlarge";
			case DisplayMetrics.DENSITY_XXHIGH: return "xxlarge";
			default: return "large";
		}
	}
	
	public static void justShowDialogWarning(final Activity activity, int messageId, final boolean finishIt) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(activity.getResources().getString(messageId)).setCancelable(false).
		setPositiveButton(activity.getResources().getString(R.string.action_ok),
			new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {
					if (finishIt) activity.finish();
				}
			}
		);
		final AlertDialog alert = builder.create();
		alert.show();
	};
	
	/**
	 * Checks if internet connection is available, given a context.
	 * @param context the context.
	 * @return true if available, false otherwise.
	 */
	public static boolean isConnectedToInternet(Context context) {
		ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		// 0 is for 3G and 1 for wifi
		if (connec.getNetworkInfo(0).getState().equals(NetworkInfo.State.CONNECTED) || connec.getNetworkInfo(1).getState().equals(NetworkInfo.State.CONNECTED)) {
			// Do something in here when we are connected
			return true;
		}
		return false;
	}
		
}
