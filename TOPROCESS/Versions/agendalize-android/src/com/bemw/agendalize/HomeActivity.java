package com.bemw.agendalize;

import java.util.ArrayList;
import java.util.List;

import com.bemw.agendalize.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class HomeActivity extends ActionBarActivity {

	// left drawer slider menu
	private CharSequence mTitle;
	private Drawable mIcon;
	private String[] mPlanetTitles;
	private Drawable[] mPlanetIcons;
	private List<SidebarRowModel> mDrawerRows;
	private ActionBarDrawerToggle mDrawerToggle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		mPlanetTitles = new String[] {
				getResources().getString(R.string.slidedrawer_agendalize),
				getResources().getString(R.string.slidedrawer_profile),
				getResources().getString(R.string.slidedrawer_history),
				getResources().getString(R.string.slidedrawer_share),
				getResources().getString(R.string.slidedrawer_settings),
				getResources().getString(R.string.slidedrawer_about)
		};
		mPlanetIcons = new Drawable[] {
				getResources().getDrawable(R.drawable.ic_launcher),
				getResources().getDrawable(R.drawable.ic_action_person_hd),
				getResources().getDrawable(R.drawable.ic_action_time_hd),
				getResources().getDrawable(R.drawable.ic_action_share_hd),
				getResources().getDrawable(R.drawable.ic_action_settings_hd),
				getResources().getDrawable(R.drawable.ic_action_about_hd)
		};
		mDrawerRows = new ArrayList<SidebarRowModel>();
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[0], R.drawable.ic_launcher));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[1], R.drawable.ic_action_person_hd));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[2], R.drawable.ic_action_time_hd));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[3], R.drawable.ic_action_share_hd));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[4], R.drawable.ic_action_settings_hd));
		mDrawerRows.add(new SidebarRowModel(mPlanetTitles[5], R.drawable.ic_action_about_hd));
		
		// put the drawer on the layout
		DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.view_home_drawerlayout);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		mDrawerToggle = new ActionBarDrawerToggle(
			this, mDrawerLayout, R.drawable.ic_drawer,
			R.string.app_contentDescription, R.string.app_contentDescription) {
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getSupportActionBar().setTitle(mTitle);
				getSupportActionBar().setIcon(mIcon);
			}
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getSupportActionBar().setTitle(mTitle);
				getSupportActionBar().setIcon(mIcon);
			}
		};
		// put the array list on the drawer and set the drawer toggle as the DrawerListener
		ListView mDrawerList = (ListView) findViewById(R.id.view_home_leftdrawer);
		mDrawerList.setAdapter(new MainMenuAdapter(this, mDrawerRows));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener(mDrawerList, mDrawerLayout));
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// actionbar menu
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		if (savedInstanceState == null) selectItem(0);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		/*switch(item.getItemId()) {
			case R.id.menu_home:
				startActivity(new Intent(this, LocalConfigDataActivity.class));
				break;
			case R.id.menu_about:
				startActivity(new Intent(this, AboutActivity.class));
				break;
			default:;
		}*/
		return super.onOptionsItemSelected(item);
	}
	
	private void selectItem(int position) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		switch (position) {
			case 0:
				fragmentManager.beginTransaction().replace(R.id.view_home_contentframe, new MainMenuFragment()).commit();
				break;
			case 1:
				//fragmentManager.beginTransaction().replace(R.id.view_home_contentframe, ProfileFragment()).commit();
				break;
		/*case 2:
				fragment = new BookmarkFragment();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
				break;
			case 3:
				fragment = new PromoteFragment();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
				break;*/
			case 5:
				fragmentManager.beginTransaction().replace(R.id.view_home_contentframe, new AboutFragment()).commit();
				break;
		default:
			break;
		}
		mTitle = mPlanetTitles[position]; getSupportActionBar().setTitle(mTitle);
		mIcon = mPlanetIcons[position]; getSupportActionBar().setIcon(mIcon);
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class MainMenuAdapter extends BaseAdapter {
		private Context context;
		private List<SidebarRowModel> rows;
		public MainMenuAdapter(Context context, List<SidebarRowModel> rows) {
			this.context = context;
			this.rows = rows;
		}
		@Override
		public int getCount() {
			return (rows != null ? rows.size() : 0);
		}
		@Override
		public Object getItem(int position) {
			return rows.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override @SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_sidemenu, null);
			}
			((ImageView)convertView.findViewById(R.id.row_sidebar_icon)).setBackgroundResource(rows.get(position).getIconId());
			((TextView)convertView.findViewById(R.id.row_sidebar_label)).setText(rows.get(position).getLabel());
			return convertView;
		}
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		private ListView mDrawerList;
		private DrawerLayout mDrawerLayout;
		public DrawerItemClickListener(ListView mDrawerList, DrawerLayout mDrawerLayout) {
			this.mDrawerList = mDrawerList;
			this.mDrawerLayout = mDrawerLayout;
		}
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
			mDrawerList.setItemChecked(position, true);
			mDrawerLayout.closeDrawer(mDrawerList);
		}
	}
	
	private class SidebarRowModel {
		private String label = "label";
		private int iconId = -1;
		public SidebarRowModel(String label, int iconId) {
			this.label = label;
			this.iconId = iconId;
		}
		public String getLabel() {
			return label;
		}
		public int getIconId() {
			return iconId;
		}
	}
	
}
