package com.bemw.agendalize.model;

import org.json.JSONObject;

public class EmployeeModel {
	
	private String login;
	private String name;
	private String lastName;
	private String dateBirthday;
	private String gender;
	private String cpf;
	private String phoneNumber;
	private String cellPhoneNumber;
	private String workPhoneNumber;
	private String address;
	private String addressNumber;
	private String complement;
	private String zipCode;
	private String city;
	private String state;
	private String country;
	private String imageUrl;
	
	public EmployeeModel() {
		login = "";
		name = "";
		lastName = "";
		dateBirthday = "";
		gender = "";
		cpf = "";
		phoneNumber = "";
		cellPhoneNumber = "";
		workPhoneNumber = "";
		address = "";
		addressNumber = "";
		complement = "";
		zipCode = "";
		city = "";
		state = "";
		country = "";
		imageUrl = "";
	}
	public EmployeeModel(String login, String name, String lastName,
			String dateBirthday, String gender, String cpf, String phoneNumber,
			String cellPhoneNumber, String workPhoneNumber, String address,
			String addressNumber, String complement, String zipCode,
			String city, String state, String country, String imageUrl) {
		this();
		this.login = login;
		this.name = name;
		this.lastName = lastName;
		this.dateBirthday = dateBirthday;
		this.gender = gender;
		this.cpf = cpf;
		this.phoneNumber = phoneNumber;
		this.cellPhoneNumber = cellPhoneNumber;
		this.workPhoneNumber = workPhoneNumber;
		this.address = address;
		this.addressNumber = addressNumber;
		this.complement = complement;
		this.zipCode = zipCode;
		this.city = city;
		this.state = state;
		this.country = country;
		this.imageUrl = imageUrl;
	}
	public EmployeeModel(JSONObject obj) {
		this();
		try { login = obj.getString("login"); } catch (Exception e) {}
		try { name = obj.getString("name"); } catch (Exception e) {}
		try { lastName = obj.getString("lastName"); } catch (Exception e) {}
		try { dateBirthday = obj.getString("dateBirthday"); } catch (Exception e) {}
		try { gender = obj.getString("gender"); } catch (Exception e) {}
		try { cpf = obj.getString("cpf"); } catch (Exception e) {}
		try { phoneNumber = obj.getString("phoneNumber"); } catch (Exception e) {}
		try { cellPhoneNumber = obj.getString("cellPhoneNumber"); } catch (Exception e) {}
		try { workPhoneNumber = obj.getString("workPhoneNumber"); } catch (Exception e) {}
		try { address = obj.getString("address"); } catch (Exception e) {}
		try { addressNumber = obj.getString("addressNumber"); } catch (Exception e) {}
		try { complement = obj.getString("complement"); } catch (Exception e) {}
		try { zipCode = obj.getString("zipCode"); } catch (Exception e) {}
		try { city = obj.getString("city"); } catch (Exception e) {}
		try { state = obj.getString("state"); } catch (Exception e) {}
		try { country = obj.getString("country"); } catch (Exception e) {}
		try { imageUrl = obj.getString("imageUrl"); } catch (Exception e) {}
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDateBirthday() {
		return dateBirthday;
	}
	public void setDateBirthday(String dateBirthday) {
		this.dateBirthday = dateBirthday;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}
	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}
	public String getWorkPhoneNumber() {
		return workPhoneNumber;
	}
	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressNumber() {
		return addressNumber;
	}
	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}
	public String getComplement() {
		return complement;
	}
	public void setComplement(String complement) {
		this.complement = complement;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("login", login); } catch (Exception e) {}
		try { obj.put("name", name); } catch (Exception e) {}
		try { obj.put("lastName", lastName); } catch (Exception e) {}
		try { obj.put("dateBirthday", dateBirthday); } catch (Exception e) {}
		try { obj.put("gender", gender); } catch (Exception e) {}
		try { obj.put("cpf", cpf); } catch (Exception e) {}
		try { obj.put("phoneNumber", phoneNumber); } catch (Exception e) {}
		try { obj.put("cellPhoneNumber", cellPhoneNumber); } catch (Exception e) {}
		try { obj.put("workPhoneNumber", workPhoneNumber); } catch (Exception e) {}
		try { obj.put("address", address); } catch (Exception e) {}
		try { obj.put("addressNumber", addressNumber); } catch (Exception e) {}
		try { obj.put("complement", complement); } catch (Exception e) {}
		try { obj.put("zipCode", zipCode); } catch (Exception e) {}
		try { obj.put("city", city); } catch (Exception e) {}
		try { obj.put("state", state); } catch (Exception e) {}
		try { obj.put("country", country); } catch (Exception e) {}
		try { obj.put("imageUrl", imageUrl); } catch (Exception e) {}
		return obj;
	}

}
