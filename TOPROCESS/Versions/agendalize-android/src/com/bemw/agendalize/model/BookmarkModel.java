package com.bemw.agendalize.model;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONException;
import org.json.JSONObject;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.util.SetUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class BookmarkModel extends ArrayList<StoreModel> {
	
	private static final long serialVersionUID = -1895264379942538905L;

	public BookmarkModel() {}

	/**
	 * Returns a ordered-by-name Provider List, that is the bookmark
	 * @return
	 */
	public BookmarkModel fromJson(JSONObject bookmark) {
		ArrayList<String> provider_names = SetUtils.copyIterator(bookmark.keys());
		Collections.sort(provider_names);
		for (int i=0; i<provider_names.size(); i++) {
			try {
				add(new StoreModel(bookmark.getJSONObject(provider_names.get(i))));
			} catch (JSONException e) {
				// if exception occurs, don't add the object
			}
		}
		return this;
	}

	/**
	 * By default, the bookmark list is a JSONObject that contains JSONObject[ProviderModel] objects in the
	 * form: { "Id1":{ProviderModel1}, "Id2":{ProviderModel2}, ..., "IdN":{ProviderModelN} }
	 * @return
	 */
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		for (int i=0; i<size(); i++) {
			try {
				obj.put(get(i).getId()+"", get(i).toJson());
			} catch (JSONException e) {}
		}
		return obj;
	}
	
	public boolean hasTheRegistry(StoreModel pm) {
		if (toJson().has(pm.getId()+""))
			return true;
		else
			return false;
	}
	
	public BookmarkModel addRegistry(Context context, StoreModel pm) {
		add(pm);
		BookmarkModel.saveBookmark(context, this);
		return this;
	}
	
	public BookmarkModel removeRegistry(Context context, StoreModel pm) {
		for (int i=0; i<size(); i++) {
			if (get(i).getName() == pm.getName() && get(i).getId() == pm.getId()) {
				remove(i);
				break;
			}
		}
		BookmarkModel.saveBookmark(context, this);
		return this;
	}
	
	public static BookmarkModel loadBookmark(Context context) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		String bookmark_data = prefsPrivate.getString(AppConstants.BOOKMARK, null);
		BookmarkModel bookmark = new BookmarkModel();
		try {
			return bookmark.fromJson(new JSONObject(bookmark_data));
		} catch (Exception e) {
			return bookmark;
		}
	}
	
	public static void saveBookmark(Context context, BookmarkModel bookmark) {
		SharedPreferences prefsPrivate = context.getSharedPreferences(AppConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
		Editor prefsPrivateEditor = prefsPrivate.edit();
		prefsPrivateEditor.putString(AppConstants.BOOKMARK, bookmark.toJson().toString());
		prefsPrivateEditor.commit();
	}

}
