package com.bemw.agendalize;

import com.bemw.agendalize.R;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class AboutFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_about, container, false);

		/* Starts the web browser activity redirecting to social page's contact url */
		((Button) rootView.findViewById(R.id.frag_about_facebook)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_FACEBOOK));
				startActivity(browserIntent);
			}
		});
		((Button) rootView.findViewById(R.id.frag_about_twitter)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_TWITTER));
				startActivity(browserIntent);
			}
		});
		
		return rootView;
	}
	
}
