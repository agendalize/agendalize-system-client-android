package com.bemw.agendalize.ui.store;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.model.StoreSearchRequestModel;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * @author Marcelo Tomio Hama
 * An activity intended to show a set of stores, either in map or list
 */
public class SetOfStoresActivity extends ActionBarActivity {
	
	private List<StoreModel> stores;
	private StoreSearchRequestModel storeSearchRequest;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setofstores);
		
		try { // populating the store list, this activity must receive something!
			stores = new ArrayList<StoreModel>();
			JSONArray arr = new JSONArray(getIntent().getStringExtra("stores"));
			for (int i=0; arr!=null && i<arr.length(); i++) {
				stores.add(new StoreModel(arr.getJSONObject(i)));
			}
			storeSearchRequest = new StoreSearchRequestModel(new JSONObject(getIntent().getStringExtra("searchParams")));
			getSupportActionBar().setTitle(getResources().getString(R.string.label_stores));
			getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_feedstores));
		} catch (Exception e) {
			finish();
		}
		
		((Button) findViewById(R.id.activity_setofstores_listbtn)).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				selectItem(0);
			}
		});
		((Button) findViewById(R.id.activity_setofstores_mapbtn)).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				selectItem(1);
			}
		});
		
		if (savedInstanceState == null) selectItem(0);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void selectItem(int position) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		switch (position) {
			case 0:
				fragmentManager.beginTransaction().replace(R.id.activity_setofstores_contentframe, new StoresListFragment()).commit();
				break;
			case 1:
				fragmentManager.beginTransaction().replace(R.id.activity_setofstores_contentframe, new StoresMapFragment()).commit();
				break;
			default:;
		}
	}
	
	public List<StoreModel> getStores() {
		return stores;
	}
	
	public StoreSearchRequestModel getSearchContext() {
		return storeSearchRequest;
	}

}
