package com.bemw.agendalize.ui.feedback;

import org.apache.http.HttpStatus;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.util.DevicePlatform;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class FeedStoreActivity extends ActionBarActivity {
	
	private static EditText storeName = null;
	private static EditText storeAddress = null;
	private static EditText storeContact = null;
	private static TextView maintext = null;
	private static LinearLayout donetext = null;
	private static LinearLayout mainlayout = null;
	private static LinearLayout donelayout = null;
	private static ScrollView scroll = null;
	private Button feedBtn = null;
	private Button doneBtn = null;
	private static ProgressDialog waitDialog = null;
	private static Thread contentLoader = null;
	private static boolean isLoading = false;
	private static FeedStoreActivity context;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = FeedStoreActivity.this;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
        setContentView(R.layout.activity_feedstore);
        
        storeName = (EditText)findViewById(R.id.activity_feedstore_storename);
        storeAddress = (EditText)findViewById(R.id.activity_feedstore_storeaddress);
        storeContact = (EditText)findViewById(R.id.activity_feedstore_storecontact);
        maintext = (TextView)findViewById(R.id.activity_feedstore_maintext);
    	donetext = (LinearLayout)findViewById(R.id.activity_feedstore_donetext);
    	mainlayout = (LinearLayout)findViewById(R.id.activity_feedstore_mainlayout);
    	donelayout = (LinearLayout)findViewById(R.id.activity_feedstore_donelayout);
    	scroll = (ScrollView)findViewById(R.id.activity_feedstore_scroll);
        feedBtn = (Button)findViewById(R.id.activity_feedstore_feedbtn);
        feedBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (storeName.getText().toString().length() > 0 &&
					(storeAddress.getText().toString().length() > 0 ||
					storeContact.getText().toString().length() > 0)) {
					createNewLoader();
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(FeedStoreActivity.this);
					builder.setMessage(getResources().getString(R.string.label_pleasefillfeedinfo)).setCancelable(false).
					setPositiveButton(getResources().getString(R.string.btn_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
				}
			}
		});
        doneBtn = (Button)findViewById(R.id.activity_feedstore_donebtn);
    	doneBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
    	
        getSupportActionBar().setTitle(getResources().getString(R.string.main_feedstore));
		getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_feedstores));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back:
				finish();
				break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private static Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (waitDialog != null) waitDialog.dismiss();
			switch (msg.what) {
				case HttpStatus.SC_REQUEST_TIMEOUT:
					AppConstants.justShowDialogWarning(context, R.string.label_timeout);
					break;
				case -1: case HttpStatus.SC_NOT_FOUND:
					AppConstants.justShowDialogWarning(context, R.string.label_noconnection);
					break;
				case HttpStatus.SC_INTERNAL_SERVER_ERROR:
					AppConstants.justShowDialogWarning(context, R.string.label_servererror);
					break;
				case HttpStatus.SC_OK:
					maintext.setVisibility(View.GONE);
					donetext.setVisibility(View.VISIBLE);
					mainlayout.setVisibility(View.GONE);
					donelayout.setVisibility(View.VISIBLE);
					scroll.setVisibility(View.GONE);
					break;
			}
			isLoading = false;
		}
	};
	
	private static final Runnable loadContent = new Runnable() {
		@Override
		public void run() {
			if (!DevicePlatform.isConnectedToInternet(context)) {
				handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
			} else {
				RESTClient client = new RESTClient();
				client.sendStoreSugestion(
						storeName.getText().toString(),
						storeAddress.getText().toString(),
						storeContact.getText().toString());
				handler.sendEmptyMessage(client.getStatus());
			}
		}
	};
	
	synchronized private static void createNewLoader() {
		if (!isLoading) {
			isLoading = true;
			waitDialog = ProgressDialog.show(
					context,
					context.getText(R.string.label_sendingdata),
					context.getText(R.string.label_pleasewait),
					false,
					false);
			waitDialog.setIcon(context.getResources().getDrawable(R.drawable.ic_launcher));
			contentLoader = new Thread(loadContent);
			contentLoader.start();
		}
	}

}
