package com.bemw.agendalize.ui.store;

import java.util.List;

import org.apache.http.HttpStatus;

import com.bemw.agendalize.AppConstants;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.model.StoreSearchRequestModel;
import com.bemw.agendalize.net.RESTClient;
import com.bemw.agendalize.util.DevicePlatform;
import com.bemw.agendalize.util.ImageUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class StoresListFragment extends Fragment {
	
	private int lastLoadCount = -1;
	private static Thread contentLoader = null;
	private static boolean isLoading = false;
	private RESTClient client = null;
	private static ListView storeList;
	private static StoreListAdapter storeListAdapter;
	private static SetOfStoresActivity context;
	private StoreSearchRequestModel storeSearchRequest;
	private List<StoreModel> stores;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storelist, container, false);
		context = ((SetOfStoresActivity)getActivity());
		storeSearchRequest = context.getSearchContext();
		stores = context.getStores();
		lastLoadCount = stores.size();
		
		// widgets and components
		storeList = (ListView) rootView.findViewById(R.id.frag_storelist_listview);
		storeList.setVisibility(View.VISIBLE);
    	storeListAdapter = new StoreListAdapter();
    	
    	storeList.setAdapter(storeListAdapter);
		storeList.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {}
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if (
					storeList.getLastVisiblePosition()+1 == totalItemCount && // user has reached the end of the list
					storeList.getAdapter().getCount() >= AppConstants.SIZE_LIST && // only enable pagination if we already done at least 1 load
					lastLoadCount == AppConstants.SIZE_LIST && // last load had fetch a complete page
					lastLoadCount != -1
				)
					createNewLoader();
			}
		});
		// add the informative item, if there is more contacts to load
		if (
			stores.size() > 0 && // it makes no sense to show "loading more" with an empty list
			lastLoadCount == AppConstants.SIZE_LIST
		) {
			StoreModel m = new StoreModel();
			m.setName("LOAD_MORE");
			stores.add(m);
			storeListAdapter.notifyDataSetChanged();
		}
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		storeList.setOnItemClickListener(new OnItemClickListener() {
			@Override
		    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
				Intent storDetail = new Intent(getActivity(), StoreDetailActivity.class);
				storDetail.putExtra("store", context.getStores().get(position).toJson().toString());
				storDetail.putExtra("search", context.getSearchContext().toJson().toString());
				startActivity(storDetail);
			}
		});
	}
	
	private static Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case HttpStatus.SC_REQUEST_TIMEOUT:
					AppConstants.justShowDialogWarning(context, R.string.label_timeout);
					break;
				case -1: case HttpStatus.SC_NOT_FOUND:
					AppConstants.justShowDialogWarning(context, R.string.label_noconnection);
					break;
				case HttpStatus.SC_INTERNAL_SERVER_ERROR:
					AppConstants.justShowDialogWarning(context, R.string.label_servererror);
					break;
				case HttpStatus.SC_OK:
					storeList.setOnItemClickListener(new OnItemClickListener() {
						@Override
					    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
							Intent storDetail = new Intent(context, StoreDetailActivity.class);
							storDetail.putExtra("store", context.getStores().get(position).toJson().toString());
							storDetail.putExtra("search", context.getSearchContext().toJson().toString());
							context.startActivity(storDetail);
						}
					});
					storeListAdapter.notifyDataSetChanged();
					break;
			}
			isLoading = false;
		}
	};
	
	private Runnable loadContent = new Runnable() {
		@Override
		public void run() {
			if (!DevicePlatform.isConnectedToInternet(context)) {
				handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
			} else {
				// fetch data from server and add then to the list
				storeSearchRequest.setListOffSet(storeSearchRequest.getListOffSet() + AppConstants.SIZE_LIST);
				client = new RESTClient();
				List<StoreModel> returnedStores = client.getStoresByContext(storeSearchRequest.toJson());
				if (client.getStatus() == HttpStatus.SC_OK) {
					// get the number of incoming contacts, returns if there is none
					lastLoadCount = returnedStores.size();
					if (lastLoadCount == 0) return;
					// removes the informative item
					if (stores.size() > 0 && stores.get(stores.size()-1).getName().equals("LOAD_MORE")) {
						stores.remove(stores.size()-1);
					}
					// attach each contact to the list
					for (int i=0; i<returnedStores.size(); i++) {
						stores.add(returnedStores.get(i));
					}
					// add the informative item, if there is more contacts to load
					if (
						stores.size() > 0 && // it makes no sense to show "loading more" with an empty list
						lastLoadCount == AppConstants.SIZE_LIST
					) {
						StoreModel m = new StoreModel();
						m.setName("LOAD_MORE");
						stores.add(m);
					}
				}
				handler.sendEmptyMessage(client.getStatus());
			}
		};
	};
	
	synchronized private void createNewLoader() {
		if (!DevicePlatform.isConnectedToInternet(context)) {
			AppConstants.justShowDialogWarning(context, R.string.label_noconnection);
		} else if (!isLoading) {
			isLoading = true;
			contentLoader = new Thread(loadContent);
			contentLoader.start();
		}
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class StoreListAdapter extends BaseAdapter {
		public StoreListAdapter() {}
		@Override
		public int getCount() {
			return stores.size();
		}
		@Override
		public Object getItem(int position) {
			return stores.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@SuppressLint("InflateParams") @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (stores.get(position).getName().equals("LOAD_MORE")) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_helperloading, null);
				TextView textinfo = ((TextView) convertView.findViewById(R.id.row_helperloading_infotxt));
				textinfo.setText(context.getString(R.string.label_loadingdata));
				return convertView;
			}
			
			// predefines the view as an inflated layout
			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_storeitem, null);
			ImageView photo = ((ImageView) convertView.findViewById(R.id.row_storeitem_photo));
			TextView name = ((TextView) convertView.findViewById(R.id.row_storeitem_name));
			RatingBar rating = ((RatingBar)convertView.findViewById(R.id.row_storeitem_rating));
			TextView info1 = ((TextView) convertView.findViewById(R.id.row_storeitem_info1));
			TextView info2 = ((TextView) convertView.findViewById(R.id.row_storeitem_info2));
			TextView address1 = ((TextView) convertView.findViewById(R.id.row_storeitem_address1));
			TextView address2 = ((TextView) convertView.findViewById(R.id.row_storeitem_address2));
			name.setText(stores.get(position).getName());
			rating.setRating(stores.get(position).getEvaluation()*5/100);
			info1.setText(getResources().getString(R.string.label_nremployees) + " " + stores.get(position).getNrEmployees());
			info2.setText(getResources().getString(R.string.label_nrservices) + " " + stores.get(position).getNrServices());
			address1.setText(stores.get(position).getAddressInfo().getCity() + ", " + stores.get(position).getAddressInfo().getState());
			address2.setText(stores.get(position).getAddressInfo().getAddress() + " " + stores.get(position).getAddressInfo().getAddressNumber());
			if (stores.get(position).getUrlPhoto() != null) {
				if (stores.get(position).getUrlPhoto().length() > 0) {
					new PhotoLoader(photo, stores.get(position).getUrlPhoto()).execute();
				}
			}
			return convertView;
		}
	}
	
	private class PhotoLoader extends AsyncTask<Void, Void, Void> {
		private Drawable drawable;
		private ImageView photo;
		private String url;
		public PhotoLoader(ImageView photo, String url) {
			this.photo = photo;
			this.url = url;
		}
		@Override
		protected Void doInBackground(Void... params) {
			drawable = ImageUtils.loadCachedImage(getActivity(), url);
			Bitmap bitmap =  ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
			drawable = new BitmapDrawable(getResources(), bitmap);
			return null;
		}
		@SuppressWarnings("deprecation") @Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			photo.setBackgroundDrawable(drawable);
		}
	}

}
