package com.bemw.agendalize;

import java.util.List;

/*import com.bemw.agendalize.ui.feedback.FeedStoreActivity;*/
//import com.bemw.agendalize.ui.search.SearchScheduleActivity;
import com.bemw.agendalize.R;
import com.bemw.agendalize.model.BookmarkModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.ui.feedback.FeedStoreActivity;
import com.bemw.agendalize.ui.feedback.FeedbackActivity;
import com.bemw.agendalize.ui.profile.BookmarkActivity;
import com.bemw.agendalize.ui.search.SearchScheduleActivity;
/*import com.bemw.agendalize.net.RESTClient;*/
import com.bemw.agendalize.R.id;
import com.bemw.agendalize.R.layout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class MainMenuFragment extends Fragment {

	private static Thread contentLoader = null;
	private static boolean isLoading = false;
	//private RESTClient wsConnection = null;
	private static ProgressDialog waitDialog = null;
	private List<StoreModel> stores;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_mainmenu, container, false);
				
		((Button) rootView.findViewById(R.id.frag_mainmenu_searchbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), SearchScheduleActivity.class));
			}
		});
		((Button) rootView.findViewById(R.id.frag_mainmenu_nearmebtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//createNewLoader();
			}
		});
		
		((Button) rootView.findViewById(R.id.frag_mainmenu_bookmarkbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), BookmarkActivity.class));
			}
		});
		((Button) rootView.findViewById(R.id.frag_mainmenu_feedstorebtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), FeedStoreActivity.class));
			}
		});
		((Button) rootView.findViewById(R.id.frag_mainmenu_feedbackbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), FeedbackActivity.class));
			}
		});
		
		return rootView;
	}
	
	/*@SuppressLint("HandlerLeak")
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 0: // displays connection fail
					handler.removeCallbacks(timeoutRunnable);
					showInternetConnectionFailure();
					break;
				case 1: // displays connection timeout
					handler.removeCallbacks(timeoutRunnable);
					showInternetConnectionTimeout();
					break;
				case 2: // displays service error
					handler.removeCallbacks(timeoutRunnable);
					showErrorMessage();
					break;
				case 3: // success (timeoutRunnable is already removed)
					Intent storeList = new Intent(getActivity(), StoresActivity.class);
					JSONArray arr = new JSONArray();
					for (int i=0; stores!=null && i<stores.size(); i++) {
						arr.put(stores.get(i).toJson());
					}
					storeList.putExtra("stores", arr.toString());
					storeList.putExtra("title", getResources().getString(R.string.menu_search));
					startActivity(storeList);
					break;
				case 4: // empty result (timeoutRunnable is already removed)
					showNoResultsMessage();
					break;
				default:;
			}
			wsConnection.disconnect();
			if (waitDialog != null) {
				waitDialog.dismiss();
				waitDialog = null;
			}
			isLoading = false;
		}
	};
	
	synchronized private void createNewLoader() {
		if (!DevicePlatform.isConnectedToInternet(getActivity())) {
			showInternetConnectionFailure();
			return;
		}
		if (waitDialog == null) {
			waitDialog = ProgressDialog.show(getActivity(), getText(R.string.app_name), getText(R.string.label_searchingstores), false, false);
			waitDialog.setIcon(getResources().getDrawable(R.drawable.ic_action_search));
			waitDialog.setOnCancelListener(new OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					// canceling the search if the user presses the back/home button
					try {
						wsConnection.disconnect();
						handler.removeCallbacks(timeoutRunnable);
				    	isLoading = false;
					} catch (Exception e) {}
				}
			});
		}
		if (!isLoading) {
			isLoading = true;
			handler.postDelayed(timeoutRunnable, InternalConstants.CONN_TIME_OUT);
			contentLoader = new Thread(loadContent);
			contentLoader.start();
		}
	}
	
	private final Runnable loadContent = new Runnable() {
		@Override
		public void run() {
			try {
				wsConnection = new RESTClient();
				if (!DevicePlatform.isConnectedToInternet(getActivity())) {
					handler.sendEmptyMessage(0);
				} else {
					// firstly, convert the address into a valid LatLon
					double[] LatLon = new double[2];
					SharedPreferences privatePrefs = getActivity().getSharedPreferences(InternalConstants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
					String lat = privatePrefs.getString(InternalConstants.LATITUDE, "0");
					String lon = privatePrefs.getString(InternalConstants.LONGITUDE, "0");
					LatLon[0] = Double.valueOf(lat);
					LatLon[1] = Double.valueOf(lon);
					// construct the json request
					StoreSearchRequestModel request = new StoreSearchRequestModel();
					if (ServiceListActivity.services == null) {
						wsConnection = new RESTClient();
						if (DevicePlatform.isConnectedToInternet(getActivity()))
							ServiceListActivity.services = wsConnection.GetServicesList();
					}
					for (int i=0; i<ServiceListActivity.services.size(); i++) request.getServices().add(ServiceListActivity.services.get(i).getCod());
					request.setLatitude(LatLon[0]);
					request.setLongitude(LatLon[1]);
					request.setHourFrom(String.format("%02d", (0/3600))+String.format("%02d",(0%60)));
					request.setHourTo(String.format("%02d", (86399/3600))+String.format("%02d",(86399%60)));
					request.setBaseDate(Calendar.getInstance().getTime().getTime());
					request.setRange(100*1000);
					// get the list of available stores
					stores = wsConnection.GetStoresByContext(request.toJson());
					handler.removeCallbacks(timeoutRunnable);
					if (stores.size() > 0) {
						handler.sendEmptyMessage(3);
					} else {
						handler.sendEmptyMessage(4);
					}
				}
			} catch (Exception e) {
				handler.sendEmptyMessage(2);
			}
		}
	};
	
	private final Runnable timeoutRunnable = new Runnable() {
		@Override
		public void run() {
			if (wsConnection != null) {
				wsConnection.disconnect();
			}
			handler.sendEmptyMessage(1);
		}
	};*/
	
	/*private void showInternetConnectionTimeout() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getActivity().getResources().getString(R.string.label_connectiontimeout)).setCancelable(false).
		setPositiveButton(getActivity().getResources().getString(R.string.btn_ok),
			new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {}
			}
		);
		final AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void showInternetConnectionFailure() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getActivity().getResources().getString(R.string.label_connectionfail)).setCancelable(false).
		setPositiveButton(getActivity().getResources().getString(R.string.btn_ok),
			new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {}
			}
		);
		final AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void showNoResultsMessage() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getActivity().getResources().getString(R.string.label_searchwithnoresults)).setCancelable(false).
		setPositiveButton(getActivity().getResources().getString(R.string.btn_ok),
			new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {}
			}
		);
		final AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void showErrorMessage() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getActivity().getResources().getString(R.string.label_anerroroccurred)).setCancelable(false).
		setPositiveButton(getActivity().getResources().getString(R.string.btn_ok),
			new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {}
			}
		);
		final AlertDialog alert = builder.create();
		alert.show();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		languageInUse = LanguageDAO.loadLangPreferences(getActivity());
		LanguageTools.setLocalizationStringValues(getActivity(), LanguageTools.LANGUAGES.valueOf(languageInUse.toUpperCase(Locale.getDefault())));
	}*/
	
}
