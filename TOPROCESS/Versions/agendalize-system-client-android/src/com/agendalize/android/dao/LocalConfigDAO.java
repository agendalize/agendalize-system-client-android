package com.agendalize.android.dao;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.agendalize.android.model.LocalConfigModel;

public class LocalConfigDAO {
	
	// local configuration persistence
	public static LocalConfigModel getLocalConfig(Context context) {
		SharedPreferences prefsPrivate = context.getSharedPreferences("PRIVATE_PREFERENCES", Context.MODE_PRIVATE);
		String lu = prefsPrivate.getString("LOCAL_USER", null);
		try {
			return new LocalConfigModel(new JSONObject(lu));
		} catch (Exception e) {
			return new LocalConfigModel();
		}
	}
	
	public static void setLocalConfig(Context context, LocalConfigModel config) {
		SharedPreferences prefsPrivate = context.getSharedPreferences("PRIVATE_PREFERENCES", Context.MODE_PRIVATE);
		Editor prefsPrivateEditor = prefsPrivate.edit();
		prefsPrivateEditor.putString("LOCAL_USER", config.toJson().toString());
		prefsPrivateEditor.commit();
	}
	
	public static void saveLatLonPreferences(String latitude, String longitude, Context context) {
		SharedPreferences prefsPrivate = context.getSharedPreferences("PRIVATE_PREFERENCES", Context.MODE_PRIVATE);
		Editor prefsPrivateEditor = prefsPrivate.edit();
		prefsPrivateEditor.putString("LATITUDE", latitude);
		prefsPrivateEditor.putString("LONGITUDE", longitude);
		prefsPrivateEditor.commit();
	}
	
	public static double[] getLatLonPreferences(Context context) {
		double latlon[] = new double[2];
		SharedPreferences privatePrefs = context.getSharedPreferences("PRIVATE_PREFERENCES", Context.MODE_PRIVATE);
		latlon[0] = Double.valueOf(privatePrefs.getString("LATITUDE", "0"));
		latlon[1] = Double.valueOf(privatePrefs.getString("LONGITUDE", "0"));
		return latlon;
	}
	
}
