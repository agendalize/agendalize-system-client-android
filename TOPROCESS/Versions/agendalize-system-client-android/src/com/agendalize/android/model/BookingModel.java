package com.agendalize.android.model;

import org.json.JSONObject;

public class BookingModel {
	
	private long id;
	// data of the service
	private long serviceId;
	private String status;
	// booked day/hour
	private long dayHourBegin;
	private long dayHourEnd;
	// data of the employee
	private long employeeId;
	private long dayAvailableId;
	
	public BookingModel() {
		id = -1;
		serviceId = -1;
		status = "status";
		dayHourBegin = -1;
		dayHourEnd = -1;
		employeeId = -1;
		dayAvailableId = -1;
	}
	public BookingModel(long id, long serviceId, String status, long dayHourBegin,
			long dayHourEnd, long employeeId, long dayAvailableId) {
		this();
		this.id = id;
		this.serviceId = serviceId;
		this.status = status;
		this.dayHourBegin = dayHourBegin;
		this.dayHourEnd = dayHourEnd;
		this.employeeId = employeeId;
		this.dayAvailableId = dayAvailableId;
	}
	public BookingModel(JSONObject obj) {
		this();
		try { id = obj.getLong("id"); } catch (Exception e) {}
		try { serviceId = obj.getLong("serviceId"); } catch (Exception e) {}
		try { status = obj.getString("status"); } catch (Exception e) {}
		try { dayHourBegin = obj.getLong("dayHourBegin"); } catch (Exception e) {}
		try { dayHourEnd = obj.getLong("dayHourEnd"); } catch (Exception e) {}
		try { employeeId = obj.getLong("employeeId"); } catch (Exception e) {}
		try { dayAvailableId = obj.getLong("dayAvailableId"); } catch (Exception e) {}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getServiceId() {
		return serviceId;
	}
	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getDayHourBegin() {
		return dayHourBegin;
	}
	public void setDayHourBegin(long dayHourBegin) {
		this.dayHourBegin = dayHourBegin;
	}
	public long getDayHourEnd() {
		return dayHourEnd;
	}
	public void setDayHourEnd(long dayHourEnd) {
		this.dayHourEnd = dayHourEnd;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public long getDayAvailableId() {
		return dayAvailableId;
	}
	public void setDayAvailableId(long dayAvailableId) {
		this.dayAvailableId = dayAvailableId;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try { obj.put("id", id); } catch (Exception e) {}
		try { obj.put("serviceId", serviceId); } catch (Exception e) {}
		try { obj.put("status", status); } catch (Exception e) {}
		try { obj.put("dayHourBegin", dayHourBegin); } catch (Exception e) {}
		try { obj.put("dayHourEnd", dayHourEnd); } catch (Exception e) {}
		try { obj.put("employeeId", employeeId); } catch (Exception e) {}
		try { obj.put("dayAvailableId", dayAvailableId); } catch (Exception e) {}
		return obj;
	}
	
}
