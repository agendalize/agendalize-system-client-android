package com.agendalize.android;

import com.agendalize.android.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;

public class AppConstants {
	
	public static final String TAG = "Agendalize";
	public static final String CACHE_FILE = "AgendalizeCache";
	public static final String domain = "http://www.agendalize.com.br/qa/";
	public static final int POSITION_TIMER = 5*60000;
	public static final int BOOTSPLASH_TIMER = 2000;
	
	public static final String PRIVATE_PREFERENCES = "PRIVATE_PREFERENCES";
	
	public static final int DATEHOUR_RETURN_VALUE = 0;
	public static final int SERVICE_RETURN_VALUE = 1;
	
	public static Drawable getServicePhotoByType(Context context, long type) {
		switch ((int)type) {
			case 1: return context.getResources().getDrawable(R.drawable.depilacao);
			case 2: return context.getResources().getDrawable(R.drawable.cabelo);
			case 3: return context.getResources().getDrawable(R.drawable.pele);
			case 4: return context.getResources().getDrawable(R.drawable.unha);
			case 5: return context.getResources().getDrawable(R.drawable.maquiagem);
			case 6: return context.getResources().getDrawable(R.drawable.spa);
			case 7: return context.getResources().getDrawable(R.drawable.massagem);
			default: return context.getResources().getDrawable(R.drawable.noimage);
		}
	};
	
	public static void justShowDialogWarning(Context context, int messageId) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(context.getResources().getString(messageId)).setCancelable(false).
		setPositiveButton(context.getResources().getString(R.string.btn_ok),
			new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {}
			}
		);
		final AlertDialog alert = builder.create();
		alert.show();
	};	
	
}
