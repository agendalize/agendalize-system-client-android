package com.agendalize.android.ui.store;

import java.util.List;

import com.agendalize.android.R;
import com.agendalize.android.model.StoreModel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class StoresListFragment extends Fragment {
	
	private ListView storeList;
	private StoreListAdapter storeListAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storelist, container, false);
		
		storeList = (ListView) rootView.findViewById(R.id.frag_storelist_listview);
		storeListAdapter = new StoreListAdapter(getActivity(), ((SetOfStoresActivity)getActivity()).getStores());
		storeList.setAdapter(storeListAdapter);
		storeList.setOnItemClickListener(new OnItemClickListener() {
			@Override
		    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
				Intent storDetail = new Intent(getActivity(), StoreDetailActivity.class);
				storDetail.putExtra("storeData", ((SetOfStoresActivity)getActivity()).getStores().get(position).toJson().toString());
				// TODO: bellow line is just for test... remove it later
				storDetail.putExtra("serviceIDs", "[13]");
				startActivity(storDetail);
			}
		});
		storeList.setFastScrollEnabled(true);
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	private class StoreListAdapter extends BaseAdapter {
		private List<StoreModel> stores;
		private Context context;
		public StoreListAdapter(Context context, List<StoreModel> stores) {
			this.stores = stores;
			this.context = context;
		}
		@Override
		public int getCount() {
			return stores.size();
		}
		@Override
		public Object getItem(int position) {
			return stores.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@SuppressLint("InflateParams") @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// predefines the view as an inflated layout
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_storeitem, null);
			}
			//ImageView photo = ((ImageView) convertView.findViewById(R.id.row_storeitem_photo));
			TextView name = ((TextView) convertView.findViewById(R.id.row_storeitem_name));
			RatingBar rating = ((RatingBar)convertView.findViewById(R.id.row_storeitem_rating));
			//TextView info1 = ((TextView) convertView.findViewById(R.id.row_storeitem_info1));
			//TextView info2 = ((TextView) convertView.findViewById(R.id.row_storeitem_info2));
			TextView address1 = ((TextView) convertView.findViewById(R.id.row_storeitem_address1));
			TextView address2 = ((TextView) convertView.findViewById(R.id.row_storeitem_address2));
			name.setText(stores.get(position).getName());
			rating.setRating(4.5f);
			//info1.setText(stores.get(position).);
			//info2.setText(stores.get(position).);
			address1.setText(stores.get(position).getAddress().getCity() + ", " + stores.get(position).getAddress().getState());
			address2.setText(stores.get(position).getAddress().getAddress() + " " + stores.get(position).getAddress().getAddressNumber());
			/*photo.setBackgroundDrawable();*/
			return convertView;
		}
	}

}
