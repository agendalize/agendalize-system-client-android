package com.agendalize.android.ui.search;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;

import com.agendalize.android.AppConstants;
import com.agendalize.android.ui.store.SetOfStoresActivity;
import com.agendalize.android.R;
import com.agendalize.android.model.ServiceModel;
import com.agendalize.android.model.StoreModel;
import com.agendalize.android.net.IRESTClient;
import com.agendalize.android.net.RESTClient;
import com.agendalize.android.net.StoreSearchRequestModel;
import com.agendalize.android.util.DevicePlatform;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;

/**
 * @author Marcelo Tomio Hama
 * An activity intended to prompt users for a filtered search overs all stores
 * This class is 100% done!
 */
public class SearchScheduleActivity extends ActionBarActivity implements IRESTClient {

	// data for searches
	private String baseAddress = "";
	private int rangeIndex;
	private int hourFrom;
	private int hourTo;
	private Calendar dayTm;
	private List<ServiceModel> services = null;
	// data for store fetch
	private RESTClient client = null;
	private ProgressDialog waitDialog = null;
	private List<StoreModel> stores = null;
	// widgets, we maintain them as global fields because we need reference to them we call search web service
	private EditText locationEdit = null;
	private Spinner rangeCb = null;
	private Button dayhourBtn = null;
	private Button serviceBtn1 = null;
	private Button serviceBtn2 = null;
	private Button serviceBtn3 = null;
	private Button serviceBtn4 = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_searchschedule);
		
		locationEdit = (EditText) findViewById(R.id.view_searchschedule_locationEdit);
		rangeCb = (Spinner) findViewById(R.id.view_searchschedule_rangecb);
		dayhourBtn = (Button) findViewById(R.id.view_searchschedule_datehourbtn);
		serviceBtn1 = (Button) findViewById(R.id.view_searchschedule_service1btn);
		serviceBtn2 = (Button) findViewById(R.id.view_searchschedule_service2btn);
		serviceBtn3 = (Button) findViewById(R.id.view_searchschedule_service3btn);
		serviceBtn4 = (Button) findViewById(R.id.view_searchschedule_service4btn);
		
		getSupportActionBar().setTitle(getResources().getString(R.string.searchschedule_search));
		getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_search_hd));
		
		getDefaultValuesForFields();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		drawWidgets();
		// actions for navigation
		((Button) findViewById(R.id.view_searchschedule_clearbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getDefaultValuesForFields();
				onResume();
			}
		});
		((Button) findViewById(R.id.view_searchschedule_searchbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				{ // TODO: remove test: uncomment bellow block and comment/delete this block
					client = new RESTClient(SearchScheduleActivity.this, SearchScheduleActivity.this);
					client.getStores();
					waitDialog = ProgressDialog.show(SearchScheduleActivity.this, getText(R.string.label_sendingdata), getText(R.string.label_pleasewait), false, true);
					waitDialog.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
					waitDialog.setOnCancelListener(new OnCancelListener() {
						public void onCancel(DialogInterface dialog) {
							client.disconnect();
						}
					});
				}
				/*if (isFieldsOk()) {
					// firstly, convert the address into a valid LatLon
					client = new RESTClient(SearchScheduleActivity.this, SearchScheduleActivity.this);
					client.getLocationInfo(baseAddress);
					waitDialog = ProgressDialog.show(SearchScheduleActivity.this, getText(R.string.label_sendingdata), getText(R.string.label_pleasewait), false, true);
					waitDialog.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
					waitDialog.setOnCancelListener(new OnCancelListener() {
						public void onCancel(DialogInterface dialog) {
							client.disconnect();
						}
					});
				}*/
			}
		});
		// scrolls to top
		((ScrollView) findViewById(R.id.view_searchschedule_scroll)).scrollTo(0, 0);
		((ScrollView) findViewById(R.id.view_searchschedule_scroll)).scrollTo(0, 0);
	}
	
	private StoreSearchRequestModel getSearchParams(double[] LatLon) {
		// construct the json request
		StoreSearchRequestModel request = new StoreSearchRequestModel();
		for (int i=0; i<services.size(); i++) request.getServices().add(services.get(i).getCod());
		request.setLatitude(LatLon[0]);
		request.setLongitude(LatLon[1]);
		request.setHourFrom(String.format("%02d", (hourFrom/3600))+String.format("%02d",(hourFrom%60)));
		request.setHourTo(String.format("%02d", (hourTo/3600))+String.format("%02d",(hourTo%60)));
		request.setBaseDate(dayTm.getTime().getTime());
		request.setRange(((rangeIndex == 0 ? 10 : (rangeIndex == 1 ? 25 : 50)))*1000);
		Log.v(AppConstants.TAG, request.toJson().toString());
		return request;
	}
	
	private void drawWidgets() {
		// location address data, need to be in onResume to proper update on activityForResult return
		locationEdit.setText(baseAddress);
		locationEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				baseAddress = locationEdit.getText().toString();
			}
		});
		// range, need to be in onResume to proper update on activityForResult return
		ArrayAdapter<CharSequence> spinnerArrayAdapter = new ArrayAdapter<CharSequence>(
				this, android.R.layout.simple_spinner_item, new CharSequence[] {
						getText(R.string.label_distanceby10km),
						getText(R.string.label_distanceby25km),
						getText(R.string.label_distanceby50km) });
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		rangeCb.setAdapter(spinnerArrayAdapter);
		rangeCb.setSelection(rangeIndex);
		rangeCb.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1, int selectedItem, long arg3) {
				rangeIndex = selectedItem;
			}
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
		// date and hour, need to be in onResume to proper update on activityForResult return
		dayhourBtn.setText(
			String.format("%02d", dayTm.get(Calendar.DAY_OF_MONTH))
			+"/"+String.format("%02d", dayTm.get(Calendar.MONTH))
			+"/"+String.format("%04d", dayTm.get(Calendar.YEAR))
			+" - "+getResources().getString(R.string.label_fromhour)
			+" "+String.format("%02d", (hourFrom/3600))+":"+String.format("%02d",(hourFrom%60))
			+" "+getResources().getString(R.string.label_tohour)
			+" "+String.format("%02d", (hourTo/3600))+":"+String.format("%02d",(hourTo%60))
		);
		dayhourBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(SearchScheduleActivity.this, ChooseDateHourActivity.class);
				i.putExtra("dayTm", dayTm.getTime().getTime());
				i.putExtra("fromHour", hourFrom);
				i.putExtra("toHour", hourTo);
				startActivityForResult(i, AppConstants.DATEHOUR_RETURN_VALUE);
			}
		});
		// selected services, need to be in onResume to proper update on activityForResult return
		if (services.size() > 0) serviceBtn1.setText(services.get(0).getShortName());
		else serviceBtn1.setText(getResources().getString(R.string.label_noservice));
		serviceBtn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(SearchScheduleActivity.this, AvailableServicesActivity.class), AppConstants.SERVICE_RETURN_VALUE);
			}
		});
		if (services.size() > 1) serviceBtn2.setText(services.get(1).getShortName());
		else serviceBtn2.setText(getResources().getString(R.string.label_noservice));
		serviceBtn2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(SearchScheduleActivity.this, AvailableServicesActivity.class), AppConstants.SERVICE_RETURN_VALUE);
			}
		});
		if (services.size() > 2) serviceBtn3.setText(services.get(2).getShortName());
		else serviceBtn3.setText(getResources().getString(R.string.label_noservice));
		serviceBtn3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(SearchScheduleActivity.this, AvailableServicesActivity.class), AppConstants.SERVICE_RETURN_VALUE);
			}
		});
		if (services.size() > 3) serviceBtn4.setText(services.get(3).getShortName());
		else serviceBtn4.setText(getResources().getString(R.string.label_noservice));
		serviceBtn4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(SearchScheduleActivity.this, AvailableServicesActivity.class), AppConstants.SERVICE_RETURN_VALUE);
			}
		});
	}
	
	private void getDefaultValuesForFields() {
		baseAddress = "";
		rangeIndex = 0;
		dayTm = Calendar.getInstance();
		hourFrom = 0;
		hourTo = 86399;
		services = new ArrayList<ServiceModel>();
		if (AvailableServicesActivity.services == null) {
			((ProgressBar) findViewById(R.id.view_searchschedule_service_progress)).setVisibility(View.VISIBLE);
			client = new RESTClient(this, this);
			client.getServicesList();
			serviceBtn1.setEnabled(false);
			serviceBtn2.setEnabled(false);
			serviceBtn3.setEnabled(false);
			serviceBtn4.setEnabled(false);
		} else {
			((ProgressBar) findViewById(R.id.view_searchschedule_service_progress)).setVisibility(View.GONE);
			serviceBtn1.setEnabled(true);
			serviceBtn2.setEnabled(true);
			serviceBtn3.setEnabled(true);
			serviceBtn4.setEnabled(true);
		}
	}
	
	private boolean isFieldsOk() {
		// check internet
		if (!DevicePlatform.isConnectedToInternet(SearchScheduleActivity.this)) {
			AppConstants.justShowDialogWarning(SearchScheduleActivity.this, R.string.label_noconnection);
			return false;
		}
		// check data fill
		if (services != null) if (services.size() > 0 && baseAddress != null && baseAddress != "") return true;
		AppConstants.justShowDialogWarning(SearchScheduleActivity.this, R.string.label_selectatleast1service);
		return false;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case (AppConstants.DATEHOUR_RETURN_VALUE): {
				if (resultCode == RESULT_OK) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(data.getLongExtra("dayTm", Calendar.getInstance().getTimeInMillis()));
					dayTm = calendar;
					hourFrom = data.getIntExtra("fromHour", 0);
					hourTo = data.getIntExtra("toHour", 86399);
				}
				break;
			}
			case (AppConstants.SERVICE_RETURN_VALUE): {
				if (resultCode == RESULT_OK) {
					try {
						JSONArray arr = new JSONArray(data.getStringExtra("selecteds"));
						services.clear();
						for (int i=0; i<arr.length(); i++) {
							services.add(new ServiceModel(arr.getJSONObject(i)));
						}
					} catch (Exception e) {}
				}
				break;
			}
		}
		// refreshing the view
		drawWidgets();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("unchecked") @Override
	public void onConnectionSuccess(String method, Object response) {
		if (method.equals("getLocationInfo")) {
			client = new RESTClient(this,  this);
			client.getStoresByContext(getSearchParams((double[])response).toJson());
		} else if (method.equals("getServicesList")) {
			((ProgressBar) findViewById(R.id.view_searchschedule_service_progress)).setVisibility(View.GONE);
			AvailableServicesActivity.services = (List<ServiceModel>)response;
			serviceBtn1.setEnabled(true);
			serviceBtn2.setEnabled(true);
			serviceBtn3.setEnabled(true);
			serviceBtn4.setEnabled(true);
		} else if (method.equals("getStoresByContext") || method.equals("getStores")) {
			waitDialog.dismiss();
			stores = (List<StoreModel>)response;
			Intent storeList = new Intent(SearchScheduleActivity.this, SetOfStoresActivity.class);
			JSONArray arr = new JSONArray();
			for (int i=0; stores!=null && i<stores.size(); i++) {
				arr.put(stores.get(i).toJson());
			}
			storeList.putExtra("title", getResources().getString(R.string.main_search));
			storeList.putExtra("icon", R.drawable.ic_action_search_hd);
			storeList.putExtra("stores", arr.toString());
			startActivity(storeList);
		}
	}

	@Override
	public void onEmptyServerResponse(String method) {
		waitDialog.dismiss();
		AppConstants.justShowDialogWarning(SearchScheduleActivity.this, R.string.label_searchwithnoresults);
	}
	
	@Override
	public void onConnectionTimeout(String method) {
		waitDialog.dismiss();
		AppConstants.justShowDialogWarning(SearchScheduleActivity.this, R.string.label_timeout);
	}
	
	@Override
	public void onConnectionFail(String method, Exception e) {
		waitDialog.dismiss();
		AppConstants.justShowDialogWarning(SearchScheduleActivity.this, R.string.label_noconnection);
	}
	
}
