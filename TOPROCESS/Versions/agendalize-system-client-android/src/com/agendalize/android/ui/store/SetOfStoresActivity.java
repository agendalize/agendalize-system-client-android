package com.agendalize.android.ui.store;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.agendalize.android.AppConstants;
import com.agendalize.android.R;
import com.agendalize.android.model.StoreModel;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * @author Marcelo Tomio Hama
 * An activity intended to show a set of stores, either in map or list
 * TODO: pagination
 */
public class SetOfStoresActivity extends ActionBarActivity {
	
	private List<StoreModel> stores;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_setofstores);
		
		try { // populating the store list, this activity must receive something!
			stores = new ArrayList<StoreModel>();
			JSONArray arr = new JSONArray(getIntent().getStringExtra("stores"));
			for (int i=0; arr!=null && i<arr.length(); i++) {
				stores.add(new StoreModel(arr.getJSONObject(i)));
			}
			getSupportActionBar().setTitle(getIntent().getStringExtra("title"));
			getSupportActionBar().setIcon(getIntent().getIntExtra("icon", R.drawable.ic_action_feedstores));
		} catch (Exception e) {
			Log.w(AppConstants.TAG, e.getLocalizedMessage());
			finish();
		}
		
		((Button) findViewById(R.id.view_setofstores_listbtn)).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				selectItem(0);
			}
		});
		((Button) findViewById(R.id.view_setofstores_mapbtn)).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				selectItem(1);
			}
		});
		
		if (savedInstanceState == null) selectItem(0);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void selectItem(int position) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		switch (position) {
			case 0:
				fragmentManager.beginTransaction().replace(R.id.view_setofstores_contentframe, new StoresListFragment()).commit();
				break;
			case 1:
				fragmentManager.beginTransaction().replace(R.id.view_setofstores_contentframe, new StoresMapFragment()).commit();
				break;
			default:;
		}
	}
	
	public List<StoreModel> getStores() {
		return stores;
	}

}
