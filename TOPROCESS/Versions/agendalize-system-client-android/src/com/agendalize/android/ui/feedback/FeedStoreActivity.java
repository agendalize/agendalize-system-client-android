package com.agendalize.android.ui.feedback;

import com.agendalize.android.AppConstants;
import com.agendalize.android.R;
import com.agendalize.android.net.IRESTClient;
import com.agendalize.android.net.RESTClient;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author Marcelo Tomio Hama
 * Prompt a indication of new store
 * This class is 100% done!
 */
public class FeedStoreActivity extends ActionBarActivity implements IRESTClient {
	
	private Button doneBtn = null;
	private RESTClient client = null;
	private ProgressDialog waitDialog = null;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_feedstore);
        
        getSupportActionBar().setTitle(getResources().getString(R.string.label_feedstore));
		getSupportActionBar().setIcon(getResources().getDrawable(R.drawable.ic_action_feedstores));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		((Button) findViewById(R.id.view_feedstore_feedbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = ((EditText) findViewById(R.id.view_feedstore_storename)).getText().toString();
				String address = ((EditText) findViewById(R.id.view_feedstore_storeaddress)).getText().toString();
				String contact = ((EditText) findViewById(R.id.view_feedstore_storecontact)).getText().toString();
				if (name.length() > 0 || address.length() > 0 || contact.length() > 0) {
					client = new RESTClient(FeedStoreActivity.this, FeedStoreActivity.this);
					client.sendStoreSugestion(name, address, contact);
					waitDialog = ProgressDialog.show(FeedStoreActivity.this, getText(R.string.label_sendingdata), getText(R.string.label_pleasewait), false, false);
					waitDialog.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
				} else {
					AppConstants.justShowDialogWarning(FeedStoreActivity.this, R.string.label_pleasefillfeedinfo);
				}
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.onlyback_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onConnectionSuccess(String method, Object response) {
		if (method.equals("sendStoreSugestion")) {
			waitDialog.dismiss();
			setContentView(R.layout.view_feedstoredone);
        	doneBtn = (Button)findViewById(R.id.view_feedstoredone_donebtn);
        	doneBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
		}
	}
	
	@Override
	public void onEmptyServerResponse(String method) {
		waitDialog.dismiss();
	}
	
	@Override
	public void onConnectionTimeout(String method) {
		waitDialog.dismiss();
		AppConstants.justShowDialogWarning(FeedStoreActivity.this, R.string.label_timeout);
	}
	
	@Override
	public void onConnectionFail(String method, Exception e) {
		waitDialog.dismiss();
		AppConstants.justShowDialogWarning(FeedStoreActivity.this, R.string.label_noconnection);
	}

}
