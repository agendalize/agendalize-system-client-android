package com.agendalize.android.ui.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agendalize.android.R;

/*import com.bemw.agendalize.R;
import com.bemw.agendalize.dao.BookmarkDAO;
import com.bemw.agendalize.dao.LocalConfigDAO;
import com.bemw.agendalize.model.BookmarkModel;
import com.bemw.agendalize.model.EmployeeModel;
import com.bemw.agendalize.model.EmployeesPerServiceModel;
import com.bemw.agendalize.model.ServiceModel;
import com.bemw.agendalize.model.StoreModel;
import com.bemw.agendalize.net.SharedData;
import com.bemw.agendalize.util.Dialer;
import com.bemw.agendalize.util.ImageUtils;*/

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class StoreInfoFragment extends Fragment {

	/*private BookmarkModel bookmark = null;
	private StoreModel store = null;
	private List<EmployeeModel>employees = new ArrayList<EmployeeModel>();
	private List<ServiceModel> services = new ArrayList<ServiceModel>();*/
	private ListView storeListContext = null;
	//private StoreInfoAdapter adapter = null;
	private TextView storeAddress = null;
	private TextView storeDescription = null;
	private Button callButton = null;
	private Button favoriteButton = null;
	private Button locationButton = null;
	private boolean employeeIsLoaded = false;
	private boolean serviceIsLoaded = false;
	
	/*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.view_storeinfo, container, false);
        store = ((StoreDetailActivity)getActivity()).getStore();
        employees = new ArrayList<EmployeeModel>();
        services = new ArrayList<ServiceModel>();
        
        bookmark = BookmarkDAO.loadBookmark(getActivity());
        storeAddress = (TextView) rootView.findViewById(R.id.view_storeinfo_address);
        storeDescription = (TextView) rootView.findViewById(R.id.view_storeinfo_description);
        storeListContext = (ListView) rootView.findViewById(R.id.view_storeinfo_listview);
        callButton = (Button) rootView.findViewById(R.id.view_storeinfo_callbtn);
        favoriteButton = (Button) rootView.findViewById(R.id.view_storeinfo_favoritebtn);
        locationButton = (Button) rootView.findViewById(R.id.view_storeinfo_locationbtn);
        
        if (adapter == null) {
			adapter = new StoreInfoAdapter();
			storeListContext.setAdapter(adapter);
		}
        setListeners();
        
        return rootView;
    }
	
	@Override
	public void onResume() {
		super.onResume();
		
		storeAddress.setText(store.getAddress().getAddress() + " n" + store.getAddress().getAddressNumber());
		storeDescription.setText(store.getDescription());
	}
	
	private void setListeners() {
		callButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (store.getContact().getPhoneNumberOne() != null && store.getContact().getPhoneNumberOne().length() > 0)
					Dialer.call(getActivity(), store.getContact().getPhoneNumberOne());
				else if (store.getContact().getPhoneNumberTwo() != null && store.getContact().getPhoneNumberTwo().length() > 0)
					Dialer.call(getActivity(), store.getContact().getPhoneNumberTwo());
				else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setMessage(getActivity().getResources().getString(R.string.label_theresnocontact)).setCancelable(false).
					setPositiveButton(getActivity().getResources().getString(R.string.btn_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
				}
			}
		});
		favoriteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!bookmark.hasTheRegistry(store)) {
					final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setMessage(store.getName() + ". " + getResources().getString(R.string.label_addasbookmark)).
					setCancelable(true).setPositiveButton(getResources().getString(R.string.btn_yes),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {
								Toast.makeText(getActivity(), R.string.label_addedbookmark, Toast.LENGTH_LONG).show();
								bookmark.addRegistry(getActivity(), store);
							}
						}
					).setNegativeButton(getResources().getString(R.string.btn_no),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setMessage(getResources().getString(R.string.label_alreadyaddedbookmark)).
					setCancelable(false).setIcon(R.drawable.ic_action_warning_hd).
					setPositiveButton(getResources().getString(R.string.btn_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog, final int id) {}
						}
					);
					final AlertDialog alert = builder.create();
					alert.show();
				}
			}
		});
		locationButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO: create the router view
				double latlon[] = LocalConfigDAO.getLatLonPreferences(getActivity());
				SharedData data = SharedData.getInstance();
		        data.setSrc_lat(latlon[0]);
		        data.setSrc_lng(latlon[1]);*/
		        //data.setDest_lat(latlon[0]/*contact.getLatitude()*/);
		        //data.setDest_lng(latlon[1]/*contact.getLongitude()*/);
		        // get distance
		        /*float higher_distance = 0;
		        String d = contact.getDistance().substring(0, contact.getDistance().length()-3);
    			float f = Float.parseFloat(d);
    			if (f > higher_distance) higher_distance = f;
		        RoutePath.distance = higher_distance;*/
		        // trace route
		        /*startActivity(new Intent(getActivity(), RoutePath.class));
			}
		});
	}*/
	
	/*public void updateServiceEmployeer(List<EmployeesPerServiceModel> employeePerServices) {
		services.clear();
		employees.clear();
		HashMap<String, Object> dict = new HashMap<String, Object>();
		for (int i=0; i<employeePerServices.size(); i++) {
			ServiceModel s = new ServiceModel();
			s.setCod(employeePerServices.get(i).getCod());
			s.setShortName(employeePerServices.get(i).getShortName());
			services.add(s);
			for (int j=0; j<employeePerServices.get(i).getEmployees().size(); j++) {
				// TODO: use bellow
				//if (!dict.containsKey(employeePerServices.get(i).getEmployees().get(j).getLogin())) {
				if (!dict.containsKey(employeePerServices.get(i).getEmployees().get(j).getName()+employeePerServices.get(i).getEmployees().get(j).getLastName())) {
					dict.put(employeePerServices.get(i).getEmployees().get(j).getName()+employeePerServices.get(i).getEmployees().get(j).getLastName(), null);
					EmployeeModel m = new EmployeeModel();
					m.setName(employeePerServices.get(i).getEmployees().get(j).getName());
					m.setLastName(employeePerServices.get(i).getEmployees().get(j).getLastName());
					m.setImageUrl(employeePerServices.get(i).getEmployees().get(j).getImageUrl());
					employees.add(m);
				}
			}
		}
		employeeIsLoaded = true;
		serviceIsLoaded = true;
		adapter.notifyDataSetInvalidated();
		adapter.notifyDataSetChanged();
	}
	
	private class StoreInfoAdapter extends BaseAdapter {
		public StoreInfoAdapter() {}
		@Override
		public int getCount() {
			return services.size() + employees.size() + 2;
		}
		@Override
		public Object getItem(int position) {
			if (position >= 1 && position <= employees.size()) return employees.get(position);
			else if (position >= employees.size() + 2 && position <= employees.size() + services.size() + 1) return services.get(position);
			else if (position == 0) return "EMPLOYEE_LABEL";
			else if (position == employees.size() + 1) return "SERVICE_LABEL";
			else return null;
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@SuppressWarnings("deprecation") @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// predefines the view as an inflated layout
			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (position == 0) { // employee label
				convertView = inflater.inflate(R.layout.row_employeelabel, null);
				if (!employeeIsLoaded) {
					((ProgressBar) convertView.findViewById(R.id.row_employeelabel_progress)).setVisibility(View.VISIBLE);
				} else {
					((ProgressBar) convertView.findViewById(R.id.row_employeelabel_progress)).setVisibility(View.GONE);
				}
			} else if (position >= 1 && position <= employees.size()) { // employees
				convertView = inflater.inflate(R.layout.row_employeedata, null);
				Drawable photo = ImageUtils.loadCachedImage(getActivity(), employees.get(position-1).getImageUrl());
				if (photo == null) {
					Bitmap bmp = ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.noimage), true);
					((ImageView) convertView.findViewById(R.id.row_employeedata_photo)).setBackgroundDrawable(new BitmapDrawable(getResources(), bmp));
				} else {
					photo = ImageUtils.resize(photo, 48, 48);
					Bitmap bmp = ((BitmapDrawable)photo).getBitmap();
					bmp = ImageUtils.getCroppedBitmap(bmp, true);
					((ImageView) convertView.findViewById(R.id.row_employeedata_photo)).setBackgroundDrawable(new BitmapDrawable(getResources(), bmp));
				}
				((TextView) convertView.findViewById(R.id.row_employeedata_name)).setText(
						employees.get(position-1).getName() + " " + employees.get(position-1).getLastName()
				);
				((TextView) convertView.findViewById(R.id.row_employeedata_phone)).setText(
						employees.get(position-1).getWorkPhoneNumber().length() > 0 ? employees.get(position-1).getPhoneNumber() : (
								employees.get(position-1).getCellPhoneNumber().length() > 0 ? employees.get(position-1).getPhoneNumber() : (
										employees.get(position-1).getPhoneNumber().length() > 0 ? employees.get(position-1).getPhoneNumber() : ""
								)
						)
				);
			} else if (position == employees.size() + 1) { // service label
				convertView = inflater.inflate(R.layout.row_servicelabel, null);
				if (!serviceIsLoaded) {
					((ProgressBar) convertView.findViewById(R.id.row_servicelabel_progress)).setVisibility(View.VISIBLE);
				} else {
					((ProgressBar) convertView.findViewById(R.id.row_servicelabel_progress)).setVisibility(View.GONE);
				}
			} else if (position >= employees.size() + 2 && position <= employees.size() + services.size() + 1) { // services
				convertView = inflater.inflate(R.layout.row_servicedata, null);
				((ImageView) convertView.findViewById(R.id.row_servicedata_photo)).setBackgroundDrawable(new BitmapDrawable(
						getResources(),
						ImageUtils.getCroppedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.noimage), false) // TODO: place the service image
				));
				((TextView) convertView.findViewById(R.id.row_servicedata_name)).setText(
						services.get(position-2-employees.size()).getShortName() + " (" + services.get(position-2-employees.size()).getTimeMinutes() + " min)"
				);
				((TextView) convertView.findViewById(R.id.row_servicedata_description)).setText(
						services.get(position-2-employees.size()).getDescription()
				);
			}
			return convertView;
		}
	}*/
	
}
