package com.agendalize.android.net;

public interface IRESTClient {
	
	public void onConnectionSuccess(String method, Object response);
	
	public void onEmptyServerResponse(String method);
	
	public void onConnectionTimeout(String method);
	
	public void onConnectionFail(String method, Exception e);

}
