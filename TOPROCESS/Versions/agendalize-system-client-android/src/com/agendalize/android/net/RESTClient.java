package com.agendalize.android.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Looper;
import android.util.Log;

import com.agendalize.android.AppConstants;
import com.agendalize.android.R;
import com.agendalize.android.model.AddressModel;
import com.agendalize.android.model.ContactsModel;
import com.agendalize.android.model.ServiceModel;
import com.agendalize.android.model.StoreModel;

public class RESTClient {
	
	private IRESTClient delegate = null;
	private Activity activity = null;
	private InputStream instream = null;
	private DefaultHttpClient httpClient = null;
	private HttpPost httpPost = null;
	private HttpParams httpParameters = null;
	
	public RESTClient(IRESTClient delegate, Activity activity) {
		this.delegate = delegate;
		this.activity = activity;
		httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, NETConstants.CONN_TIME_OUT);
		HttpConnectionParams.setSoTimeout(httpParameters, NETConstants.CONN_TIME_OUT);
		httpClient = new DefaultHttpClient(httpParameters);
	}
	
	/**
	 * Delegate returns a <b>Double[2]</b>
	 */
	public void getLocationInfo(final String address) {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				synchronized(this) {
					try {
						final double LatLon[] = new double[2];
						LatLon[0] = 0; LatLon[1] = 0; 
				        StringBuilder stringBuilder = new StringBuilder();
				        HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address.replaceAll(" ","%20") + "&sensor=false");
				        HttpClient client = new DefaultHttpClient();
				        HttpResponse response;
				        stringBuilder = new StringBuilder();
			            response = client.execute(httppost);
			            HttpEntity entity = response.getEntity();
			            InputStream stream = entity.getContent();
			            int b;
			            while ((b = stream.read()) != -1) {
			                stringBuilder.append((char) b);
			            }
			            JSONObject jsonObject = new JSONObject();
			            jsonObject = new JSONObject(stringBuilder.toString());
			        	LatLon[0] = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
			                    .getJSONObject("geometry").getJSONObject("location")
			                    .getDouble("lat");
			        	LatLon[1] = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
			                .getJSONObject("geometry").getJSONObject("location")
			                .getDouble("lng");
			        	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionSuccess("getLocationInfo", LatLon);
					        }
						});
			        } catch (ConnectTimeoutException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getLocationInfo");
					        }
						});
				    } catch (SocketTimeoutException e) {
				    	Log.w(AppConstants.TAG, e.getLocalizedMessage());
				    	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getLocationInfo");
					        }
						});
					} catch (final UnsupportedEncodingException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getLocationInfo", e);
					        }
						});
					} catch (final ClientProtocolException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getLocationInfo", e);
					        }
						});
					} catch (final JSONException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getLocationInfo", e);
					        }
						});
				    } catch (final IOException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getLocationInfo", e);
					        }
						});
					} finally {
						disconnect();
					}
				}
				Looper.loop();
			};
		}.start();
	}
	
	/**
	 * Delegate returns a <b>String</b>
	 */
	public void sendOpinionFeedback(final String feedback) {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				synchronized(this) {
					try {
						httpPost = new HttpPost(AppConstants.domain + "feedbacks/agendalize-opinions/create");
						httpPost.setHeader("Content-type", "application/json");
						httpPost.setEntity(new ByteArrayEntity(
								new JSONObject().put("opinion", feedback)
								.toString().getBytes("UTF8")
						));
						httpClient.execute(httpPost);
						httpClient.getConnectionManager().shutdown();
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionSuccess("sendOpinionFeedback", activity.getResources().getString(R.string.label_feedback_success));
					        }
						});
					} catch (ConnectTimeoutException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("sendOpinionFeedback");
					        }
						});
				    } catch (SocketTimeoutException e) {
				    	Log.w(AppConstants.TAG, e.getLocalizedMessage());
				    	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("sendOpinionFeedback");
					        }
						});
					} catch (final UnsupportedEncodingException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendOpinionFeedback", e);
					        }
						});
					} catch (final ClientProtocolException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendOpinionFeedback", e);
					        }
						});
					} catch (final JSONException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendOpinionFeedback", e);
					        }
						});
				    } catch (final IOException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendOpinionFeedback", e);
					        }
						});
					} finally {
						disconnect();
					}
				}
				Looper.loop();
			};
		}.start();
	}
	
	/**
	 * Delegate returns a <b>String</b>
	 */
	public void sendStoreSugestion(final String name, final String Address, final String contact) {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				synchronized(this) {
					try {
						httpPost = new HttpPost(AppConstants.domain + "feedbacks/store-sugestions/create");
						httpPost.setHeader("Content-type", "application/json");
						httpPost.setEntity(new ByteArrayEntity(
								new JSONObject()
								.put("name", name)
								.put("addressInfo", Address)
								.put("contactInfo", contact)
								.toString().getBytes("UTF8")
						));
						httpClient.execute(httpPost);
						httpClient.getConnectionManager().shutdown();
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionSuccess("sendStoreSugestion", activity.getResources().getString(R.string.label_feedback_success));
					        }
						});
					} catch (ConnectTimeoutException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("sendStoreSugestion");
					        }
						});
				    } catch (SocketTimeoutException e) {
				    	Log.w(AppConstants.TAG, e.getLocalizedMessage());
				    	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("sendStoreSugestion");
					        }
				    	});
					} catch (final UnsupportedEncodingException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendStoreSugestion", e);
					        }
						});
					} catch (final ClientProtocolException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendStoreSugestion", e);
					        }
						});
					} catch (final JSONException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendStoreSugestion", e);
					        }
						});
				    } catch (final IOException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("sendStoreSugestion", e);
					        }
						});
					} finally {
						disconnect();
					}
				}
				Looper.loop();
			};
		}.start();
	}
	
	/**
	 * Delegate returns a <b>List</b> of <b>ServiceModel</b>
	 */
	public void getServicesList() {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				synchronized(this) {
					try {
						final List<ServiceModel> services = new ArrayList<ServiceModel>();
						InputStream is = new URL(AppConstants.domain + "services/list").openStream();
						BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
						StringBuilder sb = new StringBuilder();
						int cp;
						while ((cp = reader.read()) != -1) {
							sb.append((char) cp);
						}
						JSONArray arr = new JSONArray(sb.toString());
						for (int i=0; i<arr.length(); i++) {
							services.add(new ServiceModel(arr.getJSONObject(i)));
						}
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	if (services.size() > 0) {
					        		delegate.onConnectionSuccess("getServicesList", services);
					        	} else {
					        		delegate.onEmptyServerResponse("getServicesList");
					        	}
					        }
						});
					} catch (ConnectTimeoutException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getServicesList");
					        }
						});
				    } catch (SocketTimeoutException e) {
				    	Log.w(AppConstants.TAG, e.getLocalizedMessage());
				    	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getServicesList");
					        }
						});
					} catch (final UnsupportedEncodingException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesList", e);
					        }
						});
					} catch (final ClientProtocolException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesList", e);
					        }
						});
					} catch (final JSONException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesList", e);
					        }
						});
				    } catch (final IOException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesList", e);
					        }
						});
					} finally {
						disconnect();
					}
				}
				Looper.loop();
			};
		}.start();
	}
	
	/**
	 * Delegate returns a <b>List</b> of <b>ServiceModel</b>
	 */
	public void getServicesByStore(final long storeId) {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				synchronized(this) {
					try {
						final List<ServiceModel> services = new ArrayList<ServiceModel>();
						InputStream is = new URL(AppConstants.domain + "stores/" + storeId + "/services/list").openStream();
						BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
						StringBuilder sb = new StringBuilder();
						int cp;
						while ((cp = reader.read()) != -1) {
							sb.append((char) cp);
						}
						JSONArray arr = new JSONArray(sb.toString());
						for (int i=0; i<arr.length(); i++) {
							services.add(new ServiceModel(arr.getJSONObject(i)));
						}
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	if (services.size() > 0) {
					        		delegate.onConnectionSuccess("getServicesByStore", services);
					        	} else {
					        		delegate.onEmptyServerResponse("getServicesByStore");
					        	}
					        }
						});
					} catch (ConnectTimeoutException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getServicesByStore");
					        }
						});
				    } catch (SocketTimeoutException e) {
				    	Log.w(AppConstants.TAG, e.getLocalizedMessage());
				    	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getServicesByStore");
					        }
						});
					} catch (final UnsupportedEncodingException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesByStore", e);
					        }
						});
					} catch (final ClientProtocolException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesByStore", e);
					        }
						});
					} catch (final JSONException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesByStore", e);
					        }
						});
				    } catch (final IOException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getServicesByStore", e);
					        }
						});
					} finally {
						disconnect();
					}
				}
				Looper.loop();
			};
		}.start();
	}
	
	/**
	 * Delegate returns a <b>List</b> of <b>StoreModel</b>
	 */
	public void getStoresByContext(final JSONObject request) {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				synchronized(this) {
					try {
						String wsResponse = "";
						String inputLine = null;
						final List<StoreModel> stores = new ArrayList<StoreModel>();
						httpPost = new HttpPost(AppConstants.domain + "stores/filter");
						httpPost.setHeader("Content-type", "application/json");
						httpPost.setEntity(new ByteArrayEntity(request.toString().getBytes("UTF8")));
						HttpEntity entity = httpClient.execute(httpPost).getEntity();
						if (entity != null) {
							BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
							while ((inputLine = reader.readLine()) != null) {
								wsResponse += inputLine;
							}
							JSONArray arr = new JSONArray(wsResponse);
							for (int i=0; i<arr.length(); i++) {
								JSONObject obj = arr.getJSONObject(i);
								ContactsModel contact = new ContactsModel(
										(obj.has("phoneNumberOne") ? obj.remove("phoneNumberOne").toString() : ""),
										(obj.has("phoneNumberTwo") ? obj.remove("phoneNumberTwo").toString() : ""),
										(obj.has("email") ? obj.remove("email").toString() : "")
								);
								AddressModel address = new AddressModel(
										(obj.has("address") ? obj.remove("address").toString() : "address"),
										(obj.has("addressNumber") ? obj.remove("addressNumber").toString() : "addressNumber"),
										(obj.has("complement") ? obj.remove("complement").toString() : "complement"),
										(obj.has("zipCode") ? obj.remove("zipCode").toString() : "zipCode"),
										(obj.has("city") ? obj.remove("city").toString() : "city"),
										(obj.has("state") ? obj.remove("state").toString() : "state"),
										(obj.has("country") ? obj.remove("country").toString() : "country")
								);
								obj.put("address", address.toJson());
								obj.put("contact", contact.toJson());
								stores.add(new StoreModel(obj));
							}
							activity.runOnUiThread(new Runnable() {
						        @Override
						        public void run() {
						        	if (stores.size() > 0) {
						        		delegate.onConnectionSuccess("getStoresByContext", stores);
						        	} else {
						        		delegate.onEmptyServerResponse("getStoresByContext");
						        	}
						        }
							});
						}
					} catch (ConnectTimeoutException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getStoresByContext");
					        }
						});
				    } catch (SocketTimeoutException e) {
				    	Log.w(AppConstants.TAG, e.getLocalizedMessage());
				    	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getStoresByContext");
					        }
						});
					} catch (final UnsupportedEncodingException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStoresByContext", e);
					        }
						});
					} catch (final ClientProtocolException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStoresByContext", e);
					        }
						});
					} catch (final JSONException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStoresByContext", e);
					        }
						});
				    } catch (final IOException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStoresByContext", e);
					        }
						});
					} finally {
						disconnect();
					}
				}
				Looper.loop();
			};
		}.start();
	}
	
	/**
	 * Delegate returns a <b>List</b> of <b>StoreModel</b>
	 */
	public void getStores() {
		new Thread() {
			@Override
			public void run() {
				Looper.prepare();
				synchronized(this) {
					try {
						final List<StoreModel> stores = new ArrayList<StoreModel>();
						InputStream is = new URL(AppConstants.domain + "stores/list").openStream();
						BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
						StringBuilder sb = new StringBuilder();
						int cp;
						while ((cp = reader.read()) != -1) {
							sb.append((char) cp);
						}
						JSONArray arr = new JSONArray(sb.toString());
						for (int i=0; i<arr.length(); i++) {
							JSONObject obj = arr.getJSONObject(i);
							ContactsModel contact = new ContactsModel(
									(obj.has("phoneNumberOne") ? obj.remove("phoneNumberOne").toString() : ""),
									(obj.has("phoneNumberTwo") ? obj.remove("phoneNumberTwo").toString() : ""),
									(obj.has("email") ? obj.remove("email").toString() : "")
							);
							AddressModel address = new AddressModel(
									(obj.has("address") ? obj.remove("address").toString() : "address"),
									(obj.has("addressNumber") ? obj.remove("addressNumber").toString() : "addressNumber"),
									(obj.has("complement") ? obj.remove("complement").toString() : "complement"),
									(obj.has("zipCode") ? obj.remove("zipCode").toString() : "zipCode"),
									(obj.has("city") ? obj.remove("city").toString() : "city"),
									(obj.has("state") ? obj.remove("state").toString() : "state"),
									(obj.has("country") ? obj.remove("country").toString() : "country")
							);
							obj.put("address", address.toJson());
							obj.put("contact", contact.toJson());
							stores.add(new StoreModel(obj));
						}
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	if (stores.size() > 0) {
					        		delegate.onConnectionSuccess("getStores", stores);
					        	} else {
					        		delegate.onEmptyServerResponse("getStores");
					        	}
					        }
						});
					} catch (ConnectTimeoutException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getStores");
					        }
						});
				    } catch (SocketTimeoutException e) {
				    	Log.w(AppConstants.TAG, e.getLocalizedMessage());
				    	activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionTimeout("getStores");
					        }
						});
					} catch (final UnsupportedEncodingException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStores", e);
					        }
						});
					} catch (final ClientProtocolException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStores", e);
					        }
						});
					} catch (final JSONException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStores", e);
					        }
						});
				    } catch (final IOException e) {
						Log.w(AppConstants.TAG, e.getLocalizedMessage());
						activity.runOnUiThread(new Runnable() {
					        @Override
					        public void run() {
					        	delegate.onConnectionFail("getStores", e);
					        }
						});
					} finally {
						disconnect();
					}
				}
				Looper.loop();
			};
		}.start();
	}
	
	public void disconnect() {
		try { // closes the connection
			if (httpPost != null) httpPost.abort();
			closeInputStream();
			if (httpClient != null) httpClient.getConnectionManager().shutdown();
		} catch (Exception e) {
			Log.w(AppConstants.TAG, e.getLocalizedMessage());
		}
	}
	
	private void closeInputStream() {
		try { // closing the input stream will trigger connection release
			if (instream != null) instream.close();
		} catch (IOException e) {
			Log.w(AppConstants.TAG, e.getLocalizedMessage());
		} catch (NullPointerException e) {
			Log.w(AppConstants.TAG, e.getLocalizedMessage());
		}
	}

}
