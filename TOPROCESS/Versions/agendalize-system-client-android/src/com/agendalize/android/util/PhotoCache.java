package com.agendalize.android.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.agendalize.android.AppConstants;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class PhotoCache {

	private static Context context;
	private static Drawable image;
	private static String client_name;
	
	private static Runnable save_task = new Runnable() {
		@Override
		public void run() {
			try {
				// Use the compress method on the Bitmap object to write image to the OutputStream
				String filename = SimpleCrypto.encrypt(AppConstants.CACHE_FILE, client_name);
				FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
				// Writing the bitmap to the output stream
				((BitmapDrawable)image).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, fos);
				fos.close();
			} catch (Exception e) {}
		}
	};
	
	public static void saveImageToInternalStorage(Context context, Drawable image, String client_name) {
		PhotoCache.context = context;
		PhotoCache.image = image;
		PhotoCache.client_name = client_name;
		new Thread(save_task).start();
	}
	
	@SuppressWarnings("deprecation")
	public static Drawable getThumbnail(Context context, String client_name) {
		Bitmap thumbnail = null;
		try {
			String name = SimpleCrypto.encrypt(AppConstants.CACHE_FILE, client_name);
			File filePath = context.getFileStreamPath(name);
			FileInputStream fi = new FileInputStream(filePath);
			thumbnail = BitmapFactory.decodeStream(fi);
		} catch (Exception ex) {
			// file does not exists
			return null;
		}
		return new BitmapDrawable(thumbnail);
	}
	
	public static boolean deleteImageFile(Context context, String client_name) {
		try {
			String name = SimpleCrypto.encrypt(AppConstants.CACHE_FILE, client_name);
			File dir = context.getFilesDir();
			File file = new File(dir, name);
			return file.delete();
		} catch (Exception e) {};
		return false;
	}
	
	public static long dirSize(File dir) {
        long result = 0;
        File[] fileList = dir.listFiles();
        for(int i = 0; i < fileList.length; i++) {
            // Recursive call if it's a directory
            if(fileList[i].isDirectory()) {
                result += dirSize(fileList [i]);
            } else {
                // Sum the file size in bytes
                result += fileList[i].length();
            }
        }
        return result; // return the file size
    }
	
}
