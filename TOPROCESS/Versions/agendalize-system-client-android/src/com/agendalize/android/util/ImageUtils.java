package com.agendalize.android.util;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import com.agendalize.android.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class ImageUtils {
	
	public static Bitmap getCroppedBitmap(Bitmap bitmap, boolean isCircle) {
	    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
	            bitmap.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	    final RectF rectf = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    if (!isCircle) {
	    	canvas.drawRoundRect(rectf, 45, 45, paint);
	    	paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		    canvas.drawBitmap(bitmap, rect, rectf, paint);
	    } else {
	    	canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
		    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		    canvas.drawBitmap(bitmap, rect, rect, paint);
	    }
	    return output;
	}
	
	@SuppressWarnings("deprecation")
	public static Drawable mergeImage(Drawable orig, Drawable over, int left, int top) {
	    Bitmap original = ((BitmapDrawable)orig).getBitmap();
	    Bitmap overlay = ((BitmapDrawable)over).getBitmap();
	    Bitmap result = Bitmap.createBitmap(original.getWidth(), original.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(result);
	    Paint paint = new Paint();
	    paint.setAntiAlias(true);

	    canvas.drawBitmap(original, 0, 0, paint);
	    canvas.drawBitmap(overlay, left, top, paint);

	    return new BitmapDrawable(result);
	}
	
	public static Drawable resizeToSquared(Drawable original, Context context, int size) {
		if (original != null) {
			Bitmap bitmap = ((BitmapDrawable)original).getBitmap();
			int w = bitmap.getWidth();
			int h = bitmap.getHeight();
			// creates the square shaped bg
			Drawable background;
			if (w > h) background = ImageUtils.resize(context.getResources().getDrawable(R.drawable.alpha0square), w, w);
			else background = ImageUtils.resize(context.getResources().getDrawable(R.drawable.alpha0square), h, h);
			// merge the original image with the bg
			Drawable squared = mergeImage(background, original, 0, (int)Math.sqrt(((w-h)*(w-h)))/2);
			// return a resized version of the squared image
			return resize(squared, size, size);
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static Drawable resize(Drawable original, int w, int h) {
		if (original != null) {
			Drawable dr = original;
			Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
			return new BitmapDrawable(Bitmap.createScaledBitmap(bitmap, w, h, true));
		}
		return original;
	}
	
	@SuppressWarnings("deprecation")
	public static Drawable loadCachedImage(Context context, String strUrl) {
		// first, let's check if the image already exists in cache
		Drawable d = PhotoCache.getThumbnail(context, strUrl);
		if (d != null) {
			return d;
		}
		// if not exists, download it
		try {
			URL url = new URL(strUrl);
			URLConnection connection = url.openConnection();
			connection.setUseCaches(true);
			Object response = connection.getContent();
			if (response == null) {
				return null;
			}
			if (response instanceof Bitmap) {
				Bitmap bitmap = (Bitmap)response;
				d = new BitmapDrawable(bitmap);
				PhotoCache.saveImageToInternalStorage(context, d, strUrl);
				return d;
			} else {
				InputStream is = (InputStream) connection.getContent();
				d = Drawable.createFromStream(is, "src name");
				PhotoCache.saveImageToInternalStorage(context, d, strUrl);
				return d;
			}
		} catch (Exception e) { }
		return null;
	}

}
