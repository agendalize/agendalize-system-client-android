package br.com.agendalize.androidapp.model;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by mthama on 25/10/15.
 */
import java.util.ArrayList;
import java.util.List;

public class BookingGroup {

    private String idGroupBooking;
    private String nameStore;
    private String dateBook;
    private String weekBook;
    private String codStatus;
    private List<ServiceHistoryGroup> employees;

    public BookingGroup() {
        idGroupBooking = "";
        nameStore = "";
        dateBook = "";
        weekBook = "";
        codStatus = "";
        employees = new ArrayList<ServiceHistoryGroup>();
    }

    public BookingGroup(String idGroupBooking, String nameStore, String dateBook,
                        String weekBook, String codStatus, List<ServiceHistoryGroup> employees) {
        this();
        this.idGroupBooking = idGroupBooking;
        this.nameStore = nameStore;
        this.dateBook = dateBook;
        this.weekBook = weekBook;
        this.codStatus = codStatus;
        this.employees = employees;
    }

    public BookingGroup(JSONObject obj) {
        this();
        try {
            this.idGroupBooking = obj.getString("idGroupBooking");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.nameStore = obj.getString("nameStore");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.dateBook = obj.getString("dateBook");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.weekBook = obj.getString("weekBook");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.codStatus = obj.getString("codStatus");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            JSONArray arr = new JSONArray(obj.getString("employees"));
            for (int i=0; i<arr.length(); i++) {
                this.employees.add(new ServiceHistoryGroup(arr.getJSONObject(i)));
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public String getIdGroupBooking() {
        return idGroupBooking;
    }

    public void setIdGroupBooking(String idGroupBooking) {
        this.idGroupBooking = idGroupBooking;
    }

    public String getNameStore() {
        return nameStore;
    }

    public void setNameStore(String nameStore) {
        this.nameStore = nameStore;
    }

    public String getDateBook() {
        return dateBook;
    }

    public void setDateBook(String dateBook) {
        this.dateBook = dateBook;
    }

    public String getWeekBook() {
        return weekBook;
    }

    public void setWeekBook(String weekBook) {
        this.weekBook = weekBook;
    }

    public String getCodStatus() {
        return codStatus;
    }

    public void setCodStatus(String codStatus) {
        this.codStatus = codStatus;
    }

    public List<ServiceHistoryGroup> getEmployees() {
        return employees;
    }

    public void setEmployees(List<ServiceHistoryGroup> employees) {
        this.employees = employees;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("idGroupBooking", idGroupBooking);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("nameStore", nameStore);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("dateBook", dateBook);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("weekBook", weekBook);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("codStatus", codStatus);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            JSONArray arr = new JSONArray();
            for (int i=0; i<employees.size(); i++) {
                arr.put(employees.get(i));
            }
            obj.put("employees", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return obj;
    }

}
