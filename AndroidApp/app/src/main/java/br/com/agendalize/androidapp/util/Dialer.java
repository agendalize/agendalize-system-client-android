package br.com.agendalize.androidapp.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

public class Dialer {
	
	public static boolean call(Context context, String phoneNumber) {
		Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
		phoneIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (checkWriteExternalPermission(context)) {
			context.startActivity(phoneIntent);
			return true;
		} else {
			return false;
		}
	}

	private static boolean checkWriteExternalPermission(Context context) {
		String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
		int res = context.checkCallingOrSelfPermission(permission);
		return (res == PackageManager.PERMISSION_GRANTED);
	}

}
