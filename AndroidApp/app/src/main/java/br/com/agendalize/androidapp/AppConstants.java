package br.com.agendalize.androidapp;

/**
 * Created by mthama on 13/10/15.
 */
public class AppConstants {

    public static final String TAG = "Agendalize";
    public static final String CACHE_FILE = "agendalize_cache";
    public static final String FragmentIndexToStartAt = "FragmentIndexToStartAt";
    public static final String SHARED_PREFS = "SHARED_PREFS";
    public static final String LOCAL_DEVICE_PROFILE = "LOCAL_DEVICE_PROFILE";
    public static final String PIN_CODE = "PIN_CODE";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";

    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // domains and url
    //public static final String domain = "http://192.168.0.102:8080/";
    //public static final String domain = "http://54.232.215.74:8080/agendalize/";
    //public static final String domain = "http://prod.agendalize.com.br/agendalize/";
    public static final String domain = "https://agendalize.com/";

    // web addresses
    public static final String OUR_FACEBOOK = "https://www.facebook.com/pages/Agendalize/496570267115536";
    public static final String OUR_TWITTER = "http://www.google.com";
    public static final String OUR_WORDPRESS = "http://blogagendalize.com.br/";

    // final values
    public static final String DEVICE_NAME = "android-aglz";
    public static final String DEVICE_KEY = "qa123";
    public static final String DEVICE_CREDENTIAL = "client_credentials";
    public static final int POSITION_TIMER = 5*60000;
    public static final int BOOTSPLASH_TIMER = 1500;
    public static final int CONN_TIME_OUT = 60000;
    public static final int SIZE_LIST = 7;
    public static final int SMS_TIMER = 300000;
    public static final int SCHEDULE_INTERVALS_MIN = 15;
    public static final int SCHEDULE_START_MIN = 8*60;
    public static final int SCHEDULE_DURATION_MIN = 14*60;

}
