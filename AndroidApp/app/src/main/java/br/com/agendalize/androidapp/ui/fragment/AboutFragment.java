package br.com.agendalize.androidapp.ui.fragment;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class AboutFragment extends Fragment {

	private static ImageButton frag_about_facebook;
	private static ImageButton frag_about_twitter;
	private static ImageButton frag_about_wordpress;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_about, container, false);

		frag_about_facebook = (ImageButton) rootView.findViewById(R.id.frag_about_facebook);
		frag_about_facebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_FACEBOOK)));
			}
		});
		frag_about_twitter = (ImageButton) rootView.findViewById(R.id.frag_about_twitter);
		frag_about_twitter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_TWITTER)));
			}
		});
		frag_about_wordpress = (ImageButton) rootView.findViewById(R.id.frag_about_wordpress);
		frag_about_wordpress.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.OUR_WORDPRESS)));
			}
		});

		return rootView;
	}

}
