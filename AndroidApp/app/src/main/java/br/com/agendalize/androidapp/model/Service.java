package br.com.agendalize.androidapp.model;

import org.json.JSONObject;

public class Service {

    private Long cod;
    private String shortName;
    private String description;
    private Long codTypeService;
    private String shortNameTypeService;
    private String descriptionTypeService;
    private String urlImage;
    private String price;
    private String timeMinutes;
    
    public Service() {
    	this.cod = 0L;
		this.shortName = "";
		this.description = "";
		this.codTypeService = 0L;
		this.shortNameTypeService = "";
		this.descriptionTypeService = "";
		this.urlImage = "";
		this.price = "";
		this.timeMinutes = "";
    }

    public Service(Long cod, String shortName, String description,
				   Long codTypeService, String shortNameTypeService,
				   String descriptionTypeService, String urlImage, String price,
				   String timeMinutes) {
		this();
		this.cod = cod;
		this.shortName = shortName;
		this.description = description;
		this.codTypeService = codTypeService;
		this.shortNameTypeService = shortNameTypeService;
		this.descriptionTypeService = descriptionTypeService;
		this.urlImage = urlImage;
		this.price = price;
		this.timeMinutes = timeMinutes;
	}

    public Service(JSONObject obj) {
    	this();
    	try {
    		this.cod = obj.getLong("cod");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.shortName = obj.getString("shortName");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.description = obj.getString("description");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.codTypeService = obj.getLong("codTypeService");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
    		this.shortNameTypeService = obj.getString("shortNameTypeService");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
    		this.descriptionTypeService = obj.getString("descriptionTypeService");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
    		this.urlImage = obj.getString("urlImage");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
    		this.price = obj.getString("price");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
    		this.timeMinutes = obj.getString("timeMinutes");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
    
	public Long getCod() {
		return cod;
	}

	public void setCod(Long cod) {
		this.cod = cod;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCodTypeService() {
		return codTypeService;
	}

	public void setCodTypeService(Long codTypeService) {
		this.codTypeService = codTypeService;
	}

	public String getShortNameTypeService() {
		return shortNameTypeService;
	}

	public void setShortNameTypeService(String shortNameTypeService) {
		this.shortNameTypeService = shortNameTypeService;
	}

	public String getDescriptionTypeService() {
		return descriptionTypeService;
	}

	public void setDescriptionTypeService(String descriptionTypeService) {
		this.descriptionTypeService = descriptionTypeService;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTimeMinutes() {
		return timeMinutes;
	}

	public void setTimeMinutes(String timeMinutes) {
		this.timeMinutes = timeMinutes;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("cod", cod);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("shortName", shortName);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("description", description);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("codTypeService", codTypeService);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("shortNameTypeService", shortNameTypeService);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("descriptionTypeService", descriptionTypeService);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("urlImage", urlImage);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("price", price);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("timeMinutes", timeMinutes);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

}
