package br.com.agendalize.androidapp.model;

import org.json.JSONObject;

/**
 * Created by mthama on 25/10/15.
 */
public class ServiceHistory {

    private String idBooking;
    private String hourBegin;
    private String description;
    private String cod;

    public ServiceHistory() {
        idBooking = "";
        hourBegin = "";
        description = "";
        cod = "";
    }

    public ServiceHistory(String idBooking, String hourBegin, String description, String cod) {
        this();
        this.idBooking = idBooking;
        this.hourBegin = hourBegin;
        this.description = description;
        this.cod = cod;
    }

    public ServiceHistory(JSONObject obj) {
        this();
        try {
            this.idBooking = obj.getString("idBooking");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.hourBegin = obj.getString("hourBegin");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.description = obj.getString("description");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.cod = obj.getString("cod");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public String getIdBooking() {
        return idBooking;
    }

    public void setIdBooking(String idBooking) {
        this.idBooking = idBooking;
    }

    public String getHourBegin() {
        return hourBegin;
    }

    public void setHourBegin(String hourBegin) {
        this.hourBegin = hourBegin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("idBooking", idBooking);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("hourBegin", hourBegin);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("description", description);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("cod", cod);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return obj;
    }

}
