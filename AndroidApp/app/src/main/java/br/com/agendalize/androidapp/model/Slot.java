package br.com.agendalize.androidapp.model;

import org.json.JSONObject;

/**
 * Created by mthama on 21/11/15.
 */
public class Slot {

    private long idBooking;
    private String customerName;
    private String serviceName;
    private String codStatus;
    private String hourBegin;
    private String codHourBegin;

    public Slot() {
        idBooking = 0;
        customerName = "";
        serviceName = "";
        codStatus = "";
        hourBegin = "";
        codHourBegin = "";
    }

    public Slot(long idBooking, String customerName, String serviceName,
                String codStatus, String hourBegin, String codHourBegin) {
        this();
        this.idBooking = idBooking;
        this.customerName = customerName;
        this.serviceName = serviceName;
        this.codStatus = codStatus;
        this.hourBegin = hourBegin;
        this.codHourBegin = codHourBegin;
    }

    public Slot(JSONObject obj) {
        this();
        try {
            idBooking = Long.valueOf(obj.getString("idBooking"));
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            customerName = obj.getString("customerName");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            serviceName = obj.getString("serviceName");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            codStatus = obj.getString("codStatus");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            hourBegin = obj.getString("hourBegin");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            codHourBegin = obj.getString("codHourBegin");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public long getIdBooking() {
        return idBooking;
    }

    public void setIdBooking(long idBooking) {
        this.idBooking = idBooking;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getCodStatus() {
        return codStatus;
    }

    public void setCodStatus(String codStatus) {
        this.codStatus = codStatus;
    }

    public String getHourBegin() {
        return hourBegin;
    }

    public void setHourBegin(String hourBegin) {
        this.hourBegin = hourBegin;
    }

    public String getCodHourBegin() {
        return codHourBegin;
    }

    public void setCodHourBegin(String codHourBegin) {
        this.codHourBegin = codHourBegin;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("idBooking", String.valueOf(idBooking));
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("customerName", customerName);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("serviceName", serviceName);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("codStatus", codStatus);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("hourBegin", hourBegin);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("codHourBegin", codHourBegin);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return obj;
    }

}
