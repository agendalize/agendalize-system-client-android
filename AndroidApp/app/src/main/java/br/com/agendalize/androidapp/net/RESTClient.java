package br.com.agendalize.androidapp.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.model.Booking;
import br.com.agendalize.androidapp.model.BookingGroup;
import br.com.agendalize.androidapp.model.EmployeeSchedule;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.model.Store;
import br.com.agendalize.androidapp.util.LogUtil;

public class RESTClient {

	private int status							= -1;
	private String responseString				= null;
    private String accessToken                  = null;
	private InputStream instream				= null;
	private DefaultHttpClient httpClient		= null;
	private HttpGet httpGet						= null;
	private HttpPost httpPost					= null;
    private HttpPut httpPut	    				= null;
	private HttpResponse response				= null;
	private HttpParams httpParameters 			= new BasicHttpParams();

	public RESTClient(Context context) {
        SharedPreferences prefsPrivate = context.getSharedPreferences(
                AppConstants.SHARED_PREFS, Context.MODE_PRIVATE);
        this.accessToken = prefsPrivate.getString(AppConstants.ACCESS_TOKEN, null);
		HttpConnectionParams.setConnectionTimeout(httpParameters, AppConstants.CONN_TIME_OUT);
		HttpConnectionParams.setSoTimeout(httpParameters, AppConstants.CONN_TIME_OUT);
		httpClient = new DefaultHttpClient(httpParameters);
	}

    public RESTClient() {
        BasicHttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 60000);
        HttpConnectionParams.setSoTimeout(httpParameters, 60000);
        httpClient = new DefaultHttpClient(httpParameters);
    }
	
	public int getStatus() {
		return status;
	}

    public String getResponseString() {
        return responseString;
    }
	
	public void requestPinSms(final String phoneNumber) {
		try {
			httpPost = new HttpPost(AppConstants.domain + "oauth/pin");
			httpPost.setHeader("Content-type", "application/json");
			JSONObject obj = new JSONObject();
			obj.put("mobilePhoneNumber", phoneNumber);
			obj.put("clientId", AppConstants.DEVICE_NAME);
			obj.put("clientSecret", AppConstants.DEVICE_KEY);
			obj.put("grantType", AppConstants.DEVICE_CREDENTIAL);
			httpPost.setEntity(new ByteArrayEntity(obj.toString().getBytes("UTF8")));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
			}
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
		disconnect();
	}
	
	public String requestAccessToken(final String phoneNumber,
			final String pinCode, final String deviceId) {
		try {
			httpPost = new HttpPost(AppConstants.domain + "oauth/checkPin");
			httpPost.setHeader("Content-type", "application/json");
			JSONObject obj = new JSONObject();
			obj.put("mobilePhoneNumber", phoneNumber);
			obj.put("pinCode", pinCode);
			obj.put("deviceId", deviceId);
			obj.put("clientId", AppConstants.DEVICE_NAME);
			obj.put("clientSecret", AppConstants.DEVICE_KEY);
			obj.put("grantType", AppConstants.DEVICE_CREDENTIAL);
			Log.i(AppConstants.TAG, "A new requestAccessToken has been made: " + obj.toString());
			httpPost.setEntity(new ByteArrayEntity(obj.toString().getBytes("UTF8")));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String wsResponse = "";
					String inputLine = null;
					BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
					while ((inputLine = reader.readLine()) != null) {
						wsResponse += inputLine;
					}
					JSONObject jsonObject = new JSONObject(wsResponse);
                    try {
                        responseString = jsonObject.getJSONObject("error").getString("messageError");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.i(AppConstants.TAG, "response is: " + responseString);
					return jsonObject.getString("accessToken");
				}
			}
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
		disconnect();
		return null;
	}

	public List<EmployeeSchedule> getEmployeeSchedules(
            long storeId, String initDate, String endDate, Long[] employeesIds) {
		List<EmployeeSchedule> schedules = new ArrayList<>();
		try {
            String ids = "_";
            for (Long id : employeesIds) {
                ids += (id + "_");
            }
            initDate = initDate.substring(0, 4) + "/" + initDate.substring(4, initDate.length());
            initDate = initDate.substring(0, 7) + "/" + initDate.substring(7, initDate.length());
            String[] initDates = initDate.split("/");
            initDate = initDates[2]+"/"+initDates[1]+"/"+initDates[0];
            endDate = endDate.substring(0, 4) + "/" + endDate.substring(4, endDate.length());
            endDate = endDate.substring(0, 7) + "/" + endDate.substring(7, endDate.length());
            String[] endDates = endDate.split("/");
            endDate = endDates[2]+"/"+endDates[1]+"/"+endDates[0];
            String query = AppConstants.domain + "booking/schedule/" + storeId +
                    "?initDate=" + initDate + "&endDate=" + endDate + "&ids=" + ids;
            //Log.i(AppConstants.TAG, "getEmployeeSchedules query is " + query);
            httpGet = new HttpGet(query);
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("Access-Token", accessToken);
            response = httpClient.execute(httpGet);
            if (response != null) {
                status = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String wsResponse = "";
                    String inputLine = null;
                    BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
                    while ((inputLine = reader.readLine()) != null) {
                        wsResponse += inputLine;
                    }
                    //LogUtil.longLogCat("getEmployeeSchedules response is: " + wsResponse);
                    JSONArray schedulesByDay = new JSONArray(wsResponse);
                    for (int i=0; i<schedulesByDay.length(); i++) {
                        schedules.add(new EmployeeSchedule(schedulesByDay.getJSONObject(i)));
                    }
                }
            }
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
        disconnect();
		return schedules;
	}

    /**
     * This call obtains latitude and longitude of a given address string. If address string is
     * invalid then this request should return null.
     * @param address is the string that defines street name, number, city, etc...
     * @return a double array containing two values, latitude and longitude.
     */
	public double[] getLocationInfo(final String address) {
		double LatLon[] = new double[2];
		try {
			int b;
	        httpPost = new HttpPost(
                    "http://maps.google.com/maps/api/geocode/json?address=" +
                            address.replaceAll(" ","%20") + "&sensor=false");
            response = httpClient.execute(httpPost);
            if (response != null) {
				status = response.getStatusLine().getStatusCode();
	            HttpEntity entity = response.getEntity();
	            StringBuilder stringBuilder = new StringBuilder();
	            InputStream stream = entity.getContent();
	            while ((b = stream.read()) != -1) {
	                stringBuilder.append((char) b);
	            }
	            JSONObject jsonObject = new JSONObject(stringBuilder.toString());
	        	LatLon[0] = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
	    			.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
	        	LatLon[1] = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
	                .getJSONObject("geometry").getJSONObject("location").getDouble("lng");
            }
		} catch (ConnectTimeoutException e) {
			status = HttpStatus.SC_REQUEST_TIMEOUT;
	    } catch (SocketTimeoutException e) {
	    	status = HttpStatus.SC_REQUEST_TIMEOUT;
		} catch (final UnsupportedEncodingException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final ClientProtocolException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
		} catch (final JSONException e) {
			status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
	    } catch (final IOException e) {
	    	status = HttpStatus.SC_NOT_FOUND;
		}
		disconnect();
		return LatLon;
	}

    /**
     * Busca os salões que ofereçam os serviços procurados pelo cliente. Os salões retornados
     * na resposta devem obedecer aos seguintes critérios: i) devem dispor de funcionários aptos
     * a executar os serviços pedidos em services; ii) os funcionários definidos devem ter
     * horário(s) livre(s) entre hourFrom e hourTo no dia baseDate; iii) o salão deve estar
     * localizado a uma distância máxima de range metros, a partir da posição latitude/longitude;
     * iv) a ausência ou inconsisência do campo services implica que o cliente busca todos os
     * serviços; v) os campos listOffSet, sizeList, latitude, longitude, hourFrom, hourTo,
     * baseDate, e range serão enviados sempre e sua(s) ausência(s)/inconsistência(s) deve
     * retornar erro.
     * @param request
     * @return
     */
	public List<Store> StoresFilterBy(final JSONObject request) {
        Log.i(AppConstants.TAG, "A new store search has been made: " + request.toString());
		List<Store> stores = new ArrayList<Store>();
		try {
			httpPost = new HttpPost(AppConstants.domain + "stores/filter");
			httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Access-Token", accessToken);
			httpPost.setEntity(new ByteArrayEntity(
					request.toString().getBytes("UTF8")
			));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String wsResponse = "";
					String inputLine = null;
					BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
					while ((inputLine = reader.readLine()) != null) {
						wsResponse += inputLine;
					}
                    //LogUtil.longLogCat("getEmployeeSchedules REQUEST is: " + request.toString());
                    //LogUtil.longLogCat("getEmployeeSchedules RESPONSE is: " + wsResponse);
					JSONArray arr = new JSONArray(wsResponse);
					for (int i=0; i<arr.length(); i++) {
						JSONObject obj = arr.getJSONObject(i);
						stores.add(new Store(obj));
					}
				}
			}
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
		disconnect();
		return stores;
	}

    /**
     * Esta chamada envia uma sugestão de salão, para ser inserido no sistema.
     * @param name
     * @param Address
     * @param contact
     */
	public void FeedbacksStoreSugestion(final String name, final String Address, final String contact) {
		try {
			httpPost = new HttpPost(AppConstants.domain + "feedbacks/store-sugestions/create");
			httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Access-Token", accessToken);
			httpPost.setEntity(new ByteArrayEntity(
					new JSONObject()
					.put("name", name)
					.put("addressInfo", Address)
					.put("contactInfo", contact)
                            .toString().getBytes("UTF8")
			));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
			}
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
		disconnect();
	}

    /**
     * Esta chamada envia um feedback, crítica, ou opinião do usuário.
     * @param message
     */
	public void FeedbacksEvaluateUs(final String message) {
		try {
			httpPost = new HttpPost(AppConstants.domain + "feedbacks/agendalize-opinions/create");
			httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Access-Token", accessToken);
			httpPost.setEntity(new ByteArrayEntity(
					new JSONObject().put("opinion", message).toString().getBytes("UTF8")
			));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
			}
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
		disconnect();
	}

	/**
	 * Ao filtrar a busca de salões, o usuário precisa saber quais serviços são oferecidos
	 * pelo sistema, podendo assim selecionar os serviços que procura.
	 * @return
	 */
	public List<Service> ServicesList() {
		List<Service> services = new ArrayList<Service>();
		try {
			httpGet = new HttpGet(AppConstants.domain + "services/list");
            httpGet.setHeader("Access-Token", accessToken);
			response = httpClient.execute(httpGet);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String wsResponse = "";
					String inputLine = null;
					BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
					while ((inputLine = reader.readLine()) != null) {
						wsResponse += inputLine;
					}
					JSONArray arr = new JSONArray(wsResponse);
					for (int i=0; i<arr.length(); i++) {
						services.add(new Service(arr.getJSONObject(i)));
					}
				}
			}
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
		disconnect();
		return services;
	}

    public void BookingCreate(List<Booking> bookings) {
        try {
            JSONArray arr = new JSONArray();
            for (int i=0; i<bookings.size(); i++) {
                arr.put(bookings.get(i).toJson());
            }
            httpPost = new HttpPost(AppConstants.domain + "booking/create");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Access-Token", accessToken);
            httpPost.setEntity(new ByteArrayEntity(
                    new JSONObject().put("bookings", arr).toString().getBytes("UTF8")
            ));
            response = httpClient.execute(httpPost);
            if (response != null) {
                status = response.getStatusLine().getStatusCode();
            } else {
                status = HttpStatus.SC_NO_CONTENT;
            }
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
        disconnect();
    }

    public void BookingCancel(long bookingGroupId, String comment) {
        try {
            Log.i(AppConstants.TAG, "BookingCancel(" + accessToken + "): ID=" + bookingGroupId + " COMMENT=" + comment);
            httpPut = new HttpPut(AppConstants.domain + "booking/cancel/" + bookingGroupId);
            httpPut.setHeader("Content-type", "application/json");
            httpPut.setHeader("Access-Token", accessToken);
            httpPut.setEntity(new ByteArrayEntity(
                    new JSONObject().put("comment", comment).toString().getBytes("UTF8")
            ));
            response = httpClient.execute(httpPut);
            if (response != null) {
                status = response.getStatusLine().getStatusCode();
            } else {
                status = HttpStatus.SC_NO_CONTENT;
            }
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
        disconnect();
    }

	public List<BookingGroup> BookingHistory(int listOffSet, int listSize) {
		List<BookingGroup> bookings = new ArrayList<BookingGroup>();
		try {
			httpPost = new HttpPost(AppConstants.domain + "booking/history");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Access-Token", accessToken);
            httpPost.setEntity(new ByteArrayEntity(
                    new JSONObject(
                            "{" +
                                "\"listOffSet\": \"" + listOffSet + "\"," +
                                "\"listSize\": \"" + listSize + "\"" +
                            "}"
                    ).toString().getBytes("UTF8")
            ));
			response = httpClient.execute(httpPost);
			if (response != null) {
				status = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String wsResponse = "";
					String inputLine = null;
					BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
					while ((inputLine = reader.readLine()) != null) {
						wsResponse += inputLine;
                    }
					JSONArray arr = new JSONArray(wsResponse);
					for (int i=0; i<arr.length(); i++) {
                        bookings.add(new BookingGroup(arr.getJSONObject(i)));
					}
				}
			}
        } catch (InterruptedIOException e) {
            status = HttpStatus.SC_REQUEST_TIMEOUT;
            e.printStackTrace();
        } catch (final UnsupportedEncodingException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final ClientProtocolException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final JSONException e) {
            status = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            e.printStackTrace();
        } catch (final IOException e) {
            status = HttpStatus.SC_NOT_FOUND;
            e.printStackTrace();
        }
		disconnect();
        return bookings;
	}
	
	// closes the connection
	public void disconnect() {
		try {
			httpPost.abort();
			instream.close();
			httpClient.getConnectionManager().shutdown();
		} catch (IOException e) {
		} catch (NullPointerException n) {}
	}

}
