package br.com.agendalize.androidapp.model;

import org.json.JSONObject;

/**
 * This class represents the contacts of a store.
 */
public class ContactStore {
	
    private String phoneNumberOne;
    private String phoneNumberTwo;
    private String email;
    private String site;
    private String facebook;
    
    public ContactStore() {
    	this.phoneNumberOne = "";
    	this.phoneNumberTwo = "";
    	this.email = "";
    	this.site = "";
    	this.facebook = "";
    }
    
    public ContactStore(String phoneNumberOne, String phoneNumberTwo,
						String email, String site, String facebook) {
		this();
		this.phoneNumberOne = phoneNumberOne;
		this.phoneNumberTwo = phoneNumberTwo;
		this.email = email;
		this.site = site;
		this.facebook = facebook;
	}

	public ContactStore(JSONObject obj) {
    	this();
    	try {
    		this.phoneNumberOne = obj.getString("phoneNumberOne");
    	} catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.phoneNumberTwo = obj.getString("phoneNumberTwo");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.email = obj.getString("email");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.site = obj.getString("site");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.facebook = obj.getString("facebook");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
    
	public String getPhoneNumberOne() {
		return phoneNumberOne;
	}

	public void setPhoneNumberOne(String phoneNumberOne) {
		this.phoneNumberOne = phoneNumberOne;
	}

	public String getPhoneNumberTwo() {
		return phoneNumberTwo;
	}

	public void setPhoneNumberTwo(String phoneNumberTwo) {
		this.phoneNumberTwo = phoneNumberTwo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("phoneNumberOne", phoneNumberOne);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("phoneNumberTwo", phoneNumberTwo);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("email", email);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("site", site);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("facebook", facebook);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

}
