package br.com.agendalize.androidapp.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Store {

    private Long id;
    private String name;
    private String description;
	private String urlPhoto;
    private AddressStore addressInfo;
    private ContactStore contactInfo;
    private List<Service> listServices;
    private List<Employee> employees;
    private List<Operation> listOperation;
    private List<String> album;
    private Float evaluation;
    
    public Store() {
    	this.id = new Long(0);
        this.name = "";
        this.description = "";
		this.urlPhoto = "";
        this.addressInfo = new AddressStore();
        this.contactInfo = new ContactStore();
        this.listServices = new ArrayList<Service>();
        this.employees = new ArrayList<Employee>();
        this.listOperation = new ArrayList<Operation>();;
        this.album = new ArrayList<String>();
        this.evaluation = new Float(0);
    }

    public Store(Long id, String name, String description, String urlPhoto,
                 AddressStore addressInfo,
                 ContactStore contactInfo,
                 List<Service> listServices,
                 List<Employee> employees,
                 List<Operation> listOperation, List<String> album,
                 Float evaluation) {
		this();
		this.id = id;
		this.name = name;
		this.description = description;
		this.urlPhoto = urlPhoto;
		this.addressInfo = addressInfo;
		this.contactInfo = contactInfo;
		this.listServices = listServices;
		this.employees = employees;
		this.listOperation = listOperation;
		this.album = album;
		this.evaluation = evaluation;
	}

    public Store(JSONObject obj) {
    	this();
    	try {
    		this.id = obj.getLong("id");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.name = obj.getString("name");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.description = obj.getString("description");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			this.urlPhoto = obj.getString("urlPhoto");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		this.addressInfo = new AddressStore(
    				new JSONObject(obj.getString("addressInfo"))
			);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
    		this.contactInfo = new ContactStore(
    				new JSONObject(obj.getString("contactInfo"))
			);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray(obj.getString("listServices"));
			for (int i=0; i<arr.length(); i++) {
				this.listServices.add(new Service(arr.getJSONObject(i)));
			}
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray(obj.getString("employees"));
			for (int i=0; i<arr.length(); i++) {
				this.employees.add(new Employee(arr.getJSONObject(i)));
			}
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray(obj.getString("listOperation"));
			for (int i=0; i<arr.length(); i++) {
				this.listOperation.add(new Operation(arr.getJSONObject(i)));
			}
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray(obj.getString("albums"));
			for (int i=0; i<arr.length(); i++) {
				this.album.add((arr.getString(i)));
			}
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
    		this.evaluation = (float) obj.getDouble("evaluation");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrlPhoto() {
		return urlPhoto;
	}

	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}

	public AddressStore getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(AddressStore addressInfo) {
		this.addressInfo = addressInfo;
	}

	public ContactStore getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactStore contactInfo) {
		this.contactInfo = contactInfo;
	}

	public List<Service> getListServices() {
		return listServices;
	}

	public void setListServices(List<Service> listServices) {
		this.listServices = listServices;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Operation> getListOperation() {
		return listOperation;
	}

	public void setListOperation(List<Operation> listOperation) {
		this.listOperation = listOperation;
	}

	public List<String> getAlbum() {
		return album;
	}

	public void setAlbum(List<String> album) {
		this.album = album;
	}

	public Float getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Float evaluation) {
		this.evaluation = evaluation;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("id", id);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("name", name);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("description", description);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("urlPhoto", urlPhoto);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("addressInfo", addressInfo.toJson());
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("contactInfo", contactInfo.toJson());
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<listServices.size(); i++) {
				arr.put(listServices.get(i).toJson());
			}
			obj.put("listServices", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<employees.size(); i++) {
				arr.put(employees.get(i).toJson());
			}
			obj.put("employees", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<listOperation.size(); i++) {
				arr.put(listOperation.get(i).toJson());
			}
			obj.put("listOperation", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<album.size(); i++) {
				arr.put(album.get(i));
			}
			obj.put("album", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("evaluation", evaluation);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

    public Service getService(long id) {
        for (int i=0; i<listServices.size(); i++) {
            if (listServices.get(i).getCod().equals(id))
                return listServices.get(i);
        }
        return null;
    }

}
