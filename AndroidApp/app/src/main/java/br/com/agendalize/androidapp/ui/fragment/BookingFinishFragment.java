package br.com.agendalize.androidapp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.ui.BookingActivity;
import br.com.agendalize.androidapp.ui.HomeActivity;

/**
 * Created by mthama on 22/11/15.
 */
public class BookingFinishFragment extends Fragment {

    //===================================================================================
    // Fields
    //===================================================================================

    private BookingActivity activity;
    private Button frag_bookingfinish_previousbtn;
    private Button frag_bookingfinish_nextbtn;


    //===================================================================================
    // Override methods
    //===================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingfinish, container, false);
        activity = (BookingActivity)getActivity();

        frag_bookingfinish_previousbtn = (Button)rootView.findViewById(R.id.frag_bookingfinish_previousbtn);
        frag_bookingfinish_previousbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.putExtra(AppConstants.FragmentIndexToStartAt, 1);
                startActivity(intent);
                activity.finish();
            }
        });

        frag_bookingfinish_nextbtn = (Button)rootView.findViewById(R.id.frag_bookingfinish_nextbtn);
        frag_bookingfinish_nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.putExtra(AppConstants.FragmentIndexToStartAt, 0);
                startActivity(intent);
                activity.finish();
            }
        });

        return rootView;
    }

}
