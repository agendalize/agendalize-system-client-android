package br.com.agendalize.androidapp.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.PhoneNumberUtils;
import android.util.DisplayMetrics;

import br.com.agendalize.androidapp.R;

public class DeviceUtils {

	public static String getResolutionAsSizeString(Activity a) {
		DisplayMetrics dm = new DisplayMetrics();
		a.getWindowManager().getDefaultDisplay().getMetrics(dm);
		switch(dm.densityDpi) {
			case DisplayMetrics.DENSITY_LOW: return "small";
			case DisplayMetrics.DENSITY_MEDIUM: return "medium";
			case DisplayMetrics.DENSITY_HIGH: return "large";
			case DisplayMetrics.DENSITY_XHIGH: return "xlarge";
			case DisplayMetrics.DENSITY_XXHIGH: return "xxlarge";
			default: return "large";
		}
	}
	
	public static void justShowDialogWarning(final Activity activity, int messageId, final boolean finishIt) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(activity.getResources().getString(messageId)).setCancelable(false).
		setPositiveButton(activity.getResources().getString(R.string.action_ok),
			new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {
					if (finishIt) activity.finish();
				}
			}
		);
		final AlertDialog alert = builder.create();
		alert.show();
	};
	
	/**
	 * Checks if internet connection is available, given a context.
	 * @param context the context.
	 * @return true if available, false otherwise.
	 */
	public static boolean isConnectedToInternet(Context context) {
		ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		// 0 is for 3G and 1 for wifi
		if (connec.getNetworkInfo(0).getState().equals(NetworkInfo.State.CONNECTED) || connec.getNetworkInfo(1).getState().equals(NetworkInfo.State.CONNECTED)) {
			// Do something in here when we are connected
			return true;
		}
		return false;
	}
	
	public static boolean isPhoneValid(String phone) {
	    return PhoneNumberUtils.isGlobalPhoneNumber(phone);
	}
	
	public static List<CharSequence> getCountryCodeArray() {
		List<CharSequence> countryList = new ArrayList<CharSequence>();
		countryList.add("Afghanistan (+93)");
		countryList.add("Albania (+355)");
		countryList.add("Algeria (+213)");
		countryList.add("American Samoa (+1)");
		countryList.add("Andorra (+376)");
		countryList.add("Angola (+244)");
		countryList.add("Anguilla (+1)");
		countryList.add("Antigua and Barbuda (+1)");
		countryList.add("Argentine (+54)");
		countryList.add("Armenia (+374)");
		countryList.add("Aruba (+297)");
		countryList.add("Ascension Island (+247)");
		countryList.add("Australia (+61)");
		countryList.add("Austria (+43)");
		countryList.add("Azerbaijan (+994)");
		countryList.add("Bahamas (+1)");
		countryList.add("Bahrain (+973)");
		countryList.add("Bangladesh (+880)");
		countryList.add("Barbados (+1)");
		countryList.add("Belarus (+375)");
		countryList.add("Belgium (+32)");
		countryList.add("Belize (+501)");
		countryList.add("Benin (+229)");
		countryList.add("Bermuda (+1)");
		countryList.add("Bhutan (+975)");
		countryList.add("Bolivia (+591)");
		countryList.add("Bosnia and Herzegovina (+387)");
		countryList.add("Botswana (+267)");
		countryList.add("Brazil (+55)");
		countryList.add("British Virgin Islands (+1)");
		countryList.add("Brunei Darussalam (+673)");
		countryList.add("Bulgaria (+359)");
		countryList.add("Burkina Faso (+226)");
		countryList.add("Burundi (+257)");
		countryList.add("Cambodia (+855)");
		countryList.add("Cameroon (+237)");
		countryList.add("Canada (+1)");
		countryList.add("Cape Verde (+238)");
		countryList.add("Cayman Islands (+1)");
		countryList.add("Central African Republic (+236)");
		countryList.add("Chad (+235)");
		countryList.add("Chile (+56)");
		countryList.add("China (+86)");
		countryList.add("Colombia (+57)");
		countryList.add("Comoros (+269)");
		countryList.add("Congo (+242)");
		countryList.add("Congo, Democratic Republic (+243)");
		countryList.add("Cook Islands (+682)");
		countryList.add("Costa Rica (+506)");
		countryList.add("Côte dIvoire (+225)");
		countryList.add("Croatia (+385)");
		countryList.add("Cuba (+53)");
		countryList.add("Cyprus (+357)");
		countryList.add("Czech Republic (+420)");
		countryList.add("Democratic People Republic of Korea (+850)");
		countryList.add("Denmark (+45)");
		countryList.add("Diego Garcia (+246)");
		countryList.add("Djibouti (+253)");
		countryList.add("Dominica (+1)");
		countryList.add("Dominican Republic (+1)");
		countryList.add("East Timor (+670)");
		countryList.add("Ecuador (+593)");
		countryList.add("Egypt (+20)");
		countryList.add("El Salvador (+503)");
		countryList.add("Equatorial Guinea (+240)");
		countryList.add("Eritrea (+291)");
		countryList.add("Estonia (+372)");
		countryList.add("Ethiopia (+251)");
		countryList.add("Falkland Islands (Malvinas) (+500)");
		countryList.add("Faroe Islands (+298)");
		countryList.add("Fiji (+679)");
		countryList.add("Finland (+358)");
		countryList.add("France (+33)");
		countryList.add("French Guiana (+594)");
		countryList.add("French Polynesia (+689)");
		countryList.add("Gabon (+241)");
		countryList.add("Gambia (+220)");
		countryList.add("Georgia (+995)");
		countryList.add("Germany (+49)");
		countryList.add("Ghana (+233)");
		countryList.add("Gibraltar (+350)");
		countryList.add("Greece (+30)");
		countryList.add("Greenland (+299)");
		countryList.add("Grenada (+1)");
		countryList.add("Guadeloupe (+590)");
		countryList.add("Guam (+1)");
		countryList.add("Guatemala (+502)");
		countryList.add("Guinea (+224)");
		countryList.add("Guinea-Bissau (+245)");
		countryList.add("Guyana (+592)");
		countryList.add("Haiti (+509)");
		countryList.add("Honduras (+504)");
		countryList.add("Hong Kong (+852)");
		countryList.add("Hungary (+36)");
		countryList.add("Iceland (+354)");
		countryList.add("India (+91)");
		countryList.add("Indonesia (+62)");
		countryList.add("Iran (+98)");
		countryList.add("Iraq (+964)");
		countryList.add("Ireland (+353)");
		countryList.add("Israel (+972)");
		countryList.add("Italy (+39)");
		countryList.add("Jamaica (+1)");
		countryList.add("Japan (+81)");
		countryList.add("Jordan (+962)");
		countryList.add("Kazakhstan (+7)");
		countryList.add("Kenya (+254)");
		countryList.add("Kiribati (+686)");
		countryList.add("Korea (Republic of) (+82)");
		countryList.add("Kuwait (+965)");
		countryList.add("Kyrgyzstan (+996)");
		countryList.add("Lao (+856)");
		countryList.add("Latvia (+371)");
		countryList.add("Lebanon (+961)");
		countryList.add("Lesotho (+266)");
		countryList.add("Liberia (+231)");
		countryList.add("Libyan (+218)");
		countryList.add("Liechtenstein (+423)");
		countryList.add("Lithuania (+370)");
		countryList.add("Luxembourg (+352)");
		countryList.add("Macao (+853)");
		countryList.add("Macedonia (+389)");
		countryList.add("Madagascar (+261)");
		countryList.add("Malawi (+265)");
		countryList.add("Malaysia (+60)");
		countryList.add("Maldives (+960)");
		countryList.add("Mali (+223)");
		countryList.add("Malta (+356)");
		countryList.add("Marshall Islands (+692)");
		countryList.add("Martinique (+596)");
		countryList.add("Mauritania (+222)");
		countryList.add("Mauritius (+230)");
		countryList.add("Mayotte (+269)");
		countryList.add("Mexico (+52)");
		countryList.add("Micronesia (+691)");
		countryList.add("Moldova (+373)");
		countryList.add("Monaco (+377)");
		countryList.add("Mongolia (+976)");
		countryList.add("Montenegro (+382)");
		countryList.add("Montserrat (+1)");
		countryList.add("Morocco (+212)");
		countryList.add("Mozambique (+258)");
		countryList.add("Myanmar (+95)");
		countryList.add("Namibia (+264)");
		countryList.add("Nauru (+674)");
		countryList.add("Nepal (+977)");
		countryList.add("Netherlands (+31)");
		countryList.add("Netherlands Antilles (+599)");
		countryList.add("New Caledonia (+687)");
		countryList.add("New Zealand (+64)");
		countryList.add("Nicaragua (+505)");
		countryList.add("Niger (+227)");
		countryList.add("Nigeria (+234)");
		countryList.add("Niue (+683)");
		countryList.add("Northern Mariana Islands (+1)");
		countryList.add("Norway (+47)");
		countryList.add("Oman (+968)");
		countryList.add("Pakistan (+92)");
		countryList.add("Palau (+680)");
		countryList.add("Panama (+507)");
		countryList.add("Papua New Guinea (+675)");
		countryList.add("Paraguay (+595)");
		countryList.add("Peru (+51)");
		countryList.add("Philippines (+63)");
		countryList.add("Poland (+48)");
		countryList.add("Portugal (+351)");
		countryList.add("Puerto Rico (+1)");
		countryList.add("Qatar (+974)");
		countryList.add("Reunion (+262)");
		countryList.add("Romania (+40)");
		countryList.add("Russian (+7)");
		countryList.add("Rwanda (Republic of) (+250)");
		countryList.add("Saint Helena (+290)");
		countryList.add("Saint Kitts and Nevis (+1)");
		countryList.add("Saint Lucia (+1)");
		countryList.add("Saint-Pierre e Miquelon (+508)");
		countryList.add("Saint Vincent (+1)");
		countryList.add("Samoa (+685)");
		countryList.add("San Marino (+378)");
		countryList.add("Sao Tome and Principe (+239)");
		countryList.add("Saudi Arabia (+966)");
		countryList.add("Senegal (+221)");
		countryList.add("Serbia (+381)");
		countryList.add("Seychelles (+248)");
		countryList.add("Sierra Leone (+232)");
		countryList.add("Singapore (+65)");
		countryList.add("Slovakia (+421)");
		countryList.add("Slovenia (+386)");
		countryList.add("Solomon Islands (+677)");
		countryList.add("Somalia (+252)");
		countryList.add("South Africa (+27)");
		countryList.add("Spain (+34)");
		countryList.add("Sri Lanka (+94)");
		countryList.add("Sudan (+249)");
		countryList.add("Suriname (+597)");
		countryList.add("Swaziland (+268)");
		countryList.add("Sweden (+46)");
		countryList.add("Switzerland (+41)");
		countryList.add("Syrian (+963)");
		countryList.add("Taiwan (+886)");
		countryList.add("Tajikistan (+992)");
		countryList.add("Tanzania (+255)");
		countryList.add("Thailand (+66)");
		countryList.add("Togo (+228)");
		countryList.add("Tokelau (+690)");
		countryList.add("Tonga (+676)");
		countryList.add("Trinidad and Tobago (+1)");
		countryList.add("Tunisia (+216)");
		countryList.add("Turkey (+90)");
		countryList.add("Turkmenistan (+993)");
		countryList.add("Turks and Caicos Islands (+1)");
		countryList.add("Tuvalu (+688)");
		countryList.add("Uganda (+256)");
		countryList.add("Ukraine (+380)");
		countryList.add("United Arab Emirates (+971)");
		countryList.add("United Kingdom (+44)");
		countryList.add("United States Virgin Islands (+1)");
		countryList.add("Uruguay (+598)");
		countryList.add("USA (+1)");
		countryList.add("Uzbekistan (+998)");
		countryList.add("Vanuatu (+678)");
		countryList.add("Vaticano (+379)");
		countryList.add("Venezuela (+58)");
		countryList.add("Vietnam (+84)");
		countryList.add("Wallis e Futuna (+681)");
		countryList.add("Yemen (+967)");
		countryList.add("Zambia (+260)");
		countryList.add("Zimbabwe (+263)");
		return countryList;
	}
		
}
