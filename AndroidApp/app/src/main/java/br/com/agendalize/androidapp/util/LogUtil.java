package br.com.agendalize.androidapp.util;

import android.util.Log;

import br.com.agendalize.androidapp.AppConstants;

/**
 * Created by mthama on 22/11/15.
 */
public class LogUtil {

    public static void longLogCat(String veryLongString) {
        int maxLogSize = 1000;
        for(int i = 0; i <= veryLongString.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i+1) * maxLogSize;
            end = end > veryLongString.length() ? veryLongString.length() : end;
            Log.v(AppConstants.TAG, veryLongString.substring(start, end));
        }
    }

}
