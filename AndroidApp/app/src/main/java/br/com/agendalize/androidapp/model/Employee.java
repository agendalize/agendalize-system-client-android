package br.com.agendalize.androidapp.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Employee {

    private Long id;
    private String name;
    private String lastName;
    private String description;
    private String imageUrl;
    private String skills;
	private List<Slot> slots;
    
    public Employee() {
    	this.id = 0L;
    	this.name = "";
    	this.lastName = "";
        this.description = "";
    	this.imageUrl = "";
    	this.skills = "_";
        this.slots = new ArrayList<>();
    }

    public Employee(long id, String name, String lastName, String description,
                    String imageUrl, String skills, List<Slot> slots) {
        this();
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.description = description;
        this.imageUrl = imageUrl;
        this.skills = skills;
        this.slots = slots;
    }

    public Employee(JSONObject obj) {
    	this();
    	try {
    		this.id = obj.getLong("id");
    	} catch (Exception e) {
            //e.printStackTrace();
        }
        if (this.id.equals(0L)) {
            try {
                this.id = obj.getLong("employeeId");
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
        try {
            this.name = obj.getString("name");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        if (this.name.equals("")) {
            try {
                this.name = obj.getString("employeeName");
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    	try {
    		this.lastName = obj.getString("lastName");
    	} catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.description = obj.getString("description");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.imageUrl = obj.getString("imageUrl");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
            this.skills = obj.getString("skills");
    	} catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            JSONArray arr = new JSONArray(obj.getString("slots"));
            for (int i=0; i<arr.length(); i++) {
                this.slots.add(new Slot(arr.getJSONObject(i)));
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

    public String getSkills() {
        return this.skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

	public List<Long> getSkillsAsArray() {
        List<Long> s = new ArrayList<Long>();
        if (skills.length() > 1) {
            String[] ids = skills.split("_");
            for (String id : ids) {
                try {
                    s.add(Long.valueOf(id));
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                }
            }
        }
        return s;
	}

	public void setSkillsAsArray(List<Long> skills) {
        this.skills = "_";
        for (int i=0; i<skills.size(); i++)
            this.skills += skills.get(i) + "_";
	}

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("id", id);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("name", name);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("lastName", lastName);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("description", description);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("imageUrl", imageUrl);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("skills", skills);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            JSONArray arr = new JSONArray();
            for (int i=0; i<slots.size(); i++) {
                arr.put(slots.get(i).toJson());
            }
            obj.put("slots", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

}
