package br.com.agendalize.androidapp.model;

public enum ScheduleStatus {
	
	AVAILABLE, PENDING, BOOKING, DISABLE, CANCELED, REJECTED, FINISHED

}
