package br.com.agendalize.androidapp.model;

import org.json.JSONObject;

/**
 * Class used as a model to represent all information about a store address.
 */
public class AddressStore {

    private String address;
    private String complement;
    private String city;
    private String state;
    private String country;
    private double latitude;
    private double longitude;
    
    public AddressStore() {
    	this.address = "";
        this.complement = "";
        this.city = "";
        this.state = "";
        this.country = "";
        this.latitude = 0;
        this.longitude = 0;
    }

    public AddressStore(String address, String complement,
						String city, String state, String country,
                        double latitude, double longitude) {
		this();
		this.address = address;
		this.complement = complement;
		this.city = city;
		this.state = state;
		this.country = country;
		this.latitude = latitude;
		this.longitude = longitude;
	}

    public AddressStore(JSONObject obj) {
    	this();
		try {
			this.address = obj.getString("address");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			this.complement = obj.getString("complement");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			this.city = obj.getString("city");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			this.state = obj.getString("state");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			this.country = obj.getString("country");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			this.latitude = obj.getDouble("latitude");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			this.longitude = obj.getDouble("longitude");
		} catch (Exception e) {
            //e.printStackTrace();
		}
    }
    
	public String getAddress() {
        return address;
	}

	public void setAddress(String address) {
        this.address = address;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("address", address);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("complement", complement);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("city", city);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("state", state);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("country", country);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("latitude", latitude);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("longitude", longitude);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

}
