package br.com.agendalize.androidapp.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.agendalize.androidapp.AppConstants;

public class StoreSearchRequest {
	
	private int listOffSet;
	private ArrayList<Long> services;
	private int listSize;
	private double latitude;
	private double longitude;
	private String hourFrom;
	private String hourTo;
	private List<String> baseDates;
	private long range;
	
	public StoreSearchRequest() {
		listOffSet = 0;
		services = new ArrayList<Long>();
		listSize = AppConstants.SIZE_LIST;
		latitude = 0;
		longitude = 0;
		hourFrom = "0000";
		hourTo = "2359";
		baseDates = new ArrayList<String>();
		range = 10000;
	}

	public StoreSearchRequest(int listOffSet, ArrayList<Long> services,
                              int listSize, double latitude, double longitude, String hourFrom,
                              String hourTo, List<String> baseDates, long range) {
		this();
		this.listOffSet = listOffSet;
		this.services = services;
		this.listSize = listSize;
		this.latitude = latitude;
		this.longitude = longitude;
		this.hourFrom = hourFrom;
		this.hourTo = hourTo;
		this.baseDates = baseDates;
		this.range = range;
	}

	public StoreSearchRequest(JSONObject obj) {
		this();
		try {
            listOffSet = Integer.valueOf(obj.getString("listOffSet"));
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = obj.getJSONArray("services");
			for (int i=0; i<arr.length(); i++) {
				services.add(Long.valueOf(arr.getString(i)));
			}
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            listSize = Integer.valueOf(obj.getString("listSize"));
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            latitude = Double.valueOf(obj.getString("latitude"));
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            longitude = Double.valueOf(obj.getString("longitude"));
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            hourFrom = obj.getString("hourFrom");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            hourTo = obj.getString("hourTo");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = obj.getJSONArray("baseDates");
			for (int i=0; i<arr.length(); i++) {
                baseDates.add(arr.getString(i));
			}
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            range = Long.valueOf(obj.getString("range"));
        } catch (Exception e) {
            //e.printStackTrace();
        }
	}
	
	public int getListOffSet() {
		return listOffSet;
	}

	public void setListOffSet(int listOffSet) {
		this.listOffSet = listOffSet;
	}

	public List<Long> getServices() {
		return services;
	}

	public void setServices(ArrayList<Long> services) {
		this.services = services;
	}

	public int getSizeList() {
		return listSize;
	}

	public void setSizeList(int listSize) {
		this.listSize = listSize;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getHourFrom() {
		return hourFrom;
	}

	public void setHourFrom(String hourFrom) {
		this.hourFrom = hourFrom;
	}

	public String getHourTo() {
		return hourTo;
	}

	public void setHourTo(String hourTo) {
		this.hourTo = hourTo;
	}

	public List<String> getBaseDates() {
		return baseDates;
	}

	public void setBaseDates(List<String> baseDates) {
		this.baseDates = baseDates;
	}

	public long getRange() {
		return range;
	}

	public void setRange(long range) {
		this.range = range;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
            obj.put("listOffSet", ""+listOffSet);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<services.size(); i++) {
				arr.put(""+services.get(i));
			}
			obj.put("services", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            obj.put("listSize", ""+listSize);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            obj.put("latitude", ""+latitude);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            obj.put("longitude", ""+longitude);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            obj.put("hourFrom", hourFrom);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            obj.put("hourTo", hourTo);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            JSONArray arr = new JSONArray();
            for (int i=0; i<baseDates.size(); i++) {
                arr.put(baseDates.get(i));
            }
            obj.put("baseDates", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            obj.put("range", ""+range);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

}
