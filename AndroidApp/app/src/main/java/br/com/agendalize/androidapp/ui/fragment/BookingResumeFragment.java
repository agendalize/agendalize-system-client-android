package br.com.agendalize.androidapp.ui.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Booking;
import br.com.agendalize.androidapp.ui.BookingActivity;
import br.com.agendalize.androidapp.util.ImageUtils;

/**
 * Created by mthama on 22/11/15.
 */
public class BookingResumeFragment extends Fragment {

    //===================================================================================
    // Fields
    //===================================================================================

    private List<Booking> bookings;
    private BookingActivity activity;
    private ViewGroup rootView;
    private TextView frag_bookingresume_monthinview;
    private TextView frag_bookingresume_dayinview;
    private TextView frag_bookingresume_weekday;
    private ListView frag_bookingresume_resumelist;
    private Button frag_bookingresume_previousbtn;
    private Button frag_bookingresume_nextbtn;

    //===================================================================================
    // Override methods
    //===================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingresume, container, false);
        activity = (BookingActivity)getActivity();

        frag_bookingresume_monthinview = (TextView)rootView.findViewById(R.id.frag_bookingresume_monthinview);
        frag_bookingresume_dayinview = (TextView)rootView.findViewById(R.id.frag_bookingresume_dayinview);
        frag_bookingresume_weekday = (TextView)rootView.findViewById(R.id.frag_bookingresume_weekday);
        /* TODO: MVP has only one booking for now...
        frag_bookingresume_resumelist = (ListView)rootView.findViewById(R.id.frag_bookingresume_resumelist);*/

        frag_bookingresume_previousbtn = (Button)rootView.findViewById(R.id.frag_bookingresume_previousbtn);
        frag_bookingresume_previousbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getBookings().clear();
                activity.getPagerView().setCurrentItem(2, true);
            }
        });
        frag_bookingresume_nextbtn = (Button)rootView.findViewById(R.id.frag_bookingresume_nextbtn);
        frag_bookingresume_nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.requestBookingTask();
            }
        });

        return rootView;
    }

    public void setupFields() {
        bookings = activity.getBookings();
        // get current date of interest
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat form = new SimpleDateFormat("yyyyMMdd");
        try {
            calendar.setTime(form.parse(bookings.get(0).getCodDay()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        frag_bookingresume_monthinview.setText(getMonthNameByIndex(calendar.get(Calendar.MONTH)));
        frag_bookingresume_dayinview.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));
        frag_bookingresume_weekday.setText(getWeekNameByIndex(calendar.get(Calendar.DAY_OF_WEEK)));

        ((TextView)rootView.findViewById(R.id.row_bookingresumetitle_hour)).setText(bookings.get(0).getBookingHour());
        new PhotoLoader(
                ((ImageView)rootView.findViewById(R.id.row_bookingresumetitle_photo)),
                ((ProgressBar)rootView.findViewById(R.id.row_bookingresumetitle_photoholder)),
                bookings.get(0).getEmployeePhoto() //TODO: MVP has only one service for now...
        ).execute();
        ((TextView)rootView.findViewById(R.id.row_bookingresumeservice_service)).setText(
                activity.getStore().getService(bookings.get(0).getServiceId()).getShortName()
        );
        ((TextView)rootView.findViewById(R.id.row_bookingresumeservice_price)).setText(
                getResources().getString(R.string.app_currencystr) + " " +
                activity.getStore().getService(bookings.get(0).getServiceId()).getPrice()
        );
        ((TextView)rootView.findViewById(R.id.row_bookingresumetotal_price)).setText(
                getResources().getString(R.string.app_currencystr) + " " +
                activity.getStore().getService(bookings.get(0).getServiceId()).getPrice()
        );
    }

    //===================================================================================
    // General methods
    //===================================================================================

    private String getMonthNameByIndex(int index) {
        switch (index) {
            case 0: return "Jan";
            case 1: return "Fev";
            case 2: return "Mar";
            case 3: return "Abr";
            case 4: return "Mai";
            case 5: return "Jun";
            case 6: return "Jul";
            case 7: return "Ago";
            case 8: return "Set";
            case 9: return "Out";
            case 10: return "Nov";
            case 11: return "Dez";
            default: return "Jan";
        }
    }

    private String getWeekNameByIndex(int index) {
        switch (index) {
            case 1: return activity.getResources().getString(R.string.label_sunday);
            case 2: return activity.getResources().getString(R.string.label_monday);
            case 3: return activity.getResources().getString(R.string.label_tuesday);
            case 4: return activity.getResources().getString(R.string.label_wednesday);
            case 5: return activity.getResources().getString(R.string.label_thursday);
            case 6: return activity.getResources().getString(R.string.label_friday);
            case 7: return activity.getResources().getString(R.string.label_saturday);
            default: return activity.getResources().getString(R.string.label_sunday);
        }
    }

    //===================================================================================
    // Inner classes
    //===================================================================================

    private class PhotoLoader extends AsyncTask<Void, Void, Void> {

        private Drawable drawable;
        private ImageView photo;
        private ProgressBar progress;
        private String url;

        public PhotoLoader(ImageView photo, ProgressBar progress, String url) {
            this.photo = photo;
            this.progress = progress;
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                drawable = ImageUtils.loadImageFromWebOperations(url);
                Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), true);
                drawable = new BitmapDrawable(getResources(), bitmap);
            } catch (Exception e) {
                drawable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                progress.setVisibility(View.GONE);
                if (drawable == null) {
                    drawable = getResources().getDrawable(R.drawable.noimage);
                }
                photo.setImageDrawable(drawable);
            } catch (IllegalStateException e) {
                // thrown if activity is no more attached
                Log.w(AppConstants.TAG, "AlbumLoaderTask: " + e.getLocalizedMessage());
            }
        }

    }

    /*private class BookingResumeAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return employeeSlotsOverDay.size();
        }

        @Override
        public Object getItem(int position) {
            return employeeSlotsOverDay.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_schedule, parent, false);
            }
            return convertView;
        }

    }*/

}
