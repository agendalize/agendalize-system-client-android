package br.com.agendalize.androidapp.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpStatus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.net.IncomingSms;
import br.com.agendalize.androidapp.net.RESTClient;

@SuppressWarnings( "deprecation" )
public class LoginActivity extends ActionBarActivity {

    // used to manage requests
    private ProgressDialog waitDialog;
    private static final Object lock = new Object();
    private IncomingHandler handler = new IncomingHandler();
    private LoginActivity context;

    private String pinCode = null;
    private String accessToken = null;
    private String completePhone;
    private CountDownTimer countDownTimer;

    private LinearLayout activity_login_countrycodelyt;
    private LinearLayout activity_login_pincodelyt;
    private ProgressBar activity_login_progress;
    private TextView activity_login_smswaittext;
    private TextView activity_login_smstimer;
    private EditText activity_login_pintxt;
    private EditText activity_login_phonenumbertxt;
    private Spinner activity_login_countrycode;

    static class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            synchronized(lock) {
                final LoginActivity context = (LoginActivity) msg.obj;
                if (context.waitDialog != null) {
                    context.waitDialog.dismiss();
                }
                switch (msg.what) {
                    case -1:
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getResources().getString(R.string.msg_servererror)).setCancelable(false).
                                setPositiveButton(context.getResources().getString(R.string.action_ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(final DialogInterface dialog, final int id) {
                                            }
                                        }
                                );
                        final AlertDialog alert = builder.create();
                        alert.show();
                        if (context.pinCode != null && context.accessToken == null) {
                            context.activity_login_countrycodelyt.setVisibility(View.VISIBLE);
                            context.activity_login_pincodelyt.setVisibility(View.GONE);
                        }
                        break;
                    case HttpStatus.SC_OK:
                        if (context.pinCode == null) {
                            // AT THIS POINT, WE HAVE REQUESTED THE PIN SMS WITH SUCCESS. NOW, WE NEED
                            // TO WAIT FOR THE SMS. A COUNTER IS DISPLAYED SHOWING THAT THE APP IS
                            // AUTOMATICALLY WAITING FOR THIS SMS. IF THIS COUNTER ENDS, THEN USER
                            // MUST PUT THE PIN CODE MANUALLY OR MAKE ANOTHER PIN REQUEST.
                            context.countDownTimer = new CountDownTimer(300000, 1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                    int progress = (int)(300000 - millisUntilFinished)/1000;
                                    context.activity_login_progress.setProgress(progress);
                                    String minutes = String.format(Locale.getDefault(), "%02d", (4-progress/60));
                                    String seconds = String.format(Locale.getDefault(), "%02d", (59-progress%60));
                                    String s = minutes + context.getResources().getString(R.string.app_m) +
                                            seconds + context.getResources().getString(R.string.app_s);
                                    context.activity_login_smstimer.setText(s);
                                }
                                @Override
                                public void onFinish() {
                                    context.countDownTimer.cancel();
                                    context.mLocalBroadcastManager.unregisterReceiver(context.mBroadcastReceiver);
                                    context.activity_login_smswaittext.setText(R.string.msg_manualpin);
                                    context.activity_login_smstimer.setVisibility(View.INVISIBLE);
                                }
                            };
                            context.countDownTimer.start();
                            // ALSO, A BROADCAST RECEIVER IS REGISTERED FOR THIS SMS INCOMING.
                            context.mLocalBroadcastManager = LocalBroadcastManager.getInstance(context);
                            IntentFilter intentFilter = new IntentFilter();
                            intentFilter.addAction(IncomingSms.PIN_RECEIVED);
                            context.mLocalBroadcastManager.registerReceiver(context.mBroadcastReceiver, intentFilter);
                            context.activity_login_countrycodelyt.setVisibility(View.GONE);
                            context.activity_login_pincodelyt.setVisibility(View.VISIBLE);
                        } else if (context.accessToken != null) {
                            // save and log access-token
                            Editor prefsPrivateEditor = context.getSharedPreferences(
                                    AppConstants.SHARED_PREFS, Context.MODE_PRIVATE).edit();
                            prefsPrivateEditor.putString(AppConstants.ACCESS_TOKEN, context.accessToken);
                            prefsPrivateEditor.apply();
                            Log.i(AppConstants.TAG + ":Login", "Received access token " + context.accessToken);
                            // at this point, we have already pin and access token, so we can start application
                            Intent newApp = new Intent(context, HomeActivity.class);
                            newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(newApp);
                            context.finish();
                        } else {
                            context.pinCode = null;
                            context.accessToken = null;
                            context.activity_login_countrycodelyt.setVisibility(View.VISIBLE);
                            context.activity_login_pincodelyt.setVisibility(View.GONE);
                            Message m = new Message();
                            m.obj = context;
                            m.what = -1;
                            context.handler.sendMessage(m);
                        }
                        break;
                    default:
                }
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;

        // create the toolbar
        setSupportActionBar((Toolbar) findViewById(R.id.activity_login_toolbar));

        // country code spinner
        ArrayAdapter<CharSequence> spinnerArrayAdapter = new ArrayAdapter<>(
                LoginActivity.this,
                android.R.layout.simple_spinner_item,
                getCountryCodeArray());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activity_login_countrycode = (Spinner) findViewById(R.id.activity_login_countrycode);
        activity_login_countrycode.setAdapter(spinnerArrayAdapter);
        activity_login_countrycode.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int selectedItem, long arg3) {
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        activity_login_countrycode.setSelection(getCountryZipCodeIndex() != -1 ? getCountryZipCodeIndex() : 0);

        // try to get phone number
        activity_login_phonenumbertxt = (EditText) findViewById(R.id.activity_login_phonenumbertxt);
        try {
            TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number() == null ? "" : tMgr.getLine1Number();
            int ccSize = activity_login_countrycode.getSelectedItem().toString().replaceAll("[^\\d+]", "").length();
            mPhoneNumber = mPhoneNumber.substring(ccSize, mPhoneNumber.length());
            if (mPhoneNumber.length() > 9)
                activity_login_phonenumbertxt.setText(mPhoneNumber);
        } catch (Exception e) {
            activity_login_phonenumbertxt.setText("");
        }
        activity_login_phonenumbertxt.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        // action bar menu
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayUseLogoEnabled(false);
            getSupportActionBar().setTitle(R.string.title_signup);
        }

        // these views are shown when we are waiting for the PIN code
        activity_login_progress = (ProgressBar) findViewById(R.id.activity_login_progress);
        activity_login_smswaittext = (TextView) findViewById(R.id.activity_login_smswaittext);
        activity_login_smstimer = (TextView) findViewById(R.id.activity_login_smstimer);

        activity_login_pintxt = (EditText) findViewById(R.id.activity_login_pintxt);
        activity_login_countrycodelyt = (LinearLayout) findViewById(R.id.activity_login_countrycodelyt);
        activity_login_pincodelyt = (LinearLayout) findViewById(R.id.activity_login_pincodelyt);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLocalBroadcastManager != null) {
            mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);
        }
    }

    private boolean isConnectedToInternet() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    synchronized private void createPinFetcher() {
        if (waitDialog == null || !waitDialog.isShowing()) {
            synchronized(lock) {
                // create the loading message
                waitDialog = ProgressDialog.show(
                        this, getText(R.string.msg_sendingdata),
                        getText(R.string.msg_pleasewait),
                        false, false);
                waitDialog.setIcon(R.drawable.ic_launcher);
                // create a thread to get sms pin
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Message m = new Message();
                        m.obj = context;
                        if (!isConnectedToInternet()) {
                            m.what = -1;
                            handler.sendMessage(m);
                        } else {
                            RESTClient client = new RESTClient();
                            client.requestPinSms(completePhone);
                            m.what = client.getStatus();
                            handler.sendMessage(m);
                        }
                    }
                }).start();
            }
        }
    }

    synchronized private void createTokenFetcher() {
        if (waitDialog == null || !waitDialog.isShowing()) {
            synchronized(lock) {
                // create the loading message
                waitDialog = ProgressDialog.show(
                        this, getText(R.string.msg_loadingdata),
                        getText(R.string.msg_waitingfortoken),
                        false, false);
                waitDialog.setIcon(R.drawable.ic_launcher);
                // create a thread to get sms pin
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Message m = new Message();
                        m.obj = context;
                        if (!isConnectedToInternet()) {
                            m.what = -1;
                            handler.sendMessage(m);
                        } else {
                            pinCode = activity_login_pintxt.getText().toString();
                            RESTClient client = new RESTClient();
                            accessToken = client.requestAccessToken(
                                    completePhone, pinCode,
                                    ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId()
                            );
                            m.what = client.getStatus();
                            handler.sendMessage(m);
                        }
                    }
                }).start();
            }
        }
    }

    private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IncomingSms.PIN_RECEIVED)) {
                String s = intent.getExtras().getString("message");
                if (s != null) {
                    String[] words = s.split(" ");
                    pinCode = words[words.length - 1];
                    // save and log PIN code
                    Editor prefsPrivateEditor = context.getSharedPreferences(
                            AppConstants.SHARED_PREFS,
                            Context.MODE_PRIVATE).edit();
                    prefsPrivateEditor.putString(AppConstants.PIN_CODE, pinCode);
                    prefsPrivateEditor.apply();
                    Log.i(AppConstants.TAG + ":Login",
                            "SenderNumber: " + intent.getExtras().getString("phoneNumber") +
                                    "; Received PIN: " + pinCode);
                    // now getting access token, this request is made automatically if sms is received
                    countDownTimer.cancel();
                    activity_login_pintxt.setText(pinCode);
                    createTokenFetcher();
                }
            }
        }
    };

    public void requestPin(View v) {
        activity_login_countrycode = (Spinner) findViewById(R.id.activity_login_countrycode);
        String cc = activity_login_countrycode.getSelectedItem().toString();
        cc = cc.replaceAll("[^\\d+]", "");
        completePhone = cc + activity_login_phonenumbertxt.getText().toString();
        completePhone = completePhone.replace(" ", "").replace("-", "").replace("+", "");
        if (activity_login_phonenumbertxt.getText().length() == 0 || !PhoneNumberUtils.isGlobalPhoneNumber(completePhone)) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.msg_pleasefillvalidphonenumber)).setCancelable(false).
                    setPositiveButton(getResources().getString(R.string.action_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {}
                            }
                    );
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            Log.i(AppConstants.TAG + ":Login", "Request PIN for " + completePhone);
            createPinFetcher();
        }
    }

    public void getToken(View v) {
        if (activity_login_pintxt.getText().length() != 4) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.msg_placeyourpintxt)).setCancelable(false).
                    setPositiveButton(getResources().getString(R.string.action_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {}
                            }
                    );
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            createTokenFetcher();
        }
    }

    public void goToFacebookPage(View v) {
        startActivity(new Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.facebook.com/pages/Agendalize/496570267115536")
        ));
    }

    public void goToTwitterPage(View v) {
        // TODO
    }

    public void goToWordPressPage(View v) {
        // TODO
    }

    public void makeTour(View v) {
        // TODO
    }

    private int getCountryZipCodeIndex() {
        try {
            TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String CountryID = manager.getSimCountryIso().toUpperCase(Locale.getDefault());
            String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
            for (String s : rl) {
                String[] g = s.split(",");
                if (g[1].trim().equals(CountryID.trim())) {
                    List<CharSequence> cl = getCountryCodeArray();
                    for (int j=0; j<cl.size(); j++) {
                        String[] c = cl.get(j).toString().split(" ");
                        if (g[0].trim().equals(c[1].replace("+", "").replace("(", "").replace(")", ""))) {
                            return j;
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            return 0;
        }
        return 0;
    }

    public List<CharSequence> getCountryCodeArray() {
        List<CharSequence> countryList = new ArrayList<>();
        countryList.add("Afghanistan (+93)");
        countryList.add("Albania (+355)");
        countryList.add("Algeria (+213)");
        countryList.add("American Samoa (+1)");
        countryList.add("Andorra (+376)");
        countryList.add("Angola (+244)");
        countryList.add("Anguilla (+1)");
        countryList.add("Antigua and Barbuda (+1)");
        countryList.add("Argentine (+54)");
        countryList.add("Armenia (+374)");
        countryList.add("Aruba (+297)");
        countryList.add("Ascension Island (+247)");
        countryList.add("Australia (+61)");
        countryList.add("Austria (+43)");
        countryList.add("Azerbaijan (+994)");
        countryList.add("Bahamas (+1)");
        countryList.add("Bahrain (+973)");
        countryList.add("Bangladesh (+880)");
        countryList.add("Barbados (+1)");
        countryList.add("Belarus (+375)");
        countryList.add("Belgium (+32)");
        countryList.add("Belize (+501)");
        countryList.add("Benin (+229)");
        countryList.add("Bermuda (+1)");
        countryList.add("Bhutan (+975)");
        countryList.add("Bolivia (+591)");
        countryList.add("Bosnia and Herzegovina (+387)");
        countryList.add("Botswana (+267)");
        countryList.add("Brazil (+55)");
        countryList.add("British Virgin Islands (+1)");
        countryList.add("Brunei Darussalam (+673)");
        countryList.add("Bulgaria (+359)");
        countryList.add("Burkina Faso (+226)");
        countryList.add("Burundi (+257)");
        countryList.add("Cambodia (+855)");
        countryList.add("Cameroon (+237)");
        countryList.add("Canada (+1)");
        countryList.add("Cape Verde (+238)");
        countryList.add("Cayman Islands (+1)");
        countryList.add("Central African Republic (+236)");
        countryList.add("Chad (+235)");
        countryList.add("Chile (+56)");
        countryList.add("China (+86)");
        countryList.add("Colombia (+57)");
        countryList.add("Comoros (+269)");
        countryList.add("Congo (+242)");
        countryList.add("Congo, Democratic Republic (+243)");
        countryList.add("Cook Islands (+682)");
        countryList.add("Costa Rica (+506)");
        countryList.add("Côte dIvoire (+225)");
        countryList.add("Croatia (+385)");
        countryList.add("Cuba (+53)");
        countryList.add("Cyprus (+357)");
        countryList.add("Czech Republic (+420)");
        countryList.add("Democratic People Republic of Korea (+850)");
        countryList.add("Denmark (+45)");
        countryList.add("Diego Garcia (+246)");
        countryList.add("Djibouti (+253)");
        countryList.add("Dominica (+1)");
        countryList.add("Dominican Republic (+1)");
        countryList.add("East Timor (+670)");
        countryList.add("Ecuador (+593)");
        countryList.add("Egypt (+20)");
        countryList.add("El Salvador (+503)");
        countryList.add("Equatorial Guinea (+240)");
        countryList.add("Eritrea (+291)");
        countryList.add("Estonia (+372)");
        countryList.add("Ethiopia (+251)");
        countryList.add("Falkland Islands (Malvinas) (+500)");
        countryList.add("Faroe Islands (+298)");
        countryList.add("Fiji (+679)");
        countryList.add("Finland (+358)");
        countryList.add("France (+33)");
        countryList.add("French Guiana (+594)");
        countryList.add("French Polynesia (+689)");
        countryList.add("Gabon (+241)");
        countryList.add("Gambia (+220)");
        countryList.add("Georgia (+995)");
        countryList.add("Germany (+49)");
        countryList.add("Ghana (+233)");
        countryList.add("Gibraltar (+350)");
        countryList.add("Greece (+30)");
        countryList.add("Greenland (+299)");
        countryList.add("Grenada (+1)");
        countryList.add("Guadeloupe (+590)");
        countryList.add("Guam (+1)");
        countryList.add("Guatemala (+502)");
        countryList.add("Guinea (+224)");
        countryList.add("Guinea-Bissau (+245)");
        countryList.add("Guyana (+592)");
        countryList.add("Haiti (+509)");
        countryList.add("Honduras (+504)");
        countryList.add("Hong Kong (+852)");
        countryList.add("Hungary (+36)");
        countryList.add("Iceland (+354)");
        countryList.add("India (+91)");
        countryList.add("Indonesia (+62)");
        countryList.add("Iran (+98)");
        countryList.add("Iraq (+964)");
        countryList.add("Ireland (+353)");
        countryList.add("Israel (+972)");
        countryList.add("Italy (+39)");
        countryList.add("Jamaica (+1)");
        countryList.add("Japan (+81)");
        countryList.add("Jordan (+962)");
        countryList.add("Kazakhstan (+7)");
        countryList.add("Kenya (+254)");
        countryList.add("Kiribati (+686)");
        countryList.add("Korea (Republic of) (+82)");
        countryList.add("Kuwait (+965)");
        countryList.add("Kyrgyzstan (+996)");
        countryList.add("Lao (+856)");
        countryList.add("Latvia (+371)");
        countryList.add("Lebanon (+961)");
        countryList.add("Lesotho (+266)");
        countryList.add("Liberia (+231)");
        countryList.add("Libyan (+218)");
        countryList.add("Liechtenstein (+423)");
        countryList.add("Lithuania (+370)");
        countryList.add("Luxembourg (+352)");
        countryList.add("Macao (+853)");
        countryList.add("Macedonia (+389)");
        countryList.add("Madagascar (+261)");
        countryList.add("Malawi (+265)");
        countryList.add("Malaysia (+60)");
        countryList.add("Maldives (+960)");
        countryList.add("Mali (+223)");
        countryList.add("Malta (+356)");
        countryList.add("Marshall Islands (+692)");
        countryList.add("Martinique (+596)");
        countryList.add("Mauritania (+222)");
        countryList.add("Mauritius (+230)");
        countryList.add("Mayotte (+269)");
        countryList.add("Mexico (+52)");
        countryList.add("Micronesia (+691)");
        countryList.add("Moldova (+373)");
        countryList.add("Monaco (+377)");
        countryList.add("Mongolia (+976)");
        countryList.add("Montenegro (+382)");
        countryList.add("Montserrat (+1)");
        countryList.add("Morocco (+212)");
        countryList.add("Mozambique (+258)");
        countryList.add("Myanmar (+95)");
        countryList.add("Namibia (+264)");
        countryList.add("Nauru (+674)");
        countryList.add("Nepal (+977)");
        countryList.add("Netherlands (+31)");
        countryList.add("Netherlands Antilles (+599)");
        countryList.add("New Caledonia (+687)");
        countryList.add("New Zealand (+64)");
        countryList.add("Nicaragua (+505)");
        countryList.add("Niger (+227)");
        countryList.add("Nigeria (+234)");
        countryList.add("Niue (+683)");
        countryList.add("Northern Mariana Islands (+1)");
        countryList.add("Norway (+47)");
        countryList.add("Oman (+968)");
        countryList.add("Pakistan (+92)");
        countryList.add("Palau (+680)");
        countryList.add("Panama (+507)");
        countryList.add("Papua New Guinea (+675)");
        countryList.add("Paraguay (+595)");
        countryList.add("Peru (+51)");
        countryList.add("Philippines (+63)");
        countryList.add("Poland (+48)");
        countryList.add("Portugal (+351)");
        countryList.add("Puerto Rico (+1)");
        countryList.add("Qatar (+974)");
        countryList.add("Reunion (+262)");
        countryList.add("Romania (+40)");
        countryList.add("Russian (+7)");
        countryList.add("Rwanda (Republic of) (+250)");
        countryList.add("Saint Helena (+290)");
        countryList.add("Saint Kitts and Nevis (+1)");
        countryList.add("Saint Lucia (+1)");
        countryList.add("Saint-Pierre e Miquelon (+508)");
        countryList.add("Saint Vincent (+1)");
        countryList.add("Samoa (+685)");
        countryList.add("San Marino (+378)");
        countryList.add("Sao Tome and Principe (+239)");
        countryList.add("Saudi Arabia (+966)");
        countryList.add("Senegal (+221)");
        countryList.add("Serbia (+381)");
        countryList.add("Seychelles (+248)");
        countryList.add("Sierra Leone (+232)");
        countryList.add("Singapore (+65)");
        countryList.add("Slovakia (+421)");
        countryList.add("Slovenia (+386)");
        countryList.add("Solomon Islands (+677)");
        countryList.add("Somalia (+252)");
        countryList.add("South Africa (+27)");
        countryList.add("Spain (+34)");
        countryList.add("Sri Lanka (+94)");
        countryList.add("Sudan (+249)");
        countryList.add("Suriname (+597)");
        countryList.add("Swaziland (+268)");
        countryList.add("Sweden (+46)");
        countryList.add("Switzerland (+41)");
        countryList.add("Syrian (+963)");
        countryList.add("Taiwan (+886)");
        countryList.add("Tajikistan (+992)");
        countryList.add("Tanzania (+255)");
        countryList.add("Thailand (+66)");
        countryList.add("Togo (+228)");
        countryList.add("Tokelau (+690)");
        countryList.add("Tonga (+676)");
        countryList.add("Trinidad and Tobago (+1)");
        countryList.add("Tunisia (+216)");
        countryList.add("Turkey (+90)");
        countryList.add("Turkmenistan (+993)");
        countryList.add("Turks and Caicos Islands (+1)");
        countryList.add("Tuvalu (+688)");
        countryList.add("Uganda (+256)");
        countryList.add("Ukraine (+380)");
        countryList.add("United Arab Emirates (+971)");
        countryList.add("United Kingdom (+44)");
        countryList.add("United States Virgin Islands (+1)");
        countryList.add("Uruguay (+598)");
        countryList.add("USA (+1)");
        countryList.add("Uzbekistan (+998)");
        countryList.add("Vanuatu (+678)");
        countryList.add("Vaticano (+379)");
        countryList.add("Venezuela (+58)");
        countryList.add("Vietnam (+84)");
        countryList.add("Wallis e Futuna (+681)");
        countryList.add("Yemen (+967)");
        countryList.add("Zambia (+260)");
        countryList.add("Zimbabwe (+263)");
        return countryList;
    }

}
