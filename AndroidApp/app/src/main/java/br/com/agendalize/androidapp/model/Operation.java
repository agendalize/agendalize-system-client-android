package br.com.agendalize.androidapp.model;

import org.json.JSONObject;

public class Operation {

    private String descriptionWeek;
    private String descriptionHourOpen;
    private String descriptionHourClose;
    private boolean flagOpen;
    
    public Operation() {
    	descriptionWeek = "";
    	descriptionHourOpen = "";
    	descriptionHourClose = "";
    	flagOpen = false;
    }
    
    public Operation(String descriptionWeek,
                     String descriptionHourOpen, String descriptionHourClose,
                     boolean flagOpen) {
		this();
		this.descriptionWeek = descriptionWeek;
		this.descriptionHourOpen = descriptionHourOpen;
		this.descriptionHourClose = descriptionHourClose;
		this.flagOpen = flagOpen;
	}

    public Operation(JSONObject obj) {
    	try {
    		descriptionWeek = obj.getString("descriptionWeek");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		descriptionHourOpen = obj.getString("descriptionHourOpen");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		descriptionHourClose = obj.getString("descriptionHourClose");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    	try {
    		flagOpen = obj.getBoolean("flagOpen");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
    
	public String getDescriptionWeek() {
		return descriptionWeek;
	}

	public void setDescriptionWeek(String descriptionWeek) {
		this.descriptionWeek = descriptionWeek;
	}

	public String getDescriptionHourOpen() {
		return descriptionHourOpen;
	}

	public void setDescriptionHourOpen(String descriptionHourOpen) {
		this.descriptionHourOpen = descriptionHourOpen;
	}

	public String getDescriptionHourClose() {
		return descriptionHourClose;
	}

	public void setDescriptionHourClose(String descriptionHourClose) {
		this.descriptionHourClose = descriptionHourClose;
	}

	public boolean isFlagOpen() {
		return flagOpen;
	}

	public void setFlagOpen(boolean flagOpen) {
		this.flagOpen = flagOpen;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("descriptionWeek", descriptionWeek);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("descriptionHourOpen", descriptionHourOpen);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("descriptionHourClose", descriptionHourClose);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			obj.put("flagOpen", flagOpen);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

}
