package br.com.agendalize.androidapp.model;

import org.json.JSONObject;

/**
 * This class is used to represent a booking slot when the booking operation is
 * being performed.
 * Created by mthama on 21/11/15.
 */
public class Booking {

    private long serviceId;
    private long employeeId;
    private String employeePhoto;
    private String bookingHour;
    private String codDay;
    private String codHour;

    public Booking() {
        serviceId = 0;
        employeeId = 0;
        employeePhoto = "";
        bookingHour = "";
        codDay = "";
        codHour = "";
    }

    public Booking(long serviceId, long employeeId, String employeePhoto, String bookingHour, String codDay, String codHour) {
        this();
        this.serviceId = serviceId;
        this.employeeId = employeeId;
        this.employeePhoto = employeePhoto;
        this.bookingHour = bookingHour;
        this.codDay = codDay;
        this.codHour = codHour;
    }

    public Booking(JSONObject obj) {
        try {
            serviceId = obj.getLong("serviceId");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            employeeId = obj.getLong("employeeId");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            codDay = obj.getString("employeePhoto");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            codDay = obj.getString("bookingHour");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            codDay = obj.getString("codDay");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            codHour = obj.getString("codHour");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeePhoto() {
        return employeePhoto;
    }

    public void getEmployeePhoto(String employeePhoto) {
        this.employeePhoto = employeePhoto;
    }

    public String getBookingHour() {
        return bookingHour;
    }

    public void setBookingHour(String bookingHour) {
        this.bookingHour = bookingHour;
    }

    public String getCodDay() {
        return codDay;
    }

    public void setCodDay(String codDay) {
        this.codDay = codDay;
    }

    public String getCodHour() {
        return codHour;
    }

    public void setCodHour(String codHour) {
        this.codHour = codHour;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("serviceId", String.valueOf(serviceId));
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("employeeId", String.valueOf(employeeId));
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("codDay", codDay);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("codHour", codHour);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return obj;
    }

}
