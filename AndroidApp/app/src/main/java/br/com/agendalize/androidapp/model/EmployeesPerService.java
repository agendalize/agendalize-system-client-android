package br.com.agendalize.androidapp.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class EmployeesPerService {
	
	private long cod;
	private String shortName;
	private List<Employee> employees;
	
	public EmployeesPerService() {
		cod = 0;
		shortName = "shortName";
		employees = new ArrayList<Employee>();
	}

	public EmployeesPerService(long cod, String shortName, List<Employee> employees) {
		this();
		this.cod = cod;
		this.shortName = shortName;
		this.employees = employees;
	}

	public EmployeesPerService(JSONObject obj) {
		this();
		try {
			cod = obj.getLong("cod");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			shortName = obj.getString("shortName");
		} catch (Exception e) {
            //e.printStackTrace();
		}
		try {
			JSONArray arr = obj.getJSONArray("employees");
			for (int i=0; i<arr.length(); i++) {
				employees.add(new Employee(arr.getJSONObject(i)));
			}
		} catch (Exception e) {
            //e.printStackTrace();
		}
	}
	
	public long getCod() {
		return cod;
	}

	public void setCod(long cod) {
		this.cod = cod;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<employees.size(); i++) {
				arr.put(employees.get(i).toJson());
			}
			obj.put("employees", arr);
		} catch (Exception e) {
            //e.printStackTrace();
		}
		return obj;
	}
	
}
