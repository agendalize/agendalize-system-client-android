package br.com.agendalize.androidapp.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.ui.HomeActivity;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.BookingGroup;
import br.com.agendalize.androidapp.net.RESTClient;
import br.com.agendalize.androidapp.util.DeviceUtils;
import br.com.agendalize.androidapp.util.ImageUtils;

/**
 * Created by mthama on 25/10/15.
 */
public class MyScheduleFragment extends Fragment {

    //===================================================================================
    // Fields
    //===================================================================================

    // used to manage lazy load pages
    private static Object lock = new Object();
    private static boolean isLoading = false;
    private static int lastLoadCount = -1;
    private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;
    private static Thread bookingCanceler = null;
    // views
    private static MyScheduleAdapter myScheduleAdapter;
    private static ListView frag_myschedule_listview;
    // views controls
    private static HomeActivity context;
    private static long bookingGroupId;
    private static String cancelMessage;
    private static List<BookingGroup> bookings;

    @SuppressLint("HandlerLeak") // TODO: verify this
    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (waitDialog != null) {
                waitDialog.dismiss();
                waitDialog = null;
            }
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                    DeviceUtils.justShowDialogWarning(context, R.string.msg_timeout, bookings.size() == 0);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                    DeviceUtils.justShowDialogWarning(context, R.string.msg_noconnection, bookings.size() == 0);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                    DeviceUtils.justShowDialogWarning(context, R.string.msg_servererror, bookings.size() == 0);
                    break;
                case HttpStatus.SC_OK:
                    if (bookings != null) {
                        if (bookings.size() == 0) {
                            DeviceUtils.justShowDialogWarning(context, R.string.msg_noresults, false);
                        } else {
                            frag_myschedule_listview.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(AbsListView view, int scrollState) {}
                                @Override
                                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    if (
                                            frag_myschedule_listview.getLastVisiblePosition()+1 == totalItemCount && // user has reached the end of the list
                                                    frag_myschedule_listview.getAdapter().getCount() >= AppConstants.SIZE_LIST && // only enable pagination if we already done at least 1 load
                                                    lastLoadCount == AppConstants.SIZE_LIST && // last load had fetch a complete page
                                                    lastLoadCount != -1
                                            ) createNewLoader();
                                }
                            });
                            frag_myschedule_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(final AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                                    if (((MyScheduleAdapter)arg0.getAdapter()).getItem(arg2).getCodStatus().equals("B") ||
                                        ((MyScheduleAdapter)arg0.getAdapter()).getItem(arg2).getCodStatus().equals("P")) {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setMessage(context.getResources().getString(R.string.msg_wantcancelingbooking)).setCancelable(true).
                                                setPositiveButton(context.getResources().getString(R.string.action_yes),
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(final DialogInterface dialog, final int id) {
                                                                LayoutInflater li = LayoutInflater.from(context);
                                                                View promptsView = li.inflate(R.layout.helper_cancelbooking, null);
                                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                                                alertDialogBuilder.setView(promptsView);
                                                                final EditText userInput = (EditText) promptsView.findViewById(R.id.helper_cancelbooking_message);
                                                                alertDialogBuilder.setCancelable(false)
                                                                        .setPositiveButton(context.getResources().getString(R.string.action_ok),
                                                                                new DialogInterface.OnClickListener() {
                                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                                        MyScheduleAdapter a = (MyScheduleAdapter) arg0.getAdapter();
                                                                                        bookingGroupId = Long.valueOf(a.getItem(arg2).getIdGroupBooking());
                                                                                        cancelMessage = userInput.getText().toString();
                                                                                        createNewCanceler();
                                                                                    }
                                                                                }).setNegativeButton(context.getResources().getString(R.string.action_cancel),
                                                                        new DialogInterface.OnClickListener() {
                                                                            public void onClick(DialogInterface dialog, int id) {
                                                                                dialog.cancel();
                                                                            }
                                                                        });
                                                                AlertDialog alertDialog = alertDialogBuilder.create();
                                                                alertDialog.show();
                                                            }
                                                        }
                                                ).setNegativeButton(context.getResources().getString(R.string.action_no),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(final DialogInterface dialog, final int id) {
                                                    }
                                                }
                                        );
                                        final AlertDialog alert = builder.create();
                                        alert.show();
                                    }
                                }
                            });
                            myScheduleAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
            }
            synchronized(lock) {
                isLoading = false;
            }
        }
    };

    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient(context);
                List<BookingGroup> newBookings = client.BookingHistory(bookings.size()+1, AppConstants.SIZE_LIST);
                // before add, remove possible "loading more" tags
                if (bookings.size() > 0) {
                    if (bookings.get(bookings.size() - 1).getNameStore().equals("LOAD_MORE")) {
                        bookings.remove(bookings.size() - 1);
                    }
                }
                // add new stores
                for (int i=0; i<newBookings.size(); i++) {
                    bookings.add(newBookings.get(i));
                }
                lastLoadCount = newBookings.size();
                // add the informative item, if there is more contacts to load (last fetch returned SIZE_LIST)
                if (bookings.size() > 0 && lastLoadCount == AppConstants.SIZE_LIST) {
                    BookingGroup m = new BookingGroup();
                    m.setNameStore("LOAD_MORE");
                    bookings.add(m);
                }
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };

    private static final Runnable cancelBooking = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                RESTClient client = new RESTClient(context);
                client.BookingCancel(bookingGroupId, cancelMessage);
                if (waitDialog != null) {
                    waitDialog.dismiss();
                    waitDialog = null;
                }
                synchronized (lock) {
                    isLoading = false;
                }
                if (client.getStatus() == HttpStatus.SC_OK) {
                    new Handler(context.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, R.string.msg_bookingcanceled, Toast.LENGTH_LONG).show();
                            }
                        }
                    );
                } else {
                    new Handler(context.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, R.string.msg_bookingnotcanceled, Toast.LENGTH_LONG).show();
                            }
                        }
                    );
                }
            }
        }
    };

    //===================================================================================
    // Override methods
    //===================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_myschedule, container, false);
        context = (HomeActivity) getActivity();

        // widgets and components
        frag_myschedule_listview = (ListView) rootView.findViewById(R.id.frag_myschedule_listview);
        bookings = new ArrayList<BookingGroup>();

        myScheduleAdapter = new MyScheduleAdapter();
        frag_myschedule_listview.setAdapter(myScheduleAdapter);
        if (bookings.size() == 0) {
            createNewLoader();
        }

        return rootView;
    }

    //===================================================================================
    // General methods
    //===================================================================================

    synchronized private static void createNewLoader() {
        if (!isLoading) {
            synchronized(lock) {
                isLoading = true;
            }
            if (bookings.size() == 0) { // we show this only if its the first loading
                waitDialog = ProgressDialog.show(
                        context,
                        context.getText(R.string.msg_loadingdata),
                        context.getText(R.string.msg_pleasewait),
                        false, false);
                waitDialog.setIcon(R.drawable.ic_launcher);
            }
            contentLoader = new Thread(loadContent);
            contentLoader.start();
        }
    }

    synchronized private static void createNewCanceler() {
        if (!isLoading) {
            synchronized(lock) {
                isLoading = true;
            }
            waitDialog = ProgressDialog.show(
                    context,
                    context.getText(R.string.msg_loadingdata),
                    context.getText(R.string.msg_cancelingbooking),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
            bookingCanceler = new Thread(cancelBooking);
            bookingCanceler.start();
        }
    }

    //===================================================================================
    // Inner classes
    //===================================================================================

    private static class MyScheduleAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return bookings.size();
        }

        @Override
        public BookingGroup getItem(int position) {
            return bookings.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (bookings.get(position).getNameStore().equals("LOAD_MORE")) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_helperloading, parent, false);
                return convertView;
            }
            // predefines the view as an inflated layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_bookinggroup, parent, false);

            int colorId = R.color.schedule_disabled;
            int stt = R.string.label_disabled;
            if (bookings.get(position).getCodStatus().equals("A")) {
                colorId = R.color.schedule_available;
                stt = R.string.label_available;
            } else if (bookings.get(position).getCodStatus().equals("B")) {
                colorId = R.color.schedule_booking;
                stt = R.string.label_booking;
            } else if (bookings.get(position).getCodStatus().equals("P")) {
                colorId = R.color.schedule_pending;
                stt = R.string.label_pending;
            } else if (bookings.get(position).getCodStatus().equals("D")) {
                colorId = R.color.schedule_disabled;
                stt = R.string.label_disabled;
            } else if (bookings.get(position).getCodStatus().equals("C")) {
                colorId = R.color.schedule_canceled;
                stt = R.string.label_canceled;
            } else if (bookings.get(position).getCodStatus().equals("R")) {
                colorId = R.color.schedule_rejected;
                stt = R.string.label_rejected;
            } else if (bookings.get(position).getCodStatus().equals("F")) {
                colorId = R.color.schedule_finished;
                stt = R.string.label_finished;
            }

            convertView.findViewById(R.id.row_bookinggroup_mainlyt).setBackgroundColor(
                    context.getResources().getColor(colorId)
            );
            LinearLayout row_bookinggroup_lyt = ((LinearLayout)convertView.findViewById(R.id.row_bookinggroup_lyt));
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_statustxt)).setText(stt);
            convertView.findViewById(R.id.row_bookinggroup_statustxt).setBackgroundColor(
                    context.getResources().getColor(colorId)
            );
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_title)).setText(
                    bookings.get(position).getWeekBook()
            );
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_title)).setTextColor(
                    context.getResources().getColor(colorId)
            );
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_storetxt)).setText(
                    bookings.get(position).getNameStore()
            );
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_dayinview)).setText(
                    bookings.get(position).getDateBook().split("/")[0]
            );
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_dayinview)).setTextColor(
                    context.getResources().getColor(colorId)
            );
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_monthinview)).setText(
                    bookings.get(position).getDateBook().split("/")[1]
            );
            ((TextView) convertView.findViewById(R.id.row_bookinggroup_monthinview)).setTextColor(
                    context.getResources().getColor(colorId)
            );

            // fill employees
            for (int i=0; i<bookings.get(position).getEmployees().size(); i++) {
                LinearLayout row = (LinearLayout) context.getLayoutInflater().inflate(R.layout.row_bookingitem, row_bookinggroup_lyt, false);
                ImageView row_bookingitem_photo = (ImageView)row.findViewById(R.id.row_bookingitem_photo);
                ProgressBar row_bookingitem_photoholder = (ProgressBar)row.findViewById(R.id.row_bookingitem_photoholder);
                TextView row_bookingitem_employeetxt = (TextView)row.findViewById(R.id.row_bookingitem_employeetxt);
                LinearLayout row_bookingitem_lyt = (LinearLayout)row.findViewById(R.id.row_bookingitem_lyt);
                new PhotoLoader(
                        row_bookingitem_photo,
                        row_bookingitem_photoholder,
                        new Integer(bookings.get(position).getEmployees().get(i).getIdEmployee()),
                        bookings.get(position).getEmployees().get(i).getUrlPhotoEmployee()).execute();
                row_bookingitem_employeetxt.setText(bookings.get(position).getEmployees().get(i).getNameEmployee());
                row_bookinggroup_lyt.addView(row);

                for (int j=0; j<bookings.get(position).getEmployees().get(i).getServices().size(); j++) {
                    LinearLayout helper = (LinearLayout) context.getLayoutInflater().inflate(R.layout.helper_bookingitem, row_bookinggroup_lyt, false);
                    TextView helper_bookingitem_hour = (TextView)helper.findViewById(R.id.helper_bookingitem_hour);
                    TextView helper_bookingitem_service = (TextView)helper.findViewById(R.id.helper_bookingitem_service);
                    helper_bookingitem_hour.setText(
                            bookings.get(position).getEmployees().get(i).getServices().get(j).getHourBegin()
                    );
                    helper_bookingitem_service.setText(
                            bookings.get(position).getEmployees().get(i).getServices().get(j).getDescription()
                    );
                    row_bookingitem_lyt.addView(helper);
                }
            }

            return convertView;
        }
    }

    private static class PhotoLoader extends AsyncTask<Void, Void, Void> {

        private Drawable drawable;
        private ProgressBar progress;
        private ImageView photo;
        private long id;
        private String url;

        public PhotoLoader(ImageView photo, ProgressBar progress, long id, String url) {
            this.photo = photo;
            this.progress = progress;
            this.id = id;
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                drawable = ImageUtils.loadImageFromWebOperations(url);
                Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
                drawable = new BitmapDrawable(context.getResources(), bitmap);
            } catch (Exception e) {
                drawable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progress.setVisibility(View.GONE);
            if (drawable == null) {
                drawable = context.getResources().getDrawable(R.drawable.noimage);
            }
            photo.setImageDrawable(drawable);
            context.setPhoto(id, drawable);
        }

    }

}
