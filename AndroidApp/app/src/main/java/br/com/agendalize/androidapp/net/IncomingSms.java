package br.com.agendalize.androidapp.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import br.com.agendalize.androidapp.AppConstants;

public class IncomingSms extends BroadcastReceiver {

	public static final String PIN_RECEIVED = "br.com.agendalize.androidapp.action.pinreceived";
	final SmsManager sms = SmsManager.getDefault();

	public void onReceive(Context context, Intent intent) {
		final Bundle bundle = intent.getExtras();
		try {
			if (bundle != null) {
				final Object[] pdusObj = (Object[]) bundle.get("pdus");
				for (int i = 0; i < pdusObj.length; i++) {
					SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
					String phoneNumber = currentMessage.getDisplayOriginatingAddress();
					String message = currentMessage.getDisplayMessageBody();
					Intent broadcast = new Intent(PIN_RECEIVED);
					broadcast.putExtra("phoneNumber", phoneNumber);
					broadcast.putExtra("message", message);
					LocalBroadcastManager.getInstance(context).sendBroadcast(broadcast);
				}
			}
		} catch (Exception e) {
			Log.e(AppConstants.TAG, "Exception smsReceiver " + e);
		}
	}
	
}
