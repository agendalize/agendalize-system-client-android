package br.com.agendalize.androidapp.ui.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpStatus;

import br.com.agendalize.androidapp.ui.HomeActivity;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.net.RESTClient;
import br.com.agendalize.androidapp.util.DeviceUtils;

public class FeedbackFragment extends Fragment {
	
	// used to manage lazy load pages
	private static Object lock = new Object();
	private static boolean isLoading = false;
	private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;
	    
    private static String message = null;
    private static HomeActivity activity = null;
    
    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	if (waitDialog != null) {
            	waitDialog.dismiss();
            	waitDialog = null;
            }
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                	DeviceUtils.justShowDialogWarning(activity, R.string.msg_timeout, false);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                	DeviceUtils.justShowDialogWarning(activity, R.string.msg_noconnection, false);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                	DeviceUtils.justShowDialogWarning(activity, R.string.msg_servererror, false);
                    break;
                case HttpStatus.SC_OK:
                	new AlertDialog.Builder(activity).setMessage(activity.getResources()
        				.getString(R.string.label_thankyouforfeedbacking)).setCancelable(false)
            			.setPositiveButton(activity.getResources().getString(R.string.action_ok),
            			new DialogInterface.OnClickListener() {
            				public void onClick(final DialogInterface dialog, final int id) {
            					activity.goToMainMenu();
            				}
            			}
            		).create().show();
                    break;
            }
            synchronized(lock) {
            	isLoading = false;
            }
        }
    };
	
	private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(activity)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
        		RESTClient client = new RESTClient(activity);
        		client.FeedbacksEvaluateUs(message);
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };
	
    //===================================================================================
    // Override methods
  	//===================================================================================
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_feedback, container, false);
        activity = (HomeActivity)getActivity();
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		((Button) activity.findViewById(R.id.frag_feedback_feedbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				message = ((EditText) activity.findViewById(R.id.frag_feedback_message)).getText().toString();
				if (message.length() > 0) {
					createNewLoader();
				} else {
					DeviceUtils.justShowDialogWarning(getActivity(), R.string.label_writehereyourfeedback, false);
				}
			}
		});
	}
	
	//===================================================================================
	// General methods
	//===================================================================================
	
	synchronized private static void createNewLoader() {
    	if (!isLoading) {
        	synchronized(lock) {
            	isLoading = true;
            }
        	waitDialog = ProgressDialog.show(
                    activity,
                    activity.getText(R.string.msg_sendingdata),
                    activity.getText(R.string.msg_pleasewait),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
            contentLoader = new Thread(loadContent);
            contentLoader.start();
    	}
    }

}
