package br.com.agendalize.androidapp.ui.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Employee;
import br.com.agendalize.androidapp.model.EmployeesPerService;
import br.com.agendalize.androidapp.util.DeviceUtils;
import br.com.agendalize.androidapp.util.ImageUtils;
import br.com.agendalize.androidapp.ui.BookingActivity;

public class BookingServiceToEmployeeFragment extends Fragment {
	
	//===================================================================================
	// Fields
	//===================================================================================

	private BookingActivity activity;
	private BookingServiceToEmployeeAdapter adapter;
	private ExpandableListView frag_bookingservicetoemployee_listview;
	
	private Button frag_bookingservicetoemployee_checkstorebtn;
	private Button frag_bookingservicetoemployee_nextbtn;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingservicetoemployee, container, false);
        activity = (BookingActivity)getActivity();
        
        frag_bookingservicetoemployee_listview = (ExpandableListView) rootView.findViewById(R.id.frag_bookingservicetoemployee_listview);
        frag_bookingservicetoemployee_checkstorebtn = (Button) rootView.findViewById(R.id.frag_bookingservicetoemployee_checkstorebtn);
        frag_bookingservicetoemployee_nextbtn = (Button) rootView.findViewById(R.id.frag_bookingservicetoemployee_nextbtn);

        setupFields();
        
        return rootView;
	}

    //===================================================================================
	// General methods
	//===================================================================================
	
	public void notifyDataListChange() {
		// at this point, we already know which services/employees were selected, so we can initiate this array
        activity.getSelectedServicesToEmployees().clear();
        for (int i=0; i<activity.getEmployeesPerService().size(); i++) {
        	activity.getSelectedServicesToEmployees().put(activity.getEmployeesPerService().get(i).getCod(), null);
		}
        adapter.notifyDataSetChanged();
	}
	
	private void setupFields() {
		adapter = new BookingServiceToEmployeeAdapter(activity.getEmployeesPerService());
		frag_bookingservicetoemployee_listview.setAdapter(adapter);
		frag_bookingservicetoemployee_listview.setGroupIndicator(null);
		frag_bookingservicetoemployee_listview.setOnChildClickListener(new OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                final Employee selected = (Employee) adapter.getChild(groupPosition, childPosition);
                activity.getSelectedServicesToEmployees().put(
                        activity.getEmployeesPerService().get(groupPosition).getCod(),
                        selected);
                frag_bookingservicetoemployee_listview.collapseGroup(groupPosition);
                frag_bookingservicetoemployee_listview.requestLayout();
                adapter.notifyDataSetChanged();
                return true;
            }
        });
	    frag_bookingservicetoemployee_checkstorebtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getPagerView().setCurrentItem(0);
			}
		});
	    frag_bookingservicetoemployee_nextbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isAllFieldsOk()) {
					activity.requestEmployeeSchedules();
				} else {
					DeviceUtils.justShowDialogWarning(activity, R.string.msg_selectaemployee, false);
				}
			}
        });
	}

	private boolean isAllFieldsOk() {
		for (int i=0; i<activity.getSelectedServicesToEmployees().size(); i++) {
		   long key = activity.getSelectedServicesToEmployees().keyAt(i);
		   if (activity.getSelectedServicesToEmployees().get(key) != null)
			   return true;
		}
		return false;
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class BookingServiceToEmployeeAdapter extends BaseExpandableListAdapter {

		private List<EmployeesPerService> service2employees;
	    
	    public BookingServiceToEmployeeAdapter(List<EmployeesPerService> service2employees) {
	        this.service2employees = service2employees;
	    }
	    
	    @Override
	    public Object getChild(int groupPosition, int childPosition) {
	        return service2employees.get(groupPosition).getEmployees().get(childPosition);
	    }
	 
	    @Override
	    public long getChildId(int groupPosition, int childPosition) {
	        return childPosition;
	    }

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
	        LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.row_employeeitem, parent, false);
	        ImageView row_employeeitem_photo = (ImageView)convertView.findViewById(R.id.row_employeeitem_photo);
			ProgressBar row_employeeitem_photoholder = (ProgressBar)convertView.findViewById(R.id.row_employeeitem_photoholder);
		    TextView row_employeeitem_name = (TextView)convertView.findViewById(R.id.row_employeeitem_name);
		    TextView row_employeeitem_phone = (TextView)convertView.findViewById(R.id.row_employeeitem_phone);
		    row_employeeitem_name.setText(
		    		service2employees.get(groupPosition).getEmployees().get(childPosition).getName()
			);
		    row_employeeitem_phone.setText(
                    service2employees.get(groupPosition).getEmployees().get(childPosition).getLastName()
            );
            new PhotoLoader(
                    row_employeeitem_photo,
                    row_employeeitem_photoholder,
                    service2employees.get(groupPosition).getEmployees().get(childPosition).getImageUrl()).execute();
	        return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return service2employees.get(groupPosition).getEmployees().size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return service2employees.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return service2employees.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            LayoutInflater infalInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_servicelabel, parent, false);
	        TextView row_servicelabel_name = ((TextView) convertView.findViewById(R.id.row_servicelabel_name));
	        ProgressBar row_servicelabel_progress = ((ProgressBar) convertView.findViewById(R.id.row_servicelabel_progress));
	        row_servicelabel_name.setText(
	        		service2employees.get(groupPosition).getShortName() + " (" +
    				(activity.getSelectedServicesToEmployees().get(service2employees.get(groupPosition).getCod()) == null ?
						activity.getResources().getString(R.string.label_employeepending) :
                        activity.getSelectedServicesToEmployees().get(service2employees.get(groupPosition).getCod()).getName()) + ")"
    		);
	        row_servicelabel_name.setCompoundDrawablesWithIntrinsicBounds(
	        		0, 0, isExpanded ? R.drawable.ic_collapse_g : R.drawable.ic_expand_g, 0);
	        row_servicelabel_progress.setVisibility(View.GONE);
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
	}

    //===================================================================================
    // Inner classes
    //===================================================================================

    private class PhotoLoader extends AsyncTask<Void, Void, Void> {

        private Drawable drawable;
        private ImageView photo;
        private ProgressBar progress;
        private String url;

        public PhotoLoader(ImageView photo, ProgressBar progress, String url) {
            this.photo = photo;
            this.progress = progress;
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                drawable = ImageUtils.loadImageFromWebOperations(url);
                Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), true);
                drawable = new BitmapDrawable(getResources(), bitmap);
            } catch (Exception e) {
                drawable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                progress.setVisibility(View.GONE);
                if (drawable == null) {
                    drawable = getResources().getDrawable(R.drawable.noimage);
                }
                photo.setImageDrawable(drawable);
            } catch (IllegalStateException e) {
                // thrown if activity is no more attached
                Log.w(AppConstants.TAG, "AlbumLoaderTask: " + e.getLocalizedMessage());
            }
        }

    }

}
