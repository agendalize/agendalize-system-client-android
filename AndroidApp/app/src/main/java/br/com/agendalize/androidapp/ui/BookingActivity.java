package br.com.agendalize.androidapp.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.LongSparseArray;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Booking;
import br.com.agendalize.androidapp.model.Employee;
import br.com.agendalize.androidapp.model.EmployeeSchedule;
import br.com.agendalize.androidapp.model.EmployeesPerService;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.model.Store;
import br.com.agendalize.androidapp.model.StoreSearchRequest;
import br.com.agendalize.androidapp.net.RESTClient;
import br.com.agendalize.androidapp.ui.fragment.BookingFinishFragment;
import br.com.agendalize.androidapp.ui.fragment.BookingPreviewFragment;
import br.com.agendalize.androidapp.ui.fragment.BookingResumeFragment;
import br.com.agendalize.androidapp.ui.fragment.BookingScheduleToEmployeeFragment;
import br.com.agendalize.androidapp.ui.fragment.BookingServiceToEmployeeFragment;

/**
 * This class should know only which store it is working with. Services and employees are
 * loaded inside its fragments, and its fields are only loaded form store model.
 * 
 * @author mthama
 */
public class BookingActivity extends AppCompatActivity {

	//===================================================================================
	// Fields
	//===================================================================================

    private final Object lock = new Object();
    private boolean isLoading = false;

	private Menu menu;
    private Toolbar activity_booking_toolbar;
	private BookingPreviewFragment bookingPreviewFrag;
	private BookingServiceToEmployeeFragment bookingServiceToEmployeeFrag;
	private BookingScheduleToEmployeeFragment bookingScheduleToEmployeeFrag;
	private BookingResumeFragment bookingScheduleResumeFrag;
	private BookingFinishFragment bookingFinishFrag;
	
	private Drawable photo;
	private Store store;
	private StoreSearchRequest requestConfigs;
	private LongSparseArray<Employee> selectedServicesToEmployees;
	private List<Drawable> albumDrawable;
    private List<EmployeeSchedule> schedules;
    private List<EmployeesPerService> employeesPerService;
    private List<Booking> bookings;
	
	private final int NUM_PAGES = 5;
	private ViewPager activity_booking_pager;
	private ScreenSlidePagerAdapter pagerAdapter;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        
        try { // populating the store list
			store = new Store(new JSONObject(getIntent().getStringExtra("storeJsonString")));
			requestConfigs = new StoreSearchRequest(new JSONObject(getIntent().getStringExtra("requestJsonString")));
		} catch (Exception e) {
			// this view is only displayed if we have a valid store model
			Toast.makeText(this, R.string.msg_notpossibletoload, Toast.LENGTH_LONG).show();
			finish();
		}

        bookingPreviewFrag = new BookingPreviewFragment();
        bookingServiceToEmployeeFrag = new BookingServiceToEmployeeFragment();
        bookingScheduleToEmployeeFrag = new BookingScheduleToEmployeeFragment();
        bookingScheduleResumeFrag = new BookingResumeFragment();
        bookingFinishFrag = new BookingFinishFragment();

        albumDrawable = new ArrayList<>();
        employeesPerService = new ArrayList<>();
        bookings = new ArrayList<>();
        activity_booking_toolbar = (Toolbar) findViewById(R.id.activity_booking_toolbar);
        activity_booking_pager = (ViewPager) findViewById(R.id.activity_booking_pager);
        selectedServicesToEmployees = new LongSparseArray<Employee>();

        setSupportActionBar(activity_booking_toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_recommend_w);
        getSupportActionBar().setTitle(store.getName());

        // Instantiate a ViewPager and a PagerAdapter.
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        activity_booking_pager.setAdapter(pagerAdapter);
        activity_booking_pager.setCurrentItem(0);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.onlyback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_back:
                Intent intent = new Intent(BookingActivity.this, HomeActivity.class);
                intent.putExtra(AppConstants.FragmentIndexToStartAt, 0);
                startActivity(intent);
                finish();
                break;
            default:;
        }
        return super.onOptionsItemSelected(item);
    }
	
	@Override
    public void onBackPressed() {
        if (activity_booking_pager.getCurrentItem() == 0) {
            Intent intent = new Intent(BookingActivity.this, HomeActivity.class);
            intent.putExtra(AppConstants.FragmentIndexToStartAt, 0);
            startActivity(intent);
            finish();
        } else {
        	activity_booking_pager.setCurrentItem(activity_booking_pager.getCurrentItem() - 1);
        }
    }
	
	//===================================================================================
	// General methods
	//===================================================================================

    public void requestBookingTask() {
        new RequestBookingTask(bookings).execute();
    }

    public void requestEmployeeSchedules() {
        // TODO: correctly fill fields
        /*new EmployeeScheduleLoader(
                37, "15/10/2015", "17/10/2015",
                new Long[] {new Long(71), new Long(892), new Long(930)}
        ).execute();*/
        List<Long> employeeIds = new ArrayList<>();
        for(int i = 0; i < selectedServicesToEmployees.size(); i++) {
            Long key = selectedServicesToEmployees.keyAt(i);
            if (selectedServicesToEmployees.get(key) != null) {
                employeeIds.add(selectedServicesToEmployees.get(key).getId());
            }
        }
        new EmployeeScheduleLoader(
                store.getId(), requestConfigs.getBaseDates().get(0), requestConfigs.getBaseDates().get(0),
                employeeIds.toArray(new Long[employeeIds.size()])
        ).execute();
    }

    public void requestBookingResume() {
        activity_booking_pager.setCurrentItem(3, true);
        bookingScheduleResumeFrag.setupFields();
    }

	public ViewPager getPagerView() {
        return activity_booking_pager;
    }

	public void notifyServiceToEmployeeDataListChange() {
		bookingServiceToEmployeeFrag.notifyDataListChange();
	}
    
    public Store getStore() {
    	return store;
    }
    
    public LongSparseArray<Employee> getSelectedServicesToEmployees() {
    	return selectedServicesToEmployees;
    }

    public List<EmployeesPerService> getEmployeesPerService() {
        employeesPerService.clear();
        for (Service service : store.getListServices()) {
            EmployeesPerService eps = new EmployeesPerService();
            eps.setCod(service.getCod());
            eps.setShortName(service.getShortName());
            for (Employee employee : store.getEmployees()) {
                for (Long codService : employee.getSkillsAsArray()) {
                    if (codService.equals(service.getCod())) {
                        List<Employee> e = eps.getEmployees();
                        e.add(employee);
                        eps.setEmployees(e);
                        break;
                    }
                }
            }
            if (eps.getEmployees().size() > 0) {
                employeesPerService.add(eps);
            }
        }
        return employeesPerService;
    }

    public List<EmployeeSchedule> getEmployeeSchedules() {
        return schedules;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public List<Drawable> getAlbumDrawables() {
    	return albumDrawable;
    }
    
    public StoreSearchRequest getStoreSearchRequest() {
    	return requestConfigs;
    }
    
	//===================================================================================
    // Inner classes
    //===================================================================================

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    	
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return bookingPreviewFrag;
                case 1:
                    return bookingServiceToEmployeeFrag;
                case 2:
                    return bookingScheduleToEmployeeFrag;
                case 3:
                	return bookingScheduleResumeFrag;
                case 4:
            		return bookingFinishFrag;
                default:
                    return bookingPreviewFrag;
            }
        }
        
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
        
    }

    private class EmployeeScheduleLoader extends AsyncTask<Void, Void, List<EmployeeSchedule>> {

        private RESTClient client;
        private ProgressDialog waitDialog;
        private long storeId;
        private String initDate;
        private String endDate;
        private Long[] ids;

        public EmployeeScheduleLoader(long storeId, String initDate, String endDate, Long[] ids) {
            this.storeId = storeId;
            this.initDate = initDate;
            this.endDate = endDate;
            this.ids = ids;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            synchronized (lock) {
                isLoading = true;
            }
            client = new RESTClient(BookingActivity.this);
            waitDialog = ProgressDialog.show(
                    BookingActivity.this,
                    BookingActivity.this.getText(R.string.msg_pleasewait),
                    BookingActivity.this.getText(R.string.msg_obtainingschedules),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
        }

        @Override
        protected List<EmployeeSchedule> doInBackground(Void... params) {
            return client.getEmployeeSchedules(storeId, initDate, endDate, ids);
        }

        @Override
        protected void onPostExecute(List<EmployeeSchedule> gotSchedules) {
            super.onPostExecute(gotSchedules);
            waitDialog.dismiss();
            if (client.getStatus() == HttpStatus.SC_OK) {
                schedules = gotSchedules;
                activity_booking_pager.setCurrentItem(2, true);
                bookingScheduleToEmployeeFrag.setupFields();
            } else {
                // TODO: handle empty content or exceptions
            }
        }
    }

    private class RequestBookingTask extends AsyncTask<Void, Void, Void> {

        private RESTClient client;
        private ProgressDialog waitDialog;
        private List<Booking> bookings;

        public RequestBookingTask(List<Booking> bookings) {
            this.bookings = bookings;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            synchronized (lock) {
                isLoading = true;
            }
            client = new RESTClient(BookingActivity.this);
            waitDialog = ProgressDialog.show(
                    BookingActivity.this,
                    BookingActivity.this.getText(R.string.msg_pleasewait),
                    BookingActivity.this.getText(R.string.msg_bookingrequest),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
        }

        @Override
        protected Void doInBackground(Void... params) {
            client.BookingCreate(bookings);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            waitDialog.dismiss();
            if (client.getStatus() == HttpStatus.SC_OK) {
                activity_booking_pager.setCurrentItem(4, true);
                Log.i(AppConstants.TAG, "Book Request Done");
            } else {
                Log.w(AppConstants.TAG, "Error, client status: " + client.getStatus());
                // TODO: handle empty content or exceptions
            }
        }

    }
	
}
