package br.com.agendalize.androidapp.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.model.AddressStore;
import br.com.agendalize.androidapp.model.ContactStore;
import br.com.agendalize.androidapp.model.Employee;
import br.com.agendalize.androidapp.model.Operation;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.model.Store;

/**
 * Created by mthama on 04/11/15.
 */
public class DBAdapter {

    private static final String TABELA_STORE = "storesTable";
    private static final String TABELA_ADDRESS = "addressesTable";
    private static final String TABELA_CONTACT = "contactsTable";
    private static final String TABELA_SERVICES = "servicesTable";
    private static final String TABELA_EMPLOYEES = "employeesTable";
    private static final String TABELA_OPERATIONS = "operationsTable";
    private static final String TABELA_PHOTOS = "photosTable";
    private static final String DATABASE_NAME = "AgendalizeDB";

    // store model table
    public static final String KEY_STORE_ROW_ID = "_id";
    public static final String KEY_STORE_ID = "storeId";
    public static final String KEY_STORE_NAME = "name";
    public static final String KEY_STORE_DESCRIPTION = "description";
    public static final String KEY_STORE_URLPHOTO = "urlPhoto";
    public static final String KEY_STORE_EVALUATION = "evaluation";
    private static final String DATABASE_CREATE_TABELA_STORE = "create table storesTable(" +
            KEY_STORE_ROW_ID + " integer primary key autoincrement, " +
            KEY_STORE_ID + " integer not null, " +
            KEY_STORE_NAME + " text not null, " +
            KEY_STORE_DESCRIPTION + " text, " +
            KEY_STORE_URLPHOTO + " text, " +
            KEY_STORE_EVALUATION + " real)";

    // store address model table
    public static final String KEY_ADDRESS_ROW_ID = "_id";
    public static final String KEY_ADDRESS_STORE_ID = "storeId";
    public static final String KEY_ADDRESS_ADDRESS = "address";
    public static final String KEY_ADDRESS_COMPLEMENT = "complement";
    public static final String KEY_ADDRESS_CITY = "city";
    public static final String KEY_ADDRESS_STATE = "state";
    public static final String KEY_ADDRESS_COUNTRY = "country";
    public static final String KEY_ADDRESS_LATITUDE = "latitude";
    public static final String KEY_ADDRESS_LONGITUDE = "longitude";
    private static final String DATABASE_CREATE_TABELA_ADDRESS = "create table addressesTable(" +
            KEY_ADDRESS_ROW_ID + " integer primary key autoincrement, " +
            KEY_ADDRESS_STORE_ID + " integer not null, " +
            KEY_ADDRESS_ADDRESS + " text not null, " +
            KEY_ADDRESS_COMPLEMENT + " text, " +
            KEY_ADDRESS_CITY + " text, " +
            KEY_ADDRESS_STATE + " text, " +
            KEY_ADDRESS_COUNTRY + " text, " +
            KEY_ADDRESS_LATITUDE + " real, " +
            KEY_ADDRESS_LONGITUDE + " real)";

    // contact model table
    public static final String KEY_CONTACT_ROW_ID = "_id";
    public static final String KEY_CONTACT_STORE_ID = "storeId";
    public static final String KEY_CONTACT_PHONE1 = "phone1";
    public static final String KEY_CONTACT_PHONE2 = "phone2";
    public static final String KEY_CONTACT_EMAIL = "email";
    public static final String KEY_CONTACT_SITE = "site";
    public static final String KEY_CONTACT_FACEBOOK = "facebook";
    private static final String DATABASE_CREATE_TABELA_CONTACT = "create table contactsTable(" +
            KEY_CONTACT_ROW_ID + " integer primary key autoincrement, " +
            KEY_CONTACT_STORE_ID + " integer not null, " +
            KEY_CONTACT_PHONE1 + " text, " +
            KEY_CONTACT_PHONE2 + " text, " +
            KEY_CONTACT_EMAIL + " text, " +
            KEY_CONTACT_SITE + " text, " +
            KEY_CONTACT_FACEBOOK + " text)";

    // service model table
    public static final String KEY_SERVICE_ROW_ID = "_id";
    public static final String KEY_SERVICE_STORE_ID = "storeId";
    public static final String KEY_SERVICE_COD = "cod";
    public static final String KEY_SERVICE_SHORT_NAME = "shortName";
    public static final String KEY_SERVICE_DESCRIPTION = "description";
    public static final String KEY_SERVICE_COD_TYPE_SERVICE = "codTypeService";
    public static final String KEY_SERVICE_SHORT_NAME_TYPE_SERVICE = "shortNameTypeService";
    public static final String KEY_SERVICE_DESCRIPTION_TYPE_SERVICE = "descriptionTypeService";
    public static final String KEY_SERVICE_URL_IMAGE = "urlImage";
    public static final String KEY_SERVICE_PRICE = "price";
    public static final String KEY_SERVICE_TIME_MINUTES = "timeMinutes";
    private static final String DATABASE_CREATE_TABELA_SERVICE = "create table servicesTable(" +
            KEY_SERVICE_ROW_ID + " integer primary key autoincrement, " +
            KEY_SERVICE_STORE_ID + " integer not null, " +
            KEY_SERVICE_COD + " integer not null, " +
            KEY_SERVICE_SHORT_NAME + " text not null, " +
            KEY_SERVICE_DESCRIPTION + " text not null, " +
            KEY_SERVICE_COD_TYPE_SERVICE + " integer, " +
            KEY_SERVICE_SHORT_NAME_TYPE_SERVICE + " text, " +
            KEY_SERVICE_DESCRIPTION_TYPE_SERVICE + " text, " +
            KEY_SERVICE_URL_IMAGE + " text, " +
            KEY_SERVICE_PRICE + " text, " +
            KEY_SERVICE_TIME_MINUTES + " integer)";

    // service model table
    public static final String KEY_EMPLOYEE_ROW_ID = "_id";
    public static final String KEY_EMPLOYEE_STORE_ID = "storeId";
    public static final String KEY_EMPLOYEE_ID = "id";
    public static final String KEY_EMPLOYEE_NAME = "name";
    public static final String KEY_EMPLOYEE_LASTNAME = "lastName";
    public static final String KEY_EMPLOYEE_DESCRIPTION = "description";
    public static final String KEY_EMPLOYEE_PHOTOURL = "photoUrl";
    public static final String KEY_EMPLOYEE_SKILLS = "skills";
    private static final String DATABASE_CREATE_TABELA_EMPLOYEE = "create table employeesTable(" +
            KEY_EMPLOYEE_ROW_ID + " integer primary key autoincrement, " +
            KEY_EMPLOYEE_STORE_ID + " integer not null, " +
            KEY_EMPLOYEE_ID + " integer not null, " +
            KEY_EMPLOYEE_NAME + " text not null, " +
            KEY_EMPLOYEE_LASTNAME + " text not null, " +
            KEY_EMPLOYEE_DESCRIPTION + " text, " +
            KEY_EMPLOYEE_PHOTOURL + " text, " +
            KEY_EMPLOYEE_SKILLS + " text)";

    public static final String KEY_OPERATION_ROW_ID = "_id";
    public static final String KEY_OPERATION_STORE_ID = "storeId";
    public static final String KEY_OPERATION_DESCRIPTION_WEEK = "descriptionWeek";
    public static final String KEY_OPERATION_DESCRIPTION_HOUR_OPEN = "descriptionHourOpen";
    public static final String KEY_OPERATION_DESCRIPTION_HOUR_CLOSE = "descriptionHourClose";
    public static final String KEY_OPERATION_DESCRIPTION_FLAG_OPEN = "flagOpen";
    private static final String DATABASE_CREATE_TABELA_OPERATION = "create table operationsTable(" +
            KEY_OPERATION_ROW_ID + " integer primary key autoincrement, " +
            KEY_OPERATION_STORE_ID + " integer not null, " +
            KEY_OPERATION_DESCRIPTION_WEEK + " text not null, " +
            KEY_OPERATION_DESCRIPTION_HOUR_OPEN + " text not null, " +
            KEY_OPERATION_DESCRIPTION_HOUR_CLOSE + " text not null, " +
            KEY_OPERATION_DESCRIPTION_FLAG_OPEN + " integer)";

    public static final String KEY_PHOTO_ROW_ID = "_id";
    public static final String KEY_PHOTO_STORE_ID = "storeId";
    public static final String KEY_PHOTO_URL = "url";
    private static final String DATABASE_CREATE_TABELA_PHOTO = "create table photosTable(" +
            KEY_PHOTO_ROW_ID + " integer primary key autoincrement, " +
            KEY_PHOTO_STORE_ID + " integer not null, " +
            KEY_PHOTO_URL + " text not null)";

    private static final int DATABASE_VERSION = 3;

    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DBAdapter(Context context) {
        DBHelper = new DatabaseHelper(context);
    }

    public DBAdapter open() throws SQLiteException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    // ---closes the database---
    public void close() {
        DBHelper.close();
    }

    /**
     * Insere um novo Store4MobileDTO no banco.
     *
     * @param store
     * @return true para sucesso false para fracasso
     */
    public boolean insertPersistentResult(Store store) {
        try {
            this.open();
            ContentValues values = new ContentValues();
            values.put(KEY_STORE_ID, store.getId());
            values.put(KEY_STORE_NAME, store.getName());
            values.put(KEY_STORE_DESCRIPTION, store.getDescription());
            values.put(KEY_STORE_URLPHOTO, store.getUrlPhoto());
            values.put(KEY_STORE_EVALUATION, store.getEvaluation());
            long idStore = db.insert(TABELA_STORE, null, values);
            if (idStore == -1) {
                return false;
            }
            if (store.getAddressInfo() != null) {
                ContentValues addressValues = new ContentValues();
                addressValues.put(KEY_ADDRESS_STORE_ID, idStore);
                addressValues.put(KEY_ADDRESS_ADDRESS, store.getAddressInfo().getAddress());
                addressValues.put(KEY_ADDRESS_COMPLEMENT, store.getAddressInfo().getComplement());
                addressValues.put(KEY_ADDRESS_CITY, store.getAddressInfo().getCity());
                addressValues.put(KEY_ADDRESS_STATE, store.getAddressInfo().getState());
                addressValues.put(KEY_ADDRESS_COUNTRY, store.getAddressInfo().getCountry());
                addressValues.put(KEY_ADDRESS_LATITUDE, store.getAddressInfo().getLatitude());
                addressValues.put(KEY_ADDRESS_LONGITUDE, store.getAddressInfo().getLongitude());
                long inserted = db.insert(TABELA_ADDRESS, null, addressValues);
                if (inserted == -1) {
                    return false;
                }
            }
            if (store.getContactInfo() != null) {
                ContentValues contactValues = new ContentValues();
                contactValues.put(KEY_CONTACT_STORE_ID, idStore);
                contactValues.put(KEY_CONTACT_PHONE1, store.getContactInfo().getPhoneNumberOne());
                contactValues.put(KEY_CONTACT_PHONE2, store.getContactInfo().getPhoneNumberTwo());
                contactValues.put(KEY_CONTACT_EMAIL, store.getContactInfo().getEmail());
                contactValues.put(KEY_CONTACT_SITE, store.getContactInfo().getSite());
                contactValues.put(KEY_CONTACT_FACEBOOK, store.getContactInfo().getFacebook());
                long inserted = db.insert(TABELA_CONTACT, null, contactValues);
                if (inserted == -1) {
                    return false;
                }
            }
            if (store.getListServices() != null) {
                for (Service service : store.getListServices()) {
                    ContentValues servicetValues = new ContentValues();
                    servicetValues.put(KEY_SERVICE_STORE_ID, idStore);
                    servicetValues.put(KEY_SERVICE_COD, service.getCod());
                    servicetValues.put(KEY_SERVICE_SHORT_NAME, service.getShortName());
                    servicetValues.put(KEY_SERVICE_DESCRIPTION, service.getDescription());
                    servicetValues.put(KEY_SERVICE_COD_TYPE_SERVICE, service.getCodTypeService());
                    servicetValues.put(KEY_SERVICE_SHORT_NAME_TYPE_SERVICE, service.getShortNameTypeService());
                    servicetValues.put(KEY_SERVICE_DESCRIPTION_TYPE_SERVICE, service.getDescriptionTypeService());
                    servicetValues.put(KEY_SERVICE_URL_IMAGE, service.getUrlImage());
                    servicetValues.put(KEY_SERVICE_PRICE, service.getPrice());
                    servicetValues.put(KEY_SERVICE_TIME_MINUTES, service.getTimeMinutes());
                    long inserted = db.insert(TABELA_SERVICES, null, servicetValues);
                    if (inserted == -1) {
                        return false;
                    }
                }
            }
            if (store.getEmployees() != null) {
                for (Employee employee : store.getEmployees()) {
                    ContentValues employeeValues = new ContentValues();
                    employeeValues.put(KEY_EMPLOYEE_STORE_ID, idStore);
                    employeeValues.put(KEY_EMPLOYEE_ID, employee.getId());
                    employeeValues.put(KEY_EMPLOYEE_NAME, employee.getName());
                    employeeValues.put(KEY_EMPLOYEE_LASTNAME, employee.getLastName());
                    employeeValues.put(KEY_EMPLOYEE_DESCRIPTION, employee.getDescription());
                    employeeValues.put(KEY_EMPLOYEE_PHOTOURL, employee.getImageUrl());
                    employeeValues.put(KEY_EMPLOYEE_SKILLS, employee.getSkills());
                    long inserted = db.insert(TABELA_EMPLOYEES, null, employeeValues);
                    if (inserted == -1) {
                        return false;
                    }
                }
            }
            if (store.getListOperation() != null) {
                for (Operation operations : store.getListOperation()) {
                    ContentValues operationValues = new ContentValues();
                    operationValues.put(KEY_OPERATION_STORE_ID, idStore);
                    operationValues.put(KEY_OPERATION_DESCRIPTION_WEEK, operations.getDescriptionWeek());
                    operationValues.put(KEY_OPERATION_DESCRIPTION_HOUR_OPEN, operations.getDescriptionHourOpen());
                    operationValues.put(KEY_OPERATION_DESCRIPTION_HOUR_CLOSE, operations.getDescriptionHourClose());
                    operationValues.put(KEY_OPERATION_DESCRIPTION_FLAG_OPEN, operations.isFlagOpen());
                    long inserted = db.insert(TABELA_OPERATIONS, null, operationValues);
                    if (inserted == -1) {
                        return false;
                    }
                }
            }
            if (store.getAlbum() != null) {
                for (String photo : store.getAlbum()) {
                    ContentValues photoValues = new ContentValues();
                    photoValues.put(KEY_PHOTO_STORE_ID, idStore);
                    photoValues.put(KEY_PHOTO_URL, photo);
                    long inserted = db.insert(TABELA_PHOTOS, null, photoValues);
                    if (inserted == -1) {
                        return false;
                    }
                }
            }
        } finally {
            this.close();
        }
        return true;
    }

    public List<Store> selectAll(String condition) {
        List<Store> out = null;
        try {
            this.open();
            Cursor resultSet = db.query(
                    TABELA_STORE, new String[] {
                            KEY_STORE_ROW_ID,
                            KEY_STORE_ID,
                            KEY_STORE_NAME,
                            KEY_STORE_DESCRIPTION,
                            KEY_STORE_URLPHOTO,
                            KEY_STORE_EVALUATION
                    }, condition, null, null, null, "_id DESC");
            out = new LinkedList<Store>();

            for (int i = 0; i < resultSet.getCount(); i++) {
                resultSet.moveToPosition(i);

                long _id = resultSet.getLong(0);
                AddressStore addressInfo = new AddressStore();
                ContactStore contactInfo = new ContactStore();
                List<Service> services = new ArrayList<Service>();
                List<Employee> employees = new ArrayList<Employee>();
                List<Operation> listOperation = new ArrayList<Operation>();
                List<String> album = new ArrayList<String>();

                // ======================================================================
                Cursor addressResultSet = db.query(TABELA_ADDRESS,
                        new String[] {
                                KEY_ADDRESS_ADDRESS,
                                KEY_ADDRESS_COMPLEMENT, KEY_ADDRESS_CITY,
                                KEY_ADDRESS_STATE, KEY_ADDRESS_COUNTRY,
                                KEY_ADDRESS_LATITUDE, KEY_ADDRESS_LONGITUDE
                        },
                        KEY_ADDRESS_STORE_ID + "= ?",
                        new String[] { String.valueOf(_id) },
                        null, null, null);
                for (int j = 0; j < addressResultSet.getCount(); j++) {
                    addressResultSet.moveToPosition(j);
                    addressInfo = new AddressStore(
                            addressResultSet.getString(0),
                            addressResultSet.getString(1),
                            addressResultSet.getString(2),
                            addressResultSet.getString(3),
                            addressResultSet.getString(4),
                            addressResultSet.getDouble(5),
                            addressResultSet.getDouble(6)
                    );
                }
                // ======================================================================
                Cursor contactResultSet = db.query(TABELA_CONTACT,
                        new String[] {
                                KEY_CONTACT_PHONE1,
                                KEY_CONTACT_PHONE2, KEY_CONTACT_EMAIL,
                                KEY_CONTACT_SITE, KEY_CONTACT_FACEBOOK
                        },
                        KEY_CONTACT_STORE_ID + "= ?",
                        new String[] { String.valueOf(_id) },
                        null, null, null);
                for (int j = 0; j < contactResultSet.getCount(); j++) {
                    contactResultSet.moveToPosition(j);
                    contactInfo = new ContactStore(
                            contactResultSet.getString(0),
                            contactResultSet.getString(1),
                            contactResultSet.getString(2),
                            contactResultSet.getString(3),
                            contactResultSet.getString(4)
                    );
                }
                // ======================================================================
                Cursor serviceResultSet = db.query(TABELA_SERVICES,
                        new String[] {
                                KEY_SERVICE_COD, KEY_SERVICE_SHORT_NAME, KEY_SERVICE_DESCRIPTION,
                                KEY_SERVICE_COD_TYPE_SERVICE, KEY_SERVICE_SHORT_NAME_TYPE_SERVICE,
                                KEY_SERVICE_DESCRIPTION_TYPE_SERVICE, KEY_SERVICE_URL_IMAGE,
                                KEY_SERVICE_PRICE, KEY_SERVICE_TIME_MINUTES
                        },
                        KEY_SERVICE_STORE_ID + "= ?",
                        new String[] { String.valueOf(_id) },
                        null, null, null);
                for (int j = 0; j < serviceResultSet.getCount(); j++) {
                    serviceResultSet.moveToPosition(j);
                    services.add(new Service(
                            serviceResultSet.getLong(0),
                            serviceResultSet.getString(1),
                            serviceResultSet.getString(2),
                            serviceResultSet.getLong(3),
                            serviceResultSet.getString(4),
                            serviceResultSet.getString(5),
                            serviceResultSet.getString(6),
                            serviceResultSet.getString(7),
                            serviceResultSet.getString(8)
                    ));
                }
                // ======================================================================
                Cursor employeeResultSet = db.query(TABELA_EMPLOYEES,
                        new String[] {
                                KEY_EMPLOYEE_ID, KEY_EMPLOYEE_NAME, KEY_EMPLOYEE_LASTNAME,
                                KEY_EMPLOYEE_DESCRIPTION, KEY_EMPLOYEE_PHOTOURL, KEY_EMPLOYEE_SKILLS
                        },
                        KEY_EMPLOYEE_STORE_ID + "= ?",
                        new String[] { String.valueOf(_id) },
                        null, null, null);
                for (int j = 0; j < employeeResultSet.getCount(); j++) {
                    employeeResultSet.moveToPosition(j);
                    employees.add(new Employee(
                            employeeResultSet.getLong(0),
                            employeeResultSet.getString(1),
                            employeeResultSet.getString(2),
                            employeeResultSet.getString(3),
                            employeeResultSet.getString(4),
                            employeeResultSet.getString(5),
                            null
                    ));
                }
                // ======================================================================
                Cursor operationResultSet = db.query(TABELA_OPERATIONS,
                        new String[] {
                                KEY_OPERATION_DESCRIPTION_WEEK,
                                KEY_OPERATION_DESCRIPTION_HOUR_OPEN,
                                KEY_OPERATION_DESCRIPTION_HOUR_CLOSE,
                                KEY_OPERATION_DESCRIPTION_FLAG_OPEN
                        },
                        KEY_OPERATION_STORE_ID + "= ?",
                        new String[] { String.valueOf(_id) },
                        null, null, null);
                for (int j = 0; j < operationResultSet.getCount(); j++) {
                    operationResultSet.moveToPosition(j);
                    listOperation.add(new Operation(
                            operationResultSet.getString(0),
                            operationResultSet.getString(1),
                            operationResultSet.getString(2),
                            (operationResultSet.getInt(3) == 1)
                    ));
                }
                // ======================================================================
                Cursor photoResultSet = db.query(TABELA_PHOTOS,
                        new String[] {
                                KEY_PHOTO_URL
                        },
                        KEY_PHOTO_STORE_ID + "= ?",
                        new String[] { String.valueOf(_id) },
                        null, null, null);
                for (int j = 0; j < photoResultSet.getCount(); j++) {
                    photoResultSet.moveToPosition(j);
                    album.add(photoResultSet.getString(0));
                }
                // ======================================================================

                Store store = new Store(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        addressInfo,
                        contactInfo,
                        services,
                        employees,
                        listOperation,
                        album,
                        resultSet.getFloat(5));

                out.add(store);

                addressResultSet.close();
                contactResultSet.close();
                serviceResultSet.close();
                employeeResultSet.close();
                operationResultSet.close();
                photoResultSet.close();
            }
            resultSet.close();

        } finally {
            this.close();

        }
        return out;
    }

    public void delete(Store resultado) {
        try {
            this.open();
            db.delete(TABELA_STORE, KEY_STORE_ROW_ID + "= ?", new String[]{""
                    + resultado.getId()});
            db.delete(TABELA_ADDRESS, KEY_ADDRESS_STORE_ID + "= ?",
                    new String[] { "" + resultado.getId() });
            db.delete(TABELA_CONTACT, KEY_CONTACT_STORE_ID + "= ?",
                    new String[] { "" + resultado.getId() });
            db.delete(TABELA_SERVICES, KEY_SERVICE_STORE_ID + "= ?",
                    new String[]{"" + resultado.getId() });
            db.delete(TABELA_EMPLOYEES, KEY_EMPLOYEE_STORE_ID + "= ?",
                    new String[] { "" + resultado.getId() });
            db.delete(TABELA_OPERATIONS, KEY_OPERATION_STORE_ID + "= ?",
                    new String[] { "" + resultado.getId() });
            db.delete(TABELA_PHOTOS, KEY_PHOTO_STORE_ID + "= ?",
                    new String[] { "" + resultado.getId() });
        } finally {
            this.close();
        }
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            Log.i(AppConstants.TAG, "Creating tables of persistence");
            db.execSQL(DATABASE_CREATE_TABELA_STORE);
            db.execSQL(DATABASE_CREATE_TABELA_ADDRESS);
            db.execSQL(DATABASE_CREATE_TABELA_CONTACT);
            db.execSQL(DATABASE_CREATE_TABELA_SERVICE);
            db.execSQL(DATABASE_CREATE_TABELA_EMPLOYEE);
            db.execSQL(DATABASE_CREATE_TABELA_OPERATION);
            db.execSQL(DATABASE_CREATE_TABELA_PHOTO);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
    }

}
