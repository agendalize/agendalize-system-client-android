package br.com.agendalize.androidapp.geo;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import br.com.agendalize.androidapp.AppConstants;

public class GPSHandler implements LocationListener {
	
	private Context context;
	private LocationManager locationManager;
	private Location currentLocation;

	public GPSHandler(Context context) {
		super();
		this.context = context;
	}
	
	public void init() {
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		try {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		} catch (IllegalArgumentException e) {
			Log.w(AppConstants.TAG, "GPS_PROVIDER unavailable");
		}
		try {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		} catch (IllegalArgumentException e) {
			Log.w(AppConstants.TAG, "NETWORK_PROVIDER unavailable");
		}
		currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (currentLocation == null) {
			currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
	}
	
	public void end() {
		if (locationManager != null) {
			locationManager.removeUpdates(this);
		}
	}	

	public Location getCurrentLocation() {
		return currentLocation;
	}
	 
	public void onLocationChanged(Location location) {
		currentLocation = location;
	}
	 
	public void onProviderDisabled(String provider) {
	}

	 
	public void onProviderEnabled(String provider) {
	}
	 
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

}
