package br.com.agendalize.androidapp.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mthama on 25/10/15.
 */

public class ServiceHistoryGroup {

    private String idEmployee;
    private String nameEmployee;
    private String urlPhotoEmployee;
    private List<ServiceHistory> services;

    public ServiceHistoryGroup() {
        idEmployee = "";
        nameEmployee = "";
        urlPhotoEmployee = "";
        services = new ArrayList<>();
    }

    public ServiceHistoryGroup(String idEmployee, String nameEmployee,
                               String urlPhotoEmployee, List<ServiceHistory> services) {
        this();
        this.idEmployee = idEmployee;
        this.nameEmployee = nameEmployee;
        this.urlPhotoEmployee = urlPhotoEmployee;
        this.services = services;
    }

    public ServiceHistoryGroup(JSONObject obj) {
        this();
        try {
            this.idEmployee = obj.getString("idEmployee");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.nameEmployee = obj.getString("nameEmployee");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            this.urlPhotoEmployee = obj.getString("urlPhotoEmployee");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            JSONArray arr = new JSONArray(obj.getString("services"));
            for (int i=0; i<arr.length(); i++) {
                this.services.add(new ServiceHistory(arr.getJSONObject(i)));
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public String getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(String idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getNameEmployee() {
        return nameEmployee;
    }

    public void setNameEmployee(String nameEmployee) {
        this.nameEmployee = nameEmployee;
    }

    public String getUrlPhotoEmployee() {
        return urlPhotoEmployee;
    }

    public void setUrlPhotoEmployee(String urlPhotoEmployee) {
        this.urlPhotoEmployee = urlPhotoEmployee;
    }

    public List<ServiceHistory> getServices() {
        return services;
    }

    public void setServices(List<ServiceHistory> services) {
        this.services = services;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("idEmployee", idEmployee);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("nameEmployee", nameEmployee);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            obj.put("urlPhotoEmployee", urlPhotoEmployee);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            JSONArray arr = new JSONArray();
            for (int i=0; i<services.size(); i++) {
                arr.put(services.get(i));
            }
            obj.put("services", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return obj;
    }

}
