package br.com.agendalize.androidapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import br.com.agendalize.androidapp.geo.SingleShotLocationProvider;
import br.com.agendalize.androidapp.util.DeviceUtils;
import br.com.agendalize.androidapp.ui.HomeActivity;
import br.com.agendalize.androidapp.ui.LoginActivity;

/**
 * Created by mthama on 20/10/15.
 */
public class BootsplashActivity extends AgendalizeActivity {

    private SharedPreferences prefsPrivate;
    private SharedPreferences.Editor prefsPrivateEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bootsplash);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        SingleShotLocationProvider.requestSingleUpdate(this, new SingleShotLocationProvider.LocationCallback() {
            @Override
            public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                if (location != null) {
                    Log.w(AppConstants.TAG, "LAT: " + location.latitude + " / LON: " + location.longitude);
                    prefsPrivate = BootsplashActivity.this.getSharedPreferences(AppConstants.SHARED_PREFS, Context.MODE_PRIVATE);
                    prefsPrivateEditor = prefsPrivate.edit();
                    prefsPrivateEditor.putFloat(AppConstants.LATITUDE, (float) location.latitude);
                    prefsPrivateEditor.putFloat(AppConstants.LONGITUDE, (float) location.longitude);
                    prefsPrivateEditor.commit();
                    // creates a new application instance
                    String pin = prefsPrivate.getString(AppConstants.PIN_CODE, null);
                    String token = prefsPrivate.getString(AppConstants.ACCESS_TOKEN, null);
                    if (pin != null && token != null) {
                        Log.w(AppConstants.TAG, "PIN: " + pin + " / TOKEN: " + token);
                        Intent newApp = new Intent(BootsplashActivity.this, HomeActivity.class);
                        newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(newApp);
                    } else {
                        Intent newApp = new Intent(BootsplashActivity.this, LoginActivity.class);
                        newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(newApp);
                    }
                    finish();
                } else {
                    DeviceUtils.justShowDialogWarning(
                            BootsplashActivity.this,
                            R.string.msg_notpossibletogetlocation,
                            true);
                }
            }
        });
    }

}