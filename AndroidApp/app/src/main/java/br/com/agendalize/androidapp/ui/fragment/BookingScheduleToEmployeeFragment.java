package br.com.agendalize.androidapp.ui.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Booking;
import br.com.agendalize.androidapp.model.EmployeeSchedule;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.model.Slot;
import br.com.agendalize.androidapp.util.DeviceUtils;
import br.com.agendalize.androidapp.ui.BookingActivity;
import br.com.agendalize.androidapp.util.ImageUtils;

public class BookingScheduleToEmployeeFragment extends Fragment {

	//===================================================================================
	// Fields
	//===================================================================================
	
	private BookingActivity activity;
	private Button frag_bookingscheduletoemployee_previousbtn;
	private Button frag_bookingscheduletoemployee_nextbtn;
    private ViewPager frag_bookingscheduletoemployee_viewpager;
    private LinearLayout frag_bookingscheduletoemployee_employeerowlyt;
    private TextView[] frag_bookingscheduletoemployee_emp;

    private int serviceIndex = 0;
	private ScheduleListAdapter adapter;
	private List<EmployeeSchedule> schedulesOverDays;
	
	private TextView frag_bookingscheduletoemployee_monthinview;
	private TextView frag_bookingscheduletoemployee_dayinview;
	private Calendar calendar;
	
	// we name the left, middle and right page
	private static final int PAGE_LEFT = 0;
	private static final int PAGE_MIDDLE = 1;
	private static final int PAGE_RIGHT = 2;

	private LayoutInflater mInflater;
	private int mSelectedPageIndex = 1;
	private PageModel[] mPageModel;
	
	//===================================================================================
	// Override methods
	//===================================================================================
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_bookingscheduletoemployee, container, false);
		activity = (BookingActivity)getActivity();
        mInflater = getActivity().getLayoutInflater();

        frag_bookingscheduletoemployee_previousbtn = (Button) rootView.findViewById(R.id.frag_bookingscheduletoemployee_previousbtn);
        frag_bookingscheduletoemployee_previousbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getPagerView().setCurrentItem(1);
                activity.notifyServiceToEmployeeDataListChange();
            }
        });
        frag_bookingscheduletoemployee_nextbtn = (Button) rootView.findViewById(R.id.frag_bookingscheduletoemployee_nextbtn);
        frag_bookingscheduletoemployee_nextbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllFieldsOk()) {
                    activity.requestBookingResume();
                } else {
                    DeviceUtils.justShowDialogWarning(activity, R.string.msg_selectaschedules, false);
                }
            }
        });

        frag_bookingscheduletoemployee_monthinview = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_monthinview);
        frag_bookingscheduletoemployee_dayinview = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_dayinview);
        frag_bookingscheduletoemployee_viewpager = (ViewPager) rootView.findViewById(R.id.frag_bookingscheduletoemployee_viewpager);
        frag_bookingscheduletoemployee_employeerowlyt = (LinearLayout) rootView.findViewById(R.id.frag_bookingscheduletoemployee_employeerowlyt);

        // TODO: dinamically create the bar of employees photo images and names
        frag_bookingscheduletoemployee_emp = new TextView[4];
        frag_bookingscheduletoemployee_emp[0] = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_emp1);
        frag_bookingscheduletoemployee_emp[1] = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_emp2);
        frag_bookingscheduletoemployee_emp[2] = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_emp3);
        frag_bookingscheduletoemployee_emp[3] = (TextView) rootView.findViewById(R.id.frag_bookingscheduletoemployee_emp4);

        // initializing the model with page model indexes of -1, 0 and 1
        mPageModel = new PageModel[3];
        for (int i = 0; i < mPageModel.length; i++) {
            mPageModel[i] = new PageModel(i - 1);
        }

		return rootView;
	}

    //===================================================================================
	// General methods
	//===================================================================================

    public void setupFields() {

        activity.getBookings().clear();
        // we always see the middle page
        schedulesOverDays = activity.getEmployeeSchedules();
        int pageOfInterestIndex = (schedulesOverDays.size()-1)/2;

        // get current date of interest
        calendar = Calendar.getInstance();
        SimpleDateFormat form = new SimpleDateFormat("dd/MM/yyyy");
        try {
            calendar.setTime(form.parse(schedulesOverDays.get(pageOfInterestIndex).getDate()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        frag_bookingscheduletoemployee_monthinview.setText(getMonthNameByIndex(calendar.get(Calendar.MONTH)));
        frag_bookingscheduletoemployee_dayinview.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));

        for (int i=0; i<4; i++) {
            frag_bookingscheduletoemployee_emp[i].setVisibility(View.GONE);
            frag_bookingscheduletoemployee_emp[i].setText("");
        }
        for (int i=0; i<schedulesOverDays.get(pageOfInterestIndex).getEmployees().size(); i++) {
            frag_bookingscheduletoemployee_emp[i].setVisibility(View.VISIBLE);
            frag_bookingscheduletoemployee_emp[i].setText(schedulesOverDays.get(pageOfInterestIndex).getEmployees().get(i).getName());
            new PhotoLoader(
                    frag_bookingscheduletoemployee_emp[i],
                    schedulesOverDays.get(pageOfInterestIndex).getEmployees().get(i).getImageUrl()).execute();
        }

        // create views for the schedule pages
        frag_bookingscheduletoemployee_viewpager.setAdapter(
                new SchedulePageAdapter(schedulesOverDays.get(pageOfInterestIndex))
        );
        frag_bookingscheduletoemployee_viewpager.setCurrentItem(PAGE_MIDDLE, false);
        frag_bookingscheduletoemployee_viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mSelectedPageIndex = position;
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    final PageModel leftPage = mPageModel[PAGE_LEFT];
                    final PageModel middlePage = mPageModel[PAGE_MIDDLE];
                    final PageModel rightPage = mPageModel[PAGE_RIGHT];
                    final int oldLeftIndex = leftPage.getIndex();
                    final int oldMiddleIndex = middlePage.getIndex();
                    final int oldRightIndex = rightPage.getIndex();
                    // user swiped to right direction --> left page
                    if (mSelectedPageIndex == PAGE_LEFT) {
                        leftPage.setIndex(oldLeftIndex - 1);
                        middlePage.setIndex(oldLeftIndex);
                        rightPage.setIndex(oldMiddleIndex);
                        setContent(PAGE_RIGHT);
                        setContent(PAGE_MIDDLE);
                        setContent(PAGE_LEFT);
                        calendar.add(Calendar.DAY_OF_YEAR, - 1);
                        frag_bookingscheduletoemployee_monthinview.setText(getMonthNameByIndex(calendar.get(Calendar.MONTH)));
                        frag_bookingscheduletoemployee_dayinview.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));
                        // user swiped to left direction --> right page
                    } else if (mSelectedPageIndex == PAGE_RIGHT) {
                        leftPage.setIndex(oldMiddleIndex);
                        middlePage.setIndex(oldRightIndex);
                        rightPage.setIndex(oldRightIndex + 1);
                        setContent(PAGE_LEFT);
                        setContent(PAGE_MIDDLE);
                        setContent(PAGE_RIGHT);
                        calendar.add(Calendar.DAY_OF_YEAR, + 1);
                        frag_bookingscheduletoemployee_monthinview.setText(getMonthNameByIndex(calendar.get(Calendar.MONTH)));
                        frag_bookingscheduletoemployee_dayinview.setText(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));
                    }
                    frag_bookingscheduletoemployee_viewpager.setCurrentItem(PAGE_MIDDLE, false);
                }
            }
        });
    }

	private String getMonthNameByIndex(int index) {
		switch (index) {
			case 0: return "Jan";
			case 1: return "Fev";
			case 2: return "Mar";
			case 3: return "Abr";
			case 4: return "Mai";
			case 5: return "Jun";
			case 6: return "Jul";
			case 7: return "Ago";
			case 8: return "Set";
			case 9: return "Out";
			case 10: return "Nov";
			case 11: return "Dez";
			default: return "Jan";
		}
	}
	
	private boolean isAllFieldsOk() {
		return (activity.getBookings().size() > 0);
	}
	
	private void setContent(int index) {
		final PageModel model = mPageModel[index];
		// TODO: properly implement here
        schedulesOverDays = activity.getEmployeeSchedules();
		/*items = new ArrayList<>();
		for (int i=0; i< AppConstants.SCHEDULE_DURATION_MIN*(60/AppConstants.SCHEDULE_INTERVALS_MIN)/60; i++) {
			items.add(new EmployeeSchedule());
		}*/
		adapter = new ScheduleListAdapter(getActivity(), schedulesOverDays.get(1));
        ((ListView)model.listview.findViewById(R.id.helper_listschedule_openHours)).setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class SchedulePageAdapter extends PagerAdapter {

        private EmployeeSchedule schedule;

        public SchedulePageAdapter(EmployeeSchedule schedule) {
            this.schedule = schedule;
        }
		
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ListView listview = (ListView)mInflater.inflate(R.layout.helper_listschedule, container, false);
			PageModel currentPage = mPageModel[position];
			currentPage.listview = listview;
			ScheduleListAdapter adapter = new ScheduleListAdapter(getActivity(), schedule);
			listview.setAdapter(adapter);
			adapter.notifyDataSetChanged();
			container.addView(listview);
			return listview;
		}

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			return view == obj;
		}
		
	}

    private class ScheduleListAdapter extends BaseAdapter {

        private Context context;
        private EmployeeSchedule schedulesOverDay;
        private List<Slot[]> employeeSlotsOverDay;

        public ScheduleListAdapter(Context context, EmployeeSchedule schedulesOverDay) {
            this.context = context;
            this.schedulesOverDay = schedulesOverDay;
            this.employeeSlotsOverDay = new ArrayList<>();
            int minorSize = 0;
            for (int i=0; i<schedulesOverDay.getEmployees().size(); i++) {
                if (minorSize == 0 || minorSize > schedulesOverDay.getEmployees().get(i).getSlots().size())
                    minorSize = schedulesOverDay.getEmployees().get(i).getSlots().size();
            }
            for (int i=0; i<minorSize; i++) {
                Slot[] slotsOfTheEmployees = new Slot[schedulesOverDay.getEmployees().size()];
                for (int j=0; j<schedulesOverDay.getEmployees().size(); j++) {
                    slotsOfTheEmployees[j] = schedulesOverDay.getEmployees().get(j).getSlots().get(i);
                }
                employeeSlotsOverDay.add(slotsOfTheEmployees);
            }
        }

        @Override
        public int getCount() {
            return employeeSlotsOverDay.size();
        }

        @Override
        public Object getItem(int position) {
            return employeeSlotsOverDay.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_schedule, parent, false);
            }
            // this fill the first columns: the hours
            final TextView hour = ((TextView)convertView.findViewById(R.id.row_schedule_hour));
            hour.setText(
                    (int)((position/4)+(AppConstants.SCHEDULE_START_MIN/60))+":"+
                    (position%4==0?"00":(position+1)%4==0?"45":(position+2)%4==0?"30":"15")
            );

            ImageButton[] row_schedule_slot = new ImageButton[4];
            row_schedule_slot[0] = ((ImageButton)convertView.findViewById(R.id.row_schedule_slot1));
            row_schedule_slot[1] = ((ImageButton)convertView.findViewById(R.id.row_schedule_slot2));
            row_schedule_slot[2] = ((ImageButton)convertView.findViewById(R.id.row_schedule_slot3));
            row_schedule_slot[3] = ((ImageButton)convertView.findViewById(R.id.row_schedule_slot4));
            // here, for each employee we fill his schedules
            for (int i=0; i<schedulesOverDay.getEmployees().size(); i++) {
                row_schedule_slot[i].setVisibility(View.VISIBLE);
                final long empId = schedulesOverDay.getEmployees().get(i).getId();
                final String empPhoto = schedulesOverDay.getEmployees().get(i).getImageUrl();
                if (employeeSlotsOverDay.get(position)[i].getCodStatus().equals("A")) {

                    final List<Service> servicesForThisEmployee = new ArrayList<>();
                    for (int l=0; l<activity.getSelectedServicesToEmployees().size(); l++) {
                        long key = activity.getSelectedServicesToEmployees().keyAt(l);
                        if (activity.getSelectedServicesToEmployees().get(key) != null) {
                            long empIdSel = activity.getSelectedServicesToEmployees().get(key).getId();
                            if (empId == empIdSel) {
                                servicesForThisEmployee.add(activity.getStore().getService(key));
                            }
                        }
                    }

                    row_schedule_slot[i].setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedavailable));
                    row_schedule_slot[i].setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder adb = new AlertDialog.Builder(context);
                            CharSequence items[] = new CharSequence[servicesForThisEmployee.size()];
                            for (int m=0; m<servicesForThisEmployee.size(); m++) {
                                items[m] = servicesForThisEmployee.get(m).getShortName();
                            }
                            adb.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    serviceIndex = which;
                                }
                            });
                            adb.setPositiveButton(context.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String codHour = hour.getText().toString().replace(":", "");
                                    if (codHour.length() != 4) codHour = "0" + codHour;
                                    activity.getBookings().add(
                                            new Booking(
                                                    servicesForThisEmployee.get(serviceIndex).getCod(), empId, empPhoto,
                                                    (int)((position/4)+(AppConstants.SCHEDULE_START_MIN/60))+":"+
                                                    (position%4==0?"00":(position+1)%4==0?"45":(position+2)%4==0?"30":"15"),
                                                    schedulesOverDay.getCodDate(), codHour)
                                    );
                                    serviceIndex = 0;
                                    Toast.makeText(
                                            context,
                                            getResources().getString(R.string.msg_serviceaddedtocart) + " " +
                                            servicesForThisEmployee.get(serviceIndex).getShortName(),
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                            adb.setNegativeButton(context.getResources().getString(R.string.action_cancel), null);
                            adb.setTitle(context.getResources().getString(R.string.label_selectyourservice));
                            adb.show();
                        }
                    });
                } else if (employeeSlotsOverDay.get(position)[i].getCodStatus().equals("P")) {
                    row_schedule_slot[i].setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedpending));
                    row_schedule_slot[i].setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(context, context.getResources().getString(R.string.msg_slotpending), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (employeeSlotsOverDay.get(position)[i].getCodStatus().equals("B")) {
                    row_schedule_slot[i].setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundedbooking));
                    row_schedule_slot[i].setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(context, context.getResources().getString(R.string.msg_slotbocked), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (employeeSlotsOverDay.get(position)[i].getCodStatus().equals("D")) {
                    row_schedule_slot[i].setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_roundeddisabled));
                    row_schedule_slot[i].setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(context, context.getResources().getString(R.string.msg_slotblocked), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
            return convertView;
        }

    }
	
	private class PageModel {

		private int index;
		public ListView listview;

		public PageModel(int index) {
			this.index = index;
			setIndex(index);
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}
		
	}

    private class PhotoLoader extends AsyncTask<Void, Void, Void> {

        private Drawable drawable;
        private TextView photo;
        private String url;

        public PhotoLoader(TextView photo, String url) {
            this.photo = photo;
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                drawable = ImageUtils.loadImageFromWebOperations(url);
                Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), true);
                drawable = new BitmapDrawable(getResources(), bitmap);
            } catch (Exception e) {
                drawable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (drawable == null) {
                    drawable = getResources().getDrawable(R.drawable.ic_person_b);
                }
                photo.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
            } catch (IllegalStateException e) {
                // thrown if activity is no more attached
                Log.w(AppConstants.TAG, "AlbumLoaderTask: " + e.getLocalizedMessage());
            }
        }

    }
	
}
