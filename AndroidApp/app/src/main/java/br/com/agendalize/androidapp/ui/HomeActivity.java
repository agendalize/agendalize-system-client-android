package br.com.agendalize.androidapp.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.LongSparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.model.Store;
import br.com.agendalize.androidapp.model.StoreSearchRequest;
import br.com.agendalize.androidapp.ui.fragment.AboutFragment;
import br.com.agendalize.androidapp.ui.fragment.FavoriteFragment;
import br.com.agendalize.androidapp.ui.fragment.FeedbackFragment;
import br.com.agendalize.androidapp.ui.fragment.MyScheduleFragment;
import br.com.agendalize.androidapp.ui.fragment.RecommendStoreFragment;
import br.com.agendalize.androidapp.ui.fragment.StoreFilterFragment;
import br.com.agendalize.androidapp.ui.fragment.StoreListFragment;
import br.com.agendalize.androidapp.ui.fragment.StoreMapFragment;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //===================================================================================
    // Fields
    //===================================================================================

    // views
    private boolean isOfflineEnabled = false;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private Menu menu;
    // view controls
    private int selectedDrawerIndex;
    private boolean isListViewMode;
    private boolean isFilterViewEnabled;
    private boolean newFilteredSearch;
    // used by filter
    private double myLocation[];
    private LongSparseArray<Drawable> photos;
    private List<Store> stores;
    // fragments
    private StoreListFragment storeList;
    private StoreMapFragment storeMap;
    private MyScheduleFragment mySchedule;
    private FavoriteFragment favorite;
    private FeedbackFragment feedback;
    private RecommendStoreFragment recommendStore;
    private AboutFragment about;
    private StoreFilterFragment storeFilter;
    private int fragmentIndexToStartAt;

    //===================================================================================
    // Override methods
    //===================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        try {
            fragmentIndexToStartAt = getIntent().getIntExtra(AppConstants.FragmentIndexToStartAt, 0);
        } catch (Exception e) {
            fragmentIndexToStartAt = 0;
        }

        storeList = new StoreListFragment();
        storeMap = new StoreMapFragment();
        mySchedule = new MyScheduleFragment();
        favorite = new FavoriteFragment();
        feedback = new FeedbackFragment();
        recommendStore = new RecommendStoreFragment();
        about = new AboutFragment();
        storeFilter = new StoreFilterFragment();

        SharedPreferences prefsPrivate = HomeActivity.this.getSharedPreferences(
                AppConstants.SHARED_PREFS, Context.MODE_PRIVATE);
        myLocation = new double[2];
        myLocation[0] = prefsPrivate.getFloat(AppConstants.LATITUDE, .0f);
        myLocation[1] = prefsPrivate.getFloat(AppConstants.LONGITUDE, .0f);


        toolbar = (Toolbar) findViewById(R.id.activity_home_toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.activity_home_drawerlyt);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        navigationView = (NavigationView) findViewById(R.id.activity_home_navview);
        photos = new LongSparseArray<Drawable>();
        stores = new ArrayList<Store>();

        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        isListViewMode = true;
        isFilterViewEnabled = false;
        newFilteredSearch = false;
        selectItem(fragmentIndexToStartAt);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_home_drawerlyt);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (selectedDrawerIndex != 0) {
            selectItem(0);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        if (menu != null) {
            Fragment cur = getSupportFragmentManager().findFragmentById(R.id.activity_home_contentframe);
            if (cur instanceof StoreListFragment || cur instanceof  StoreMapFragment) {
                navigationView.setCheckedItem(R.id.nav_searchandschedule);
                menu.getItem(0).setVisible(false);
                if (isListViewMode) {
                    menu.getItem(1).setVisible(true);
                    menu.getItem(2).setVisible(false);
                } else {
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(true);
                }
                menu.getItem(3).setVisible(false);
            } else if (cur instanceof  MyScheduleFragment) {
                navigationView.setCheckedItem(R.id.nav_myschedule);
                menu.getItem(0).setVisible(true);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(false);
                menu.getItem(3).setVisible(false);
            } else if (cur instanceof FavoriteFragment) {
                navigationView.setCheckedItem(R.id.nav_favorites);
                menu.getItem(0).setVisible(true);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(false);
                menu.getItem(3).setVisible(false);
            } else if (cur instanceof RecommendStoreFragment) {
                navigationView.setCheckedItem(R.id.nav_recommend);
                menu.getItem(0).setVisible(true);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(false);
                menu.getItem(3).setVisible(false);

            // share fragment

            } else if (cur instanceof FeedbackFragment) {
                navigationView.setCheckedItem(R.id.nav_feedback);
                menu.getItem(0).setVisible(true);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(false);
                menu.getItem(3).setVisible(false);
            } else if (cur instanceof AboutFragment) {
                navigationView.setCheckedItem(R.id.nav_about);
                menu.getItem(0).setVisible(true);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(false);
                menu.getItem(3).setVisible(false);
            } else {
                navigationView.setCheckedItem(R.id.nav_searchandschedule);
                menu.getItem(0).setVisible(false);
                if (isListViewMode) {
                    menu.getItem(1).setVisible(true);
                    menu.getItem(2).setVisible(false);
                } else {
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(true);
                }
                menu.getItem(3).setVisible(false);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_back:
                selectItem(0);
                break;
            case R.id.action_mapview:
                isListViewMode = false;
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(true);
                selectItem(0);
                break;
            case R.id.action_listview:
                isListViewMode = true;
                menu.getItem(1).setVisible(true);
                menu.getItem(2).setVisible(false);
                selectItem(0);
                break;
            case R.id.action_send:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_searchandschedule:
                if (isListViewMode) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, storeList).commit();
                    if (menu != null) {
                        menu.getItem(0).setVisible(false);
                        menu.getItem(1).setVisible(true);
                        menu.getItem(2).setVisible(false);
                        menu.getItem(3).setVisible(false);
                        setActionsEnabled(true);
                    }
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, storeMap).commit();
                    if (menu != null) {
                        menu.getItem(0).setVisible(false);
                        menu.getItem(1).setVisible(false);
                        menu.getItem(2).setVisible(true);
                        menu.getItem(3).setVisible(false);
                        setActionsEnabled(true);
                    }
                }
                setTitle(R.string.title_searchandschedule);
                navigationView.setCheckedItem(R.id.nav_searchandschedule);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 0;
                break;
            case R.id.nav_myschedule:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, mySchedule).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_myschedule);
                navigationView.setCheckedItem(R.id.nav_myschedule);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 1;
                break;
            case R.id.nav_favorites:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, favorite).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_favorite);
                navigationView.setCheckedItem(R.id.nav_favorites);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 2;
                break;
            case R.id.nav_settings:
                isFilterViewEnabled = false;
                selectedDrawerIndex = 3;
                break;
            case R.id.nav_recommend:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, recommendStore).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_recommend);
                navigationView.setCheckedItem(R.id.nav_recommend);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 4;
                break;
            case R.id.nav_share:
                isFilterViewEnabled = false;
                selectedDrawerIndex = 5;
                break;
            case R.id.nav_feedback:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, feedback).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_feedback);
                navigationView.setCheckedItem(R.id.nav_feedback);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 6;
                break;
            case R.id.nav_about:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, about).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_about);
                navigationView.setCheckedItem(R.id.nav_about);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 7;
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_home_drawerlyt);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //===================================================================================
    // General methods
    //===================================================================================

    public void requestFilteredSearch(ArrayList<Service> services, double[] latlon,
                                      int hourFrom, int hourTo, ArrayList<String> baseDates, int rangeIndex) {
        newFilteredSearch = true;
        stores.clear();
        selectItem(0);
        storeList.requestFilteredSearch(services, latlon, hourFrom, hourTo, baseDates, rangeIndex);
    }

    public void setActionsEnabled(boolean isEnabled) {
        for (int i=0; i<menu.size(); i++) {
            if (isEnabled) {
                menu.getItem(i).setEnabled(true);
            } else {
                menu.getItem(i).setEnabled(false);
            }
        }
    }

    public boolean isNewFilteredSearch() {
        newFilteredSearch = false;
        return newFilteredSearch;
    }

    public void goToMainMenu() {
        selectItem(0);
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    public double[] getLocation() {
        return myLocation;
    }

    public LongSparseArray<Drawable> getPhotos() {
        return photos;
    }

    public void setPhoto(long id, Drawable photo) {
        photos.put(id, photo);
    }

    public void setOffLineMode(boolean isOfflineEnabled) {
        this.isOfflineEnabled = isOfflineEnabled;
    }

    public StoreSearchRequest getStoreSearchRequest() {
        return storeList.getSearchParams();
    }

    // Swaps fragments in the main content view
    public void selectItem (int position) {
        switch (position) {
            case 0:
                if (isListViewMode) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, storeList).commit();
                    if (menu != null) {
                        menu.getItem(0).setVisible(false);
                        menu.getItem(1).setVisible(true);
                        menu.getItem(2).setVisible(false);
                        menu.getItem(3).setVisible(false);
                        setActionsEnabled(true);
                    }
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, storeMap).commit();
                    if (menu != null) {
                        menu.getItem(0).setVisible(false);
                        menu.getItem(1).setVisible(false);
                        menu.getItem(2).setVisible(true);
                        menu.getItem(3).setVisible(false);
                        setActionsEnabled(true);
                    }
                }
                setTitle(R.string.title_searchandschedule);
                navigationView.setCheckedItem(R.id.nav_searchandschedule);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 0;
                break;
            case 1:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, mySchedule).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_myschedule);
                navigationView.setCheckedItem(R.id.nav_myschedule);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 1;
                break;
            case 2:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, favorite).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_favorite);
                navigationView.setCheckedItem(R.id.nav_favorites);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 2;
                break;
            case 4:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, recommendStore).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_recommend);
                navigationView.setCheckedItem(R.id.nav_recommend);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 4;
                break;
            case 6:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, feedback).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_feedback);
                navigationView.setCheckedItem(R.id.nav_feedback);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 6;
                break;
            case 7:
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, about).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_about);
                navigationView.setCheckedItem(R.id.nav_about);
                isFilterViewEnabled = false;
                selectedDrawerIndex = 7;
                break;
            case 8: // store filter
                getSupportFragmentManager().beginTransaction().replace(R.id.activity_home_contentframe, storeFilter).commit();
                if (menu != null) {
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(false);
                    setActionsEnabled(true);
                }
                setTitle(R.string.title_filter);
                isFilterViewEnabled = true;
                selectedDrawerIndex = 8;
                break;
        }
        getSupportFragmentManager().executePendingTransactions();
    }

}
