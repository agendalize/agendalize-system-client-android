package br.com.agendalize.androidapp.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class EmployeeSchedule {

	private String date;
	private String codDate;
	private List<Employee> employees;
	
	public EmployeeSchedule() {
		date = "";
		codDate = "";
		employees = new ArrayList<>();
	}

	public EmployeeSchedule(String date, String codDate, List<Employee> employees) {
		this();
		this.date = date;
		this.codDate= codDate;
		this.employees = employees;
	}

	public EmployeeSchedule(JSONObject obj) {
		this();
		try {
            date = obj.getString("date");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        try {
            codDate = obj.getString("codDate");
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = obj.getJSONArray("employees");
			for (int i=0; i<arr.length(); i++) {
                employees.add(new Employee(arr.getJSONObject(i)));
			}
        } catch (Exception e) {
            //e.printStackTrace();
        }
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCodDate() {
		return codDate;
	}

	public void setCodDate(String codDate) {
		this.codDate = codDate;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
            obj.put("date", date);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
            obj.put("codDate", codDate);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		try {
			JSONArray arr = new JSONArray();
			for (int i=0; i<employees.size(); i++) {
				arr.put(employees.get(i).toJson());
			}
			obj.put("employees", arr);
        } catch (Exception e) {
            //e.printStackTrace();
        }
		return obj;
	}

}
