package br.com.agendalize.androidapp.ui.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.agendalize.androidapp.model.StoreSearchRequest;
import br.com.agendalize.androidapp.ui.BookingActivity;
import br.com.agendalize.androidapp.ui.HomeActivity;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Store;
import br.com.agendalize.androidapp.util.DBAdapter;
import br.com.agendalize.androidapp.util.ImageUtils;

/**
 * Created by mthama on 02/11/15.
 */
public class FavoriteFragment extends Fragment {

    //===================================================================================
    // Fields
    //===================================================================================

    private ListView storeList					= null;
    private BookmarkListAdapter adapter			= null;
    private TextView emptyLabel					= null;
    private TextView todeletetxt				= null;
    private List<Store> stores		= null;
    private HomeActivity context                = null;

    //===================================================================================
    // Override methods
    //===================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_favorite, container, false);
        context = (HomeActivity)getActivity();
        stores = new DBAdapter(context).selectAll(null);

        storeList = (ListView) rootView.findViewById(R.id.frag_favorite_list);
        emptyLabel = (TextView) rootView.findViewById(R.id.frag_favorite_empty);
        todeletetxt = (TextView) rootView.findViewById(R.id.frag_favorite_todeletetxt);

        /*getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_bookmark));
        getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.ic_star_w));*/

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (stores.size() == 0) {
            emptyLabel.setVisibility(View.VISIBLE);
            storeList.setVisibility(View.GONE);
            todeletetxt.setVisibility(View.GONE);
        } else {
            emptyLabel.setVisibility(View.GONE);
            storeList.setVisibility(View.VISIBLE);
            todeletetxt.setVisibility(View.VISIBLE);
            adapter = new BookmarkListAdapter();
            storeList.setAdapter(adapter);
            storeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                    Intent newApp = new Intent(context, BookingActivity.class);
                    newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    newApp.putExtra("storeJsonString", stores.get(position).toJson().toString());
                    newApp.putExtra("requestJsonString", getSearchParams().toJson().toString());
                    context.startActivity(newApp);
                }
            });
            storeList.setLongClickable(true);
            storeList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View parent, final int position, long id) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.msg_removebookmark)).setCancelable(true).
                            setPositiveButton(getResources().getString(R.string.action_yes),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog, final int id) {
                                            (new DBAdapter(context)).delete(stores.get(position));
                                            stores = new DBAdapter(context).selectAll(null);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                            ).setNegativeButton(getResources().getString(R.string.action_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {
                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();
                    return true;
                }
            });
            adapter.notifyDataSetChanged();
        }
    }

    private StoreSearchRequest getSearchParams() {
        // construct the json request
        StoreSearchRequest request = new StoreSearchRequest();
        request.setListOffSet(0);
        // looking at user current location
        request.setLatitude(context.getLocation()[0]);
        request.setLongitude(context.getLocation()[1]);
        Calendar dayTm = Calendar.getInstance();
        final String dateStr = ""+dayTm.get(Calendar.YEAR)+dayTm.get(Calendar.MONTH)+dayTm.get(Calendar.DAY_OF_MONTH);
        request.setBaseDates(new ArrayList<String>() {{
            add(dateStr);
        }});
        request.setHourFrom("0000");
        request.setHourTo("2359");
        request.setRange(10000);
        return request;
    }

    //===================================================================================
    // Inner classes
    //===================================================================================

    public class BookmarkListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return stores.size();
        }

        @Override
        public Object getItem(int position) {
            return stores.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_storeitem, parent, false);
            }
            ((TextView) convertView.findViewById(R.id.row_storeitem_storename)).setText(
                    stores.get(position).getName() + ""
            );
            ((RatingBar) convertView.findViewById(R.id.row_storeitem_storerating)).setRating(
                    stores.get(position).getEvaluation()
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_numemployees)).setText(
                    stores.get(position).getEmployees().size() + ""
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_numservices)).setText(
                    stores.get(position).getListServices().size() + ""
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_citystate)).setText(
                    stores.get(position).getAddressInfo().getCity() + ", " +
                            stores.get(position).getAddressInfo().getState()
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_streetname)).setText(
                    stores.get(position).getAddressInfo().getAddress() + " " +
                            (stores.get(position).getAddressInfo().getComplement().equals("null") ? "" :
                                    ", " + stores.get(position).getAddressInfo().getComplement())
            );
            if (context.getPhotos().get(stores.get(position).getId()) != null) {
                ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageDrawable(context.getPhotos().get(stores.get(position).getId()));
                ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
            } else {
                if (stores.get(position).getAlbum().get(0) != null) {
                    if (stores.get(position).getAlbum().get(0).length() > 0) {
                        new PhotoLoader(
                                ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)),
                                ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)),
                                stores.get(position).getId(),
                                stores.get(position).getAlbum().get(0)).execute();
                    } else {
                        ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageResource(R.drawable.noimage);
                        ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
                        context.setPhoto(stores.get(position).getId(), context.getResources().getDrawable(R.drawable.noimage));
                    }
                } else {
                    ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageResource(R.drawable.noimage);
                    ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
                    context.setPhoto(stores.get(position).getId(), context.getResources().getDrawable(R.drawable.noimage));
                }
            }
            return convertView;
        }

    }

    private class PhotoLoader extends AsyncTask<Void, Void, Void> {

        private Drawable drawable;
        private ProgressBar progress;
        private ImageView photo;
        private long id;
        private String url;

        public PhotoLoader(ImageView photo, ProgressBar progress, long id, String url) {
            this.photo = photo;
            this.progress = progress;
            this.id = id;
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                drawable = ImageUtils.loadImageFromWebOperations(url);
                Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
                drawable = new BitmapDrawable(context.getResources(), bitmap);
            } catch (Exception e) {
                drawable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progress.setVisibility(View.GONE);
            if (drawable == null) {
                drawable = context.getResources().getDrawable(R.drawable.noimage);
            }
            photo.setImageDrawable(drawable);
            context.setPhoto(id, drawable);
        }

    }

}
