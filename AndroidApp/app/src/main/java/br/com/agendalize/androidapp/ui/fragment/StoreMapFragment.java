package br.com.agendalize.androidapp.ui.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.model.Store;
import br.com.agendalize.androidapp.ui.BookingActivity;
import br.com.agendalize.androidapp.ui.HomeActivity;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.geo.LocationUtils;
import br.com.agendalize.androidapp.ui.view.MapWrapperLayout;
import br.com.agendalize.androidapp.ui.view.OnInfoWindowElemTouchListener;

public class StoreMapFragment extends Fragment implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
        GoogleMap.OnMapLongClickListener {

    private final Object lock = new Object();
    private boolean isLoading = false;
    private HomeActivity context;
    private GoogleMap map;
    private LatLng currentLocation;
    private LocationRequest mLocationRequest;
    private LocationClient mLocationClient;

    private ViewGroup infoWindow;
    private TextView infoTitle;
    private TextView infoSnippet;
    private Button infoButton;
    private OnInfoWindowElemTouchListener infoButtonListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storemap, container, false);
        context = (HomeActivity)getActivity();

        final MapWrapperLayout mapWrapperLayout = (MapWrapperLayout)rootView.findViewById(R.id.map_relative_layout);
        map = ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.frag_storemap_mapview)).getMap();
        map.getUiSettings().setZoomControlsEnabled(false);
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng point) {
                if (!isLoading) {
                    Location location = new Location("");
                    location.setLatitude(point.latitude);
                    location.setLongitude(point.longitude);
                    onLocationChanged(location);
                }
            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.frag_storemap_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        mapWrapperLayout.init(map, getPixelsFromDp(context, 39 + 20));

        this.infoWindow = (ViewGroup)inflater.inflate(R.layout.helper_infowindow, container, false);
        this.infoTitle = (TextView)infoWindow.findViewById(R.id.helper_infowindow_title);
        this.infoSnippet = (TextView)infoWindow.findViewById(R.id.helper_infowindow_snippet);
        this.infoButton = (Button)infoWindow.findViewById(R.id.helper_infowindow_button);
        this.infoButtonListener = new OnInfoWindowElemTouchListener(infoButton,
                getResources().getDrawable(R.color.normalBlue),
                getResources().getDrawable(R.color.darkBlue)) {
            @Override
            protected void onClickConfirmed(View v, Marker marker) {
                try {
                    Store s = new Store(new JSONObject(marker.getSnippet()));
                    Intent newApp = new Intent(context, BookingActivity.class);
                    newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    newApp.putExtra("storeJsonString", s.toJson().toString());
                    newApp.putExtra("requestJsonString", context.getStoreSearchRequest().toJson().toString());
                    context.startActivity(newApp);
                    context.finish();
                } catch (Exception e) {
                    Toast.makeText(context, R.string.msg_notpossibletoload, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        };
        this.infoButton.setOnTouchListener(infoButtonListener);

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                if (marker.getTitle() == null) return null;
                infoTitle.setText(marker.getTitle());
                try {
                    Store s = new Store(new JSONObject(marker.getSnippet()));
                    infoSnippet.setText(
                            s.getAddressInfo().getAddress().length() < 20 ?
                                    s.getAddressInfo().getAddress() :
                                    s.getAddressInfo().getAddress().substring(0, 20) + "..."
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
                infoButtonListener.setMarker(marker);
                mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow);
                return infoWindow;
            }
        });

        return rootView;
    }

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }

    @Override
    public void onStart() {
        super.onStart();
        // we need to check if we have location services, or this feature should not be available
        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}
        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(context.getResources().getString(R.string.msg_locationserviceabsent));
            dialog.setPositiveButton(context.getResources().getString(R.string.action_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                }
            });
            dialog.show();
        } else {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
            mLocationRequest.setSmallestDisplacement(LocationUtils.MIN_DISTANCE_CHANGE_FOR_UPDATES);
            mLocationClient = new LocationClient(getActivity(), this, this);
            mLocationClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mLocationClient != null) {
            if (mLocationClient.isConnected()) {
                mLocationClient.disconnect();
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng point) {
        currentLocation = point;
        setLocation(point);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(AppConstants.TAG, "Location Services connected");
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }

    @Override
    public void onDisconnected() {
        Log.d(AppConstants.TAG, "Location Services disconnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(AppConstants.TAG, "onConnectionFailed");
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                if (e != null) e.printStackTrace();
            }
        } else {
            Log.i(AppConstants.TAG, "Location: no resolution is available");
        }
    }

    @Override
    synchronized public void onLocationChanged(Location location) {
        synchronized (lock) {
            if (!isLoading) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(location.getLatitude(), location.getLongitude()), 13));
                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                setLocation(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        }
    }

    private void loadStores() {
        for (Store store : context.getStores()) {
            try {
                map.addMarker(new MarkerOptions()
                        .snippet(store.toJson().toString())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher))
                        .title(store.getName())
                        .position(new LatLng(store.getAddressInfo().getLatitude(), store.getAddressInfo().getLongitude())));
            } catch (NumberFormatException e) {
                Log.w(AppConstants.TAG, e.getLocalizedMessage());
            }
        }
    }

    private void setLocation(LatLng latLong) {
        map.clear();
        map.addMarker(new MarkerOptions().position(latLong));
        map.addCircle(new CircleOptions().center(latLong).radius(1000)
                .strokeColor(Color.RED).strokeWidth(1.0f)
                .fillColor(0x33D4223B));
        loadStores();
    }

}
