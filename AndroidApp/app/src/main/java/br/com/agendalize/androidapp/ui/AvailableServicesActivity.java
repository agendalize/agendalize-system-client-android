package br.com.agendalize.androidapp.ui;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.LongSparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.util.ImageUtils;

public class AvailableServicesActivity extends AppCompatActivity {

    private LongSparseArray<Drawable> photos;
	public static List<Service> services;

    private Toolbar activity_availableservices_toolbar;
	private ServicesAdapter servicesAdapter;
	private AutoCompleteTextView activity_availableservices_autocompleteedit;
	private ImageButton activity_availableservices_cleartxtbtn;
	private Button activity_availableservices_finishbtn;
	private ListView activity_availableservices_listview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_availableservices);

        photos = new LongSparseArray<Drawable>();
        activity_availableservices_toolbar = (Toolbar) findViewById(R.id.activity_availableservices_toolbar);
		activity_availableservices_autocompleteedit = (AutoCompleteTextView) findViewById(R.id.activity_availableservices_autocompleteedit);
		activity_availableservices_cleartxtbtn = (ImageButton) findViewById(R.id.activity_availableservices_cleartxtbtn);
		activity_availableservices_listview = (ListView) findViewById(R.id.activity_availableservices_listview);
		activity_availableservices_finishbtn = (Button) findViewById(R.id.activity_availableservices_finishbtn);
		
		activity_availableservices_finishbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int[] selecteds = servicesAdapter.getSelected();
				JSONArray arr = new JSONArray();
				for (int i=3; i>=0; i--) {
					if (selecteds[i] != -1) arr.put(services.get(selecteds[i]).toJson());
				}
				Intent returnIntent = new Intent();
				returnIntent.putExtra("selecteds", arr.toString());
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});

        setSupportActionBar(activity_availableservices_toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.label_services));
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		servicesAdapter = new ServicesAdapter(AvailableServicesActivity.this, services);
		activity_availableservices_listview.setAdapter(servicesAdapter);
		activity_availableservices_listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
		    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
				servicesAdapter.setSelected(position);
			}
		});
		activity_availableservices_listview.setFastScrollEnabled(true);
		
		setSearchEditFeatures();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.action_back: finish(); break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void setSearchEditFeatures() {
		if (activity_availableservices_autocompleteedit.getText().length() == 0)
            activity_availableservices_cleartxtbtn.setVisibility(View.INVISIBLE);
		else
            activity_availableservices_cleartxtbtn.setVisibility(View.VISIBLE);
        activity_availableservices_cleartxtbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity_availableservices_autocompleteedit.setText("");
				activity_availableservices_cleartxtbtn.setVisibility(View.INVISIBLE);
			}
		});
		activity_availableservices_autocompleteedit.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {
				if (activity_availableservices_autocompleteedit.getText().length() >= 1) {
                    activity_availableservices_cleartxtbtn.setVisibility(View.VISIBLE);
					// set filter service list
					List<Service> filtered = new ArrayList<Service>();
					for (int i=0; i<services.size(); i++) {
						if (services.get(i).getShortName().contains(activity_availableservices_autocompleteedit.getText()))
							filtered.add(services.get(i));
					}
					servicesAdapter = new ServicesAdapter(AvailableServicesActivity.this, filtered);
					activity_availableservices_listview.setAdapter(servicesAdapter);
					activity_availableservices_listview.setOnItemClickListener(new OnItemClickListener() {
						@Override
					    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
							servicesAdapter.setSelected(position);
						}
					});
					activity_availableservices_listview.setFastScrollEnabled(true);
				} else if (activity_availableservices_autocompleteedit.getText().length() == 0) {
					activity_availableservices_cleartxtbtn.setVisibility(View.INVISIBLE);
					// set default service list
					servicesAdapter = new ServicesAdapter(AvailableServicesActivity.this, services);
					activity_availableservices_listview.setAdapter(servicesAdapter);
					activity_availableservices_listview.setOnItemClickListener(new OnItemClickListener() {
						@Override
					    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
							servicesAdapter.setSelected(position);
						}
					});
					activity_availableservices_listview.setFastScrollEnabled(true);
				}
			}
		});
		activity_availableservices_autocompleteedit.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {}
		});
		activity_availableservices_autocompleteedit.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					return true;
			    } else if (keyCode == 66) {}
				return false;
			}
		});
	}
	
	//===================================================================================
	// Inner classes
	//===================================================================================
	
	private class ServicesAdapter extends BaseAdapter {
		
		private Context context;
		private List<Service> services;
		private int selected1;
		private int selected2;
		private int selected3;
		private int selected4;
		
		public ServicesAdapter(Context context, List<Service> services) {
			this.context = context;
			this.services = services;
			this.selected1 = -1;
			this.selected2 = -1;
			this.selected3 = -1;
			this.selected4 = -1;
		}
		
		public int[] getSelected() {
			int[] s = {selected1, selected2, selected3, selected4};
			return s;
		}
		
		public void setSelected(int selected) {
			if (selected4 == selected) {
				selected4 = -1;
			} else if (selected3 == selected) {
				selected3 = selected4;
				selected4 = -1;
			} else if (selected2 == selected) {
				selected2 = selected3;
				selected3 = selected4;
				selected4 = -1;
			} else if (selected1 == selected) {
				selected1 = selected2;
				selected2 = selected3;
				selected3 = selected4;
				selected4 = -1;
			} else {
				selected4 = selected3;
				selected3 = selected2;
				selected2 = selected1;
				selected1 = selected;
			}
			notifyDataSetChanged();
		}
		
		@Override
		public int getCount() {
			return services.size();
		}
		
		@Override
		public Object getItem(int position) {
			return services.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// pre-defines the view as an inflated layout
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.row_serviceitem, parent, false);
			}
			ImageView photo = ((ImageView) convertView.findViewById(R.id.row_serviceitem_photo));
			TextView title = ((TextView) convertView.findViewById(R.id.row_serviceitem_name));
			TextView description = ((TextView) convertView.findViewById(R.id.row_serviceitem_description));
			CheckBox isselected = ((CheckBox) convertView.findViewById(R.id.row_serviceitem_isselected));
			
			title.setText(services.get(position).getShortName());
			description.setText(services.get(position).getDescription());
			if (selected1 == position || selected2 == position || selected3 == position || selected4 == position)
				isselected.setChecked(true);
			else
				isselected.setChecked(false);

            if (photos.get(services.get(position).getCodTypeService()) != null) {
                ((ImageView) convertView.findViewById(R.id.row_serviceitem_photo)).setImageDrawable(
                        photos.get(services.get(position).getCodTypeService()
                ));
                ((ProgressBar) convertView.findViewById(R.id.row_serviceitem_photoholder)).setVisibility(View.GONE);
            } else {
                if (services.get(position).getUrlImage() != null) {
                    if (services.get(position).getUrlImage().length() > 0) {
                        new PhotoLoader(
                                ((ImageView) convertView.findViewById(R.id.row_serviceitem_photo)),
                                ((ProgressBar) convertView.findViewById(R.id.row_serviceitem_photoholder)),
                                services.get(position).getCodTypeService(),
                                services.get(position).getUrlImage()).execute();
                    } else {
                        ((ImageView) convertView.findViewById(R.id.row_serviceitem_photo)).setImageResource(R.drawable.noimage);
                        ((ProgressBar) convertView.findViewById(R.id.row_serviceitem_photoholder)).setVisibility(View.GONE);
                        photos.put(services.get(position).getCodTypeService(), context.getResources().getDrawable(R.drawable.noimage));
                    }
                } else {
                    ((ImageView) convertView.findViewById(R.id.row_serviceitem_photo)).setImageResource(R.drawable.noimage);
                    ((ProgressBar) convertView.findViewById(R.id.row_serviceitem_photoholder)).setVisibility(View.GONE);
                    photos.put(services.get(position).getCodTypeService(), context.getResources().getDrawable(R.drawable.noimage));
                }
            }
			return convertView;
		}
		
	}

    //===================================================================================
    // Inner classes
    //===================================================================================

    private class PhotoLoader extends AsyncTask<Void, Void, Void> {

        private Drawable drawable;
        private ImageView photo;
        private ProgressBar progress;
        private long id;
        private String url;

        public PhotoLoader(ImageView photo, ProgressBar progress, long id, String url) {
            this.photo = photo;
            this.progress = progress;
            this.id = id;
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                drawable = ImageUtils.loadImageFromWebOperations(url);
                Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
                drawable = new BitmapDrawable(getResources(), bitmap);
            } catch (Exception e) {
                drawable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progress.setVisibility(View.GONE);
            if (drawable == null) {
                drawable = getResources().getDrawable(R.drawable.noimage);
            }
            photo.setImageDrawable(drawable);
            photos.put(id, drawable);
        }

    }
	
}
