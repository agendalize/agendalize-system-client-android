package br.com.agendalize.androidapp.ui;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.ui.view.RangeSeekBar;

public class ChooseDateHourActivity extends AppCompatActivity {

    private Toolbar activity_choosedatehour_toolbar;
	private TextView fromHour;
	private TextView toHour;
	private CalendarView calendar;
	private long curDate = 0;
	private int from = 0;
	private int to = 86399;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choosedatehour);

        activity_choosedatehour_toolbar = (Toolbar) findViewById(R.id.activity_choosedatehour_toolbar);
		fromHour = (TextView) findViewById(R.id.activity_datehour_from);
		toHour = (TextView) findViewById(R.id.activity_datehour_to);
		calendar = (CalendarView) findViewById(R.id.activity_datehour_calendar);
		
		try {
			from = getIntent().getIntExtra("fromHour", 0);
			to = getIntent().getIntExtra("toHour", 86399);
			calendar.setDate(getIntent().getLongExtra("dayTm", Calendar.getInstance().getTime().getTime()), true, false);
		} catch (Exception e) {
			Log.w(AppConstants.TAG, e.getLocalizedMessage());
			finish();
		}

        setSupportActionBar(activity_choosedatehour_toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.label_chosedayhour));
	}
	
	@SuppressLint("SimpleDateFormat") @Override
	protected void onResume() {
		super.onResume();

		try {
			// create RangeSeekBar as Integer range between 20 and 75
			RangeSeekBar<Integer> seekBar = new RangeSeekBar<Integer>(0, 86399, this);
			seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
			        @Override
			        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
			            // handle changed range values
			        	from = minValue;
			        	to = maxValue;
			        	fromHour.setText(String.format("%02d", (minValue/3600))+":"+String.format("%02d",(minValue%60)));
			        	toHour.setText(String.format("%02d", (maxValue/3600))+":"+String.format("%02d",(maxValue%60)));
			        }
			});
			seekBar.setSelectedMinValue(from);
			seekBar.setSelectedMaxValue(to);
			fromHour.setText(String.format("%02d", (from/3600))+":"+String.format("%02d",(from%60)));
        	toHour.setText(String.format("%02d", (to/3600))+":"+String.format("%02d",(to%60)));
			// add RangeSeekBar to pre-defined layout
			ViewGroup layout = (ViewGroup) findViewById(R.id.activity_datehour);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
			        LayoutParams.MATCH_PARENT,
			        LayoutParams.WRAP_CONTENT
			);
			params.setMargins(8, 0, 8, 172);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			seekBar.setLayoutParams(params);
			layout.addView(seekBar);
		} catch (Exception e) {}
		
		calendar.setOnDateChangeListener(new OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
            	GregorianCalendar gc = new GregorianCalendar(year, month, dayOfMonth);
            	curDate = gc.getTimeInMillis();
            }
        });
		
		((Button) findViewById(R.id.activity_datehour_finishbtn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent returnIntent = new Intent();
				returnIntent.putExtra("dayTm", curDate);
				returnIntent.putExtra("fromHour", from);
				returnIntent.putExtra("toHour", to);
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.onlyback, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.action_back:
                finish();
                break;
			default:;
		}
		return super.onOptionsItemSelected(item);
	}

}
