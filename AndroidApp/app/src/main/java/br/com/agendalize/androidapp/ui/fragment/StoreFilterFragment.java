package br.com.agendalize.androidapp.ui.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpStatus;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;

import br.com.agendalize.androidapp.ui.HomeActivity;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.net.RESTClient;
import br.com.agendalize.androidapp.util.DeviceUtils;
import br.com.agendalize.androidapp.ui.AvailableServicesActivity;
import br.com.agendalize.androidapp.ui.ChooseDateHourActivity;

public class StoreFilterFragment extends Fragment {

    //===================================================================================
    // Fields
    //===================================================================================

    private static final int DATEHOUR_RETURN_VALUE = 0;
    private static final int SERVICE_RETURN_VALUE = 1;

    // used to manage lazy load pages
    private static Object lock = new Object();
    private static boolean isLoading = false;
    private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;

    private static String baseAddress;
    private static double[] latlon;
    private static int rangeIndex;
    private static ArrayList<String> baseDates;
    private static int hourFrom;
    private static int hourTo;
    private static Calendar dayTm;
    private static ArrayList<Service> services;
    private static HomeActivity activity;

    private ScrollView frag_storefilter_scroll;
    private EditText frag_storefilter_locationEdit;
    private Spinner frag_storefilter_rangecb;
    private ImageButton frag_storefilter_service_reloadbtn;
    private ProgressBar frag_storefilter_service_progress;
    private Button frag_storefilter_datehourbtn;
    private Button frag_storefilter_service1btn;
    private Button frag_storefilter_service2btn;
    private Button frag_storefilter_service3btn;
    private Button frag_storefilter_service4btn;
    private FloatingActionButton fab;

    @SuppressLint("HandlerLeak") // TODO: verify this
    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (waitDialog != null) {
                waitDialog.dismiss();
                waitDialog = null;
            }
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                    DeviceUtils.justShowDialogWarning(activity, R.string.msg_timeout, false);
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                    DeviceUtils.justShowDialogWarning(activity, R.string.msg_noconnection, false);
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                    DeviceUtils.justShowDialogWarning(activity, R.string.msg_servererror, false);
                    break;
                case HttpStatus.SC_OK:
                    activity.requestFilteredSearch(services, latlon, hourFrom, hourTo, baseDates, rangeIndex);
                    break;
            }
            synchronized(lock) {
                isLoading = false;
            }
        }
    };

    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(activity)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                if (baseAddress != null && baseAddress.length() > 0) {
                    RESTClient client = new RESTClient(activity);
                    latlon = client.getLocationInfo(baseAddress);
                    handler.sendEmptyMessage(client.getStatus());
                } else {
                    latlon = new double[] {-1,-1};
                    handler.sendEmptyMessage(HttpStatus.SC_OK);
                }
            }
        }
    };

    //===================================================================================
    // Override methods
    //===================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storefilter, container, false);
        activity = (HomeActivity)getActivity();

        frag_storefilter_scroll = (ScrollView) rootView.findViewById(R.id.frag_storefilter_scroll);
        frag_storefilter_locationEdit = (EditText) rootView.findViewById(R.id.frag_storefilter_locationEdit);
        frag_storefilter_rangecb = (Spinner) rootView.findViewById(R.id.frag_storefilter_rangecb);
        frag_storefilter_datehourbtn = (Button) rootView.findViewById(R.id.frag_storefilter_datehourbtn);
        frag_storefilter_service_reloadbtn = (ImageButton) rootView.findViewById(R.id.frag_storefilter_service_reloadbtn);
        frag_storefilter_service_progress = (ProgressBar) rootView.findViewById(R.id.frag_storefilter_service_progress);
        frag_storefilter_service1btn = (Button) rootView.findViewById(R.id.frag_storefilter_service1btn);
        frag_storefilter_service2btn = (Button) rootView.findViewById(R.id.frag_storefilter_service2btn);
        frag_storefilter_service3btn = (Button) rootView.findViewById(R.id.frag_storefilter_service3btn);
        frag_storefilter_service4btn = (Button) rootView.findViewById(R.id.frag_storefilter_service4btn);
        fab = (FloatingActionButton) rootView.findViewById(R.id.frag_storefilter_fab);

        // actions for navigation
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewLoader();
            }
        });

        getDefaultValuesForFields();
        // load list of available services
        if (AvailableServicesActivity.services == null) {
            new ServiceLoader().execute();
        } else if (AvailableServicesActivity.services.size() == 0) {
            new ServiceLoader().execute();
        } else {
            frag_storefilter_service1btn.setEnabled(true);
            frag_storefilter_service2btn.setEnabled(true);
            frag_storefilter_service3btn.setEnabled(true);
            frag_storefilter_service4btn.setEnabled(true);
            frag_storefilter_service_progress.setVisibility(View.GONE);
        }
        drawWidgets();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (DATEHOUR_RETURN_VALUE): {
                if (resultCode == HomeActivity.RESULT_OK) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(data.getLongExtra("dayTm", Calendar.getInstance().getTimeInMillis()));
                    dayTm = calendar;
                    baseDates.clear();
                    baseDates.add((new SimpleDateFormat("yyyyMMdd")).format(dayTm.getTime()));
                    hourFrom = data.getIntExtra("fromHour", 0);
                    hourTo = data.getIntExtra("toHour", 86399);
                }
                break;
            }
            case (SERVICE_RETURN_VALUE): {
                if (resultCode == HomeActivity.RESULT_OK) {
                    try {
                        JSONArray arr = new JSONArray(data.getStringExtra("selecteds"));
                        services.clear();
                        for (int i=0; i<arr.length(); i++) {
                            services.add(new Service(arr.getJSONObject(i)));
                        }
                    } catch (Exception e) {}
                }
                break;
            }
        }
        // refreshing the view
        drawWidgets();
    }

    //===================================================================================
    // General methods
    //===================================================================================

    private boolean isFieldsOk() {
        if (services != null) if (services.size() > 0) return true;
        DeviceUtils.justShowDialogWarning(activity, R.string.msg_selectatleast1service, false);
        return false;
    }

    private void drawWidgets() {
        // location address data, need to be in onResume to proper update on activityForResult return
        frag_storefilter_locationEdit.setText(baseAddress);
        frag_storefilter_locationEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {
                baseAddress = frag_storefilter_locationEdit.getText().toString();
            }
        });
        // range, need to be in onResume to proper update on activityForResult return
        ArrayAdapter<CharSequence> spinnerArrayAdapter = new ArrayAdapter<CharSequence>(
                activity, android.R.layout.simple_spinner_item, new CharSequence[] {
                getText(R.string.label_distanceby10km),
                getText(R.string.label_distanceby25km),
                getText(R.string.label_distanceby50km) });
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        frag_storefilter_rangecb.setAdapter(spinnerArrayAdapter);
        frag_storefilter_rangecb.setSelection(rangeIndex);
        frag_storefilter_rangecb.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int selectedItem, long arg3) {
                rangeIndex = selectedItem;
            }
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
        // date and hour, need to be in onResume to proper update on activityForResult return
        frag_storefilter_datehourbtn.setText(
                String.format("%02d", dayTm.get(Calendar.DAY_OF_MONTH))
                        + "/" + String.format("%02d", (dayTm.get(Calendar.MONTH) + 1))
                        + "/" + String.format("%04d", dayTm.get(Calendar.YEAR))
                        + " - " + getResources().getString(R.string.label_fromhour)
                        + " " + String.format("%02d", (hourFrom / 3600)) + ":" + String.format("%02d", (hourFrom % 60))
                        + " " + getResources().getString(R.string.label_tohour)
                        + " " + String.format("%02d", (hourTo / 3600)) + ":" + String.format("%02d", (hourTo % 60))
        );
        frag_storefilter_datehourbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, ChooseDateHourActivity.class);
                i.putExtra("dayTm", dayTm.getTime().getTime());
                i.putExtra("fromHour", hourFrom);
                i.putExtra("toHour", hourTo);
                startActivityForResult(i, DATEHOUR_RETURN_VALUE);
            }
        });
        // selected services, need to be in onResume to proper update on activityForResult return
        if (services.size() > 0) frag_storefilter_service1btn.setText(services.get(0).getShortName());
        else frag_storefilter_service1btn.setText(getResources().getString(R.string.label_noservice));
        frag_storefilter_service1btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 1) frag_storefilter_service2btn.setText(services.get(1).getShortName());
        else frag_storefilter_service2btn.setText(getResources().getString(R.string.label_noservice));
        frag_storefilter_service2btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 2) frag_storefilter_service3btn.setText(services.get(2).getShortName());
        else frag_storefilter_service3btn.setText(getResources().getString(R.string.label_noservice));
        frag_storefilter_service3btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        if (services.size() > 3) frag_storefilter_service4btn.setText(services.get(3).getShortName());
        else frag_storefilter_service4btn.setText(getResources().getString(R.string.label_noservice));
        frag_storefilter_service4btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(activity, AvailableServicesActivity.class), SERVICE_RETURN_VALUE);
            }
        });
        // scrolls to top
        frag_storefilter_scroll.scrollTo(0, 0);
        frag_storefilter_scroll.scrollTo(0, 0);
    }

    private void getDefaultValuesForFields() {
        baseAddress = "";
        rangeIndex = 0;
        dayTm = Calendar.getInstance();
        baseDates = new ArrayList<String>();
        baseDates.add((new SimpleDateFormat("yyyyMMdd")).format(dayTm.getTime()));
        hourFrom = 0;
        hourTo = 86399;
        services = new ArrayList<Service>();
    }

    synchronized private static void createNewLoader() {
        if (!isLoading) {
            synchronized(lock) {
                isLoading = true;
            }
            waitDialog = ProgressDialog.show(
                    activity,
                    activity.getText(R.string.msg_checklocation),
                    activity.getText(R.string.msg_pleasewait),
                    false, false);
            waitDialog.setIcon(R.drawable.ic_launcher);
            contentLoader = new Thread(loadContent);
            contentLoader.start();
        }
    }

    //===================================================================================
    // Inner classes
    //===================================================================================

    private class ServiceLoader extends AsyncTask<Void, Void, Void> {
        private RESTClient client = new RESTClient(getContext());
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            frag_storefilter_service1btn.setEnabled(false);
            frag_storefilter_service2btn.setEnabled(false);
            frag_storefilter_service3btn.setEnabled(false);
            frag_storefilter_service4btn.setEnabled(false);
            frag_storefilter_service_reloadbtn.setVisibility(View.GONE);
            frag_storefilter_service_progress.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            client = new RESTClient(getContext());
            AvailableServicesActivity.services = client.ServicesList();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (client.getStatus() == HttpStatus.SC_OK) {
                frag_storefilter_service1btn.setEnabled(true);
                frag_storefilter_service2btn.setEnabled(true);
                frag_storefilter_service3btn.setEnabled(true);
                frag_storefilter_service4btn.setEnabled(true);
            } else {
                frag_storefilter_service_reloadbtn.setVisibility(View.VISIBLE);
                frag_storefilter_service_reloadbtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frag_storefilter_service_reloadbtn.setVisibility(View.GONE);
                        new ServiceLoader().execute();
                    }
                });
            }
            frag_storefilter_service_progress.setVisibility(View.GONE);
        }
    }

}
