package br.com.agendalize.androidapp.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import org.apache.http.HttpStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.agendalize.androidapp.AppConstants;
import br.com.agendalize.androidapp.ui.HomeActivity;
import br.com.agendalize.androidapp.R;
import br.com.agendalize.androidapp.model.Service;
import br.com.agendalize.androidapp.model.Store;
import br.com.agendalize.androidapp.model.StoreSearchRequest;
import br.com.agendalize.androidapp.net.RESTClient;
import br.com.agendalize.androidapp.util.DeviceUtils;
import br.com.agendalize.androidapp.util.ImageUtils;
import br.com.agendalize.androidapp.ui.BookingActivity;

public class StoreListFragment extends Fragment {

    //===================================================================================
    // Fields
    //===================================================================================

    // used to manage lazy load pages
    private static Object lock = new Object();
    private static boolean isLoading = false;
    private static int lastLoadCount = -1;
    private static ProgressDialog waitDialog = null;
    private static Thread contentLoader = null;
    // views
    private static StoreListAdapter storeListAdapter;
    private static ListView frag_storelist_listview;
    private FloatingActionButton fab;
    // views controls
    private static HomeActivity context;
    // data for store fetch
    private static ArrayList<Service> services = null;
    private static String hourFrom = null;
    private static String hourTo = null;
    private static ArrayList<String> baseDates;
    private static double lat = 0;
    private static double lon = 0;
    private static long range = 0;

    @SuppressLint("HandlerLeak") // TODO: verify this
    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (waitDialog != null) {
                waitDialog.dismiss();
                waitDialog = null;
            }
            switch (msg.what) {
                case HttpStatus.SC_REQUEST_TIMEOUT:
                    if (context.getStores().size() != 0) {
                        DeviceUtils.justShowDialogWarning(context, R.string.msg_timeout, false);
                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getResources().getString(R.string.msg_offlinemode)).setCancelable(false).
                                setPositiveButton(context.getResources().getString(R.string.action_ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(final DialogInterface dialog, final int id) {
                                                context.setOffLineMode(true);
                                            }
                                        }
                                );
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                    break;
                case -1: case HttpStatus.SC_NOT_FOUND:
                    if (context.getStores().size() != 0) {
                        DeviceUtils.justShowDialogWarning(context, R.string.msg_noconnection, false);
                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getResources().getString(R.string.msg_offlinemode)).setCancelable(false).
                                setPositiveButton(context.getResources().getString(R.string.action_ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(final DialogInterface dialog, final int id) {
                                                context.setOffLineMode(true);
                                            }
                                        }
                                );
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                    if (context.getStores().size() != 0) {
                        DeviceUtils.justShowDialogWarning(context, R.string.msg_servererror, false);
                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getResources().getString(R.string.msg_offlinemode)).setCancelable(false).
                                setPositiveButton(context.getResources().getString(R.string.action_ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(final DialogInterface dialog, final int id) {
                                                context.setOffLineMode(true);
                                            }
                                        }
                                );
                        final AlertDialog alert = builder.create();
                        alert.show();
                    }
                    break;
                case HttpStatus.SC_OK:
                    if (context.getStores() != null) {
                        if (context.getStores().size() == 0) {
                            DeviceUtils.justShowDialogWarning(context, R.string.msg_noresults, false);
                        } else {
                            frag_storelist_listview.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(AbsListView view, int scrollState) {}
                                @Override
                                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    if (
                                        frag_storelist_listview.getLastVisiblePosition()+1 == totalItemCount && // user has reached the end of the list
                                        frag_storelist_listview.getAdapter().getCount() >= AppConstants.SIZE_LIST && // only enable pagination if we already done at least 1 load
                                        lastLoadCount == AppConstants.SIZE_LIST && // last load had fetch a complete page
                                        lastLoadCount != -1
                                    ) createNewLoader();
                                }
                            });
                            frag_storelist_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                                    Intent newApp = new Intent(context, BookingActivity.class);
                                    newApp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    newApp.putExtra("storeJsonString", context.getStores().get(arg2).toJson().toString());
                                    newApp.putExtra("requestJsonString", getSearchParams().toJson().toString());
                                    context.startActivity(newApp);
                                    context.finish();
                                }
                            });
                            storeListAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
            }
            synchronized(lock) {
                isLoading = false;
            }
        }
    };

    private static final Runnable loadContent = new Runnable() {
        @Override
        public void run() {
            if (!DeviceUtils.isConnectedToInternet(context)) {
                handler.sendEmptyMessage(HttpStatus.SC_NOT_FOUND);
            } else {
                if (context.isNewFilteredSearch()) {
                    Log.i(AppConstants.TAG, "A new filtered store search has been made: " + getSearchParams().toJson().toString());
                } else {

                }
                RESTClient client = new RESTClient(context);
                List<Store> newStores = client.StoresFilterBy(getSearchParams().toJson());
                // before add, remove possible "loading more" tags
                if (context.getStores().size() > 0) {
                    if (context.getStores().get(context.getStores().size()-1).getName().equals("LOAD_MORE")) {
                        context.getStores().remove(context.getStores().size() - 1);
                    }
                }
                // add new stores
                for (int i=0; i<newStores.size(); i++) {
                    context.getStores().add(newStores.get(i));
                }
                lastLoadCount = newStores.size();
                // add the informative item, if there is more contacts to load (last fetch returned SIZE_LIST)
                if (context.getStores().size() > 0 && lastLoadCount == AppConstants.SIZE_LIST) {
                    Store m = new Store();
                    m.setName("LOAD_MORE");
                    context.getStores().add(m);
                }
                handler.sendEmptyMessage(client.getStatus());
            }
        }
    };

    //===================================================================================
    // Override methods
    //===================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_storelist, container, false);
        context = (HomeActivity)getActivity();

        services = new ArrayList<Service>();
        baseDates = new ArrayList<String>();
        baseDates.add((new SimpleDateFormat("yyyyMMdd")).format(Calendar.getInstance().getTime()));
        hourFrom = "0000";
        hourTo = "2359";
        range = 10000;

        // widgets and components
        frag_storelist_listview = (ListView) rootView.findViewById(R.id.frag_storelist_listview);
        fab = (FloatingActionButton) rootView.findViewById(R.id.frag_storelist_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.selectItem(8);
            }
        });

        storeListAdapter = new StoreListAdapter();
        frag_storelist_listview.setAdapter(storeListAdapter);
        if (context.getStores().size() == 0 && !context.isNewFilteredSearch()) {
            createNewLoader();
        } else {
            handler.sendEmptyMessage(HttpStatus.SC_OK);
        }

        return rootView;
    }

    //===================================================================================
    // General methods
    //===================================================================================

    synchronized private static void createNewLoader() {
        if (!isLoading) {
            synchronized(lock) {
                isLoading = true;
            }
            if (context.getStores().size() == 0) { // we show this only if its the first loading
                waitDialog = ProgressDialog.show(
                        context,
                        context.getText(R.string.msg_loadingdata),
                        context.getText(R.string.msg_pleasewait),
                        false, false);
                waitDialog.setIcon(R.drawable.ic_launcher);
            }
            contentLoader = new Thread(loadContent);
            contentLoader.start();
        }
    }

    public static StoreSearchRequest getSearchParams() {
        // construct the json request
        StoreSearchRequest request = new StoreSearchRequest();
        for (int i=0; i<services.size(); i++)
            request.getServices().add(services.get(i).getCod());

        // TODO: remove these adds as they are just for test
        //request.getServices().add(new Long(4));
        //request.getServices().add(new Long(5));
        //request.getServices().add(new Long(42));
        //request.getServices().add(new Long(43));
        //request.getServices().add(new Long(44));
        //request.getServices().add(new Long(46));
        //request.getServices().add(new Long(48));
        //request.getServices().add(new Long(52));
        //request.getServices().add(new Long(110));
        // TODO: remove these adds as they are just for test

        request.setListOffSet(context.getStores().size());
        // if LatLon is both 0, then we're looking at user current location
        if (lat == 0 && lon == 0) {
            request.setLatitude(context.getLocation()[0]);
            request.setLongitude(context.getLocation()[1]);
        } else {
            request.setLatitude(lat);
            request.setLongitude(lon);
        }

        if (baseDates.size() == 0) {
            baseDates.add((new SimpleDateFormat("yyyyMMdd")).format(Calendar.getInstance().getTime()));
        }
        request.setBaseDates(baseDates);

        // TODO: remove these adds as they are just for test
        //request.setBaseDates(new ArrayList<String>(){{add("20151029");}});
        // TODO: remove these adds as they are just for test

        request.setHourFrom(hourFrom);
        request.setHourTo(hourTo);
        request.setRange(range);
        return request;
    }

    public void requestFilteredSearch(ArrayList<Service> services, double[] latlon,
                                      int hourFrom, int hourTo, ArrayList<String> baseDates, int rangeIndex) {
        StoreListFragment.services = services;
        if (latlon[0] == -1 && latlon[1] == -1) {
            lat = context.getLocation()[0];
            lon = context.getLocation()[1];
        } else {
            lat = latlon[0];
            lon = latlon[1];
        }
        StoreListFragment.baseDates = baseDates;
        StoreListFragment.hourFrom = String.format("%02d", (hourFrom/3600))+String.format("%02d",(hourFrom%60));
        StoreListFragment.hourTo = String.format("%02d", (hourTo/3600))+String.format("%02d",(hourTo%60));
        StoreListFragment.range = ((rangeIndex == 0 ? 10 : (rangeIndex == 1 ? 25 : 50)))*1000;
        createNewLoader();
    }

    //===================================================================================
    // Inner classes
    //===================================================================================

    private static class StoreListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return context.getStores().size();
        }

        @Override
        public Object getItem(int position) {
            return context.getStores().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (context.getStores().get(position).getName().equals("LOAD_MORE")) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_helperloading, parent, false);
                return convertView;
            }
            // predefines the view as an inflated layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_storeitem, parent, false);

            ((TextView) convertView.findViewById(R.id.row_storeitem_storename)).setText(
                    context.getStores().get(position).getName()
            );
            ((RatingBar) convertView.findViewById(R.id.row_storeitem_storerating)).setRating(
                    context.getStores().get(position).getEvaluation()
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_numemployees)).setText(
                    String.valueOf(context.getStores().get(position).getEmployees().size())
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_numservices)).setText(
                    String.valueOf(context.getStores().get(position).getListServices().size())
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_citystate)).setText(
                    context.getStores().get(position).getAddressInfo().getCity() + ", " +
                            context.getStores().get(position).getAddressInfo().getState()
            );
            ((TextView) convertView.findViewById(R.id.row_storeitem_streetname)).setText(
                    context.getStores().get(position).getAddressInfo().getAddress() + " " +
                            (context.getStores().get(position).getAddressInfo().getComplement().equals("null") ? "" :
                                    ", " + context.getStores().get(position).getAddressInfo().getComplement())
            );
            if (context.getPhotos().get(context.getStores().get(position).getId()) != null) {
                ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageDrawable(context.getPhotos().get(context.getStores().get(position).getId()));
                ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
            } else {
                if (context.getStores().get(position).getUrlPhoto() != null) {
                    if (context.getStores().get(position).getUrlPhoto().length() > 0) {
                        new PhotoLoader(
                                ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)),
                                ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)),
                                context.getStores().get(position).getId(),
                                context.getStores().get(position).getUrlPhoto()).execute();
                    } else {
                        ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageResource(R.drawable.noimage);
                        ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
                        context.setPhoto(context.getStores().get(position).getId(), context.getResources().getDrawable(R.drawable.noimage));
                    }
                } else {
                    ((ImageView) convertView.findViewById(R.id.row_storeitem_photo)).setImageResource(R.drawable.noimage);
                    ((ProgressBar) convertView.findViewById(R.id.row_storeitem_photoholder)).setVisibility(View.GONE);
                    context.setPhoto(context.getStores().get(position).getId(), context.getResources().getDrawable(R.drawable.noimage));
                }
            }
            return convertView;
        }

    }

    private static class PhotoLoader extends AsyncTask<Void, Void, Void> {

        private Drawable drawable;
        private ProgressBar progress;
        private ImageView photo;
        private long id;
        private String url;

        public PhotoLoader(ImageView photo, ProgressBar progress, long id, String url) {
            this.photo = photo;
            this.progress = progress;
            this.id = id;
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                drawable = ImageUtils.loadImageFromWebOperations(url);
                Bitmap bitmap = ImageUtils.getCroppedBitmap(((BitmapDrawable) drawable).getBitmap(), false);
                drawable = new BitmapDrawable(context.getResources(), bitmap);
            } catch (Exception e) {
                drawable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progress.setVisibility(View.GONE);
            if (drawable == null) {
                drawable = context.getResources().getDrawable(R.drawable.noimage);
            }
            photo.setImageDrawable(drawable);
            context.setPhoto(id, drawable);
        }

    }

}
